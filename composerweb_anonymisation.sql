/* ===============================================================
 ComposerWeb Scramble

 Author: Ramon Buckland
 Date: 10 June 2014
 
 Version Date       Author    Comment
 V7      2014-06-07 RamonB    Added the Sys
 V8      2014-11-11 RamonB    Changed to reference the Composer tables on tempdb instead.
                              The tables required to be BCP'd are
					   tempdb..entity
					   tempdb..title
					   tempdb..address
					   tempdb..dealer
					   tempdb..dealer_branch
					   tempdb..adviser_account
					   tempdb..employer
					   
					   Technically, we would ONLY require the changed (scrambled) tables, which would be 
							entity and address
							
						However, to make the script Multi environment, just copy the above and 
						all will work well (else, you would change the tempdb.. of the others to AG_<ENVIRON>_CW_RO_Composer
						for each environment / table.
v9      2014-11-12 GarrettD  modified cw_bulk_data section -truncated additional tables to remove FKs. Corrected minor typo's.
V10     2015-01-20 RamonB    Changed the email address from testaccount@aegon-psl.co.uk to a non-existent email 'scrambled@noemail.nodomain'
                             Aegon does not have a catch all test account at the moment.
V11     2015-02-01 RamonB removed the advsr suffixes							 
V12     2015-03-07 RamonB There appears some null names coming from Composer. 'Protection has been applied' for isnull()
V13     2015-03-09 RamonB Bring back to life the sysadmin accounts
V14     2019-01-21 LyndonG 	Migrated to MS SQL Server - newid changed with no parameter. Delete tables not truncate.
							Changed cw_address - Composer linked procressing. Now uses a temporary table for the update as previous process was taking a long time.
                          

=============================================================== */


print '::: cw_investor - Composer linked'
go

-- 1. First - collect all the scrambled data from Composer
-- all cw_investor entries total = 161,931
-- this updates 93484
create table #upd_control (
  control_id numeric(15,0) not null,
  complete int default 0 not null
)
insert into #upd_control (control_id)
select cw_investor_id
from cw_investor i
   join PRD_STGComposer..entity e on e.entity_id = i.composer_investor_id
   
create clustered index idx_1 on #upd_control(control_id)

while exists (select 1 from #upd_control where complete <> 2)
begin 
    begin transaction 	
	
    set rowcount 10000
    update #upd_control
	   set complete = 1
	 where complete = 0
    set rowcount 0	   
    
	update cw_investor
	set title = t.name
	   ,first_name = e.given_names
	   ,surname = e.name
	   ,mobile_phone = e.mobile_number
	   ,fax = e.fax_number
	   ,email = e.email_address
	   ,birth_date = e.birth_date
	   ,home_phone = e.home_phone_number
	   ,work_phone = e.work_phone_number
	   ,national_insurance_no = e.tax_file_number
	from cw_investor i
	   join PRD_STGComposer..entity e on e.entity_id = i.composer_investor_id
	   join #upd_control updc on updc.control_id = i.cw_investor_id and updc.complete = 1
	   left join PRD_STGComposer..title t on e.title_id = t.title_id

    update #upd_control
	   set complete = 2
	 where complete = 1
	   
	commit transaction
	   
end	   

drop table #upd_control
go

-- 2. Second - grab the first "address" from the address table that is scrambled

print '::: cw_address - Composer linked'
go

create table #upd_control (
  control_id numeric(15,0) not null,
  complete int default 0 not null
)

insert into #upd_control (control_id)
select a.cw_address_id
from cw_address a
   join cw_investor_address ia on ia.cw_address_id = a.cw_address_id
   join cw_investor i on i.cw_investor_id = ia.cw_investor_id
   join PRD_STGComposer..entity e on e.entity_id = i.composer_investor_id
   left join PRD_STGComposer..title t on e.title_id = t.title_id
   join PRD_STGComposer..address addr on addr.entity_id = e.entity_id 
   where addr.address_id = ( select max(address_id) from PRD_STGComposer..address ab where ab.entity_id = e.entity_id )
   
create clustered index idx_1 on #upd_control(control_id)
      
    select  cw_address_id = a.cw_address_id
	        ,street_number = addr.street_number
		    ,street =  addr.street
		    ,address1 = null
		    ,address2 = null
		    ,address3 = addr.suburb
		    ,county = addr.district
		    ,city = addr.suburb
		    ,postcode = addr.postcode
		    ,property_name = addr.property_name
		    ,country = addr.countrycode
		    ,care_of_name = addr.care_of_name
    into #cw_addrress_staging
	from cw_address a
	   join cw_investor_address ia on ia.cw_address_id = a.cw_address_id
	   join #upd_control updc on updc.control_id = a.cw_address_id
	   join cw_investor i on i.cw_investor_id = ia.cw_investor_id
	   join PRD_STGComposer..entity e on e.entity_id = i.composer_investor_id
	   left join PRD_STGComposer..title t on e.title_id = t.title_id
	   join PRD_STGComposer..address addr on addr.entity_id = e.entity_id 
	   where addr.address_id = ( select max(address_id) from PRD_STGComposer..address ab where ab.entity_id = e.entity_id )
     
    begin tran
      
    update cw_address
    set street_number = addr.street_number
		,street =  addr.street
		,address1 = null
		,address2 = null
		,address3 = addr.address3
		,county = addr.county
		,city = addr.city
		,postcode = addr.postcode
		,property_name = addr.property_name
		,country = addr.country
		,care_of_name = addr.care_of_name
	from cw_address a
	join #cw_addrress_staging addr on addr.cw_address_id = a.cw_address_id
	   
	commit transaction	

drop table #upd_control
drop table #cw_addrress_staging
go   

-- 3. third change all the prospects that did not make the cut above (inverse of the join) from 1.
-- this updates 68448   
print '::: cw_investor - Not Composer linked'
go

create table #upd_control (
  control_id numeric(15,0) not null,
  complete int default 0 not null
)
insert into #upd_control (control_id)
select i.cw_investor_id
from cw_investor i
where not exists (select 1 from PRD_STGComposer..entity e where e.entity_id = i.composer_investor_id)

create clustered index idx_1 on #upd_control(control_id)

while exists (select 1 from #upd_control where complete <> 2)
begin 
    begin transaction 	
	
    set rowcount 10000
    update #upd_control
	   set complete = 1
	 where complete = 0
    set rowcount 0	   
    
	update cw_investor
	set title =  null
	   ,first_name = case i.cw_investor_id % 10 when	0	then 	'Chris' when	1	then 	'Pete' when	2	then 	'Bo' when	3	then 	'Terry'
					when	4	then 	'Lesley' when	5	then 	'Jess' when	6	then 	'Hillary' when	7	then 	'Alex' when	8	then 	'Petra' when	9	then 	'Bri' end
	   ,surname = case i.cw_investor_id % 13  when	0	then 	'Agate' when	1	then 	'Alexandrite' when	2	then 	'Andalusite' when	3	then 	'Axinite' when	4	then 	'Benitoite'
					when	5	then 	'Bixbite' when	6	then 	'Cassiterite' when	7	then 	'Chrysocolla' when	8	then 	'Chrysoprase' when	9	then 	'Clinohumite' when	10	then 	'Cordierite'
					when	11	then 	'Danburite' when	12	then 	'Diamond' when	13	then 	'Diopside'  end
	   ,mobile_phone = '07000000000'
	   ,fax = null
	   ,email = 'scrambled@noemail.nodomain'
	   ,birth_date = '1960/01/01'
	   ,home_phone = '01000 000 000'
	   ,work_phone = '01000 111 111'
	   ,national_insurance_no = (case i.cw_investor_id % 5 when 0 then 'HM' when 1 then 'CS' when 2 then 'TZ' when 3 then 'PH' when 4 then 'NA' else 'LR' end) + right('00000' + convert(varchar,(i.cw_investor_id*11) % 999999),6) + (case when i.cw_investor_id % 2 = 1 then 'A' else 'B' end)
	from cw_investor i
	   join #upd_control updc on updc.control_id = i.cw_investor_id and updc.complete = 1
	 where not exists (select 1 from PRD_STGComposer..entity e where e.entity_id = i.composer_investor_id)
	 
    update #upd_control
	   set complete = 2
	 where complete = 1
	   
	commit transaction	
end	
drop table #upd_control
go
   
-- // Link tables .. Ignoring
-- cw_investor_address	163,317
-- cw_investor_bankaccount	6,002


/* ************* cw_address ********************** */
-- cw_address	180,782
print '::: cw_address - Not Composer linked'
go

create table #upd_control (
  control_id numeric(15,0) not null,
  complete int default 0 not null
)

insert into #upd_control (control_id)
select a.cw_address_id
from cw_address a
    join cw_investor_address ia on ia.cw_address_id = a.cw_address_id
	join cw_investor i on i.cw_investor_id = ia.cw_investor_id
where i.composer_investor_id is null	

create clustered index idx_1 on #upd_control(control_id)

while exists (select 1 from #upd_control where complete <> 2)
begin 
    begin transaction 	
	
    set rowcount 10000
    update #upd_control
	   set complete = 1
	 where complete = 0
    set rowcount 0	   
	   
	update cw_address
	set street_number = convert(varchar,(a.cw_address_id % 99) + 1)
	   ,street =  case a.cw_address_id % 7 when 0 then 'Little ' when 1 then 'Big ' when 2 then 'Small ' when 3 then 'Round ' else 'Tiny ' end
				+ case a.cw_address_id % 11 when 0 then 'Brick' when 1 then 'Wood' when 2 then 'Stick' when 3 then 'Plastic' when 4 then 'Glass' 
										  when 5 then 'Air' when 6 then 'Fire' when 7 then 'Water' when 8 then 'Grass' when 9 then 'Earth' when 10 then 'Stone' end
				+ ' '
				+ case a.cw_address_id % 13 when 0 then 'Road' when 1 then 'Drive' when 2 then 'Avenue' when 3 then 'Street' when 4 then 'Close' 
										  when 5 then 'End' when 6 then 'Side' when 7 then 'Lane' when 8 then 'Laneway' when 9 then 'Hill' when 10 then 'Way'
										  when 9 then 'Course' when 10 then 'Path' end
		--, address1 and address2 are null always
		,address1 = null
		,address2 = null
		,address3 = case a.cw_address_id % 11 when 0 then 'Hammer' when 1 then 'Shovel' when 2 then 'Nail' when 3 then 'Wrench' when 4 then 'Saw' 
										  when 5 then 'Socket' when 6 then 'Pencil' when 7 then 'Ruler' when 8 then 'Level' when 9 then 'Awl' when 10 then 'Adze' end
				+ ' '
				+ case a.cw_address_id % 5 when 0 then 'Down' when 1 then 'Side' when 2 then 'North' when 3 then 'Town' when 4 then 'Hamlet' end
		,county = case a.cw_address_id % 7 when 0 then 'Countyville' when 1 then 'Antsville' when 2 then 'Squirrelville' when 3 then 'Countyside' else null end
		,country = 'GBR'
		,city = case a.cw_address_id % 66
		when	0	then 	'Aberdeen' when	1	then 	'Armagh' when	2	then 	'Bangor' when	3	then 	'Bath' when	4	then 	'Belfast' when	5	then 	'Birmingham' when	6	then 	'Bradford'
		when	7	then 	'Brighton and Hove' when	8	then 	'Bristol' when	9	then 	'Cambridge' when	10	then 	'Canterbury' when	11	then 	'Cardiff' when	12	then 	'Carlisle' when	13	then 	'Chester'
		when	14	then 	'Chichester' when	15	then 	'City of London' when	16	then 	'Coventry' when	17	then 	'Derby' when	18	then 	'Dundee' when	19	then 	'Durham' when	20	then 	'Edinburgh'
		when	21	then 	'Ely' when	22	then 	'Exeter' when	23	then 	'Glasgow' when	24	then 	'Gloucester' when	25	then 	'Hereford' when	26	then 	'Inverness'
		when	27	then 	'Kingston upon Hull' when	28	then 	'Lancaster' when	29	then 	'Leeds' when	30	then 	'Leicester' when	31	then 	'Lichfield'
		when	32	then 	'Lincoln' when	33	then 	'Lisburn' when	34	then 	'Liverpool' when	35	then 	'Londonderry' when	36	then 	'Manchester' when	37	then 	'Newcastle upon Tyne'
		when	38	then 	'Newport' when	39	then 	'Newry' when	40	then 	'Norwich' when	41	then 	'Nottingham' when	42	then 	'Oxford' when	43	then 	'Peterborough' 
		when	44	then 	'Plymouth' when	45	then 	'Portsmouth' when	46	then 	'Preston' when	47	then 	'Ripon' when	48	then 	'Salford' when	49	then 	'Salisbury' when	50	then 	'Sheffield'
		when	51	then 	'Southampton' when	52	then 	'St Albans' when	53	then 	'St Davids' when	54	then 	'Stirling' when	55	then 	'Stoke-on-Trent' when	56	then 	'Sunderland'
		when	57	then 	'Swansea' when	58	then 	'Truro' when	59	then 	'Wakefield' when	60	then 	'Wells' when	61	then 	'Westminster' when	62	then 	'Winchester'
		when	63	then 	'Wolverhampton' when	64	then 	'Worcester' when	65	then 	'York' end	
		,postcode = case a.cw_address_id % 5 when 0 then 'NE' when 1 then 'GW' when 2 then 'ST' when 3 then 'BS' when 4 then 'AB' end + convert(varchar,(a.cw_address_id % 40) + 1) + 
			   ' ' + convert(varchar,(a.cw_address_id % 8)+1) + case a.cw_address_id % 6 when 0 then 'AX' when 1 then 'TG' when 2 then 'ER' when 3 then 'NJ' when 4 then 'WD' when 5 then 'GA' end
		,property_name = null
		,care_of_name = null
	from cw_address a
	   join #upd_control updc on updc.control_id = a.cw_address_id and updc.complete = 1
		join cw_investor_address ia on ia.cw_address_id = a.cw_address_id
		join cw_investor i on i.cw_investor_id = ia.cw_investor_id
	where i.composer_investor_id is null
	

    update #upd_control
	   set complete = 2
	 where complete = 1
	   
	commit transaction	

end	
drop table #upd_control
go  
	

-- cw_asset_market_identifier	124,732 :: Leaving - Non identifying data
-- cw_asset_unit_price	29,361,917 :: Leaving - Non identifying data

/* ************* cw_audit_entry ********************** */
-- cw_audit_entry	1,866,753

print '::: delete table cw_audit_entry'
go
	
delete cw_audit_entry
go

-- cw_audit_type	3 :: Leaving

/* ************* cw_bank_account ********************** */
-- cw_bank_account	16,730

print '::: cw_bank_account - for Investor'
go


update cw_bank_account
set account_holder = i.first_name + ' ' + i.surname
   ,sort_code       =  '601234'
   ,account_number = right('9999999' + convert(varchar,(cba.cw_bank_account_id * 5) % 99999999),8)
from cw_bank_account cba
   join cw_investor_bankaccount iba on iba.cw_bank_account_id = cba.cw_bank_account_id
   join cw_investor i on i.cw_investor_id = iba.cw_investor_id   
go

print '::: cw_bank_account - unknowns'
go


update cw_bank_account
set     account_holder  = 'Unknown',
        sort_code       =  '601234',
        account_number  = '99999999'
from cw_bank_account cba
where not exists (select 1 from cw_investor_bankaccount iba where iba.cw_bank_account_id = cba.cw_bank_account_id)

go

/* ******************************* Bulk Data tables **************************************** */
/*
cw_bulk_data	1,273
cw_bulk_data_argument	1,241
cw_bulk_data_item	877,257
cw_bulk_data_item_format	28,877
cw_bulk_data_row	37,340
cw_bulk_process	0
cw_bulk_process_param	0
*/

/*
print '::: delete tables'
go

delete cw_bulk_data
go

delete cw_bulk_data_argument
go

delete cw_bulk_data_item
go

delete cw_bulk_data_item_format
go

delete cw_bulk_data_row
go

delete cw_bulk_data_report
go

delete cw_bulk_process 
go

delete cw_bulk_process_param 
go

delete cw_multifile_process
go

*/

UPDATE cw_bulk_data_item set data = NULL

-- EMPTY cw_bulk_process	0
-- EMPTY cw_bulk_process_param	0
print '::: cw_payload'
go


update cw_payload
set description = null
   ,case_reference = null
   
go

-- go



	
-- cw_commission	124,602 :: leave (just figures)
-- cw_crystallisation_details	10,722 :: leave (just figures)

/* ******************************* cw_dealer **************************************** */
-- cw_dealer	1,957

print '::: cw_dealer'
go

update cw_dealer 
set dealer_name = de.name
from cw_dealer cd
  join PRD_STGComposer..dealer d on d.dealer_id = cd.composer_dealer_id 
  join PRD_STGComposer..entity de on de.entity_id = d.entity_id 
go  

/* ******************************* cw_dealer **************************************** */
print '::: cw_dealer_branch'
go

update cw_dealer_branch 
set dealer_branch_name = dbe.name
from cw_dealer_branch cdb
  join PRD_STGComposer..dealer_branch db on db.dealer_branch_id = cdb.composer_dealer_branch_id 
  join PRD_STGComposer..entity dbe on dbe.entity_id = db.entity_id    
go  


-- leaving:: cw_declaration_question	0
-- leaving:: cw_deposit	64,659
-- leaving:: cw_dms	121,162
-- leaving:: cw_draw_down	0
-- leaving::  nothing identifiable -- cw_employee_details	163
-- leaving:: cw_external_payload	0
-- leaving::  nothing identifiable --cw_inv_model_port_split	862,151

-- leaving::  nothing identifiable  cw_investment_model	188,868

/* ******************************* cw_isa_transfer_in **************************************** */
-- cw_isa_transfer_in	7,532
print '::: cw_isa_transfer_in'
go
update cw_isa_transfer_in
   set 
  --  provider_address =    :: leave it
  --,plan_manager_name =   :: leave it
    provider_phone = '0800 000 0000'
   ,provider_reference = substring(convert(varchar(40), newid()),1,5)
go   
      
/* ******************************* cw_memo_asset **************************************** */
print '::: cw_memo_asset'
go
-- cw_memo_asset	7,459
update cw_memo_asset
  set 
      -- we leave the asset name if it's a market linked asset .. 
	  -- but otherwise strip it (as there are pension references in the name etc)
     asset_name = case a.market_id when null then 
	                  case when a.number_of_units is null then 
	                       'My External Asset ' + substring(convert(varchar(40), newid()),1,5)
					  else 
					       'Share Scheme no. ' + substring(convert(varchar(40), newid()),1,5)
				      end
				  else
				      a.asset_name
				  end
	,external_reference = null 
from cw_memo_asset a
go

 

-- leaving::  nothing identifiable cw_memo_asset_tax_type	2
-- leaving::  nothing identifiable cw_memo_asset_type	33
-- leaving::  nothing identifiable cw_multifile_process	0
-- leaving::  nothing identifiable cw_offplatform_bce	1,701
-- :: purcged (above) cw_payload	156,659

/* ******************************* cw_reregistration **************************************** */
print '::: cw_pension_income'
go
update cw_pension_income -- just obliterate amounts for safety
set total_gross_income = 1234.00 - (a.cw_pension_income_id % 118) + (a.total_gross_income - 56)
from cw_pension_income a
go

-- leaving::  nothing identifiable  cw_pension_income	616
-- leaving:: cw_portfolio_split	1,049,022

/* ******************************* cw_reregistration **************************************** */
-- cw_reregistration	1,954
print '::: cw_reregistration'
go
update cw_reregistration
set provider_phone = '0800 000 0000'
   ,provider_reference =  substring(convert(varchar(40), newid()),1,6)
go


-- leaving::  nothing identifiable  cw_retirement_details	119,184

/* ******************************* cw_sipp_transfer_in **************************************** */
-- cw_sipp_transfer_in	141,211
print '::: cw_sipp_transfer_in'
go
create table #upd_control (
  control_id numeric(15,0) not null,
  complete int default 0 not null
)

create clustered index idx_1 on #upd_control(control_id)

insert into #upd_control (control_id)
select cw_transaction_id
from cw_sipp_transfer_in 
   
while exists (select 1 from #upd_control where complete <> 2)
begin 
    begin transaction 	
	
    set rowcount 10000
    update #upd_control
	   set complete = 1
	 where complete = 0
    set rowcount 0	
	
	update cw_sipp_transfer_in -- just obliterate amounts for safety
	set non_crystalised_value = 1234.00 - (a.cw_transaction_id % 118) + (a.non_crystalised_value - 117)
		,provider_ref =  substring(convert(varchar(40), newid()),1,6)
	from cw_sipp_transfer_in a
	   join #upd_control updc on updc.control_id = a.cw_transaction_id and updc.complete = 1

    update #upd_control
	   set complete = 2
	 where complete = 1
	   
	commit transaction	

end	
drop table #upd_control
go  	



-- leaving::  nothing identifiable cw_source_of_wealth	1,715
-- leaving::  nothing identifiable cw_source_of_wealth_type	15
-- leaving::  nothing identifiable cw_switch	10,125
-- leaving::  nothing identifiable cw_tran_in_port_split	22,654
-- leaving::  nothing identifiable cw_tran_out_port_split	88,893
-- leaving::  nothing identifiable cw_transaction	226,209
-- leaving::  nothing identifiable cw_transaction_sow	1,715
-- leaving::  nothing identifiable cw_unique_key	1
-- leaving::  nothing identifiable cw_withdrawal	727
-- leaving::  nothing identifiable dealer	1
-- leaving::  nothing identifiable dealer_branch	1
-- leaving::  nothing identifiable entity	914
-- leaving::  nothing identifiable trig_cw_memo_asset	0
-- leaving::  usec_function	36
-- leaving::  usec_function_attribute	0
-- leaving::  usec_function_group	34
-- leaving::  usec_function_type	1

/* ******************************* SSO Tables **************************************** */
print '::: delete table usec_idp_role'
go

delete usec_idp_role
go

print '::: delete table usec_idp_user'
go

delete usec_idp_user
go

print '::: delete table usec_idp'
go

delete usec_idp
go

-- leaving:: usec_role_definition	11
-- leaving:: usec_role_defn_attribute	50
-- leaving:: usec_role_defn_func_grp_xref	81
-- leaving:: usec_role_defn_function_xref	0
-- leaving:: usec_role_defn_imp_fgrp_ex_xref	0
-- leaving:: usec_role_defn_imp_xref	10
-- leaving:: usec_user_role	49,297
-- leaving:: usec_user_role_addl_func_grp	442
-- leaving:: usec_user_role_addl_function	0
-- leaving:: usec_user_role_attribute	305,015



/*


  Coming to the last ones.. yay! 
  Coming to the last ones.. yay! 


 */
/* ******************************* cw_user_account **************************************** */
/* * change ALL  to Password2 * */
print '::: cw_user_account - passwords'
go
create table #upd_control (
  control_id numeric(15,0) not null,
  complete int default 0 not null
)

create clustered index idx_1 on #upd_control(control_id)

insert into #upd_control (control_id)
select usec_user_account_id
from usec_user_account 
   
while exists (select 1 from #upd_control where complete <> 2)
begin 
    begin transaction 	
	
    set rowcount 10000
    update #upd_control
	   set complete = 1
	 where complete = 0
    set rowcount 0	
	

		update usec_user_account
		set      password = '7063cd175219bf2e4fb2a904a5137856d695aeae6799d237ce8cffb3cc31596d'
				,password_salt = 'iLC6tvCB'
		from usec_user_account a
	   join #upd_control updc on updc.control_id = a.usec_user_account_id and updc.complete = 1

    update #upd_control
	   set complete = 2
	 where complete = 1
	   
	commit transaction	

end	
drop table #upd_control
go 


print '::: cw_user_account - sysadmin accounts make status ACTIVE'  
GO

update usec_user_account
   set      status = 'A'
where username like 'sysadmin%'
GO
   
		
-- change all the 'number' user names 
-- this works .. select count(distinct convert(varchar,(convert(numeric,username) * 2 ) % 99999999))
-- this also works select count(distinct right('55555555' + convert(varchar,usec_user_account_id),8))

/*

update usec_user_account
set     username = right('55555555' + convert(varchar,usec_user_account_id),8) 
where username NOT LIKE '%[^0-9]%'

*/
print '::: cw_user_account - username'  
go

  -- char 9 length starting with a '5'
  update usec_user_account
   set     username = '5' + right('0000000' + convert(varchar,usec_user_account_id),8)
  where username NOT LIKE '%[^0-9]%'
  
go


/*
select username, right('55555555' + convert(varchar,row_id),8) as new_username
from #newusername
*/

-- : some other forms that work would be to setup a temp table and repeatedly generate a newid() and strip out the numbers 
-- and keep going  whilst you don't have a clash .. 
-- using a sequential number is easiest I think .. 

/* ******************************* usec_user_account_attribute **************************************** */

/*
  select distinct attribute_name
  from usec_user_account_attribute
  
  == * means we will change it
  
		*	forename
		*	surname
			current_login
			last_login
			logon_attempts
		*	email
		*	security_answer
			date_of_birth
			gender
		*	security_question
			login_attempts
		*	contact_number
		*	mobile_number
*/			
print '::: usec_user_account_attribute - security_question security_answer email contact_number mobile_number'
go

create table #upd_control (
  control_id numeric(15,0) not null,
  complete int default 0 not null
)

create clustered index idx_1 on #upd_control(control_id)

insert into #upd_control (control_id)
select a.usec_user_account_attribute_id
from usec_user_account_attribute a
where a.attribute_name in ('security_question','security_answer','email','contact_number','mobile_number' ) 
   
while exists (select 1 from #upd_control where complete <> 2)
begin 
    begin transaction 	
	
    set rowcount 10000
    update #upd_control
	   set complete = 1
	 where complete = 0
    set rowcount 0	
			

		update usec_user_account_attribute
		  set attribute_value = case a.attribute_name
		  when 'security_question' then 'What is your favourite colour?'
		  when 'security_answer'   then 'blue'
		  when 'email'             then 'scrambled@noemail.nodomain'
		  when 'contact_number'    then '07000000000'
		  when 'mobile_number'    then '07000000000'  
		  end
		from usec_user_account_attribute a
		join #upd_control updc on updc.control_id = a.usec_user_account_attribute_id and updc.complete = 1
		where a.attribute_name in ('security_question','security_answer','email','contact_number','mobile_number' ) 

    update #upd_control
	   set complete = 2
	 where complete = 1
	   
	commit transaction	

end	
drop table #upd_control
go 


-- usec_user_account_attribute	345,068


/*
 now .. let's get their real 'scrambled' name from Composer
 But we need to do each "type" separately
  162	Adviser   <-- adviser
  163	Firm Administrator  <-- dealer
  164	Firm Owner  <-- dealer
  165	Investor <-- entity
  166	Paraplanner <-- dealer paraplanner
167	System Administrator <-- ** leave **
  168	Employer Administrator <-- employer 
  169	Employer <-- employer
  170	Scheme Adviser <-- employer
???   171	Employee <-- investor
  172	Onboarder <-- *random*

*/
print '::: usec_user_account_attribute - Names for Adviser'
go

update usec_user_account_attribute
  set attribute_value = case a.attribute_name
  when 'forename' then isnull(e.given_names,'Non')
  when 'surname'  then e.name
  end
from usec_user_account_attribute a  
     join usec_user_account uacc on uacc.usec_user_account_id = a.usec_user_account_id
     join usec_user_role urole on urole.usec_user_account_id = uacc.usec_user_account_id
     join usec_role_definition role_def on role_def.usec_role_definition_id = urole.usec_role_definition_id
	 join usec_user_role_attribute uura on uura.usec_user_role_id = urole.usec_user_role_id and uura.attribute_name = 'party_id'
	 join PRD_STGComposer..adviser_account aa on aa.adviser_account_id = convert(numeric,uura.attribute_value)
     join PRD_STGComposer..entity e on e.entity_id = aa.entity_id
where a.attribute_name in ('forename','surname') 
  and role_def.role_definition_name = 'Adviser'
go  
  
print '::: usec_user_account_attribute - Names for Firm Administrator'
go
  
  -- 'Firm Administrator'   --> dealer
update usec_user_account_attribute
  set attribute_value = case a.attribute_name
  when 'forename' then isnull(e.given_names,'')
  when 'surname'  then e.name
  end
from usec_user_account_attribute a  
     join usec_user_account uacc on uacc.usec_user_account_id = a.usec_user_account_id
     join usec_user_role urole on urole.usec_user_account_id = uacc.usec_user_account_id
     join usec_role_definition role_def on role_def.usec_role_definition_id = urole.usec_role_definition_id
	 join usec_user_role_attribute uura on uura.usec_user_role_id = urole.usec_user_role_id and uura.attribute_name = 'party_id'
	 join PRD_STGComposer..dealer d on d.dealer_id = convert(numeric,uura.attribute_value)
     join PRD_STGComposer..entity e on e.entity_id = d.entity_id
where a.attribute_name in ('forename','surname') 
  and role_def.role_definition_name = 'Firm Administrator'  
go  
  
print '::: usec_user_account_attribute - Names for Firm Owner'
go
  
  -- 'Firm Owner'   --> dealer
update usec_user_account_attribute
  set attribute_value = case a.attribute_name
  when 'forename' then isnull(e.given_names,'')
  when 'surname'  then e.name + ' FO'
  end
from usec_user_account_attribute a  
     join usec_user_account uacc on uacc.usec_user_account_id = a.usec_user_account_id
     join usec_user_role urole on urole.usec_user_account_id = uacc.usec_user_account_id
     join usec_role_definition role_def on role_def.usec_role_definition_id = urole.usec_role_definition_id
	 join usec_user_role_attribute uura on uura.usec_user_role_id = urole.usec_user_role_id and uura.attribute_name = 'party_id'
	 join PRD_STGComposer..dealer d on d.dealer_id = convert(numeric,uura.attribute_value)
     join PRD_STGComposer..entity e on e.entity_id = d.entity_id
where a.attribute_name in ('forename','surname') 
  and role_def.role_definition_name = 'Firm Owner'  
go 
    
print '::: usec_user_account_attribute - Names for Investor'
go
    
  -- 'Investor'   --> Investor
update usec_user_account_attribute
  set attribute_value = case a.attribute_name
  when 'forename' then isnull(e.given_names,'Firstname')
  when 'surname'  then isnull(e.name,'Surname')
  end
from usec_user_account_attribute a  
     join usec_user_account uacc on uacc.usec_user_account_id = a.usec_user_account_id
     join usec_user_role urole on urole.usec_user_account_id = uacc.usec_user_account_id
     join usec_role_definition role_def on role_def.usec_role_definition_id = urole.usec_role_definition_id
	 join usec_user_role_attribute uura on uura.usec_user_role_id = urole.usec_user_role_id and uura.attribute_name = 'party_id'
	 join PRD_STGComposer..entity e on e.entity_id = convert(numeric,uura.attribute_value)
where a.attribute_name in ('forename','surname') 
  and role_def.role_definition_name = 'Investor' 
go  
  
print '::: usec_user_account_attribute - Names for Employer'
go
      
  -- 'Employer'   --> Employer
update usec_user_account_attribute
  set attribute_value = case a.attribute_name
  when 'forename' then isnull(e.given_names,'')
  when 'surname'  then e.name 
  end
from usec_user_account_attribute a  
     join usec_user_account uacc on uacc.usec_user_account_id = a.usec_user_account_id
     join usec_user_role urole on urole.usec_user_account_id = uacc.usec_user_account_id
     join usec_role_definition role_def on role_def.usec_role_definition_id = urole.usec_role_definition_id
	 join usec_user_role_attribute uura on uura.usec_user_role_id = urole.usec_user_role_id and uura.attribute_name = 'party_id'
     join PRD_STGComposer..employer emp on emp.employer_id = convert(numeric,uura.attribute_value)
     join PRD_STGComposer..entity e on e.entity_id = emp.entity_id
where a.attribute_name in ('forename','surname') 
  and role_def.role_definition_name = 'Employer' 

go  
  
print '::: usec_user_account_attribute - Names for Employer Administrator'
go  
  -- 'Employer Administrator'   --> Employer
update usec_user_account_attribute
  set attribute_value = case a.attribute_name
  when 'forename' then isnull(e.given_names,'')
  when 'surname'  then e.name + ' Emp-Admin'
  end
from usec_user_account_attribute a  
     join usec_user_account uacc on uacc.usec_user_account_id = a.usec_user_account_id
     join usec_user_role urole on urole.usec_user_account_id = uacc.usec_user_account_id
     join usec_role_definition role_def on role_def.usec_role_definition_id = urole.usec_role_definition_id
	 join usec_user_role_attribute uura on uura.usec_user_role_id = urole.usec_user_role_id and uura.attribute_name = 'party_id'
     join PRD_STGComposer..employer emp on emp.employer_id = convert(numeric,uura.attribute_value)
     join PRD_STGComposer..entity e on e.entity_id = emp.entity_id
where a.attribute_name in ('forename','surname') 
  and role_def.role_definition_name = 'Employer Administrator'
go  
  
print '::: usec_user_account_attribute - Names for Scheme Adviser'
go    
  -- 'Scheme Adviser'   --> adviser_account
update usec_user_account_attribute
  set attribute_value = case a.attribute_name
  when 'forename' then isnull(e.given_names,'')
  when 'surname'  then e.name
  end
from usec_user_account_attribute a  
     join usec_user_account uacc on uacc.usec_user_account_id = a.usec_user_account_id
     join usec_user_role urole on urole.usec_user_account_id = uacc.usec_user_account_id
     join usec_role_definition role_def on role_def.usec_role_definition_id = urole.usec_role_definition_id
	 join usec_user_role_attribute uura on uura.usec_user_role_id = urole.usec_user_role_id and uura.attribute_name = 'party_id'
	 join PRD_STGComposer..adviser_account aa on aa.adviser_account_id = convert(numeric,uura.attribute_value)
     join PRD_STGComposer..entity e on e.entity_id = aa.entity_id
where a.attribute_name in ('forename','surname') 
  and role_def.role_definition_name = 'Scheme Adviser'
go  
  
print '::: usec_user_account_attribute - Names for Paraplanner,Onboarder,Employee'
go      
  -- and the rest that are non Sys Admin ... these don't have entities in Composer.
update usec_user_account_attribute
  set attribute_value = case a.attribute_name
  when 'forename' then case a.usec_user_account_id % 10 when	0	then 	'Chris' when	1	then 	'Pete' when	2	then 	'Bo' when	3	then 	'Terry'
				when	4	then 	'Lesley' when	5	then 	'Jess' when	6	then 	'Hillary' when	7	then 	'Alex' when	8	then 	'Petra' when	9	then 	'Bri' end
  when 'surname'  then role_def.role_definition_name + '-' + convert(varchar,uacc.usec_user_account_id)
  end
from usec_user_account_attribute a  
     join usec_user_account uacc on uacc.usec_user_account_id = a.usec_user_account_id
     join usec_user_role urole on urole.usec_user_account_id = uacc.usec_user_account_id
     join usec_role_definition role_def on role_def.usec_role_definition_id = urole.usec_role_definition_id
where a.attribute_name in ('forename','surname') 
  and role_def.role_definition_name in ('Paraplanner','Onboarder','Employee')
  
go

print '::: usec_user_account_attribute - Names for Sys Admins'
go
-- last set is all the 'numbered' username Sys Admins

update usec_user_account_attribute
  set attribute_value = case a.attribute_name
  when 'forename' then case a.usec_user_account_id % 10 when	0	then 	'Chris' when	1	then 	'Pete' when	2	then 	'Bo' when	3	then 	'Terry'
				when	4	then 	'Lesley' when	5	then 	'Jess' when	6	then 	'Hillary' when	7	then 	'Alex' when	8	then 	'Petra' when	9	then 	'Bri' end
  when 'surname'  then role_def.role_definition_name + '-' + convert(varchar,uacc.usec_user_account_id)
  end
from usec_user_account uacc 
     join usec_user_account_attribute a   on uacc.usec_user_account_id = a.usec_user_account_id and a.attribute_name = 'forename'
     join usec_user_role urole on urole.usec_user_account_id = uacc.usec_user_account_id
     join usec_role_definition role_def on role_def.usec_role_definition_id = urole.usec_role_definition_id
where role_def.role_definition_name = 'System Administrator'
  and a.attribute_name in ('forename','surname') 
  and uacc.username NOT LIKE '%[^0-9]%'
go


print '::: usec_user_account_attribute - Washup for non standard (this gives 0 updated)'
go

update usec_user_account_attribute
  set attribute_value = case a.attribute_name
  when 'forename' then case a.usec_user_account_id % 10 when	0	then 	'Chris' when	1	then 	'Pete' when	2	then 	'Bo' when	3	then 	'Terry'
				when	4	then 	'Lesley' when	5	then 	'Jess' when	6	then 	'Hillary' when	7	then 	'Alex' when	8	then 	'Petra' when	9	then 	'Bri' end
  when 'surname'  then role_def.role_definition_name + '-' + convert(varchar,uacc.usec_user_account_id)
  end
from usec_user_account uacc 
     join usec_user_account_attribute a   on uacc.usec_user_account_id = a.usec_user_account_id and a.attribute_name = 'forename'
     join usec_user_role urole on urole.usec_user_account_id = uacc.usec_user_account_id
     join usec_role_definition role_def on role_def.usec_role_definition_id = urole.usec_role_definition_id
where role_def.role_definition_name in ( 'Adviser', 'Scheme Adviser' , 'Firm Administrator' , 'Investor' , 'Firm Owner', 'Employer', 'Employer Administrator',  'Scheme Adviser'  )
  and not exists (select 1 from usec_user_role_attribute uura where uura.usec_user_role_id = urole.usec_user_role_id and uura.attribute_name = 'party_id')
  and a.attribute_name in ('forename','surname') 
  and uacc.username NOT LIKE '%[^0-9]%'
go
