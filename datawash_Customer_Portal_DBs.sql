/* 
    (c) Aegon 2018
    
	Full version of test Data wash script for Users across 
	the Portal Databases:
		PortalSec
		CustPortal
		AdvPortal (prospects)
	
	The script is run as a single update across databases to ensure
	consistancy in the data and within a transaction to ensure ACID properties
	are maintained.

	Details of the wash rules are in "ARC2 Datawash Rules.xlsx"

	Note that the environment variable should be set for the required target 
	test environment as this is used to prefix emails.
	
*/
SET NOCOUNT ON;
SET XACT_ABORT ON;
SET IMPLICIT_TRANSACTIONS OFF;
SET DATEFORMAT YMD;
GO


BEGIN TRY

    /* variables and constants */
    DECLARE
		@EnvironmentPrefix VARCHAR(10) = 'AR6'
      , @StepMessage VARCHAR(300)
      , @Rowcount INT
      , @DatawashUser VARCHAR(30) = 'DATAWASH_' + CONVERT(VARCHAR(10), GETDATE(), 112)
      , @SQL NVARCHAR(MAX) = ''
      , @DebugFlag BIT = 1 /* if data before vs after is required =1, else set to 0 to suppress */
    ;

    DECLARE @TableUpdates TABLE
        (
            TableName NVARCHAR(200)
        );
    INSERT INTO
        @TableUpdates
        (
            TableName
        )
    VALUES
-- SQL Prompt formatting off
        ('ToWash_PRA_PortalSec.dbo.USERS')
      , ('ToWash_PRA_CustPortal.dbo.EMAIL_MESSAGE_AUDIT')
      , ('ToWash_PRA_CustPortal.dbo.ONE_TIME_ACCESS_AUDIT')
      , ('ToWash_PRA_CustPortal.dbo.RESET_PASSWORD_REQUEST')
      , ('ToWash_PRA_CustPortal.dbo.SESSION_DATA')
	  , ('ToWash_PRA_AdvPortal.dbo.Prospect');
-- SQL Prompt formatting on



    SET @StepMessage = CONVERT(VARCHAR(23), GETDATE(), 121) + ': Starting Datawash';
    PRINT @StepMessage;

    IF @DebugFlag = 1
        BEGIN
            SELECT
                @SQL = @SQL + N'SELECT TOP (20) ''' + TableName + N''' AS [Table before], * FROM ' + TableName + '; '
            FROM
                @TableUpdates;

            --print @SQL
            EXECUTE sys.sp_executesql @stmt = @SQL;

        END;


    /* 
	Get the list of data to update into temp table first then use for multiple updates 
 */
    IF OBJECT_ID(N'tempdb..#UserKeys') IS NOT NULL
        DROP TABLE #UserKeys;

    CREATE TABLE #UserKeys
        (
            email VARCHAR(100)
          , UserId INT
          , UserGUID NVARCHAR(255)/* they do not use the uniqueidentifier datatype */
          , washed_email VARCHAR(100)
        );


    INSERT INTO
        #UserKeys
        (
            email
          , UserId
          , UserGUID
          , washed_email
        )
    /* not all aspnet_users are Advisers e.g. AUK Admin / System Administrator / AUK Admin, Back Office User */
    SELECT
        u.username
      , u.id
      , u.guid
      , washed_email = @EnvironmentPrefix + '_User_' + CAST(u.id AS VARCHAR(10)) + '@Aegon.co.uk'
    FROM
        ToWash_PRA_PortalSec.dbo.USERS u;



    BEGIN TRANSACTION;

    /* PortalSec.dbo.USERS  */
    SET @StepMessage = CONVERT(VARCHAR(23), GETDATE(), 121) + ': Updating data in PortalSec.dbo.USERS...';
    PRINT @StepMessage;

    UPDATE
        u
    SET
        username = k.washed_email
      , name = 'User ' + CAST(id AS VARCHAR(10)) + ' Fullname'
    FROM
        ToWash_PRA_PortalSec.dbo.USERS AS u
        INNER JOIN #UserKeys AS k
            ON k.email = u.username;

    SELECT
        @Rowcount = @@ROWCOUNT;
    PRINT '   Rows updated: ' + CAST(@Rowcount AS VARCHAR(10));



    /* CustPortal.dbo.EMAIL_MESSAGE_AUDIT  */
    SET @StepMessage = CONVERT(VARCHAR(23), GETDATE(), 121) + ': Updating data in CustPortal.dbo.EMAIL_MESSAGE_AUDIT...';
    PRINT @StepMessage;

    UPDATE
        u
    SET
        toEmailAddress = k.washed_email
    FROM
        ToWash_PRA_CustPortal.dbo.EMAIL_MESSAGE_AUDIT AS u
        INNER JOIN #UserKeys AS k
            ON k.email = u.toEmailAddress;

    SELECT
        @Rowcount = @@ROWCOUNT;
    PRINT '   Rows updated: ' + CAST(@Rowcount AS VARCHAR(10));



    /* CustPortal.dbo.ONE_TIME_ACCESS_AUDIT  */
    SET @StepMessage = CONVERT(VARCHAR(23), GETDATE(), 121) + ': Updating data in CustPortal.dbo.ONE_TIME_ACCESS_AUDIT...';
    PRINT @StepMessage;

    UPDATE
        u
    SET
        emailAddress = CASE
                           WHEN u.emailAddress IS NOT NULL THEN k.washed_email
                           ELSE NULL
                       END
      , phoneNumber = CASE
                          WHEN u.phoneNumber IS NOT NULL THEN RIGHT('00000000000' + CAST(k.UserId AS VARCHAR(10)), 11)
                          ELSE NULL
                      END
    FROM
        ToWash_PRA_CustPortal.dbo.ONE_TIME_ACCESS_AUDIT AS u
        INNER JOIN #UserKeys AS k
            ON k.UserGUID = u.wuid;

    SELECT
        @Rowcount = @@ROWCOUNT;
    PRINT '   Rows updated: ' + CAST(@Rowcount AS VARCHAR(10));




    /* CustPortal.dbo.RESET_PASSWORD_REQUEST  */
    SET @StepMessage = CONVERT(VARCHAR(23), GETDATE(), 121) + ': Updating data in CustPortal.dbo.RESET_PASSWORD_REQUEST...';
    PRINT @StepMessage;

    UPDATE
        u
    SET
        emailAddress = k.washed_email
    FROM
        ToWash_PRA_CustPortal.dbo.RESET_PASSWORD_REQUEST AS u
        INNER JOIN #UserKeys AS k
            ON k.email = u.emailAddress;

    SELECT
        @Rowcount = @@ROWCOUNT;
    PRINT '   Rows updated: ' + CAST(@Rowcount AS VARCHAR(10));



    /* CustPortal.dbo.SESSION_DATA  */
    SET @StepMessage = CONVERT(VARCHAR(23), GETDATE(), 121) + ': Updating data in CustPortal.dbo.SESSION_DATA...';
    PRINT @StepMessage;

    UPDATE
        u
    SET
        value = k.washed_email
    FROM
        ToWash_PRA_CustPortal.dbo.SESSION_DATA AS u
        INNER JOIN #UserKeys AS k
            ON k.email = u.value
    WHERE
        u.name = 'Username';

    SELECT
        @Rowcount = @@ROWCOUNT;
    PRINT '   Rows updated: ' + CAST(@Rowcount AS VARCHAR(10));


    /* AdvPortal.dbo.Prospect */
    SET @StepMessage = CONVERT(VARCHAR(23), GETDATE(), 121) + ': Updating data in AdvPortal.dbo.Prospect...';
    PRINT @StepMessage;

    UPDATE
        p
    SET
        Name1 = 'Prospect_' + CAST(ProspectId AS VARCHAR(10)) + '_Forename'
      , Name2 = 'Prospect_' + CAST(ProspectId AS VARCHAR(10)) + '_Surname'
      , DateOfBirth = DATEADD(DAY, CONVERT(INT, RAND() * 100), DateOfBirth)
      , FormData = NULL
      , CreatedBy = @DatawashUser
      , ModifiedBy = @DatawashUser
      , ModifiedOn = GETDATE()
    FROM
        ToWash_PRA_AdvPortal.dbo.Prospect AS p;


    SELECT
        @Rowcount = @@ROWCOUNT;
    PRINT '   Rows updated: ' + CAST(@Rowcount AS VARCHAR(10));

    IF @DebugFlag = 1
        BEGIN
            SELECT
                @SQL = @SQL + N'SELECT TOP (20) ''' + TableName + N''' AS [Table after], * FROM ' + TableName + '; '
            FROM
                @TableUpdates;

            --print @SQL
            EXECUTE sys.sp_executesql @stmt = @SQL;

        END;


    -- ROLLBACK TRANSACTION;
    COMMIT TRANSACTION;

    SET @StepMessage = CONVERT(VARCHAR(23), GETDATE(), 121) + ': Datawash Complete';
    PRINT @StepMessage;

END TRY
BEGIN CATCH

    PRINT CONVERT(VARCHAR(23), GETDATE(), 121) + ': An error has occurred datawashing the data. Last successful step: ' + @StepMessage;
    PRINT ERROR_MESSAGE();

    IF XACT_STATE() = 1
        ROLLBACK TRANSACTION;

	RAISERROR(50001, 10, 127);

END CATCH;

/* clean up temp table(s) */
IF OBJECT_ID(N'tempdb..#UserKeys') IS NOT NULL
    DROP TABLE #UserKeys;


