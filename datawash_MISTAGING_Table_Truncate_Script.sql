USE MISTAGING;

/*
    DataWash MISTAGING Table Truncate

    Truncates the tables referenced in the Composer / MI_WAREHOUSE DataWash script for completeness.
*/

SET NOCOUNT ON;

DECLARE @Sql NVARCHAR(MAX)= N'', @DebugFlag BIT= 0;

DECLARE @TablesToProcess TABLE
([ProcessOrder] INT NOT NULL IDENTITY PRIMARY KEY,
 [TableName]    SYSNAME NOT NULL
);

DECLARE @TablesToTruncate TABLE
([TruncateOrder] INT NOT NULL IDENTITY PRIMARY KEY,
 [TableName]     SYSNAME NOT NULL
);

BEGIN TRY

    RAISERROR(N'MISTAGING Truncation Script Started', 0, 1);

    -- Insert in the order you wish to process
    INSERT INTO @TablesToProcess([TableName])
    VALUES(N'address'), (N'entity'), (N'correspondence'), (N'beneficiary'), (N'divorce_details'), (N'received_money'), (N'expectation'), (N'bank_account'), (N'pdc_auddis'), (N'auddis_submit_detail'), (N'external_party'), (N'related_entity'), (N'adaptor_gateway_message'), (N'member_account'), (N'note'), (N'mifid_identification'), (N'mifid_reporting_transaction'), (N'party_attribute'), (N'identification'), (N'payment_request'), (N'protection'), (N'spouse'), (N'audit_transaction');

    -- Derive actual tables to truncate from LoadType
    WITH [CTE_1]
	    AS (SELECT [tp].[ProcessOrder],
				CASE
				    WHEN [tl].[LoadType] = 'FULL'
				    THEN N'[MISTAGING].[dbo].' + QUOTENAME([tl].[TableName])
				    WHEN [tl].[LoadType] IN('DIFF', 'TAIL')
				    THEN N'[MISTAGING].[extract].' + QUOTENAME([tl].[TableName] + N'_ACTION') + N';[MISTAGING].[extract].' + QUOTENAME([tl].[TableName] + N'_DEL') + N';[MISTAGING].[extract].' + QUOTENAME([tl].[TableName] + N'_HASH') + N';[MISTAGING].[extract].' + QUOTENAME([tl].[TableName] + N'_INS')
				END [list_1]
		   FROM [AdminDB].[dbo].[TableLoad] [tl]
			   JOIN @TablesToProcess [tp] ON [tp].[TableName] = [tl].[TableName]),
	    [CTE_2]
	    AS (SELECT [CTE_1].[ProcessOrder],
				CAST(N'<root><r>' + replace([CTE_1].[list_1], ';', '</r><r>') + '</r></root>' AS XML) [list_2]
		   FROM [CTE_1])
	    INSERT INTO @TablesToTruncate([TableName])
			 SELECT [x].value('.', 'sysname') [a]
			 FROM [CTE_2]
				 CROSS APPLY [list_2].[nodes]('/root/r') [x]([x])
			 ORDER BY [CTE_2].[ProcessOrder];

    -- select the row count before 
    SET @Sql = N'';
    SELECT @Sql = @Sql + N'SELECT  ''' + [tt].[TableName] + N''' AS [Table], format(COUNT(*), ''#,##0'') as [Row count before]  FROM ' + [tt].[TableName] + N' UNION ALL '
    FROM @TablesToTruncate AS [tt]
    ORDER BY [tt].[TruncateOrder];

    SET @Sql = LEFT(@Sql, LEN(@Sql) - 10);
    IF @DebugFlag = 1
	   PRINT @Sql;
    EXEC [sys].[sp_executesql]
	    @Sql;

    -- truncate the tables
    SET @Sql = N'';
    SELECT @Sql = @Sql + N'TRUNCATE TABLE ' + [tt].[TableName] + N';'
    FROM @TablesToTruncate [tt]
    ORDER BY [tt].[TruncateOrder];

    IF @DebugFlag = 1
	   PRINT @Sql;
    EXEC [sys].[sp_executesql]
	    @Sql;

    -- select the row count after 
    SET @Sql = N'';
    SELECT @Sql = @Sql + N'SELECT  ''' + [tt].[TableName] + N''' AS [Table], format(COUNT(*), ''#,##0'') as [Row count after]  FROM ' + [tt].[TableName] + N' UNION ALL '
    FROM @TablesToTruncate AS [tt]
    ORDER BY [tt].[TruncateOrder];

    SET @Sql = LEFT(@Sql, LEN(@Sql) - 10);
    IF @DebugFlag = 1
	   PRINT @Sql;
    EXEC [sys].[sp_executesql]
	    @Sql;
END TRY
BEGIN CATCH
    SELECT ERROR_NUMBER() AS [ErrorNumber],
		 ERROR_SEVERITY() AS [ErrorSeverity],
		 ERROR_STATE() AS [ErrorState],
		 ERROR_PROCEDURE() AS [ErrorProcedure],
		 ERROR_LINE() AS [ErrorLine],
		 ERROR_MESSAGE() AS [ErrorMessage];
END CATCH;

RAISERROR(N'MISTAGING Truncation Script Ended', 0, 1);