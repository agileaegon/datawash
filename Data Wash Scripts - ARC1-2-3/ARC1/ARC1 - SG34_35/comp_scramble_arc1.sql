BEGIN
   SET NOCOUNT ON;
	 
	DECLARE	@StepMessage  NVARCHAR(4000)
		  , @ErrorMessage NVARCHAR(4000)
		  , @batchSize INT = 20000
		  , @startTime DATETIME
		  , @endTime DATETIME
		  , @rows_updated INT
		  , @next_firstname_id INT
		  , @next_row_id INT
		  , @num_firstnames INT
		  , @next_surname_id INT
		  , @num_surnames INT
		  , @iStart int
		  , @iEnd int
		  , @iIncr int
		  , @iCntr int
		 
BEGIN TRY
	SET @StepMessage = convert(varchar,getdate(),109) + ' Start Scramble'
	RAISERROR(@StepMessage,0,1) WITH NOWAIT
	SET @startTime = GETDATE();
	 
	/************************************* Create Stored Procedure ****************************************/
	-- this sproc is for string splitting
	IF EXISTS
        ( SELECT 
                *
          FROM [sys].[objects]
          WHERE [name] = 'sp_tmp_split_strings'
                AND [type] = 'P'
        ) 
    BEGIN
        DROP PROCEDURE 
              [dbo].[sp_tmp_split_strings];
    END;

	EXECUTE [dbo].[sp_executesql] @Statement = N'
	create proc sp_tmp_split_strings (
	  @list 		varchar(4096),
	  @delimiter 	char(1)
	) AS
	BEGIN

	   DECLARE @value NVARCHAR(4000) -- maximum allowable size in sql server is 4000 changed from 4096
	   DECLARE @position INT

	   SET @list = LTRIM(RTRIM(@list))+ '',''
	   SET @position = CHARINDEX(@delimiter, @list, 1)

	   IF REPLACE(@list, @delimiter, '''') <> ''''  -- replace str_REPLACE with REPLACE
	   BEGIN
			  WHILE @position > 0
			  BEGIN
					 SET @value = LTRIM(RTRIM(LEFT(@list, @position - 1)))
					 INSERT INTO #names VALUES (@value)
					 SET @list = RIGHT(@list, LEN(@list) - @position)
					 SET @position = CHARINDEX(@delimiter, @list, 1)
			  END
	   END
	END
	';
	
	/*************************************** Create Tables ******************************************/
	
	IF OBJECT_ID('tempdb..#investors_to_change') IS NOT NULL 
		DROP TABLE #investors_to_change

	create table #investors_to_change 
	(
		row_id 				numeric(7,0) identity,
		email_address 		varchar(60) null,
		mobile_number 		varchar(20) null,
		preferred_name 		varchar(40) null,
		home_phone_number 	varchar(20) null,
		work_phone_number 	varchar(20) null,
		fax_number 			varchar(20) null,
		salutation 			varchar(50) null,
		contact_name 		varchar(40) null,
		mailing_title 		varchar(60) null,
		company_rego_no 	varchar(20) null,
		tax_file_number 	varchar(11) null,
		name 				varchar(60) not null,
		given_names 		varchar(60) null,
		entity_id 			int not null,
		title_id 			int null,
		gender 				char(1) not null,
		complete 			int default 0 not null   -- control flag for updates
	)

	IF Object_Id('tempdb..#names') is not null 
		drop table #names
	
	create table #names 
	(
		name varchar(100)
	)
	
	IF Object_Id('tempdb..#surnames') is not null 
		drop table #surnames

	create table #surnames 
	(
		surname_id 	numeric(7,0) identity,
		surname 	VARCHAR(50)
	)	

	IF Object_Id('tempdb..#firstnames') is not null 
		drop table #firstnames

	create table #firstnames 
	(
		firstname_id 	numeric(7,0) identity,
		firstname 		VARCHAR(50),
		gender 			char(1)
	)
 
	/****************************** Collect into a temp table, all the investors we wish to change **************************/
	SET @StepMessage = 'Block 1 - Start';
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;
	SET @startTime = GETDATE();
	
	exec sp_tmp_split_strings 'Adams,Henderson,Ellis,Meyer,Price,Stone,Rice,Chapman,Simpson,Ramirez,Lopez,Thomas,Barnes,Matthews,Bell,Gordon,Holmes,Knight,Walker,Rivera,White,Cunningham,Bennett,Burke,Webb,Fernandez,Hansen,Rogers,Daniels,Owens,Phillips,Moore,Murray,Morgan,Wallace,Edwards,Duncan,Carter,Kennedy,Cole,Robinson,Reed,Mccoy,Howell,Richards,Torres,Black,Lane,Butler,George,Palmer,Martinez,Morrison,Rodriguez,Parker,Gilbert,Gomez,Gonzales,Evans,Clark,Perry,Kelley,Howard,Young,Medina,Frazier,Gutierrez,Henry,Jones,Anderson,Sanders,Ortiz,Harper,Nichols,Ruiz,Watson,Hall,Austin,Shaw,Hart,Mills,Perez,Sims,Reynolds,Dean,Carroll,Welch,Alexander,Jacobs,Baker,Johnston,Pierce,Mendoza,Mcdonald,Berry,Freeman,Hunt,Richardson,West,Lawson,Wells,Tucker,Collins,Montgomery,Powell,Rose,Burns,Ray,Bishop,Taylor,Turner,Morris,Cruz,Myers,Simmons,Gonzalez,Miller,Warren,Alvarez,Davis,Cox,Bailey,Lewis,Wagner,Washington,Thompson,Smith,Fields,Williams,Woods,Hayes,Mitchell,Brown,Lynch,Carpenter,Diaz,Morales,Wright,Stephens,Burton,Jackson,Arnold,Reyes,Ross,Elliott,Stevens,Bradley,Wood,Johnson,Armstrong,Hicks,Roberts,Campbell,Banks,Spencer,Ford,Wheeler,Lee,Ward,Crawford,Vasquez,Dixon,Griffin,Bowman,Murphy,Hanson,Patterson,Cooper,Jordan,Greene,Willis,Gibson,Garrett,Ferguson,Olson,King,Payne,Gardner,Harris,Fuller,Robertson,Riley,Sanchez,Romero,Graham,Grant,Reid,Porter,Dunn,Marshall,Flores,Ramos,Cook,Hawkins,James,Williamson,Larson,Kelly,Mason,Andrews,Day,Brooks,Boyd,Allen,Perkins,Peters,Fox,Hudson,Little,Garza,Gray,Harrison,Stanley,Hill,Scott,Franklin,Bryant,Nelson,Wilson,Oliver,Hamilton,Weaver,Stewart,Fowler,Fisher,Hughes,Long,Garcia,Hunter,Ryan,Lawrence,Chavez,Nguyen,Schmidt,Moreno,Carr,Castillo,Jenkins,Foster,Sullivan,Coleman,Peterson,Martin,Hernandez,Watkins,Kim,Russell,Harvey,Green,Snyder',
					   ',' -- delimeter

	insert into #surnames (surname)
	select name from #names

	delete from #names

	exec sp_tmp_split_strings 'Ann@F,Roy@M,John@M,Diane@F,Nicole@F,Ronald@M,Chris@M,Phillip@M,Denise@F,Amanda@F,Wanda@F,Beverly@F,Patricia@F,Debra@F,Ruth@F,Billy@M,Keith@M,Cynthia@F,Irene@F,Kathy@F,Anne@F,Jonathan@M,Dorothy@F,Julie@F,Gary@M,Joyce@F,Joseph@M,Andrew@M,Rachel@F,Jeffrey@M,Justin@M,Frank@M,Mark@M,Donna@F,Stephen@M,Catherine@F,Jose@M,Christopher@M,Brian@M,Michael@M,Sara@F,Diana@F,Patrick@M,Lisa@F,Kenneth@M,Angela@F,Bruce@M,Scott@M,Ruby@F,Stephanie@F,Gerald@M,Virginia@F,Harold@M,Gregory@M,Philip@M,Terry@M,Robert@M,Jesse@M,Larry@M,Albert@M,Janice@F,Joan@F,Jacqueline@F,Martin@M,Thomas@M,Ryan@M,Samuel@M,Janet@F,Jennifer@F,Russell@M,Jessica@F,Steve@M,Willie@M,Ashley@F,Walter@M,Katherine@F,Karen@F,Helen@F,Wayne@M,Melissa@F,Kathleen@F,Susan@F,Timothy@M,Eric@M,Barbara@F,Maria@F,Emily@F,Christine@F,Daniel@M,Norma@F,Ernest@M,Sean@M,Raymond@M,Teresa@F,Howard@M,Johnny@M,Alice@F,Elizabeth@F,Andrea@F,Kelly@F,Edward@M,Henry@M,Bonnie@F,Shawn@M,Frances@F,Christina@F,Paula@F,Juan@M,Judy@F,Tina@F,Brenda@F,Pamela@F,Ralph@M,Jimmy@M,Matthew@M,Marie@F,James@M,Carl@M,Rose@F,Adam@M,Mary@F,Jason@M,Milo@M,Lillian@F,Carolyn@F,Amy@F,Mildred@F,Earl@M,Joshua@M,Sharon@F,Robin@F,Louis@M,Gloria@F,Brandon@M,Michelle@F,Douglas@M,Louise@F,Clarence@M,Eugene@M,Charles@M,Victor@M,Theresa@F,Jane@F,Jerry@M,Heather@F,Lawrence@M,William@M,Nicholas@M,Deborah@F,Donald@M,Evelyn@F,Jack@M,Jean@F,Annie@F,Arthur@M,Julia@F,Anthony@M,Antonio@M,Nancy@F,Craig@M,Betty@F,Benjamin@M,Martha@F,Peter@M,Doris@F,Randy@M,Bobby@M,Cheryl@F,Todd@M,Sarah@F,Harry@M,Phyllis@F,Kevin@M,Jeremy@M,Marilyn@F,Linda@F,Judith@F,David@M,Fred@M,Dennis@M,Carol@F,Sandra@F,Kathryn@F,Richard@M,Steven@M,Anna@F,Lois@F,George@M,Kimberly@F,Carlos@M,Aaron@M,Rebecca@F,Alan@M,Margaret@F,Joe@M,Shirley@F,Roger@M,Laura@F,Tammy@F,Paul@M,Lori@F',',' -- delimeter

	-- pull out the gender and name off the names table
	insert into #firstnames (firstname,gender)
	select 	left(name,charindex('@',name,1)-1), right(name,1)
	from 	#names

	delete from #names;

	--Now do the work for Investors
	insert into #investors_to_change 
	(entity_id, email_address,title_id,mobile_number,preferred_name,home_phone_number,work_phone_number,fax_number,salutation,
	contact_name,mailing_title,company_rego_no,tax_file_number,name,given_names,gender)
	select distinct e.entity_id,
					e.email_address,
					e.title_id,
					e.mobile_number,
					e.preferred_name,
					e.home_phone_number,
					e.work_phone_number,
					e.fax_number,
					e.salutation,
					e.contact_name,
					e.mailing_title,
					e.company_rego_no,
					e.tax_file_number,
					e.name,
					e.given_names,
					e.gender
	from 	member_account ma
			join entity e on e.entity_id = ma.entity_id
	where 	e.entity_id not in ( 
					977    -- over and unders account 
				   ,996    -- live proving account
		   )

	CREATE NONCLUSTERED INDEX IDX_investors_to_change ON #investors_to_change(entity_id)

	CREATE NONCLUSTERED INDEX IDX2_investors_to_change ON #investors_to_change(row_id,complete)

	select count(1) as number_of_investors from #investors_to_change
	 
	-- strip out the data and replace 'basic random generated'
	update	#investors_to_change
	set 	tax_file_number = (case entity_id % 5 when 0 then 'HM' when 1 then 'CS' when 2 then 'TZ' when 3 then 'PH' when 4 then 'NA' else 'LR' end) + right('00000' + convert(varchar,(entity_id*11) % 999999),6) + (case when entity_id % 2 = 1 then 'A' else 'B' end),
			email_address = 'scrambled@noemail.nodomain',
			mobile_number = '07000000000',  -- Real Looking :: '07' + right('000000000' + convert(varchar,(ma.entity_id * 3) % 999999999),9)
			preferred_name = NULL,
			home_phone_number = '01000 000 000',
			work_phone_number = '01000 111 111',
			fax_number = NULL,
			contact_name = NULL,
			mailing_title = NULL,
			company_rego_no = NULL
	
	SET @endTime = GETDATE();
	SET @StepMessage = 'Block 1 - Finish. Time Taken - ' + CONVERT(VARCHAR(20), @endTime - @startTime, 114);
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;

	/***** This next part - we cycle through bulk updates to the temp table by "random name number" sets *****/
	
	SET @StepMessage = 'Block 2 - Start';
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;
	SET @startTime = GETDATE();

	select @batchSize = 20000
	select @num_firstnames = count(1) from #firstnames
	select @next_row_id = 0
	select @rows_updated = @batchSize

	while ( @rows_updated = @batchSize )
	begin
		 select @next_row_id = min(row_id) from #investors_to_change where row_id > @next_row_id and complete = 0

		 if @next_row_id is null
		   break

		 set rowcount @batchSize
	 
		 update #investors_to_change
		 set 	given_names = #firstnames.firstname,
				title_id = (case when #firstnames.gender='M' then 1 when #firstnames.gender='F' Then 2 else NULL end),
				salutation = (case when #firstnames.gender='M' then 'Mr '+#firstnames.firstname when #firstnames.gender='F' then 'Mrs '+#firstnames.firstname end),
				gender = #firstnames.gender,
				complete = 1
		 from 	#firstnames
		 where 	((row_id % @num_firstnames) + 1 = #firstnames.firstname_id) and row_id >= @next_row_id


		 select @rows_updated = @@rowcount
	 
		 print 'Next Row ID = ' + convert(varchar, @next_row_id) + ' Rows Updated = ' + convert(varchar, @rows_updated)
	 
		 set rowcount 0
	end

	select given_names, count(1) 'Count Check' from #investors_to_change group by given_names order by given_names

	SET @endTime = GETDATE();
	SET @StepMessage = 'Block 2 - Finish. Time Taken - ' + CONVERT(VARCHAR(20), @endTime - @startTime, 114);
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;

	/*************************************** Reset to do the surnames ******************************************/
	SET @StepMessage = 'Block 3 - Start';
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;
	SET @startTime = GETDATE();

	update #investors_to_change set complete = 0

	select convert(varchar,getdate(),109) + 'DT before scramble2'
	
	select @batchSize = 20000
	select @num_surnames = count(1) from #surnames
	select @next_row_id = 0
	select @rows_updated = @batchSize

	while ( @rows_updated = @batchSize )
	begin
		 select @next_row_id = min(row_id) from #investors_to_change where row_id > @next_row_id and complete = 0

		 if @next_row_id is null
		   break

		 set rowcount @batchSize
	 
		 update	#investors_to_change
		 set 	name = #surnames.surname,
				complete = 1
		 from 	#surnames
		 where ((row_id % @num_surnames) + 1 = #surnames.surname_id) and row_id >= @next_row_id
	 
		 select @rows_updated = @@rowcount
	 
		 print 'Next Row ID = ' + convert(varchar, @next_row_id) + ' Rows Updated = ' + convert(varchar, @rows_updated)
	 
		 set rowcount 0
	end

	select @StepMessage = convert(varchar,getdate(),109) + ' DT after scramble2'
	RAISERROR(@StepMessage,0,1) WITH NOWAIT

	select name, count(1) 'Count Check' from #investors_to_change group by name order by name

	SET @endTime = GETDATE();
	SET @StepMessage = 'Block 3 - Finish. Time Taken - ' + CONVERT(VARCHAR(20), @endTime - @startTime, 114);
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;

	/********************************* Investor *********************************/

    SET @StepMessage = 'Block 4 - Start';
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;
	SET @startTime = GETDATE();
	
	IF EXISTS 
		(	SELECT * 
			FROM sys.objects 
			WHERE [name] = N'au_entity' AND [type] = 'TR'
		)
	BEGIN
		ALTER TABLE [entity] DISABLE TRIGGER [au_entity];      
	END;

	Update 	entity
	set 	given_names       = ic.given_names,
			email_address     = ic.email_address,
			title_id          = ic.title_id,
			mobile_number     = ic.mobile_number,
			preferred_name    = ic.preferred_name,
			home_phone_number = ic.home_phone_number,
			work_phone_number = ic.work_phone_number,
			fax_number        = ic.fax_number,
			salutation        = ic.salutation,
			contact_name      = ic.contact_name,
			mailing_title     = ic.mailing_title,
			company_rego_no   = ic.company_rego_no,
			tax_file_number   = ic.tax_file_number,
			name              = ic.name,
			gender 		      = ic.gender
	from 	entity e
			join #investors_to_change ic on ic.entity_id = e.entity_id

	IF EXISTS 
		(	SELECT * 
			FROM sys.objects 
			WHERE [name] = N'au_entity' AND [type] = 'TR'
		)
	BEGIN
		ALTER TABLE [entity] ENABLE TRIGGER [au_entity];      
	END;

	SET @endTime = GETDATE();
	SET @StepMessage = 'Block 4 - Finish. Time Taken - ' + CONVERT(VARCHAR(20), @endTime - @startTime, 114);
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;

	/************************ Address **************************/
    
	SET @StepMessage = 'Block 5 - Start';
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;
	SET @startTime = GETDATE();

	IF EXISTS
		(	SELECT * 
			FROM sys.objects 
			WHERE [name] = N'au_address' AND [type] = 'TR'
		)
	BEGIN
		ALTER TABLE [address] DISABLE TRIGGER [au_address];
	END;

	update 	address
	set 	care_of_name = null,
			property_name = case address_id % 8 when 0 then 'The Villa' when 1 then 'The House' when 2 then 'Mansion Manor' when 3 then 'The Vista' when 4 then 'The Greenroof' when 5 then 'Tinhouse' when 6 then 'The Flat' when 7 then 'The Stable' end,
			street_number = convert(varchar,(address_id % 101 ) + 1),
			street = (case address_id % 3 when 0 then 'Big' when 1 then 'Small' when 2 then 'Middle' end) + ' ' + (case address_id % 3 when 0 then 'Street' when 1 then 'Drive' when 2 then 'Avenue' end),
			street2 = 'Over' + case address_id % 7 when 0 then 'two' when 1 then 'three' when 2 then 'four' when 3 then 'five' when 4 then 'six' when 5 then 'seven' when 6 then 'nine' end,
			suburb = case address_id % 5 when 0 then 'Longton' when 1 then 'Ellen-on-Sea' when 2 then 'Brackenside' when 3 then 'Viewton' when 4 then 'Breen' end,
			district = case address_id % 6 when 0 then 'Suburbian' when 1 then 'Localshire' when 2 then 'Otherside' when 3 then 'Outston' when 4 then 'Greyville' when 5 then 'Hillend' end,
			statecode = CASE [address_id] % 7 WHEN 0 THEN 'Hillwood' WHEN 1 THEN 'Sunnydale' WHEN 2 THEN 'Venusville' WHEN 3 THEN 'Bedrock' WHEN 4 THEN 'Neverland' WHEN 5 THEN 'Midgar' WHEN 6 THEN 'Springfield' END,
			postcode = case address_id % 5 when 0 then 'NE' when 1 then 'GW' when 2 then 'ST' when 3 then 'BS' when 4 then 'AB' end + convert(varchar,(address_id % 40) + 1) + 
			   ' ' + convert(varchar,(address_id % 8)+1) + 
			   case address_id % 6 when 0 then 'AX' when 1 then 'TG' when 2 then 'ER' when 3 then 'NJ' when 4 then 'WD' when 5 then 'GA' end
	from 	address ad
			join member_account ma on ma.entity_id = ad.entity_id
	where 	ma.entity_id not in ( 
					977    -- over and unders account 
				   ,996    -- live proving account
		   )

	IF EXISTS
		(	SELECT * 
			FROM sys.objects 
			WHERE [name] = N'au_address' AND [type] = 'TR'
		)
	BEGIN
		ALTER TABLE [address] ENABLE TRIGGER [au_address];
	END;

	SET @endTime = GETDATE();
	SET @StepMessage = 'Block 5 - Finish. Time Taken - ' + CONVERT(VARCHAR(20), @endTime - @startTime, 114);
	RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;

	/************************ Adviser Address **************************/

    SET @StepMessage = 'Block 6 - Start';
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;
	SET @startTime = GETDATE();

	IF EXISTS
		(	SELECT * 
			FROM sys.objects 
			WHERE [name] = N'au_address' AND [type] = 'TR'
		)
	BEGIN
		ALTER TABLE [address] DISABLE TRIGGER [au_address];
	END;

	update 	address
	set 	care_of_name = null,
			property_name = case address_id % 8 when 0 then 'The Villa' when 1 then 'The House' when 2 then 'Mansion Manor' when 3 then 'The Vista' when 4 then 'The Greenroof' when 5 then 'Tinhouse' when 6 then 'The Flat' when 7 then 'The Stable' end,
			street_number = convert(varchar,(address_id % 101 ) + 1),
			street = (case address_id % 3 when 0 then 'Big' when 1 then 'Small' when 2 then 'Middle' end) + ' ' + (case address_id % 3 when 0 then 'Street' when 1 then 'Drive' when 2 then 'Avenue' end),
			street2 = 'Over' + case address_id % 7 when 0 then 'two' when 1 then 'three' when 2 then 'four' when 3 then 'five' when 4 then 'six' when 5 then 'seven' when 6 then 'nine' end,
			suburb = case address_id % 5 when 0 then 'Longton' when 1 then 'Ellen-on-Sea' when 2 then 'Brackenside' when 3 then 'Viewton' when 4 then 'Breen' end,
			district = case address_id % 6 when 0 then 'Suburbian' when 1 then 'Localshire' when 2 then 'Otherside' when 3 then 'Outston' when 4 then 'Greyville' when 5 then 'Hillend' end,
			statecode = CASE [address_id] % 7 WHEN 0 THEN 'Hillwood' WHEN 1 THEN 'Sunnydale' WHEN 2 THEN 'Venusville' WHEN 3 THEN 'Bedrock' WHEN 4 THEN 'Neverland' WHEN 5 THEN 'Midgar' WHEN 6 THEN 'Springfield' END,
			postcode = case address_id % 5 when 0 then 'NE' when 1 then 'GW' when 2 then 'ST' when 3 then 'BS' when 4 then 'AB' end + convert(varchar,(address_id % 40) + 1) + 
			   ' ' + convert(varchar,(address_id % 8)+1) + 
			   case address_id % 6 when 0 then 'AX' when 1 then 'TG' when 2 then 'ER' when 3 then 'NJ' when 4 then 'WD' when 5 then 'GA' end
	from 	address ad
			join adviser_account aa on aa.entity_id = ad.entity_id

	IF EXISTS
		(	SELECT * 
			FROM sys.objects 
			WHERE [name] = N'au_address' AND [type] = 'TR'
		)
	BEGIN
		ALTER TABLE [address] ENABLE TRIGGER [au_address];
	END;

	SET @endTime = GETDATE();
	SET @StepMessage = 'Block 6 - Finish. Time Taken - ' + CONVERT(VARCHAR(20), @endTime - @startTime, 114);
	RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;

	/************************ Dealer Network Address **************************/

    SET @StepMessage = 'Block 7 - Start';
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;
	SET @startTime = GETDATE();

	IF EXISTS
		(	SELECT * 
			FROM sys.objects 
			WHERE [name] = N'au_address' AND [type] = 'TR'
		)
	BEGIN
		ALTER TABLE [address] DISABLE TRIGGER [au_address];
	END;

	update 	address
	set 	care_of_name = null,
			property_name = case address_id % 8 when 0 then 'The Villa' when 1 then 'The House' when 2 then 'Mansion Manor' when 3 then 'The Vista' when 4 then 'The Greenroof' when 5 then 'Tinhouse' when 6 then 'The Flat' when 7 then 'The Stable' end,
			street_number = convert(varchar,(address_id % 101 ) + 1),
			street = (case address_id % 3 when 0 then 'Big' when 1 then 'Small' when 2 then 'Middle' end) + ' ' + (case address_id % 3 when 0 then 'Street' when 1 then 'Drive' when 2 then 'Avenue' end),
			street2 = 'Over' + case address_id % 7 when 0 then 'two' when 1 then 'three' when 2 then 'four' when 3 then 'five' when 4 then 'six' when 5 then 'seven' when 6 then 'nine' end,
			suburb = case address_id % 5 when 0 then 'Longton' when 1 then 'Ellen-on-Sea' when 2 then 'Brackenside' when 3 then 'Viewton' when 4 then 'Breen' end,
			district = case address_id % 6 when 0 then 'Suburbian' when 1 then 'Localshire' when 2 then 'Otherside' when 3 then 'Outston' when 4 then 'Greyville' when 5 then 'Hillend' end,
			statecode = CASE [address_id] % 7 WHEN 0 THEN 'Hillwood' WHEN 1 THEN 'Sunnydale' WHEN 2 THEN 'Venusville' WHEN 3 THEN 'Bedrock' WHEN 4 THEN 'Neverland' WHEN 5 THEN 'Midgar' WHEN 6 THEN 'Springfield' END,
			postcode = case address_id % 5 when 0 then 'NE' when 1 then 'GW' when 2 then 'ST' when 3 then 'BS' when 4 then 'AB' end + convert(varchar,(address_id % 40) + 1) + 
			   ' ' + convert(varchar,(address_id % 8)+1) + 
			   case address_id % 6 when 0 then 'AX' when 1 then 'TG' when 2 then 'ER' when 3 then 'NJ' when 4 then 'WD' when 5 then 'GA' end
	from 	address ad
			join dealer_branch db on db.entity_id = ad.entity_id

	IF EXISTS
		(	SELECT * 
			FROM sys.objects 
			WHERE [name] = N'au_address' AND [type] = 'TR'
		)
	BEGIN
		ALTER TABLE [address] ENABLE TRIGGER [au_address];
	END;

	SET @endTime = GETDATE();
	SET @StepMessage = 'Block 7 - Finish. Time Taken - ' + CONVERT(VARCHAR(20), @endTime - @startTime, 114);
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;

	/************************ Dealer Address **************************/

	SET @StepMessage = 'Block 8 - Start';
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;
	SET @startTime = GETDATE();

	IF EXISTS
		(	SELECT * 
			FROM sys.objects 
			WHERE [name] = N'au_address' AND [type] = 'TR'
		)
	BEGIN
		ALTER TABLE [address] DISABLE TRIGGER [au_address];
	END;

	update 	address
	set 	care_of_name = null,
			property_name = case address_id % 8 when 0 then 'The Villa' when 1 then 'The House' when 2 then 'Mansion Manor' when 3 then 'The Vista' when 4 then 'The Greenroof' when 5 then 'Tinhouse' when 6 then 'The Flat' when 7 then 'The Stable' end,
			street_number = convert(varchar,(address_id % 101 ) + 1),
			street = (case address_id % 3 when 0 then 'Big' when 1 then 'Small' when 2 then 'Middle' end) + ' ' + (case address_id % 3 when 0 then 'Street' when 1 then 'Drive' when 2 then 'Avenue' end),
			street2 = 'Over' + case address_id % 7 when 0 then 'two' when 1 then 'three' when 2 then 'four' when 3 then 'five' when 4 then 'six' when 5 then 'seven' when 6 then 'nine' end,
			suburb = case address_id % 5 when 0 then 'Longton' when 1 then 'Ellen-on-Sea' when 2 then 'Brackenside' when 3 then 'Viewton' when 4 then 'Breen' end,
			district = case address_id % 6 when 0 then 'Suburbian' when 1 then 'Localshire' when 2 then 'Otherside' when 3 then 'Outston' when 4 then 'Greyville' when 5 then 'Hillend' end,
			statecode = CASE [address_id] % 7 WHEN 0 THEN 'Hillwood' WHEN 1 THEN 'Sunnydale' WHEN 2 THEN 'Venusville' WHEN 3 THEN 'Bedrock' WHEN 4 THEN 'Neverland' WHEN 5 THEN 'Midgar' WHEN 6 THEN 'Springfield' END,
			postcode = case address_id % 5 when 0 then 'NE' when 1 then 'GW' when 2 then 'ST' when 3 then 'BS' when 4 then 'AB' end + convert(varchar,(address_id % 40) + 1) + 
			   ' ' + convert(varchar,(address_id % 8)+1) + 
			   case address_id % 6 when 0 then 'AX' when 1 then 'TG' when 2 then 'ER' when 3 then 'NJ' when 4 then 'WD' when 5 then 'GA' end
	from 	address ad
			join dealer d on d.entity_id = ad.entity_id

	IF EXISTS
		(	SELECT * 
			FROM sys.objects 
			WHERE [name] = N'au_address' AND [type] = 'TR'
		)
	BEGIN
		ALTER TABLE [address] ENABLE TRIGGER [au_address];
	END;

	SET @endTime = GETDATE();
	SET @StepMessage = 'Block 8 - Finish. Time Taken - ' + CONVERT(VARCHAR(20), @endTime - @startTime, 114);
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;

	/************************ Employer Address **************************/
	
	SET @StepMessage = 'Block 9 - Start';
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;
	SET @startTime = GETDATE();

	IF EXISTS
		(	SELECT * 
			FROM sys.objects 
			WHERE [name] = N'au_address' AND [type] = 'TR'
		)
	BEGIN
		ALTER TABLE [address] DISABLE TRIGGER [au_address];
	END;

	update 	address
	set		care_of_name = null,
			property_name = case address_id % 8 when 0 then 'The Villa' when 1 then 'The House' when 2 then 'Mansion Manor' when 3 then 'The Vista' when 4 then 'The Greenroof' when 5 then 'Tinhouse' when 6 then 'The Flat' when 7 then 'The Stable' end,
			street_number = convert(varchar,(address_id % 101 ) + 1),
			street = (case address_id % 3 when 0 then 'Big' when 1 then 'Small' when 2 then 'Middle' end) + ' ' + (case address_id % 3 when 0 then 'Street' when 1 then 'Drive' when 2 then 'Avenue' end),
			street2 = 'Over' + case address_id % 7 when 0 then 'two' when 1 then 'three' when 2 then 'four' when 3 then 'five' when 4 then 'six' when 5 then 'seven' when 6 then 'nine' end,
			suburb = case address_id % 5 when 0 then 'Longton' when 1 then 'Ellen-on-Sea' when 2 then 'Brackenside' when 3 then 'Viewton' when 4 then 'Breen' end,
			district = case address_id % 6 when 0 then 'Suburbian' when 1 then 'Localshire' when 2 then 'Otherside' when 3 then 'Outston' when 4 then 'Greyville' when 5 then 'Hillend' end,
			statecode = CASE [address_id] % 7 WHEN 0 THEN 'Hillwood' WHEN 1 THEN 'Sunnydale' WHEN 2 THEN 'Venusville' WHEN 3 THEN 'Bedrock' WHEN 4 THEN 'Neverland' WHEN 5 THEN 'Midgar' WHEN 6 THEN 'Springfield' END,
			postcode = case address_id % 5 when 0 then 'NE' when 1 then 'GW' when 2 then 'ST' when 3 then 'BS' when 4 then 'AB' end + convert(varchar,(address_id % 40) + 1) + 
			   ' ' + convert(varchar,(address_id % 8)+1) + 
			   case address_id % 6 when 0 then 'AX' when 1 then 'TG' when 2 then 'ER' when 3 then 'NJ' when 4 then 'WD' when 5 then 'GA' end
	from 	address ad
			join employer e on e.entity_id = ad.entity_id

	IF EXISTS
		(	SELECT * 
			FROM sys.objects 
			WHERE [name] = N'au_address' AND [type] = 'TR'
		)
	BEGIN
		ALTER TABLE [address] ENABLE TRIGGER [au_address];
	END;

	SET @endTime = GETDATE();
	SET @StepMessage = 'Block 9 - Finish. Time Taken - ' + CONVERT(VARCHAR(20), @endTime - @startTime, 114);
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;

	/*************************************** Related Entity Address ******************************************/

    SET @StepMessage = 'Block 10 - Start';
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;
	SET @startTime = GETDATE();

    -- Address for related entities.
    IF EXISTS 
		(	SELECT * 
			FROM sys.objects 
			WHERE [name] = N'au_address' AND [type] = 'TR'
		)
	BEGIN
		ALTER TABLE [address] DISABLE TRIGGER [au_address];      
	END;

	UPDATE [address]
       SET [property_name] = 'The House'
         , [street_number] = '27'
         , [street] = 'Acacia Avenue'
         , [street2] = 'Overbridge'
         , [suburb] = 'Longtown'
         , [district] = 'Acacialand'
		 , [statecode] = 'Hillwood'
    FROM [address] [ad]
        JOIN [related_to] [rt]
            ON [rt].[related_entity_id] = [ad].[entity_id];

	IF EXISTS 
		(	SELECT * 
			FROM sys.objects 
			WHERE [name] = N'au_address' AND [type] = 'TR'
		)
	BEGIN
		ALTER TABLE [address] ENABLE TRIGGER [au_address];      
	END;

    SET @endTime = GETDATE();
	SET @StepMessage = 'Block 10 - Finish. Time Taken - ' + CONVERT(VARCHAR(20), @endTime - @startTime, 114);
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;

	/******************************************* Adviser *********************************************/
    SET @StepMessage = 'Block 11 - Start';
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;
	SET @startTime = GETDATE();

	IF EXISTS 
		(	SELECT * 
			FROM sys.objects 
			WHERE [name] = N'au_entity' AND [type] = 'TR'
		)
	BEGIN
		ALTER TABLE [entity] DISABLE TRIGGER [au_entity];      
	END;

	Update 	entity
	set  	given_names = (case when gender='M' then case e.entity_id % 11 when 0 then 'Bob' when 1 then 'Harry' when 2 then 'Jim' when 3 then 'Wayne' when 4 then 'Arthur' 
																		when 5 then 'Adam' when 6 then 'David' when 7 then 'Paul' when 8 then 'Thomas' when 9 then 'Donald' when 10 then 'Shawn' end
							 else                 case e.entity_id % 7 when 0 then 'Jean' when 1 then 'Sue' when 2 then 'Lori' when 3 then 'Helen' when 4 then 'Sarah' 
																	   when 5 then 'Norma' when 6 then 'Doris' end
						 end),
			name = case e.entity_id % 19 when 0 then 'Mills' when 1 then 'Diaz' when 2 then 'Reyes' when 3 then 'Hunter' when 4 then 'Ferguson' 
										   when 5 then 'Hill' when 6 then 'Wells' when 7 then 'King' when 8 then 'Bishop' when 9 then 'Harvey'
										   when 10 then 'Chavez' when 11 then 'Robinson' when 12 then 'King' when 13 then 'Armstrong' when 14 then 'Harvey' 
										   when 15 then 'Roberts' when 16 then 'Morrison' when 17 then 'Gonzales' when 18 then 'Bishop' when 19 then 'Bishop' end 
				+ ' (' + convert(varchar,db.dealer_id) + '/' + convert(varchar,db.dealer_branch_id) + '/' + convert(varchar,adv_acc.adviser_account_id) + ')'
				+ ' ' + case sf.short_code when 'AOR' then 'AOR' when 'AEGON' then 'ARC' when 'ARR' then 'RR' end
				+ case when e.name like '% DM' then ' DM' else '' end, -- add discretionary suffix
			email_address = 'scrambled@noemail.nodomain',
			title_id      =  (case when gender='M' then 1 when gender='F' Then 2 else NULL end),
			mobile_number = '07000000000',
			preferred_name = (case when gender='M' then 'Mr' when gender='F' Then 'Mrs' else NULL end) + ' IFA',
			home_phone_number = '01000 000 000',
			work_phone_number = '01000 111 111',
			fax_number = NULL,
			contact_name = NULL,
			mailing_title = NULL,
			company_rego_no = NULL,
			tax_file_number = (case when entity_type ='I' then 'TA000000C' else '000000000' end)
	from 	adviser_account adv_acc
	join  	entity e on e.entity_id = adv_acc.entity_id
			join dealer_branch db on db.dealer_branch_id = adv_acc.dealer_branch_id
			join subfund_associated_party sap on sap.party_id = db.dealer_branch_id and sap.party_type_id = 26
			join subfund sf on sf.subfund_id = sap.subfund_id
	where 
			adv_acc.adviser_account_id not in (
			1, --          Blueplanet Direct
			2, --          Non Advised
			4, --          Live Test
			14, -- "Non Advised" -- we keep
			4967, --    Front Office
			2031, --    Non Advised
			10706, --  Non Advised
			10707, --  Non Advised
			10708, --  Non Advised
			10709, --  Non Advised
			10710, --  Non Advised
			10711, --  Non Advised
			10712, --  Non Advised
			10713, --  Non Advised
			10714  -- Non Advised
	)  
	and
			db.dealer_branch_id  not in (
			1, --          Blueplanet Direct Branch
			2, --          AEGON At Retirment
			3, --          Test branch
			4, --          AEGON Retirement Choices Sales
			5, --          AEGON Workplace
			526, --      AEGON One Retirement
			2895, --    AEGON Retiready 0%
			2896, --    AEGON Retiready 6%
			2897, --    AEGON Retiready 12%
			2898, --    AEGON Retiready 19%
			2899, --    AEGON Retiready 25%
			2900, --    AEGON Retiready 31%
			2901, --    AEGON Retiready 37%
			2902, --    AEGON Retiready 44%
			2903  --  AEGON Retiready 50%
			)
	and 	db.dealer_id <> 1805 -- retiready

	update	entity
	set 	salutation = 'Dear ' + (case when gender='M' then 'Mr' when gender='F' Then 'Mrs' else NULL end) + ' ' + name,
			preferred_name = (case when gender='M' then 'Mr' when gender='F' Then 'Mrs' else NULL end) + ' ' + name
	from 	adviser_account adv_acc
			join entity e on e.entity_id = adv_acc.entity_id

	IF EXISTS 
		(	SELECT * 
			FROM sys.objects 
			WHERE [name] = N'au_entity' AND [type] = 'TR'
		)
	BEGIN
		ALTER TABLE [entity] ENABLE TRIGGER [au_entity];      
	END;

	SET @endTime = GETDATE();
	SET @StepMessage = 'Block 11 - Finish. Time Taken - ' + CONVERT(VARCHAR(20), @endTime - @startTime, 114);
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;

	/*************************** Dealer **************************/

	SET @StepMessage = 'Block 12 - Start';
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;
	SET @startTime = GETDATE();

	IF EXISTS 
		(	SELECT * 
			FROM sys.objects 
			WHERE [name] = N'au_entity' AND [type] = 'TR'
		)
	BEGIN
		ALTER TABLE [entity] DISABLE TRIGGER [au_entity];      
	END;

	Update	entity
	set  	given_names = NULL,
			email_address = 'scrambled@noemail.nodomain',
			title_id      =  (case when gender='M' then 1 when gender='F' Then 2 else NULL end),
			mobile_number = '07000000000',
			home_phone_number = '01000 000 000',
			work_phone_number = '01000 111 111',
			fax_number = NULL,
			salutation    = 'Dear Sir / Madam',
			contact_name = NULL,
			mailing_title = NULL,
			company_rego_no = NULL,
			tax_file_number = (case when entity_type ='I' then 'AA000000D' else '000000000' end),
			name   = case e.entity_id % 8 when 0 then 'Light ' when 1 then 'Dark ' when 2 then 'Mid ' else ' ' end + case e.entity_id % 50 
			when	0	then 	'An White'
			when	1	then 	'Aqua'
			when	2	then 	'Aqua'
			when	3	then 	'Azure'
			when	4	then 	'Beige'
			when	5	then 	'Bisque'
			when	6	then 	'Black'
			when	7	then 	'Almond'
			when	8	then 	'Blue'
			when	9	then 	'Mustard'
			when	10	then 	'Brown'
			when	11	then 	'Burly'
			when	12	then 	'C Blue'
			when	13	then 	'Chart'
			when	14	then 	'Choc'
			when	15	then 	'Coral'
			when	16	then 	'Cornf'
			when	17	then 	'Corns'
			when	18	then 	'Crimson'
			when	19	then 	'Cyan'
			when	20	then 	'Blue'
			when	21	then 	'Cyan'
			when	22	then 	'Golden'
			when	23	then 	'Gray'
			when	24	then 	'Green'
			when	25	then 	'Khaki'
			when	26	then 	'Magenta'
			when	27	then 	'Green'
			when	28	then 	'Orange'
			when	29	then 	'Orchid'
			when	30	then 	'Red'
			when	31	then 	'Salmon'
			when	32	then 	'S Green'
			when	33	then 	'S Blue'
			when	34	then 	'S Gray'
			when	35	then 	'Turqu'
			when	36	then 	'Violet'
			when	37	then 	'Pink'
			when	38	then 	'Sky Blue'
			when	39	then 	'Dim Gray'
			when	40	then 	'Dod Blue'
			when	41	then 	'Fireb'
			when	42	then 	'O White'
			when	43	then 	'F Green'
			when	44	then 	'Fuchsia'
			when	45	then 	'Gains'
			when	46	then 	'Eggs'
			when	47	then 	'Gold'
			when	48	then 	'Golden'
			when	49	then 	'Yellow' end + ' ' + 
			case e.entity_id % 3 when 0 then 'Best' when 1 then 'Link' when 2 then 'Top' end + ' ' +
			case e.entity_id % 4 when 0 then 'Solutions' when 1 then 'Network' when 2 then 'Advisers' when 3 then 'Growth' end 
			+ ' (' + convert(varchar,d.dealer_id) + ')'
			+ case when e.name like '% DM' then ' DM' else '' end -- add discretionary suffix
	from 	entity e
			join dealer d on e.entity_id = d.entity_id
	where 
			d.dealer_id <> 1  -- Blueplanet Direct Network
	and 	d.dealer_id <> 2  -- AEGON Retirement Choices Direct ARC 
	and 	d.dealer_id <> 3  -- AEGON Retirement Choices Sales ARC
	and 	d.dealer_id <> 299  -- AEGON One Retirement Direct AOR
	and 	d.dealer_id <> 1805 -- AEGON Retiready  

	/*
		branches are assigned to the subfund, so we will look at all branches to see if they are all of one "subfund type"
		and rename accordingly else .leave. the suffix alone.  ('')	
	*/

	update entity 
	  set name = name + ' ' + 
                 
					 (case 
							   when not exists 
								 (
					select db.dealer_id
					 from dealer_branch db
									join subfund_associated_party sap on sap.party_id = db.dealer_branch_id and sap.party_type_id = 26
									  join subfund sf on sf.subfund_id = sap.subfund_id
				  where db.dealer_id = d.dealer_id
				  and (sf.short_code = 'AOR' or sf.short_code = 'AEGON' or sf.short_code = 'ARR')
                             
								 ) then ''
							   when not exists 
								 (
					select db.dealer_id
					 from dealer_branch db
									join subfund_associated_party sap on sap.party_id = db.dealer_branch_id and sap.party_type_id = 26
									  join subfund sf on sf.subfund_id = sap.subfund_id
				  where db.dealer_id = d.dealer_id
				  and (sf.short_code = 'AOR' or sf.short_code = 'AEGON')
                             
								 ) then 'RR'
							
							   when not exists 
								 (
                             
					select db.dealer_id
					  from dealer_branch db
									join subfund_associated_party sap on sap.party_id = db.dealer_branch_id and sap.party_type_id = 26
									  join subfund sf on sf.subfund_id = sap.subfund_id
				  where db.dealer_id = d.dealer_id
				  and (sf.short_code = 'ARR' or sf.short_code = 'AEGON')
                             
								 ) then 'AOR'

								when not exists 
								 (
                             
					select db.dealer_id
					  from dealer_branch db
									join subfund_associated_party sap on sap.party_id = db.dealer_branch_id and sap.party_type_id = 26
									  join subfund sf on sf.subfund_id = sap.subfund_id
				  where db.dealer_id = d.dealer_id
				  and (sf.short_code = 'ARR' or sf.short_code = 'AOR')
                             
								 ) then 'ARC'
				 else ''
				 end
				 )
	from 	entity e 
			join dealer d on d.entity_id = e.entity_id
	where   d.dealer_id <> 1  -- Blueplanet Direct Network
	and 	d.dealer_id <> 2  -- AEGON Retirement Choices Direct ARC
	and 	d.dealer_id <> 3  -- AEGON Retirement Choices Sales ARC
	and 	d.dealer_id <> 299  -- AEGON One Retirement Direct AOR
	and 	d.dealer_id <> 1805 -- AEGON Retiready  

	update 	entity
	set 	preferred_name = name
	from 	entity e
			join dealer d on e.entity_id = d.entity_id

	IF EXISTS 
		(	SELECT * 
			FROM sys.objects 
			WHERE [name] = N'au_entity' AND [type] = 'TR'
		)
	BEGIN
		ALTER TABLE [entity] ENABLE TRIGGER [au_entity];      
	END;

	SET @endTime = GETDATE();
	SET @StepMessage = 'Block 12 - Finish. Time Taken - ' + CONVERT(VARCHAR(20), @endTime - @startTime, 114);
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;
	
	/*************************** Dealer Branch ************************/

	SET @StepMessage = 'Block 13 - Start';
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;
	SET @startTime = GETDATE();

	IF EXISTS 
		(	SELECT * 
			FROM sys.objects 
			WHERE [name] = N'au_entity' AND [type] = 'TR'
		)
	BEGIN
		ALTER TABLE [entity] DISABLE TRIGGER [au_entity];      
	END;

	Update e
	set 	given_names = NULL,
			email_address = 'scrambled@noemail.nodomain',
			title_id      =  (case when e.gender='M' then 1 when e.gender='F' Then 2 else NULL end),
			mobile_number = '07000000000',
			home_phone_number = '01000 000 000',
			work_phone_number = '01000 111 111',
			fax_number = NULL,
			salutation    = 'Dear Sir / Madam',
			contact_name = NULL,
			mailing_title = NULL,
			company_rego_no = NULL,
			tax_file_number = (case when e.entity_type ='I' then 'AB000000B' else '000000000' end),
			name = SUBSTRING(de.name, 1, CHARINDEX(' ', de.name, 1) - 1) + ' '
					   + ' (' + convert(varchar,db.dealer_id) + '/' + convert(varchar,db.dealer_branch_id) + ')'
         			   + ' ' + case sf.short_code when 'AOR' then 'AOR' when 'AEGON' then 'ARC' when 'ARR' then 'RR' end
					   + (case when e.name like '% DM' then ' DM' else '' end)  -- add discretionary suffix				   
	from dealer_branch db
			join  entity e on e.entity_id = db.entity_id
			join dealer d on db.dealer_id = d.dealer_id
			join entity de on de.entity_id = d.entity_id
			join subfund_associated_party sap on sap.party_id = db.dealer_branch_id
			join subfund sf on sf.subfund_id = sap.subfund_id
	where 
			db.dealer_branch_id  not in (
			1, --          Blueplanet Direct Branch
			2, --          AEGON At Retirment
			3, --          Test branch
			4, --          AEGON Retirement Choices Sales
			5, --          AEGON Workplace
			526, --      AEGON One Retirement
			2895, --    AEGON Retiready 0%
			2896, --    AEGON Retiready 6%
			2897, --    AEGON Retiready 12%
			2898, --    AEGON Retiready 19%
			2899, --    AEGON Retiready 25%
			2900, --    AEGON Retiready 31%
			2901, --    AEGON Retiready 37%
			2902, --    AEGON Retiready 44%
			2903  --  AEGON Retiready 50%
			)
	and 	d.dealer_id <> 1805

	update 	entity
	set 	preferred_name = substring(name,1,40)
	from 	entity e
			join dealer_branch db on e.entity_id = db.entity_id

	IF EXISTS 
		(	SELECT * 
			FROM sys.objects 
			WHERE [name] = N'au_entity' AND [type] = 'TR'
		)
	BEGIN
		ALTER TABLE [entity] ENABLE TRIGGER [au_entity];      
	END;

	SET @endTime = GETDATE();
	SET @StepMessage = 'Block 13 - Finish. Time Taken - ' + CONVERT(VARCHAR(20), @endTime - @startTime, 114);
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;

	/*************************** Investment Manager ************************/

	SET @StepMessage = 'Block 14 - Start';
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;
	SET @startTime = GETDATE();

	IF EXISTS 
		(	SELECT * 
			FROM sys.objects 
			WHERE [name] = N'au_entity' AND [type] = 'TR'
		)
	BEGIN
		ALTER TABLE [entity] DISABLE TRIGGER [au_entity];      
	END;

	Update 	entity
	set  	fax_number = '+44 1632 555 555'
	from 	investment_manager db
			join entity e on e.entity_id = db.entity_id

	IF EXISTS 
		(	SELECT * 
			FROM sys.objects 
			WHERE [name] = N'au_entity' AND [type] = 'TR'
		)
	BEGIN
		ALTER TABLE [entity] ENABLE TRIGGER [au_entity];      
	END;

	SET @endTime = GETDATE();
		SET @StepMessage = 'Block 14 - Finish. Time Taken - ' + CONVERT(VARCHAR(20), @endTime - @startTime, 114);
		RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;
	
	/*************************** Employer **************************/
	
	SET @StepMessage = 'Block 15 - Start';
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;
	SET @startTime = GETDATE();

	IF EXISTS 
		(	SELECT * 
			FROM sys.objects 
			WHERE [name] = N'au_entity' AND [type] = 'TR'
		)
	BEGIN
		ALTER TABLE [entity] DISABLE TRIGGER [au_entity];      
	END;

	Update entity
	set 	given_names = NULL,
			email_address = 'scrambled@noemail.nodomain',
			title_id      =  (case when gender='M' then 1 when gender='F' Then 2 else NULL end),
			mobile_number = '07000000000',
			home_phone_number = '01000 000 000',
			work_phone_number = '01000 111 111',
			fax_number = NULL,
			salutation    = 'Dear Sir / Madam',
			contact_name = NULL,
			mailing_title = NULL,
			company_rego_no = NULL,
			tax_file_number = (case when entity_type ='I' then 'AA000000D' else '000000000' end),
			name = 
			case e.entity_id % 50 
			when	0	then 	'Brown Beautv'
			when	1	then 	'Burbank'
			when	2	then 	'Calrose'
			when	3	then 	'Charles Downing'
			when	4	then 	'Chippewa'
			when	5	then 	'Dakota Red'
			when	6	then 	'Earlaine'
			when	7	then 	'Early Ohio'
			when	8	then 	'Early Rose'
			when	9	then 	'Erie'
			when	10	then 	'Garnet Chili'
			when	11	then 	'Green Mountain'
			when	12	then 	'Harmon'
			when	13	then 	'Houma I'
			when	14	then 	'Irish Cobbler'
			when	15	then 	'Kasota'
			when	16	then 	'Katahdin'
			when	17	then 	'Kennebec'
			when	18	then 	'Menominee'
			when	19	then 	'Mesaba'
			when	20	then 	'Mohawk'
			when	21	then 	'Norkota'
			when	22	then 	'Ontario'
			when	23	then 	'Pawnee'
			when	24	then 	'Pontiac'
			when	25	then 	'President'
			when	26	then 	'Red McClure'
			when	27	then 	'Red Warba'
			when	28	then 	'Rural New Yorker'
			when	29	then 	'Russet Burbank '
			when	30	then 	'Russet Rural'
			when	31	then 	'Sebago'
			when	32	then 	'Sequoia'
			when	33	then 	'Spaulding Rose'
			when	34	then 	'Teton'
			when	35	then 	'Triumphant Inc'
			when	36	then 	'Warba'
			when	37	then 	'Blue Christy'
			when	38	then 	'Columbia'
			when	39	then 	'Earlaine'
			when	40	then 	'Early Blue '
			when	41	then 	'Earlv Michigan'
			when	42	then 	'Golden'
			when	43	then 	'Cobbler'
			when	44	then 	'Mahr'
			when	45	then 	'McCormick'
			when	46	then 	'Nittany'
			when	47	then 	'Pennigan'
			when	48	then 	'Strawberrv'
			when	49	then 	'White Gold' end + ' ' + UPPER(substring(convert(varchar(50), newid()),1,1)) +
			case e.entity_id % 3 when 0 then ' Corp' else case e.entity_id % 4 when 0 then ' Ltd' else '' end end
	from 	entity e
			join employer emp on e.entity_id = emp.entity_id

	update 	entity
			set preferred_name = substring(name,1,40)
	from 	entity e
			join employer emp on e.entity_id = emp.entity_id

	IF EXISTS 
		(	SELECT * 
			FROM sys.objects 
			WHERE [name] = N'au_entity' AND [type] = 'TR'
		)
	BEGIN
		ALTER TABLE [entity] ENABLE TRIGGER [au_entity];      
	END;

	SET @endTime = GETDATE();
	SET @StepMessage = 'Block 15 - Finish. Time Taken - ' + CONVERT(VARCHAR(20), @endTime - @startTime, 114);
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;

	/*************************** Related Entities **************************/

	SET @StepMessage = 'Block 16 - Start';
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;
	SET @startTime = GETDATE();

	IF EXISTS 
		(	SELECT * 
			FROM sys.objects 
			WHERE [name] = N'au_entity' AND [type] = 'TR'
		)
	BEGIN
		ALTER TABLE [entity] DISABLE TRIGGER [au_entity];      
	END;

	Update entity
	set  	given_names = 
			case e.entity_id % 10
			when	0	then 	'Chris'
			when	1	then 	'Pete'
			when	2	then 	'Bo'
			when	3	then 	'Terry'
			when	4	then 	'Lesley'
			when	5	then 	'Jess'
			when	6	then 	'Hillary'
			when	7	then 	'Alex'
			when	8	then 	'Petra'
			when	9	then 	'Bri'
			end,
			email_address = 'scrambled@noemail.nodomain',
			title_id      =  (case when gender='M' then 1 when gender='F' Then 2 else NULL end),
			mobile_number = '07000000000',
			home_phone_number = '01000 000 000',
			work_phone_number = '01000 111 111',
			fax_number = NULL,
			salutation    = 'Dear Sir / Madam',
			contact_name = NULL,
			mailing_title = NULL,
			company_rego_no = NULL,
			tax_file_number = (case when entity_type ='I' then 'AA000000D' else '000000000' end),
			name   = 
			case e.entity_id % 50 
			when	0	then 	'Agate'
			when	1	then 	'Alexandrite'
			when	2	then 	'Andalusite'
			when	3	then 	'Axinite'
			when	4	then 	'Benitoite'
			when	5	then 	'Bixbite'
			when	6	then 	'Cassiterite'
			when	7	then 	'Chrysocolla'
			when	8	then 	'Chrysoprase'
			when	9	then 	'Clinohumite'
			when	10	then 	'Cordierite'
			when	11	then 	'Danburite'
			when	12	then 	'Diamond'
			when	13	then 	'Diopside'
			when	14	then 	'Dioptase'
			when	15	then 	'Dumortierite'
			when	16	then 	'Emerald'
			when	17	then 	'Feldspar'
			when	18	then 	'Amazonite'
			when	19	then 	'Labradorite'
			when	20	then 	'Moonstone'
			when	21	then 	'Sunstone'
			when	22	then 	'Garnet'
			when	23	then 	'Hessonite'
			when	24	then 	'Hambergite'
			when	25	then 	'Hematite'
			when	26	then 	'Jade'
			when	27	then 	'Jasper'
			when	28	then 	'Kornerupine'
			when	29	then 	'Kunzite'
			when	30	then 	'Lapis lazuli'
			when	31	then 	'Malachite'
			when	32	then 	'Opal'
			when	33	then 	'Peridot'
			when	34	then 	'Prehnite'
			when	35	then 	'Pyrite'
			when	36	then 	'Quartz'
			when	37	then 	'Agate'
			when	38	then 	'Amethyst'
			when	39	then 	'Aventurine'
			when	40	then 	'Citrine'
			when	41	then 	'Chalcedony'
			when	42	then 	'Onyx'
			when	43	then 	'Rhodochrosite'
			when	44	then 	'Ruby'
			when	45	then 	'Sapphire'
			when	46	then 	'Spinel'
			when	47	then 	'Sugilite'
			when	48	then 	'Tanzanite'
			when	49	then 	'Topaz'
	end
	from 	entity e
			join related_to rto on e.entity_id = rto.related_entity_id

	IF EXISTS 
		(	SELECT * 
			FROM sys.objects 
			WHERE [name] = N'au_entity' AND [type] = 'TR'
		)
	BEGIN
		ALTER TABLE [entity] ENABLE TRIGGER [au_entity];      
	END;
	
	SET @endTime = GETDATE();
	SET @StepMessage = 'Block 16 - Finish. Time Taken - ' + CONVERT(VARCHAR(20), @endTime - @startTime, 114);
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;

	/*************************** Now change the related entity name **************************/

	SET @StepMessage = 'Block 17 - Start';
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;
	SET @startTime = GETDATE();
	
	IF EXISTS 
		(	SELECT * 
			FROM sys.objects 
			WHERE [name] = N'au_related_to' AND [type] = 'TR'
		)
	BEGIN
		ALTER TABLE [related_to] DISABLE TRIGGER [au_related_to];      
	END;

	update 	related_to
			set name = substring(e.given_names,1,8) + ' ' + substring(e.name,1,35),
				notation =	( CASE
								WHEN rto.notation IS NULL THEN NULL
								ELSE 'scramble'
							END )
	from 	related_to rto
			join entity e on e.entity_id = rto.related_entity_id

	IF EXISTS 
		(	SELECT * 
			FROM sys.objects 
			WHERE [name] = N'au_related_to' AND [type] = 'TR'
		)
	BEGIN
		ALTER TABLE [related_to] ENABLE TRIGGER [au_related_to];      
	END;
	
	SET @endTime = GETDATE();
	SET @StepMessage = 'Block 17 - Finish. Time Taken - ' + CONVERT(VARCHAR(20), @endTime - @startTime, 114);
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;

	/*************************** Update related entity preferred name **************************/

	SET @StepMessage = 'Block 18 - Start';
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;
	SET @startTime = GETDATE();

	IF EXISTS 
		(	SELECT * 
			FROM sys.objects 
			WHERE [name] = N'au_entity' AND [type] = 'TR'
		)
	BEGIN
		ALTER TABLE [entity] DISABLE TRIGGER [au_entity];      
	END;

	update 	entity
	set 	preferred_name = substring(rto.name,1,40)
	from 	entity e
			join related_to rto on e.entity_id = rto.related_entity_id

	IF EXISTS 
		(	SELECT * 
			FROM sys.objects 
			WHERE [name] = N'au_entity' AND [type] = 'TR'
		)
	BEGIN
		ALTER TABLE [entity] ENABLE TRIGGER [au_entity];      
	END;

	SET @endTime = GETDATE();
	SET @StepMessage = 'Block 18 - Finish. Time Taken - ' + CONVERT(VARCHAR(20), @endTime - @startTime, 114);
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;

	/*************************************** Entity - update the remaining fields ******************************************/
	
	SET @StepMessage = 'Block 19 - Start';
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;
	SET @startTime = GETDATE();

	IF EXISTS 
		(	SELECT * 
			FROM sys.objects 
			WHERE [name] = N'au_entity' AND [type] = 'TR'
		)
	BEGIN
		ALTER TABLE [entity] DISABLE TRIGGER [au_entity];      
	END;

	UPDATE
		entity
	SET
		short_name = CASE 
						WHEN short_name IS NULL THEN NULL
                        ELSE 'ABC'
                    END
		, birth_date =	CASE
							WHEN birth_date IS NOT NULL
								AND
									( DATEDIFF(YEAR, birth_date, GETDATE()) > 18
									OR DATEDIFF(YEAR, birth_date, GETDATE()) < 1
									)
								THEN DATEADD([dd], CAST(RAND(DATEDIFF(DAY, 0, birth_date)) * 100 + 10 AS INT), birth_date)	/* add random days from 10 to 110 */
							WHEN birth_date IS NOT NULL
								AND
									( DATEDIFF(YEAR, birth_date, GETDATE()) <= 18
									OR DATEDIFF(YEAR, birth_date, GETDATE()) >= 1
									)
								THEN DATEADD([dd], CAST(RAND(DATEDIFF(DAY, 0, birth_date)) * -100 - 10 AS INT), birth_date)	/* subract random days from 10 to 110 */
							ELSE NULL
						END
		, deceased_date = CASE
							WHEN deceased_date IS NULL 
								THEN NULL
							ELSE DATEADD([dd], CAST(RAND(DATEDIFF(DAY, 0, deceased_date)) * 100 + 10 AS INT), deceased_date)
						END
		, retirement_date = CASE
								WHEN retirement_date IS NULL 
									THEN NULL
								ELSE DATEADD([dd], CAST(RAND(DATEDIFF(DAY, 0, retirement_date)) * 100 + 10 AS INT), retirement_date)
							END
		, birthplace = CASE 
							WHEN birthplace IS NULL 
								THEN NULL
							ELSE
								CASE entity_id % 6
								  WHEN 0
									THEN 'Suburbian'
								  WHEN 1
									THEN 'Localshire'
								  WHEN 2
									THEN 'Otherside'
								  WHEN 3
									THEN 'Outston'
								  WHEN 4
									THEN 'Greyville'
								  WHEN 5
									THEN 'Hillend'
								END
						END
		, title_text =	CASE
							WHEN title_text IS NULL
								THEN NULL
							WHEN title_text = ''
								THEN ''
							ELSE
								CASE [entity_id] % 4
								  WHEN 0
									THEN 'Mr'
								  WHEN 1
									THEN 'Mrs'
								  WHEN 2
									THEN 'Mr & Mrs'
								  WHEN 3
									THEN 'Mr & Miss'
								END
						END
		, giin = CASE
                      WHEN [giin] IS NOT NULL
                        THEN 'Removed'
                    END;

	IF EXISTS 
		(	SELECT * 
			FROM sys.objects 
			WHERE [name] = N'au_entity' AND [type] = 'TR'
		)
	BEGIN
		ALTER TABLE [entity] ENABLE TRIGGER [au_entity];      
	END;

	SET @endTime = GETDATE();
	SET @StepMessage = 'Block 19 - Finish. Time Taken - ' + CONVERT(VARCHAR(20), @endTime - @startTime, 114);
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;

	/* ******************************* Some safety catch alls ******************************** */
	
	SET @StepMessage = 'Block 20 - Start';
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;
	SET @startTime = GETDATE();

	IF EXISTS 
		(	SELECT * 
			FROM sys.objects 
			WHERE [name] = N'au_entity' AND [type] = 'TR'
		)
	BEGIN
		ALTER TABLE [entity] DISABLE TRIGGER [au_entity];      
	END;

	-- all 'other' email addresses have to change
	UPDATE 	entity
	SET 	email_address = 'scrambled@noemail.nodomain'
	WHERE 	email_address <> 'scrambled@noemail.nodomain'
	AND 	email_address IS NOT NULL

	IF EXISTS 
		(	SELECT * 
			FROM sys.objects 
			WHERE [name] = N'au_entity' AND [type] = 'TR'
		)
	BEGIN
		ALTER TABLE [entity] ENABLE TRIGGER [au_entity];      
	END;

	SET @endTime = GETDATE();
	SET @StepMessage = 'Block 20 - Finish. Time Taken - ' + CONVERT(VARCHAR(20), @endTime - @startTime, 114);
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;

	/*************************************** Bank Account / Sort Code ******************************************/

    SET @StepMessage = 'Block 21 - Start';
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;
	SET @startTime = GETDATE();

	IF EXISTS 
		(	SELECT * 
			FROM sys.objects 
			WHERE [name] = N'au_bank_account' AND [type] = 'TR'
		)
	BEGIN
		ALTER TABLE [bank_account] DISABLE TRIGGER [au_bank_account];      
	END;

	SELECT 
       @iStart = min(bank_account_id),
       @iCntr = max(bank_account_id)
	FROM 
       bank_account

	SELECT 
		@iIncr = 20000,
		@iEnd = @iStart + @iIncr;
	
	WHILE @iStart <= @iCntr
	BEGIN
		UPDATE 
			[bank_account]
		SET 
			[account_number] = RIGHT('99999999' + CONVERT(VARCHAR, [bacc].[bank_account_id] % 234011), 8)
			, [name] = ( CASE 
							WHEN e.entity_id IS NULL THEN 'Data Obfuscated'
							ELSE LEFT(ISNULL([e].[given_names] + ' ', '') + [e].[name], 40)
						END )
			, [customer_reference] = ( CASE
										WHEN [bacc].[customer_reference] IS NULL THEN NULL
										ELSE 'scrambled ref'
									END )
			, [bic_code] = ( CASE
								WHEN bic_code IS NULL THEN NULL
								ELSE 'MIDLGB00' /*Dummy bic_code*/
							END )
			, [intnl_account_number] = ( CASE
											WHEN intnl_account_number IS NULL THEN NULL
											ELSE 'GB00MIDL00000000000000' /*Dummy intnl a/c no*/
										END )
		FROM 
			[bank_account] [bacc]
			LEFT JOIN [entity_bank_account] [entbacc] ON [entbacc].[bank_account_id] = [bacc].[bank_account_id]
			LEFT JOIN [entity] [e] ON [entbacc].[entity_id] = [e].[entity_id]
			WHERE 	[bacc].bank_account_id between @iStart and @iEnd
 
	  SELECT @iStart = @iEnd +1,
			 @iEnd = @iEnd + @iIncr
	END

	IF EXISTS 
		(	SELECT * 
			FROM sys.objects 
			WHERE [name] = N'au_bank_account' AND [type] = 'TR'
		)
	BEGIN
		ALTER TABLE [bank_account] ENABLE TRIGGER [au_bank_account];      
	END;

	SET @endTime = GETDATE();
	SET @StepMessage = 'Block 21 - Finish. Time Taken - ' + CONVERT(VARCHAR(20), @endTime - @startTime, 114);
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;

	/*************************************** Correspondence ******************************************/

	SET @StepMessage = 'Block 22 - Start';
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;
	SET @startTime = GETDATE();

	IF EXISTS 
		(	SELECT * 
			FROM sys.objects 
			WHERE [name] = N'au_correspondence' AND [type] = 'TR'
		)
	BEGIN
		ALTER TABLE [correspondence] DISABLE TRIGGER [au_correspondence];      
	END;

	SELECT 
       @iStart = min(correspondence_id),
       @iCntr = max(correspondence_id)
	FROM 
       correspondence

	SELECT 
		@iIncr = 1000000,
		@iEnd = @iStart + @iIncr;
	
	WHILE @iStart <= @iCntr
	BEGIN
	  update 	correspondence
	  set 		given_names = substring(e.given_names,1,40),
				name = substring(e.name,1,40)
	  From 		correspondence corr
				join entity e on e.entity_id = corr.entity_id
				join member_account ma on ma.entity_id = e.entity_id
	  where 	corr.correspondence_id between @iStart and @iEnd

	  SELECT @iStart = @iEnd +1,
			 @iEnd = @iEnd + @iIncr
	END

	IF EXISTS 
		(	SELECT * 
			FROM sys.objects 
			WHERE [name] = N'au_correspondence' AND [type] = 'TR'
		)
	BEGIN
		ALTER TABLE [correspondence] ENABLE TRIGGER [au_correspondence];      
	END;

	SET @endTime = GETDATE();
	SET @StepMessage = 'Block 22 - Finish. Time Taken - ' + CONVERT(VARCHAR(20), @endTime - @startTime, 114);
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;

	/*************************** Remove all the old Audit Data *****************************/

	SET @StepMessage = 'Block 23 - Start';
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;
	SET @startTime = GETDATE();

	SELECT 
       @iStart = min(audit_transaction_id),
       @iCntr = max(audit_transaction_id)
	FROM 
       audit_transaction

	SELECT 
		@iIncr = 1000000,
		@iEnd = @iStart + @iIncr;
	
	WHILE @iStart <= @iCntr
	BEGIN
		/*
		This does a select change of many audit_types - but as more audit_types  are added, this script 
		would need to be added to - but sure as eggs it would get missed.
		*/
		update 	au
		set 	old_value = (case when len(ltrim(old_value)) > 1 then 'Old value for party id ' + convert(varchar,party_id) else old_value end),
				new_value = (case when len(ltrim(new_value)) > 1 then 'New value for Party id ' + convert(varchar,party_id) else new_value end)
		FROM    audit_transaction au
		where 	audit_type_id in (6,7,9,10,11,13,18,21,38,39,200,100,101,102,201,202,3902,1504,1507,1501, 1500,3900,1400,1401,3307, 3308,3309)
		AND  	au.audit_transaction_id between @iStart and @iEnd

		SELECT @iStart = @iEnd +1,
			@iEnd = @iEnd + @iIncr
	END  
 
	SET @endTime = GETDATE();
	SET @StepMessage = 'Block 23 - Finish. Time Taken - ' + CONVERT(VARCHAR(20), @endTime - @startTime, 114);
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;

	/************************************ Adaptor Gateway (inbound CBIS XML requests) **************************************/
	SET @StepMessage = 'Block 24 - Start';
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;
	SET @startTime = GETDATE();

	IF EXISTS 
		(	 
			SELECT 
				1 
			FROM 
				sys.indexes 
			WHERE 
				name='idx_agm_msgid' 
				AND object_id = OBJECT_ID('[dbo].[adaptor_gateway_message]')
		)
	BEGIN
		DROP INDEX idx_agm_msgid ON [dbo].[adaptor_gateway_message]
	END

	CREATE NONCLUSTERED INDEX [idx_agm_msgid] ON [dbo].[adaptor_gateway_message] ([adaptor_gateway_message_id])
		INCLUDE ([web_user_id],[web_user_name])   
	
	SELECT 
       @iStart = min(adaptor_gateway_message_id),
       @iCntr = max(adaptor_gateway_message_id)
	FROM 
       adaptor_gateway_message

	SELECT 
		@iIncr = 1000000,
		@iEnd = @iStart + @iIncr;
	
	WHILE @iStart <= @iCntr
	BEGIN
		UPDATE
			agm
		SET
			xml_message = ''
			,  web_user_name = CASE 
								WHEN web_user_name IS NULL  THEN NULL
								ELSE
									CASE
										WHEN e.entity_id is null THEN 'Scrambled@noemail.nodomain'
									ELSE e.email_address
									END						END
		FROM 
			adaptor_gateway_message(nolock)  agm 
			LEFT JOIN entity(nolock)  e on e.entity_id = agm.web_user_id
		WHERE
			agm.adaptor_gateway_message_id between @iStart and @iEnd

	  SELECT @iStart = @iEnd +1,
			 @iEnd = @iEnd + @iIncr
	END

	IF EXISTS 
		(	 
			SELECT * 
			FROM sys.indexes 
			WHERE name='idx_agm_msgid' AND object_id = OBJECT_ID('[dbo].[adaptor_gateway_message]')
		)
	BEGIN
	      DROP INDEX idx_agm_msgid ON [dbo].[adaptor_gateway_message]
	END

	SET @endTime = GETDATE();
	SET @StepMessage = 'Block 24 - Finish. Time Taken - ' + CONVERT(VARCHAR(20), @endTime - @startTime, 114);
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;

	/*************************************** addacs_file_entry ******************************************/
	
	SET @StepMessage = 'Block 25 - Start';
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;
	SET @startTime = GETDATE();

	UPDATE
		afe
	SET
		afe.existing_bank_account_name = CASE 
											WHEN e.entity_id IS NULL THEN 'Data Obfuscated'
											ELSE LEFT(ISNULL(e.given_names + ' ', '') + e.name, 40)
										END
		, afe.existing_bank_account_number = CASE 
												WHEN ba.account_number IS NULL THEN RIGHT('99999999' + CONVERT(VARCHAR, afe.addacs_file_entry_id % 234011), 8)
												ELSE ba.account_number 
											END
		, afe.existing_sort_code = RIGHT('99999999' + CONVERT(VARCHAR, afe.addacs_file_entry_id % 234011), 6)
		, afe.new_bank_account_name = CASE
										WHEN new_bank_account_name IS NULL THEN NULL
										ELSE 
											CASE 
												WHEN e.entity_id IS NULL THEN 'Data Obfuscated'
												ELSE LEFT('New' + ' ' + ISNULL(e.given_names, '') + ' ' + e.name, 40)
											END
										END
		, afe.new_bank_account_number = CASE
											WHEN new_bank_account_number IS NULL THEN NULL
											ELSE 
												CASE 
													WHEN ba.account_number IS NULL THEN RIGHT('88888888' + CONVERT(VARCHAR, afe.addacs_file_entry_id % 234011), 8)
													ELSE ba.account_number 
												END
											END
		, afe.new_sort_code = CASE 
								WHEN new_sort_code IS NULL THEN NULL
								ELSE RIGHT('88888888' + CONVERT(VARCHAR, afe.addacs_file_entry_id % 234011), 6)
							END
		, afe.reference = CASE 
							WHEN e.name IS NULL THEN 'Data Obfuscated'
							ELSE STUFF(reference, CHARINDEX(' ', reference) + 1, LEN(e.name), LEFT(UPPER(e.name), 20 - CHARINDEX(' ', reference)))
						END
	FROM 
		addacs_file_entry afe
		LEFT JOIN pdc p ON p.pdc_id = afe.pdc_id
		LEFT JOIN pdc_member_account pma ON pma.pdc_id = p.pdc_id
		LEFT JOIN member_account ma on ma.member_account_id = pma.member_account_id
		LEFT JOIN entity e ON e.entity_id = ma.entity_id
		LEFT JOIN entity_bank_account eba on eba.entity_id = e.entity_id
		LEFT JOIN bank_account ba on ba.bank_account_id = eba.bank_account_id

	SET @endTime = GETDATE();
	SET @StepMessage = 'Block 25 - Finish. Time Taken - ' + CONVERT(VARCHAR(20), @endTime - @startTime, 114);
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;

	/*************************************** arudd_file_entry ******************************************/

	SET @StepMessage = 'Block 26 - Start';
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;
	SET @startTime = GETDATE();

	UPDATE
		afe
	SET
		afe.direct_debit_account_number = CASE 
											WHEN ba.account_number IS NULL THEN RIGHT('99999999' + CONVERT(VARCHAR, afe.arudd_file_entry_id % 234011), 8)
											ELSE ba.account_number 
										 END
	   , afe.direct_debit_sort_code = RIGHT('99999999' + CONVERT(VARCHAR, afe.arudd_file_entry_id % 234011), 6)
	   , afe.direct_debit_account_name = CASE 
											WHEN e.entity_id IS NULL THEN 'Data Obfuscated'
											ELSE LEFT(ISNULL(e.given_names + ' ', '') + e.name, 40)
										END
	   , afe.direct_debit_reference =  CASE
										WHEN e.name IS NULL THEN 'Data Obfuscated'
										ELSE STUFF(direct_debit_reference, CHARINDEX(' ', direct_debit_reference) + 1, LEN(e.name), LEFT(UPPER(e.name), 20 - CHARINDEX(' ', direct_debit_reference)))
									END
	  , afe.originator_account_number = RIGHT('99999999' + CONVERT(VARCHAR, afe.originator_account_number % 234012), 8)
	  , afe.originator_account_sortcode = RIGHT('99999999' + CONVERT(VARCHAR, afe.originator_account_sortcode % 234012), 6)
	FROM 
		arudd_file_entry afe
		LEFT JOIN member_account ma on ma.member_account_id = afe.member_account_id
		LEFT JOIN entity e on e.entity_id = ma.entity_id
		LEFT JOIN entity_bank_account eba on eba.entity_id = e.entity_id
		LEFT JOIN bank_account ba on ba.bank_account_id = eba.bank_account_id
	
	SET @endTime = GETDATE();
	SET @StepMessage = 'Block 26 - Finish. Time Taken - ' + CONVERT(VARCHAR(20), @endTime - @startTime, 114);
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;
 
	/*************************************** Direct Debits ******************************************/

    SET @StepMessage = 'Block 27 - Start';
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;
	SET @startTime = GETDATE();

    UPDATE
		auddis_submit_detail
	SET
		destination_account_number = RIGHT('99999999' + CONVERT(VARCHAR, auddis_submit_detail_id % 234011), 8)
	  , destination_sort_code = RIGHT('99999999' + CONVERT(VARCHAR, auddis_submit_detail_id % 234011), 6)
	  , users_reference = 'Data Obfuscated'
	  , destination_account_name = 'Data Obfuscated'
	  , originating_account_number = RIGHT('99999999' + CONVERT(VARCHAR, originating_account_number % 234012), 8)
	  , originating_sort_code = RIGHT('99999999' + CONVERT(VARCHAR, originating_sort_code % 234012), 6)

    SET @endTime = GETDATE();
	SET @StepMessage = 'Block 27 - Finish. Time Taken - ' + CONVERT(VARCHAR(20), @endTime - @startTime, 114);
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;

	/*************************************** Bank_reconciliation ******************************************/

	SET @StepMessage = 'Block 28 - Start';
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;
	SET @startTime = GETDATE();

	IF EXISTS 
		(	SELECT * 
			FROM sys.objects 
			WHERE [name] = N'au_bank_reconciliation' AND [type] = 'TR'
		)
	BEGIN
		ALTER TABLE [bank_reconciliation] DISABLE TRIGGER [au_bank_reconciliation];      
	END;

	UPDATE 
		bank_reconciliation
	SET 
		notation = 'Data Obfuscated'
	WHERE 
		notation IS NOT NULL
 
	IF EXISTS 
		(	SELECT * 
			FROM sys.objects 
			WHERE [name] = N'au_bank_reconciliation' AND [type] = 'TR'
		)
	BEGIN
		ALTER TABLE [bank_reconciliation] ENABLE TRIGGER [au_bank_reconciliation];      
	END;

	SET @endTime = GETDATE();
	SET @StepMessage = 'Block 28 - Finish. Time Taken - ' + CONVERT(VARCHAR(20), @endTime - @startTime, 114);
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;

	/*************************************** Bank_statement ******************************************/

	SET @StepMessage = 'Block 29 - Start';
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;
	SET @startTime = GETDATE();

	SELECT 
       @iStart = min(bank_statement_id),
       @iCntr = max(bank_statement_id)
	FROM 
       bank_statement

	SELECT 
		@iIncr = 100000,
		@iEnd = @iStart + @iIncr;
	
	WHILE @iStart <= @iCntr
	BEGIN
	   UPDATE 
			bs
		SET 
			reference_number = CASE
								WHEN reference_number IS NULL THEN NULL
								ELSE 'Data Obfuscated'
							END
		  , additional_reference = CASE
										WHEN additional_reference IS NULL THEN NULL
										ELSE 'Data Obfuscated'
									END
		FROM
			bank_statement bs
		WHERE 	
			bs.bank_statement_id between @iStart and @iEnd
			
	  SELECT @iStart = @iEnd +1,
			 @iEnd = @iEnd + @iIncr
	END

	SET @endTime = GETDATE();
	SET @StepMessage = 'Block 29 - Finish. Time Taken - ' + CONVERT(VARCHAR(20), @endTime - @startTime, 114);
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;

	/*************************************** Bank_transfer_request ******************************************/

	SET @StepMessage = 'Block 30 - Start';
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;
	SET @startTime = GETDATE();

	UPDATE 
		bank_transfer_request
	SET 
		notation = 'Data Obfuscated'
	WHERE 
		notation IS NOT NULL

	SET @endTime = GETDATE();
	SET @StepMessage = 'Block 30 - Finish. Time Taken - ' + CONVERT(VARCHAR(20), @endTime - @startTime, 114);
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;

	/*************************************** Benefit_calculation ******************************************/

	SET @StepMessage = 'Block 31 - Start';
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;
	SET @startTime = GETDATE();

	UPDATE
		benefit_calculation
	SET
		enhanced_hmrc_cert_no = (CASE WHEN enhanced_hmrc_cert_no IS NULL THEN NULL
									  ELSE SUBSTRING(CONVERT(VARCHAR(100), NEWID()), 0, CHARINDEX('-', CONVERT(VARCHAR(100), NEWID()))) + CHAR(CAST((90 - 65) * RAND() + 65 AS INTEGER))
								 END)
	  , primary_hmrc_cert_no = (CASE WHEN primary_hmrc_cert_no IS NULL THEN NULL
									 ELSE SUBSTRING(CONVERT(VARCHAR(100), NEWID()), 0, CHARINDEX('-', CONVERT(VARCHAR(100), NEWID()))) + CHAR(CAST((90 - 65) * RAND() + 65 AS INTEGER))
								END)
	  , pension_hmrc_cert_no = (CASE WHEN pension_hmrc_cert_no IS NULL THEN NULL
									 ELSE SUBSTRING(CONVERT(VARCHAR(100), NEWID()), 0, CHARINDEX('-', CONVERT(VARCHAR(100), NEWID()))) + CHAR(CAST((90 - 65) * RAND() + 65 AS INTEGER))
								END)
	  , overseas_hmrc_cert_no = (CASE WHEN overseas_hmrc_cert_no IS NULL THEN NULL
									  ELSE SUBSTRING(CONVERT(VARCHAR(100), NEWID()), 0, CHARINDEX('-', CONVERT(VARCHAR(100), NEWID()))) + CHAR(CAST((90 - 65) * RAND() + 65 AS INTEGER))
								 END)
	  , non_resident_hmrc_cert_no = (CASE WHEN non_resident_hmrc_cert_no IS NULL THEN NULL
										  ELSE SUBSTRING(CONVERT(VARCHAR(100), NEWID()), 0, CHARINDEX('-', CONVERT(VARCHAR(100), NEWID()))) + CHAR(CAST((90 - 65) * RAND() + 65 AS INTEGER))
									 END)
	  , fixed_hmrc_cert_no = (CASE WHEN fixed_hmrc_cert_no IS NULL THEN NULL
								   ELSE SUBSTRING(CONVERT(VARCHAR(100), NEWID()), 0, CHARINDEX('-', CONVERT(VARCHAR(100), NEWID()))) + CHAR(CAST((90 - 65) * RAND() + 65 AS INTEGER))
							  END)
	  , individual_hmrc_cert_no = (CASE WHEN individual_hmrc_cert_no IS NULL THEN NULL
										ELSE SUBSTRING(CONVERT(VARCHAR(100), NEWID()), 0, CHARINDEX('-', CONVERT(VARCHAR(100), NEWID()))) + CHAR(CAST((90 - 65) * RAND() + 65 AS INTEGER))
								   END)
	  , court_order_reference = (CASE WHEN court_order_reference IS NULL THEN NULL
										ELSE SUBSTRING(CONVERT(VARCHAR(100), NEWID()), 0, CHARINDEX('-', CONVERT(VARCHAR(100), NEWID()))) + CHAR(CAST((90 - 65) * RAND() + 65 AS INTEGER))
								   END)

	SET @endTime = GETDATE();
	SET @StepMessage = 'Block 31 - Finish. Time Taken - ' + CONVERT(VARCHAR(20), @endTime - @startTime, 114);
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;

	/*************************************** Benefit_crystallisation_event ******************************************/

	SET @StepMessage = 'Block 32 - Start';
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;
	SET @startTime = GETDATE();

	IF EXISTS 
		(	SELECT * 
			FROM sys.objects 
			WHERE [name] = N'au_benefit_crystalisationevent' AND [type] = 'TR'
		)
	BEGIN
		ALTER TABLE [benefit_crystallisation_event] DISABLE TRIGGER [au_benefit_crystalisationevent];      
	END;

	UPDATE
		benefit_crystallisation_event
	SET
		enhanced_hmrc_cert_no = (CASE WHEN enhanced_hmrc_cert_no IS NULL THEN NULL
									  ELSE SUBSTRING(CONVERT(VARCHAR(100), NEWID()), 0, CHARINDEX('-', CONVERT(VARCHAR(100), NEWID()))) + CHAR(CAST((90 - 65) * RAND() + 65 AS INTEGER))
								 END)
	  , primary_hmrc_cert_no = (CASE WHEN primary_hmrc_cert_no IS NULL THEN NULL
									 ELSE SUBSTRING(CONVERT(VARCHAR(100), NEWID()), 0, CHARINDEX('-', CONVERT(VARCHAR(100), NEWID()))) + CHAR(CAST((90 - 65) * RAND() + 65 AS INTEGER))
								END)
	  , pension_hmrc_cert_no = (CASE WHEN pension_hmrc_cert_no IS NULL THEN NULL
									 ELSE SUBSTRING(CONVERT(VARCHAR(100), NEWID()), 0, CHARINDEX('-', CONVERT(VARCHAR(100), NEWID()))) + CHAR(CAST((90 - 65) * RAND() + 65 AS INTEGER))
								END)
	  , overseas_hmrc_cert_no = (CASE WHEN overseas_hmrc_cert_no IS NULL THEN NULL
									  ELSE SUBSTRING(CONVERT(VARCHAR(100), NEWID()), 0, CHARINDEX('-', CONVERT(VARCHAR(100), NEWID()))) + CHAR(CAST((90 - 65) * RAND() + 65 AS INTEGER))
								 END)
	  , non_resident_hmrc_cert_no = (CASE WHEN non_resident_hmrc_cert_no IS NULL THEN NULL
										  ELSE SUBSTRING(CONVERT(VARCHAR(100), NEWID()), 0, CHARINDEX('-', CONVERT(VARCHAR(100), NEWID()))) + CHAR(CAST((90 - 65) * RAND() + 65 AS INTEGER))
									 END)
	  , fixed_hmrc_cert_no = (CASE WHEN fixed_hmrc_cert_no IS NULL THEN NULL
								   ELSE SUBSTRING(CONVERT(VARCHAR(100), NEWID()), 0, CHARINDEX('-', CONVERT(VARCHAR(100), NEWID()))) + CHAR(CAST((90 - 65) * RAND() + 65 AS INTEGER))
							  END)
	  , individual_hmrc_cert_no = (CASE WHEN individual_hmrc_cert_no IS NULL THEN NULL
										ELSE SUBSTRING(CONVERT(VARCHAR(100), NEWID()), 0, CHARINDEX('-', CONVERT(VARCHAR(100), NEWID()))) + CHAR(CAST((90 - 65) * RAND() + 65 AS INTEGER))
								   END)
 
	IF EXISTS 
		(	SELECT * 
			FROM sys.objects 
			WHERE [name] = N'au_benefit_crystalisationevent' AND [type] = 'TR'
		)
	BEGIN
		ALTER TABLE [benefit_crystallisation_event] ENABLE TRIGGER [au_benefit_crystalisationevent];      
	END;

	SET @endTime = GETDATE();
	SET @StepMessage = 'Block 32 - Finish. Time Taken - ' + CONVERT(VARCHAR(20), @endTime - @startTime, 114);
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;

	/*************************************** Benefit_calculation_payment ******************************************/

	SET @StepMessage = 'Block 33 - Start';
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;
	SET @startTime = GETDATE();

	UPDATE
		bcp
	SET
		payee_name = CASE 
						 WHEN bcp.payee_name IS NULL THEN NULL
						 ELSE
							LEFT(CASE
									WHEN [e].[gender] = 'M' THEN 'Mr '
									WHEN [e].[gender] = 'F' THEN 'Mrs '
								END + ISNULL(e.given_names + ' ', '') + e.name, 60)
					END
	FROM benefit_calculation_payment bcp
	JOIN benefit_calculation bc ON bc.benefit_calculation_id = bcp.benefit_calculation_id
	JOIN member_account ma ON ma.member_account_id = bc.related_account_id
	JOIN entity e on e.entity_id = ma.entity_id

	SET @endTime = GETDATE();
	SET @StepMessage = 'Block 33 - Finish. Time Taken - ' + CONVERT(VARCHAR(20), @endTime - @startTime, 114);
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;

    /*************************************** Beneficiary ******************************************/

    SET @StepMessage = 'Block 34 - Start';
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;
	SET @startTime = GETDATE();

	IF EXISTS 
		(	SELECT * 
			FROM sys.objects 
			WHERE [name] = N'au_beneficiary' AND [type] = 'TR'
		)
	BEGIN
		ALTER TABLE [beneficiary] DISABLE TRIGGER [au_beneficiary];      
	END;

	UPDATE 
	 beneficiary
	SET
		name = 'NameRemoved'
	  , date_of_birth =	CASE 
							WHEN date_of_birth IS NULL THEN NULL
							ELSE
								CASE 
									WHEN (DATEDIFF(YEAR, date_of_birth, GETDATE()) > 18 OR DATEDIFF(YEAR, date_of_birth, GETDATE()) < 1)
										THEN DATEADD(dd, CAST(RAND(DATEDIFF(DAY, 0, date_of_birth)) * 100 + 10 AS INT), date_of_birth) /* add random days from 10 to 110 */
									WHEN (DATEDIFF(YEAR, date_of_birth, GETDATE()) <= 18 OR DATEDIFF(YEAR, date_of_birth, GETDATE()) >= 1) 
										THEN DATEADD(dd, CAST(RAND(DATEDIFF(DAY, 0, date_of_birth)) * -100 - 10 AS INT), date_of_birth) /* subract random days from 10 to 110 */
								END
						END;

	IF EXISTS 
		(	SELECT * 
			FROM sys.objects 
			WHERE [name] = N'au_beneficiary' AND [type] = 'TR'
		)
	BEGIN
		ALTER TABLE [beneficiary] ENABLE TRIGGER [au_beneficiary];      
	END;

    SET @endTime = GETDATE();
	SET @StepMessage = 'Block 34 - Finish. Time Taken - ' + CONVERT(VARCHAR(20), @endTime - @startTime, 114);
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;

	/*************************************** Complying_fund ******************************************/

	SET @StepMessage = 'Block 35 - Start';
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;
	SET @startTime = GETDATE();

	UPDATE
		complying_fund
	SET
		email_address = CASE
							WHEN email_address IS NULL THEN NULL
							ELSE 'Scrambled@noemail.nodomain'
						END
		, fax_number = CASE
							WHEN fax_number IS NULL THEN NULL
							ELSE '08000000000'
						END
		, hmrc_employer_accts_office_ref = CASE 
											   WHEN hmrc_employer_accts_office_ref IS NULL THEN NULL
											   ELSE SUBSTRING(CONVERT(VARCHAR(100), NEWID()), 0, CHARINDEX('-', CONVERT(VARCHAR(100), NEWID())))
											END
		 , hmrc_gateway_user_id = CASE
									WHEN hmrc_gateway_user_id IS NULL THEN NULL
									ELSE '444444444444'
								END
		 , hmrc_gateway_user_password = CASE
											WHEN hmrc_gateway_user_password IS NULL THEN NULL
											ELSE 'deleted'
										END
		 , paye_reference = CASE
								WHEN paye_reference IS NULL THEN NULL
								ELSE SUBSTRING(CONVERT(VARCHAR(100), NEWID()), 0, CHARINDEX('-', CONVERT(VARCHAR(100), NEWID())))
							END
		 , pension_scheme_tax_ref_number = CASE
												WHEN pension_scheme_tax_ref_number IS NULL THEN NULL
												ELSE SUBSTRING(CONVERT(VARCHAR(100), NEWID()), 0, CHARINDEX('-', CONVERT(VARCHAR(100), NEWID())))
											END
		 , phone_number = CASE
							WHEN phone_number IS NULL THEN NULL
							ELSE '01000 111 111'
						END
		 , settlement_system_reference = CASE
											WHEN settlement_system_reference IS NULL THEN NULL
											ELSE SUBSTRING(CONVERT(VARCHAR(100), NEWID()), 0, CHARINDEX('-', CONVERT(VARCHAR(100), NEWID())))
										END

	SET @endTime = GETDATE();
	SET @StepMessage = 'Block 35 - Finish. Time Taken - ' + CONVERT(VARCHAR(20), @endTime - @startTime, 114);
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;

	/*************************************** Divorce Details ******************************************/

    SET @StepMessage = 'Block 36 - Start';
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;
	SET @startTime = GETDATE();

	IF EXISTS 
		(	SELECT * 
			FROM sys.objects 
			WHERE [name] = N'au_divorce_details' AND [type] = 'TR'
		)
	BEGIN
		ALTER TABLE [divorce_details] DISABLE TRIGGER [au_divorce_details];      
	END;

    UPDATE 
		[divorce_details]
    SET 
		[court_order_number] = CASE
									WHEN court_order_number IS NULL THEN NULL
									ELSE 'AA11A00000'
								END
      , [notation] = CASE
                          WHEN [notation] IS NULL THEN NULL
                          ELSE 'Text removed'
                        END;

	IF EXISTS 
		(	SELECT * 
			FROM sys.objects 
			WHERE [name] = N'au_divorce_details' AND [type] = 'TR'
		)
	BEGIN
		ALTER TABLE [divorce_details] ENABLE TRIGGER [au_divorce_details];      
	END;

    SET @endTime = GETDATE();
	SET @StepMessage = 'Block 36 - Finish. Time Taken - ' + CONVERT(VARCHAR(20), @endTime - @startTime, 114);
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;

	/*************************************** Employee_details ******************************************/

	SET @StepMessage = 'Block 37 - Start';
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;
	SET @startTime = GETDATE();

	/* We're updating the salary as someone with an extreme salary may easily be uniquely identified e.g. a chief executive.
	The below code will generate a random number between 10k and 1m. */
	IF EXISTS 
		(	SELECT * 
			FROM sys.objects 
			WHERE [name] = N'au_employee_details' AND [type] = 'TR'
		)
	BEGIN
		ALTER TABLE employee_details DISABLE TRIGGER au_employee_details;      
	END;

	SELECT 
       @iStart = min(employee_details_id),
       @iCntr = max(employee_details_id)
	FROM 
       employee_details

	SELECT 
		@iIncr = 20000,
		@iEnd = @iStart + @iIncr;
	
	WHILE @iStart <= @iCntr
	BEGIN
		UPDATE
			employee_details
		SET
			salary = CAST(RAND(employee_details_id) * (1000000 - 10000) + 10000 AS decimal(10,2))
		WHERE
			salary > 10000
			AND employee_details_id between @iStart and @iEnd
		
		SELECT @iStart = @iEnd +1,
				@iEnd = @iEnd + @iIncr
	END

	IF EXISTS 
	(	SELECT * 
		FROM sys.objects 
		WHERE [name] = N'au_employee_details' AND [type] = 'TR'
	)
	BEGIN
		ALTER TABLE employee_details ENABLE TRIGGER au_employee_details;      
	END;

	SET @endTime = GETDATE();
	SET @StepMessage = 'Block 37 - Finish. Time Taken - ' + CONVERT(VARCHAR(20), @endTime - @startTime, 114);
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;

	/*************************************** Expectation ******************************************/

    SET @StepMessage = 'Block 38 - Start';
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;
	SET @startTime = GETDATE();

	IF EXISTS 
		(	SELECT * 
			FROM sys.objects 
			WHERE [name] = N'au_expectation' AND [type] = 'TR'
		)
	BEGIN
		ALTER TABLE [expectation] DISABLE TRIGGER [au_expectation];      
	END;

    UPDATE [expectation]
       SET [notation] = NULL
    WHERE 
          [party_type_id] = 1
          AND [notation] IS NOT NULL
          AND CONVERT(VARCHAR, [notation]) != '';

	IF EXISTS 
		(	SELECT * 
			FROM sys.objects 
			WHERE [name] = N'au_expectation' AND [type] = 'TR'
		)
	BEGIN
		ALTER TABLE [expectation] ENABLE TRIGGER [au_expectation];      
	END;

	SET @endTime = GETDATE();
	SET @StepMessage = 'Block 38 - Finish. Time Taken - ' + CONVERT(VARCHAR(20), @endTime - @startTime, 114);
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;

	/*************************************** External Party ******************************************/

    SET @StepMessage = 'Block 39 - Start';
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;
	SET @startTime = GETDATE();

    UPDATE [external_party]
       SET [external_party_name] = CASE [external_party_id] % 10
                                     WHEN 0
                                       THEN 'Zero Sun'
                                     WHEN 1
                                       THEN 'One Mercury'
                                     WHEN 2
                                       THEN 'Two Venus'
                                     WHEN 3
                                       THEN 'Three Earth'
                                     WHEN 4
                                       THEN 'Four Mars'
                                     WHEN 5
                                       THEN 'Five Jupiter'
                                     WHEN 6
                                       THEN 'Six Saturn'
                                     WHEN 7
                                       THEN 'Seven Neptune'
                                     WHEN 8
                                       THEN 'Eight Pluto'
                                     WHEN 9
                                       THEN 'Nine Solar'
                                   END
    WHERE 
          [party_type_id] IN ( 43, 44, 45, 46 );

    SET @endTime = GETDATE();
	SET @StepMessage = 'Block 39 - Finish. Time Taken - ' + CONVERT(VARCHAR(20), @endTime - @startTime, 114);
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;

	/*************************************** FATCA Identification ******************************************/

    SET @StepMessage = 'Block 40 - Start';
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;
	SET @startTime = GETDATE();

	IF EXISTS 
		(	SELECT * 
			FROM sys.objects 
			WHERE [name] = N'au_identification' AND [type] = 'TR'
		)
	BEGIN
		ALTER TABLE [identification] DISABLE TRIGGER [au_identification];      
	END;

    UPDATE [identification]
       SET [tax_identification_number] = SUBSTRING(CONVERT(VARCHAR(100), NEWID()), 0, CHARINDEX('-', CONVERT(VARCHAR(100), NEWID()))) + CHAR(CAST( ( 90 - 65 ) * RAND() + 65 AS INTEGER))
    WHERE 
          [tax_identification_number] IS NOT NULL;

	IF EXISTS 
		(	SELECT * 
			FROM sys.objects 
			WHERE [name] = N'au_identification' AND [type] = 'TR'
		)
	BEGIN
		ALTER TABLE [identification] ENABLE TRIGGER [au_identification];      
	END;

    SET @endTime = GETDATE();
	SET @StepMessage = 'Block 40 - Finish. Time Taken - ' + CONVERT(VARCHAR(20), @endTime - @startTime, 114);
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;

	/*************************************** Investment_manager ******************************************/

	SET @StepMessage = 'Block 41 - Start';
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;
	SET @startTime = GETDATE();

	IF EXISTS 
		(	SELECT * 
			FROM sys.objects 
			WHERE [name] = N'au_investment_manager' AND [type] = 'TR'
		)
	BEGIN
		ALTER TABLE [investment_manager] DISABLE TRIGGER [au_investment_manager];      
	END;
 
	UPDATE 
		investment_manager
	SET 
		notation = 'Data Obfuscated'
	WHERE 
		notation IS NOT NULL

	IF EXISTS 
		(	SELECT * 
			FROM sys.objects 
			WHERE [name] = N'au_investment_manager' AND [type] = 'TR'
		)
	BEGIN
		ALTER TABLE [investment_manager] ENABLE TRIGGER [au_investment_manager];      
	END;

	SET @endTime = GETDATE();
	SET @StepMessage = 'Block 41 - Finish. Time Taken - ' + CONVERT(VARCHAR(20), @endTime - @startTime, 114);
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;

	/*************************************** Master_model ******************************************/

	SET @StepMessage = 'Block 42 - Start';
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;
	SET @startTime = GETDATE();

	IF EXISTS 
		(	SELECT * 
			FROM sys.objects 
			WHERE [name] = N'au_master_model' AND [type] = 'TR'
		)
	BEGIN
		ALTER TABLE [master_model] DISABLE TRIGGER [au_master_model];      
	END;

	IF OBJECT_ID('tempdb..#master_model_scramble') IS NOT NULL 
		DROP TABLE #master_model_scramble

	CREATE TABLE #master_model_scramble 
	(
	master_model_id 	int not null PRIMARY KEY,
		new_name 		varchar(80) ,

	)
	--generate the new scrabled names
	INSERT INTO #master_model_scramble 
	(
		master_model_id, 
		new_name
	)
	SELECT
		master_model_id,
		CASE master_model_id % 14
			WHEN 0 THEN 'Dynamic'
			WHEN 1 THEN 'Balanced'
			WHEN 2 THEN 'Cautious'
			WHEN 3 THEN 'Adventurous'
			WHEN 4 THEN 'Strategic'
			WHEN 5 THEN 'Speculative'
			WHEN 6 THEN 'Defensive'
			WHEN 7 THEN 'Moderate'
			WHEN 8 THEN 'Global'
			WHEN 9 THEN 'Capital'
			WHEN 10 THEN 'Managed'
			WHEN 11 THEN 'Alpha'
			WHEN 12 THEN 'Conservative'
			WHEN 13 THEN 'Aggressive'
		END + ' ' +
		CASE master_model_id % 13
			WHEN 0 THEN ''
			WHEN 1 THEN 'Explicit'
			WHEN 2 THEN 'High'
			WHEN 3 THEN 'Medium'
			WHEN 4 THEN 'Low'
			WHEN 5 THEN 'Passive'
			WHEN 6 THEN 'Active'
			WHEN 7 THEN 'Equity'
			WHEN 8 THEN 'Above'
			WHEN 9 THEN 'Ethical'
			WHEN 10 THEN 'Recovery'
			WHEN 11 THEN 'UT'
			WHEN 12 THEN 'ISA'
		END + ' ' +
		CASE master_model_id % 10
			WHEN 0 THEN ''
			WHEN 1 THEN 'Income'
			WHEN 2 THEN 'Portfolio'
			WHEN 3 THEN 'Risk'
			WHEN 4 THEN 'Level'
			WHEN 5 THEN 'Profile'
			WHEN 6 THEN 'Yield'
			WHEN 7 THEN 'Equity'
			WHEN 8 THEN 'Index'
			WHEN 9 THEN 'Growth'
		END + ' ' +
		CASE master_model_id % 28
			WHEN 0 THEN ''
			WHEN 1 THEN '1'
			WHEN 2 THEN '2'
			WHEN 3 THEN '3'
			WHEN 4 THEN '4'
			WHEN 5 THEN '5'
			WHEN 4 THEN '10%'
			WHEN 6 THEN '20%'
			WHEN 7 THEN '30%'
			WHEN 8 THEN '40%'
			WHEN 9 THEN '50%'
			WHEN 10 THEN 'Q1'
			WHEN 11 THEN 'Q2'
			WHEN 12 THEN 'Q3'
			WHEN 13 THEN 'Q4'
			WHEN 14 THEN 'Monthly'
			WHEN 15 THEN 'Quarterly'
			WHEN 16 THEN 'Jan'
			WHEN 17 THEN 'Feb'
			WHEN 18 THEN 'Mar'
			WHEN 19 THEN 'Apr'
			WHEN 20 THEN 'May'
			WHEN 21 THEN 'Jun'
			WHEN 22 THEN 'Jul'
			WHEN 23 THEN 'Aug'
			WHEN 24 THEN 'Sep'
			WHEN 25 THEN 'Oct'
			WHEN 26 THEN 'Nov'
			WHEN 27 THEN 'Dec'
		END + ' ' +
		CASE master_model_id % 9
			WHEN 0 THEN ''
			WHEN 1 THEN '2013'
			WHEN 2 THEN '2014'
			WHEN 3 THEN '2015'
			WHEN 4 THEN '2016'
			WHEN 5 THEN '2017'
			WHEN 6 THEN '2018'
			WHEN 7 THEN '2019'
			WHEN 8 THEN '2020'
		END + ' ' +
		CASE master_model_id % 8
			WHEN 0 THEN ''
			WHEN 1 THEN 'Cash'
			WHEN 2 THEN '- V1'
			WHEN 3 THEN '- V2'
			WHEN 4 THEN '- V3'
			WHEN 5 THEN '- V4'
			WHEN 6 THEN '- V5'
			WHEN 7 THEN '- V6'
		END 

	FROM 
		master_model

	--update master model name to the new scrambled name
	UPDATE
		mm
	SET
		name = mms.new_name
	FROM 
		#master_model_scramble mms
		JOIN master_model mm on mms.master_model_id = mm.master_model_id

	--master model name needs to be unique at each level
	IF EXISTS (	SELECT 
					name
				FROM
					master_model
				GROUP BY
					owner_party_id,
					name
				HAVING
					COUNT(*) > 1 )
	BEGIN
		WITH cte_models_to_update AS (
			SELECT 
				mm.master_model_id
				, mm.owner_party_id
				, mm.name + CAST(ROW_NUMBER() OVER (partition by mm.owner_party_id, mm.name ORDER BY mm.owner_party_id) AS VARCHAR) AS updated_name
			FROM
				master_model mm
				JOIN (	SELECT 
							owner_party_id
						  , name
						FROM
							master_model
						GROUP BY
							owner_party_id
						  , name
						HAVING
							COUNT(*) > 1 
					) x ON mm.owner_party_id = x.owner_party_id
						AND mm.name	= x.name
		)
		UPDATE
			mm
		SET
			mm.name = mu.updated_name
		FROM 
			master_model mm
			JOIN cte_models_to_update mu ON mu.master_model_id = mm.master_model_id;
	END

	IF EXISTS 
		(	SELECT * 
			FROM sys.objects 
			WHERE [name] = N'au_master_model' AND [type] = 'TR'
		)
	BEGIN
		ALTER TABLE [master_model] ENABLE TRIGGER [au_master_model];      
	END;

		IF OBJECT_ID('tempdb..#master_model_scramble') IS NOT NULL 
		DROP TABLE #master_model_scramble

	SET @endTime = GETDATE();
	SET @StepMessage = 'Block 42 - Finish. Time Taken - ' + CONVERT(VARCHAR(20), @endTime - @startTime, 114);
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;

	/*************************************** Investment_model ******************************************/

	SET @StepMessage = 'Block 43 - Start';
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;
	SET @startTime = GETDATE();

	IF EXISTS 
		(	SELECT * 
			FROM sys.objects 
			WHERE [name] = N'au_investment_model' AND [type] = 'TR'
		)
	BEGIN
		ALTER TABLE [investment_model] DISABLE TRIGGER [au_investment_model];      
	END;

	UPDATE
		im
	SET
		short_code = LEFT(mm.name, 40)
	  , description = mm.name
	FROM
		investment_model im
		JOIN master_model mm ON mm.master_model_id = im.master_model_id

	IF EXISTS 
		(	SELECT * 
			FROM sys.objects 
			WHERE [name] = N'au_investment_model' AND [type] = 'TR'
		)
	BEGIN
		ALTER TABLE [investment_model] ENABLE TRIGGER [au_investment_model];      
	END;

	SET @endTime = GETDATE();
	SET @StepMessage = 'Block 43 - Finish. Time Taken - ' + CONVERT(VARCHAR(20), @endTime - @startTime, 114);
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;
	
	/*************************************** Account Designation ******************************************/

    SET @StepMessage = 'Block 44 - Start';
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;
	SET @startTime = GETDATE();

	IF EXISTS 
		(	SELECT * 
			FROM sys.objects 
			WHERE [name] = N'au_member_account' AND [type] = 'TR'
		)
	BEGIN
		ALTER TABLE [member_account] DISABLE TRIGGER [au_member_account];      
	END;

	UPDATE 
		[member_account]
    SET 
		[account_designation] = SUBSTRING(CONVERT(VARCHAR(100), NEWID()), 0, CHARINDEX('-', CONVERT(VARCHAR(100), NEWID())))
    WHERE 
        [account_designation] IS NOT NULL;

	IF EXISTS 
		(	SELECT * 
			FROM sys.objects 
			WHERE [name] = N'au_member_account' AND [type] = 'TR'
		)
	BEGIN
		ALTER TABLE [member_account] ENABLE TRIGGER [au_member_account];      
	END;

    SET @endTime = GETDATE();
	SET @StepMessage = 'Block 44 - Finish. Time Taken - ' + CONVERT(VARCHAR(20), @endTime - @startTime, 114);
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;

	/*************************************** MIFID tables ******************************************/

    SET @StepMessage = 'Block 45 - Start';
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;
	SET @startTime = GETDATE();

	IF EXISTS 
		(	SELECT * 
			FROM sys.objects 
			WHERE [name] = N'au_mifid_identification' AND [type] = 'TR'
		)
	BEGIN
		ALTER TABLE [mifid_identification] DISABLE TRIGGER [au_mifid_identification];      
	END;

    UPDATE 
		[mifid_identification]
    SET 
		[identification_reference] = SUBSTRING(CONVERT(VARCHAR(100), NEWID()), 0, CHARINDEX('-', CONVERT(VARCHAR(100), NEWID()))) + CHAR(CAST( ( 90 - 65 ) * RAND() + 65 AS INTEGER));

	IF EXISTS 
		(	SELECT * 
			FROM sys.objects 
			WHERE [name] = N'au_mifid_identification' AND [type] = 'TR'
		)
	BEGIN
		ALTER TABLE [mifid_identification] ENABLE TRIGGER [au_mifid_identification];      
	END;

    SET @endTime = GETDATE();
	SET @StepMessage = 'Block 45 - Finish. Time Taken - ' + CONVERT(VARCHAR(20), @endTime - @startTime, 114);
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;

	/*************************************** MIFID tables ******************************************/

	SET @StepMessage = 'Block 46 - Start';
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;
	SET @startTime = GETDATE();

    UPDATE [mifid_reporting_transaction]
       SET [buyer_first_name] = ( CASE
                                    WHEN [buyer_first_name] IS NULL
                                      THEN NULL
                                    ELSE 'Aaa'
                                  END )
         , [buyer_surname] = ( CASE
                                 WHEN [buyer_surname] IS NULL
                                   THEN NULL
                                 WHEN [buyer_surname] LIKE 'WINTERFLOOD%'
                                   THEN [buyer_surname]
                                 ELSE 'Bbb'
                               END )
         , [buy_decision_maker_first_name] = ( CASE
                                                 WHEN [buy_decision_maker_first_name] IS NULL
                                                   THEN NULL
                                                 ELSE 'Ccc'
                                               END )
         , [buy_decision_maker_surname] = ( CASE
                                              WHEN [buy_decision_maker_surname] IS NULL
                                                THEN NULL
                                              WHEN [buy_decision_maker_surname] LIKE 'WINTERFLOOD%'
                                                THEN [buy_decision_maker_surname]
                                              ELSE 'Ddd'
                                            END )
         , [seller_first_name] = ( CASE
                                     WHEN [seller_first_name] IS NULL
                                       THEN NULL
                                     ELSE 'Eee'
                                   END )
         , [seller_surname] = ( CASE
                                  WHEN [seller_surname] IS NULL
                                    THEN NULL
                                  WHEN [seller_surname] LIKE 'WINTERFLOOD%'
                                    THEN [seller_surname]
                                  WHEN [seller_surname] LIKE 'AEGON%'
                                    THEN [seller_surname]
                                  ELSE 'Fff'
                                END )
         , [sell_dec_maker_first_name] = ( CASE
                                             WHEN [sell_dec_maker_first_name] IS NULL
                                               THEN NULL
                                             ELSE 'Ggg'
                                           END )
         , [sell_dec_maker_surname] = ( CASE
                                          WHEN [sell_dec_maker_surname] IS NULL
                                            THEN NULL
                                          WHEN [sell_dec_maker_surname] LIKE 'WINTERFLOOD%'
                                            THEN [sell_dec_maker_surname]
                                          WHEN [sell_dec_maker_surname] LIKE 'AEGON%'
                                            THEN [sell_dec_maker_surname]
                                          ELSE 'Hhh'
                                        END )
		, [buy_decision_maker_dob] = ( CASE
										WHEN buy_decision_maker_dob IS NULL THEN NULL
										ELSE DATEADD(dd, CAST(RAND(DATEDIFF(DAY, 0, buy_decision_maker_dob)) * 100 + 10 AS INT), buy_decision_maker_dob)
									END )
		, [buyer_dob] = ( CASE
							WHEN buyer_dob IS NULL THEN NULL
							ELSE DATEADD(dd, CAST(RAND(DATEDIFF(DAY, 0, buyer_dob)) * 100 + 10 AS INT), buyer_dob)
						END)
		, [sell_decision_maker_dob] = ( CASE
										WHEN sell_decision_maker_dob IS NULL THEN NULL
										ELSE DATEADD(dd, CAST(RAND(DATEDIFF(DAY, 0, sell_decision_maker_dob)) * 100 + 10 AS INT), sell_decision_maker_dob)
									END )
		, [seller_dob] = ( CASE
							WHEN seller_dob IS NULL THEN NULL
							ELSE DATEADD(dd, CAST(RAND(DATEDIFF(DAY, 0, seller_dob)) * 100 + 10 AS INT), seller_dob)
						END )

    SET @endTime = GETDATE();
	SET @StepMessage = 'Block 46 - Finish. Time Taken - ' + CONVERT(VARCHAR(20), @endTime - @startTime, 114);
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;

	/*************************************** Payment Request ******************************************/

    SET @StepMessage = 'Block 47 - Start';
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;
	SET @startTime = GETDATE();

	IF EXISTS 
		(	SELECT * 
			FROM sys.objects 
			WHERE [name] = N'au_payment_request' AND [type] = 'TR'
		)
	BEGIN
		ALTER TABLE [payment_request] DISABLE TRIGGER [au_payment_request];      
	END;

    UPDATE [payment_request]
       SET [payee_name] = LEFT(	CASE
									WHEN [e].[title_id] = 1
										THEN 'Mr'
									WHEN [e].[title_id] = 2
										THEN 'Mrs'
									ELSE ''
								END + [e].[name], 60)
    FROM [payment_request] [pr]
        JOIN [entity] [e]
            ON [e].[entity_id] = [pr].[entity_id]
    WHERE 
          [party_type_id] = 1
          AND [pr].[payee_name] IS NOT NULL;

	IF EXISTS 
		(	SELECT * 
			FROM sys.objects 
			WHERE [name] = N'au_payment_request' AND [type] = 'TR'
		)
	BEGIN
		ALTER TABLE [payment_request] ENABLE TRIGGER [au_payment_request];      
	END;

	SET @endTime = GETDATE();
	SET @StepMessage = 'Block 47 - Finish. Time Taken - ' + CONVERT(VARCHAR(20), @endTime - @startTime, 114);
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;

	/*************************************** Direct Debits ******************************************/

    SET @StepMessage = 'Block 48 - Start';
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;
	SET @startTime = GETDATE();

    UPDATE [pdc_auddis]
       SET [pdc_auddis_reference] = ( CAST([pdc_auddis_id] AS VARCHAR) + ' scramble' );

	SET @endTime = GETDATE();
	SET @StepMessage = 'Block 48 - Finish. Time Taken - ' + CONVERT(VARCHAR(20), @endTime - @startTime, 114);
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;

	/*************************************** pension_payment_method ******************************************/

	SET @StepMessage = 'Block 49 - Start';
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;
	SET @startTime = GETDATE();

	IF EXISTS 
		(	SELECT * 
			FROM sys.objects 
			WHERE [name] = N'au_pension_payment_method' AND [type] = 'TR'
		)
	BEGIN
		ALTER TABLE [pension_payment_method] DISABLE TRIGGER [au_pension_payment_method];      
	END;

	UPDATE
		ppm
	SET
		payee_name = CASE
						WHEN ppm.payee_name IS NULL THEN NULL
						ELSE 
							LEFT(CASE 
									WHEN e.title_id = 1 THEN 'Mr '
									WHEN e.title_id = 2 THEN 'Mrs '
									ELSE ''
								END + ISNULL(e.given_names + ' ', '') + e.name, 40)
						END
	FROM 
		dbo.pension_payment_method ppm
		JOIN dbo.member_account ma ON ma.member_account_id = ppm.member_account_id
		JOIN dbo.entity e ON e.entity_id = ma.entity_id
	WHERE 
		ppm.payee_name IS NOT NULL

	IF EXISTS 
		(	SELECT * 
			FROM sys.objects 
			WHERE [name] = N'au_pension_payment_method' AND [type] = 'TR'
		)
	BEGIN
		ALTER TABLE [pension_payment_method] ENABLE TRIGGER [au_pension_payment_method];      
	END;
 
	SET @endTime = GETDATE();
	SET @StepMessage = 'Block 49 - Finish. Time Taken - ' + CONVERT(VARCHAR(20), @endTime - @startTime, 114);
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;

	/*************************************** Pension Lifetime Allowance Protection ******************************************/

    SET @StepMessage = 'Block 50 - Start';
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;
	SET @startTime = GETDATE();

	IF EXISTS 
		(	SELECT * 
			FROM sys.objects 
			WHERE [name] = N'au_protection' AND [type] = 'TR'
		)
	BEGIN
		ALTER TABLE [protection] DISABLE TRIGGER [au_protection];      
	END;

    UPDATE [protection]
       SET [enhanced_hmrc_cert_no] = ( CASE
                                         WHEN [enhanced_hmrc_cert_no] IS NULL
                                           THEN NULL
                                         ELSE SUBSTRING(CONVERT(VARCHAR(100), NEWID()), 0, CHARINDEX('-', CONVERT(VARCHAR(100), NEWID()))) + CHAR(CAST( ( 90 - 65 ) * RAND() + 65 AS INTEGER))
                                       END )
         , [primary_hmrc_cert_no] = ( CASE
                                        WHEN [primary_hmrc_cert_no] IS NULL
                                          THEN NULL
                                        ELSE SUBSTRING(CONVERT(VARCHAR(100), NEWID()), 0, CHARINDEX('-', CONVERT(VARCHAR(100), NEWID()))) + CHAR(CAST( ( 90 - 65 ) * RAND() + 65 AS INTEGER))
                                      END )
         , [pension_hmrc_cert_no] = ( CASE
                                        WHEN [pension_hmrc_cert_no] IS NULL
                                          THEN NULL
                                        ELSE SUBSTRING(CONVERT(VARCHAR(100), NEWID()), 0, CHARINDEX('-', CONVERT(VARCHAR(100), NEWID()))) + CHAR(CAST( ( 90 - 65 ) * RAND() + 65 AS INTEGER))
                                      END )
         , [overseas_hmrc_cert_no] = ( CASE
                                         WHEN [overseas_hmrc_cert_no] IS NULL
                                           THEN NULL
                                         ELSE SUBSTRING(CONVERT(VARCHAR(100), NEWID()), 0, CHARINDEX('-', CONVERT(VARCHAR(100), NEWID()))) + CHAR(CAST( ( 90 - 65 ) * RAND() + 65 AS INTEGER))
                                       END )
         , [non_resident_hmrc_cert_no] = ( CASE
                                             WHEN [non_resident_hmrc_cert_no] IS NULL
                                               THEN NULL
                                             ELSE SUBSTRING(CONVERT(VARCHAR(100), NEWID()), 0, CHARINDEX('-', CONVERT(VARCHAR(100), NEWID()))) + CHAR(CAST( ( 90 - 65 ) * RAND() + 65 AS INTEGER))
                                           END )
         , [fixed_hmrc_cert_no] = ( CASE
                                      WHEN [fixed_hmrc_cert_no] IS NULL
                                        THEN NULL
                                      ELSE SUBSTRING(CONVERT(VARCHAR(100), NEWID()), 0, CHARINDEX('-', CONVERT(VARCHAR(100), NEWID()))) + CHAR(CAST( ( 90 - 65 ) * RAND() + 65 AS INTEGER))
                                    END )
         , [individual_hmrc_cert_no] = ( CASE
                                           WHEN [individual_hmrc_cert_no] IS NULL
                                             THEN NULL
                                           ELSE SUBSTRING(CONVERT(VARCHAR(100), NEWID()), 0, CHARINDEX('-', CONVERT(VARCHAR(100), NEWID()))) + CHAR(CAST( ( 90 - 65 ) * RAND() + 65 AS INTEGER))
                                         END );

	IF EXISTS 
		(	SELECT * 
			FROM sys.objects 
			WHERE [name] = N'au_protection' AND [type] = 'TR'
		)
	BEGIN
		ALTER TABLE [protection] ENABLE TRIGGER [au_protection];      
	END;

	SET @endTime = GETDATE();
	SET @StepMessage = 'Block 50 - Finish. Time Taken - ' + CONVERT(VARCHAR(20), @endTime - @startTime, 114);
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;

	/*************************************** Received Money ******************************************/

    SET @StepMessage = 'Block 51 - Start';
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;
	SET @startTime = GETDATE();

    UPDATE [received_money]
       SET [reference_number] = '99999999'
         , [drawer_name] = LEFT([e].[name], 40)
    FROM [received_money] [rm]
        JOIN [correspondence] [corr]
            ON [rm].[correspondence_id] = [corr].[correspondence_id]
        JOIN [entity] [e]
            ON [e].[entity_id] = [corr].[entity_id]
        JOIN [member_account] [ma]
            ON [ma].[entity_id] = [e].[entity_id]
    WHERE 
          [rm].[bsb_number] IS NOT NULL;

    SET @endTime = GETDATE();
	SET @StepMessage = 'Block 51 - Finish. Time Taken - ' + CONVERT(VARCHAR(20), @endTime - @startTime, 114);
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;

	/*************************************** Spouse ******************************************/

    SET @StepMessage = 'Block 52 - Start';
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;
	SET @startTime = GETDATE();

	IF EXISTS 
		(	SELECT * 
			FROM sys.objects 
			WHERE [name] = N'au_spouse' AND [type] = 'TR'
		)
	BEGIN
		ALTER TABLE [spouse] DISABLE TRIGGER [au_spouse];      
	END;

	UPDATE
		s
	SET
		name =	CASE
					WHEN s.name IS NULL THEN NULL
					ELSE CASE
							WHEN se.name IS NULL THEN 'deleted'
							ELSE se.name
					END
				END
		, birth_date =	CASE
							WHEN s.birth_date IS NULL THEN NULL
							ELSE CASE
									WHEN se.birth_date IS NULL THEN DATEADD(dd, CAST(RAND(DATEDIFF(DAY, 0, s.birth_date)) * 100 + 10 AS INT), s.birth_date)
									ELSE se.birth_date
							END
						END
		, gender =	CASE
						WHEN se.gender IS NULL THEN 'M'
						ELSE se.gender
					END
	FROM 
		spouse s
		JOIN entity e on e.entity_id = s.entity_id
		LEFT JOIN entity se on se.entity_id = s.spouse_entity_id

	IF EXISTS 
		(	SELECT * 
			FROM sys.objects 
			WHERE [name] = N'au_spouse' AND [type] = 'TR'
		)
	BEGIN
		ALTER TABLE [spouse] ENABLE TRIGGER [au_spouse];      
	END;

    SET @endTime = GETDATE();
	SET @StepMessage = 'Block 52 - Finish. Time Taken - ' + CONVERT(VARCHAR(20), @endTime - @startTime, 114);
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;

	/*************************************** Party Attributes ******************************************/

    SET @StepMessage = 'Block 53 - Start';
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;
	SET @startTime = GETDATE();

	IF EXISTS 
		(	SELECT * 
			FROM sys.objects 
			WHERE [name] = N'au_party_attribute' AND [type] = 'TR'
		)
	BEGIN
		ALTER TABLE [party_attribute] DISABLE TRIGGER [au_party_attribute];      
	END;

	UPDATE 
		pa
	SET 
		attribute_value = CASE 
							WHEN a.description = 'PSO Number' 
								AND pa.attribute_value IS NOT NULL
									THEN SUBSTRING(CONVERT(VARCHAR(100), NEWID()), 0, CHARINDEX('-', CONVERT(VARCHAR(100), NEWID())))
							WHEN a.description = 'HMRC Certificate Number' 
								AND pa.attribute_value IS NOT NULL
									THEN SUBSTRING(CONVERT(VARCHAR(100), NEWID()), 0, CHARINDEX('-', CONVERT(VARCHAR(100), NEWID())))
							WHEN a.description = 'Post Sales Illustration File Name' 
								AND pa.attribute_value IS NOT NULL
									THEN '9999999_1-AAaZZZZZ\RemovedFilePath-' + CAST(pa.party_attribute_id AS VARCHAR) + '.pdf'
							WHEN a.description = 'SMS Preferences for receiving marketing material' 
								AND pa.attribute_value IS NOT NULL
									THEN 'deleted'
							WHEN a.description = 'SOCIAL_MEDIA Preferences for receiving marketing material' 
								AND pa.attribute_value IS NOT NULL
									THEN 'deleted'
							WHEN a.description = 'Trustee Title and Name' 
								AND pa.attribute_value IS NOT NULL
									THEN 'deleted'
							ELSE
								pa.attribute_value
						END
	FROM 
		party_attribute pa
		JOIN attribute a ON a.attribute_id = pa.attribute_id
	WHERE 
		a.description in (
							'HMRC Certificate Number'
						  , 'PSO Number'
						  , 'Post Sales Illustration File Name'
						  , 'SMS Preferences for receiving marketing material'
						  , 'SOCIAL_MEDIA Preferences for receiving marketing material'
						  , 'Trustee Title and Name'
						)
	
	--Update bank statement name
	UPDATE
		pa
	SET
		attribute_value =	CASE	
								WHEN ba.bank_account_id IS NULL THEN '333333, 22222222'
								ELSE ib.bsb_number + ', ' + RTRIM(ba.account_number)
							END
	FROM
		party_attribute pa
		JOIN attribute a ON a.attribute_id = pa.attribute_id
		JOIN member_account ma ON ma.member_account_id = pa.party_id AND pa.party_type_id = 1
		JOIN entity e ON e.entity_id = ma.entity_id
		LEFT JOIN entity_bank_account eba ON eba.entity_id = pa.party_id
		LEFT JOIN bank_account ba ON ba.bank_account_id = eba.bank_account_id
		LEFT JOIN institution_branch ib on ib.institution_branch_id = ba.institution_branch_id
	WHERE
		a.description = 'Bank Statement Name'
	
	--Update verified bank account details
	UPDATE
		pa
	SET
		attribute_value = x.new_value
	FROM 
		party_attribute pa
		JOIN (	
				SELECT 
					pa1.party_attribute_id
					, pa1.attribute_value
					, CASE
						WHEN ba.bank_account_id IS NULL THEN '333333, 22222222,'
						ELSE ib.bsb_number + ', ' + RTRIM(ba.account_number) + ', ' + ISNULL(ba.customer_reference, '')
					END + CAST(ROW_NUMBER() OVER (PARTITION BY pa1.party_id ORDER BY ba.bank_account_id) AS VARCHAR) AS new_value
				FROM 
					dbo.party_attribute pa1
					JOIN attribute a ON a.attribute_id = pa1.attribute_id
					LEFT JOIN entity_bank_account eba ON eba.entity_id = pa1.party_id
					LEFT JOIN bank_account ba ON ba.bank_account_id = eba.bank_account_id
					LEFT JOIN institution_branch ib on ib.institution_branch_id = ba.institution_branch_id
				WHERE 
					pa1.party_type_id = 20 
					and a.description = 'Verified bank details'
			) x ON x.party_attribute_id = pa.party_attribute_id
				AND x.attribute_value = pa.attribute_value

	IF EXISTS 
		(	SELECT * 
			FROM sys.objects 
			WHERE [name] = N'au_party_attribute' AND [type] = 'TR'
		)
	BEGIN
		ALTER TABLE [party_attribute] ENABLE TRIGGER [au_party_attribute];      
	END;

	SET @endTime = GETDATE();
	SET @StepMessage = 'Block 53 - Finish. Time Taken - ' + CONVERT(VARCHAR(20), @endTime - @startTime, 114);
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;

	/*************************************** TRUNCATE TABLES ******************************************/

	SET @StepMessage = 'Block 54 - Start';
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;
	SET @startTime = GETDATE();

	--TRUNCATE TABLE ann_idd_review_results;
	TRUNCATE TABLE trans_int_payment;
	TRUNCATE TABLE tmp_product_provider_customer;
	TRUNCATE TABLE tmp_payment_request;
	TRUNCATE TABLE tmp_il_takeup;
	TRUNCATE TABLE tmp_audit_member_account;
	TRUNCATE TABLE tmp_audit_entity;
	TRUNCATE TABLE tmp_audit_bank_account;
	TRUNCATE TABLE tmp_audit_address;
	TRUNCATE TABLE trans_int_recv_provider;
	TRUNCATE TABLE transfer_client_details;
	TRUNCATE TABLE transfer_p45_detail;
	TRUNCATE TABLE surcharge_extract;
	TRUNCATE TABLE tax_office_file_details;
	--TRUNCATE TABLE entity_scramble;
    
	SET @endTime = GETDATE();
	SET @StepMessage = 'Block 54 - Finish. Time Taken - ' + CONVERT(VARCHAR(20), @endTime - @startTime, 114);
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;

	/*************************************** DELETE TABLES ******************************************/

	--There is a foreign key constraint, so can't truncate
	SET @StepMessage = 'Block 55 - Start';
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;
	SET @startTime = GETDATE();

	DELETE FROM note;
	DELETE FROM lisa_property;
	DELETE FROM tax_office_file_sub_header;
	DELETE FROM external_bank_account;

	SET @endTime = GETDATE();
	SET @StepMessage = 'Block 55 - Finish. Time Taken - ' + CONVERT(VARCHAR(20), @endTime - @startTime, 114);
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;

	/*************************************** Clean up ******************************************/
 
	DROP PROC sp_tmp_split_strings

	IF OBJECT_ID('tempdb..#surnames') IS NOT NULL 
		DROP TABLE #surnames

	IF OBJECT_ID('tempdb..#firstnames') IS NOT NULL 
		DROP TABLE #firstnames

	IF OBJECT_ID('tempdb..#names') IS NOT NULL 
		DROP TABLE #names

	IF OBJECT_ID('tempdb..#investors_to_change') IS NOT NULL 
		DROP TABLE #investors_to_change

	SET @StepMessage = convert(varchar,getdate(),109) + ' Finish Scramble'
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;
	
	--ROllBACK TRANSACTION
END TRY
BEGIN CATCH
    SELECT 
          @ErrorMessage = @StepMessage + ' - ' + ERROR_MESSAGE();

    RAISERROR(@ErrorMessage, 16, 1);
	
	--ROllBACK TRANSACTION
END CATCH;
END;