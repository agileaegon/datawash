BEGIN
    SET NOCOUNT ON;

	DECLARE	@StepMessage  NVARCHAR(4000)
		  , @ErrorMessage NVARCHAR(4000)
		  , @Rowcount INT
		  , @startTime DATETIME
		  , @endTime DATETIME
		  , @iIncr int
		  , @iStart BIGINT
		  , @iEnd BIGINT
		  , @iCntr BIGINT

BEGIN TRY

	/*************************************** Payload  ******************************************/
	SET @StepMessage = 'Block 1 - Start';
	RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;
	SET @startTime = GETDATE();
 
	SELECT 
       @iStart = min(ApplicationId),
       @iCntr = max(ApplicationId)
	FROM 
       Payload

	SELECT 
		@iIncr = 20000,
		@iEnd = @iStart + @iIncr;
	
	WHILE @iStart <= @iCntr
	BEGIN
		UPDATE 
			p
		SET
			IncommingPayload = '<xmlremoved/>'
		  , OutgoingPayload = CASE
								WHEN OutgoingPayload IS NULL THEN NULL
								ELSE '<xmlremoved/>'
							END
		FROM 
			Payload p
		WHERE
			 ApplicationId between @iStart and @iEnd
 
		SELECT 
			@iStart = @iEnd +1,
			@iEnd = @iEnd + @iIncr
	END

	SET @endTime = GETDATE();
	SET @StepMessage = 'Block 1 - Finish. Time Taken - ' + CONVERT(VARCHAR(20), @endTime - @startTime, 114);
	RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;

	/*************************************** ReReg ******************************************/
	SET @StepMessage = 'Block 2 - Start';
	RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;
	SET @startTime = GETDATE();

	UPDATE 
		ReReg
	SET 
		reference = 'Data obfuscated'
	WHERE 
		reference IS NOT NULL

	SELECT	@Rowcount = @@ROWCOUNT;
	PRINT '   Rows updated: ' + CAST(@Rowcount AS VARCHAR(10));

	SET @endTime = GETDATE();
	SET @StepMessage = 'Block 2 - Finish. Time Taken - ' + CONVERT(VARCHAR(20), @endTime - @startTime, 114);
	RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;

	/*************************************** Truncate tables ******************************************/
	SET @StepMessage = 'Block 3 - Start';
	RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;
	SET @startTime = GETDATE();

	TRUNCATE TABLE AUKImportCwebpopulation
	TRUNCATE TABLE ClientAddress
	TRUNCATE TABLE ClientDetails

	SET @endTime = GETDATE();
	SET @StepMessage = 'Block 3 - Finish. Time Taken - ' + CONVERT(VARCHAR(20), @endTime - @startTime, 114);
	RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;

END TRY

BEGIN CATCH

    SELECT @ErrorMessage = @StepMessage + ' - ' + ERROR_MESSAGE();
    RAISERROR(@ErrorMessage, 16, 1);

IF @@TRANCOUNT > 0
	ROLLBACK

END CATCH
END	