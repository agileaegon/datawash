-- Script 2 of 3 required for the subsequent Data Washing of the PRD_Imago database to produce useful test data.  These scripts came from GBST.
-- This script is run on the Production (UN-Washed) ComposerWeb database PRD_STGComposerWeb BEFORE it gets Washed.

select usec_user_account_id, username
from usec_user_account
order by usec_user_account_id
