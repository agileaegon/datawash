-- Script 1 of 3 required for the subsequent Data Washing of the PRD_Imago database to produce useful test data.  These scripts came from GBST.
-- This script is run on the Data Washed ComposerWeb database PRD_STGComposerWeb

select uacc.usec_user_account_id, uacc.username, a_first.attribute_value as firstname, a_last.attribute_value as lastname, a_email.attribute_value as email
from usec_user_account uacc
     join usec_user_account_attribute a_first on uacc.usec_user_account_id = a_first.usec_user_account_id and a_first.attribute_name = 'forename'
     join usec_user_account_attribute a_last on uacc.usec_user_account_id = a_last.usec_user_account_id and a_last.attribute_name = 'surname'
     join usec_user_account_attribute a_email on uacc.usec_user_account_id = a_email.usec_user_account_id and a_email.attribute_name = 'email'     
order by usec_user_account_id


