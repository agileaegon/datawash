-- Script 3 of 3 required for the subsequent Data Washing of the PRD_Imago database to produce useful test data.  These scripts came from GBST.
-- This script is run on the Data Washed Composer BackOffice database PRD_STGComposer

SELECT 'update [AdviserFirm] set [NAME] = ''' 
       + e.name 
       + ''' WHERE [REFERENCE] = ''' 
       + isnull(convert(varchar,a.fsa_firm_reference_id),convert(varchar,d.fsa_firm_reference_id)) + ''''
from entity e 
     join dealer_branch a on a.entity_id = e.entity_id 
     join dealer d on d.dealer_id = a.dealer_id 
     join fsa_firm_reference ref on ref.fsa_firm_reference_id = isnull(a.fsa_firm_reference_id,d.fsa_firm_reference_id)       
     join fsa_firm_curr_auth_status auth on auth.fsa_firm_curr_auth_status_id = ref.fsa_firm_curr_auth_status_id  

