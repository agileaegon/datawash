-- 16/01/2020 SK - ENV-4006 Env Ops asked for this new script to be added, to make the data useful for Test Environments

USE ToWash_PRD_Imago50
GO

update UserProfile
	set UserName = r.UserName
from [dbo].[UserProfile] up
join [dbo].[RegisteredUser] r on up.UserId = r.UserProfileId
where up.UserName <> r.Username
GO