BEGIN
    SET NOCOUNT ON;

	DECLARE	@StepMessage  NVARCHAR(4000)
		  , @ErrorMessage NVARCHAR(4000)
		  , @Rowcount INT
		  , @startTime DATETIME
		  , @endTime DATETIME;

BEGIN TRY
	
	/*************************************** QuoteClient ******************************************/
	SET @StepMessage = 'Block 1 - Start';
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;
	SET @startTime = GETDATE();
 
	UPDATE
		qc
	SET
		AdviserName = CASE
						WHEN AdviserName IS NULL THEN NULL
						ELSE a.Name
					END
	  , GrossSalary = CASE
							WHEN ISNULL(GrossSalary, 0) = 0 THEN GrossSalary
							ELSE CAST(RAND(qc.Id) * 100000 AS decimal(10,2))
						END
	FROM
		QuoteClient qc
		LEFT JOIN AdviserFirm a on a.id = qc.AdviserFirmId

	SET @endTime = GETDATE();
	SET @StepMessage = 'Block 1 - Finish. Time Taken - ' + CONVERT(VARCHAR(20), @endTime - @startTime, 114);
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;

    /*************************************** QuoteClientHistory ******************************************/
	SET @StepMessage = 'Block 2 - Start';
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;
	SET @startTime = GETDATE();

	UPDATE
		qch
	SET
		AdviserName = CASE
						WHEN AdviserName IS NULL THEN NULL
						ELSE a.Name
					END
	  , GrossSalary = CASE
							WHEN ISNULL(GrossSalary, 0) = 0 THEN GrossSalary
							ELSE CAST(RAND(qch.Id) * 100000 AS decimal(10,2))
						END
	FROM
		QuoteClientHistory qch
		LEFT JOIN AdviserFirm a on a.id = qch.AdviserFirmId

	SELECT	@Rowcount = @@ROWCOUNT;
    PRINT '   Rows updated: ' + CAST(@Rowcount AS VARCHAR(10));

	SET @endTime = GETDATE();
	SET @StepMessage = 'Block 2 - Finish. Time Taken - ' + CONVERT(VARCHAR(20), @endTime - @startTime, 114);
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;

	/*************************************** Delete data ******************************************/
	SET @StepMessage = 'Block 3 - Start';
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;
	SET @startTime = GETDATE();

	Print 'DATA RowCounts Before'
	SELECT sc.name + '.' + ta.name TableName,
		   SUM(pa.rows) RowCnt
	FROM sys.tables ta
		INNER JOIN sys.partitions pa
			ON pa.OBJECT_ID = ta.OBJECT_ID
		INNER JOIN sys.schemas sc
			ON ta.schema_id = sc.schema_id
	WHERE ta.is_ms_shipped = 0
		  AND pa.index_id IN ( 1, 0 )
	GROUP BY sc.name,
			 ta.name
	ORDER BY ta.name

	-- auth and session data
	DELETE FROM [AuditLog]
	DELETE FROM [UserAudit]
	DELETE FROM [UserAuditHelperTable]
	DELETE FROM [AuthenticationTicket]
	DELETE FROM [ContactAddress]
	DELETE FROM [ContactAddressHistory]
	DELETE FROM [ContactInstanceHistory]
	DELETE FROM [ContactMethod]
	DELETE FROM [ContactMethodHistory]

	-- quotes data
	DELETE FROM [QuoteProductCharge]
	DELETE FROM [QuoteFundCharge]
	DELETE FROM [QuoteFund]
	DELETE FROM [QuoteVariableIncome]
	DELETE FROM [QuoteContribution]
	DELETE FROM [QuoteTranche]
	DELETE FROM [QuoteOrganisation]
	DELETE FROM [QuoteOrganisationHistory]
	DELETE FROM [QuoteProductChargeHistory]
	DELETE FROM [QuoteFundChargeHistory]
	DELETE FROM [QuoteFundHistory]
	DELETE FROM [QuoteVariableIncomeHistory]
	DELETE FROM [QuoteContributionHistory]
	DELETE FROM [QuoteTrancheHistory]
	DELETE FROM [QuoteHistory]
	DELETE FROM [QuoteFundProvider]
	DELETE FROM [QuoteFundProviderHistory]
	DELETE FROM [QuoteIllustrationVersion]
	DELETE FROM [QuotePerson]
	DELETE FROM [QuoteProduct]
	DELETE FROM [QuoteProductHistory]
	DELETE FROM [QuoteProductChargeTier]
	DELETE FROM [QuoteProductChargeTierHistory]
	DELETE FROM [QuoteResultBlock]
	DELETE FROM [QuoteResultBlockHistory]
	DELETE FROM [QuoteSystemDefault]
	DELETE FROM [QuoteSystemDefaultHistory]
	DELETE FROM [Quote]
	DELETE FROM [PdfMigration]

	--- batch item data
	DELETE FROM BatchItemData
	DELETE FROM BatchItem
	DELETE FROM Batch

	-- member / person data
	DELETE FROM [Client]
	DELETE FROM [ClientHistory]
	DELETE FROM [Person]
	DELETE FROM [PersonHistory]

	PRINT 'DATA RowCounts After'
	SELECT sc.name + '.' + ta.name TableName,
		   SUM(pa.rows) RowCnt
	FROM sys.tables ta
		INNER JOIN sys.partitions pa
			ON pa.OBJECT_ID = ta.OBJECT_ID
		INNER JOIN sys.schemas sc
			ON ta.schema_id = sc.schema_id
	WHERE ta.is_ms_shipped = 0
		  AND pa.index_id IN ( 1, 0 )
	GROUP BY sc.name,
			 ta.name
	ORDER BY ta.name
	
	SET @endTime = GETDATE();
	SET @StepMessage = 'Block 3 - Finish. Time Taken - ' + CONVERT(VARCHAR(20), @endTime - @startTime, 114);
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;

END TRY

BEGIN CATCH

    SELECT @ErrorMessage = @StepMessage + ' - ' + ERROR_MESSAGE();
    RAISERROR(@ErrorMessage, 16, 1);

IF @@TRANCOUNT > 0
	ROLLBACK

END CATCH
END
