/*
 This script updates the Imago API username and token.
 To be used when refreshing test environments.
*/

USE ToWash_PRD_Imago50
GO

DECLARE @env CHAR(3) = 'in3'		-- <===================== CHANGE THIS TO MATCH THE REQUIRED TEST ENVIRONMENT FOR EACH RUN


DECLARE @token CHAR(8) = 'anac0nDA!'
DECLARE @svc_account1 CHAR(18) = (SELECT  ('svctoolsiisappl' + @env))
DECLARE @svc_account2 CHAR(18) = (SELECT  ('svcssad' + @env))

UPDATE RegisteredUser
SET Username = @svc_account1, APIToken = @token
WHERE Username = 'svctoolsiisapplprd'

UPDATE RegisteredUser
SET Username = @svc_account2, APIToken = @token
WHERE Username = 'svcssadprd'
