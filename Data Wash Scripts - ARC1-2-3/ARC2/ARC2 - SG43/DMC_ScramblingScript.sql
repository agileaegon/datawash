BEGIN
    SET NOCOUNT ON;

	DECLARE	@StepMessage  NVARCHAR(4000)
		  , @ErrorMessage NVARCHAR(4000)
		  , @Rowcount INT
		  , @startTime DATETIME
		  , @endTime DATETIME;

BEGIN TRY

	/********************************* Sync local tables with Composer *************************************/
    SET @StepMessage = 'Block 1 - Start';
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;
	SET @startTime = GETDATE();

	--Sync dmc_member_account & dmc_party_external_reference tables with Composer
	EXEC fc_proc_sync_data

	SET @endTime = GETDATE();
	SET @StepMessage = 'Block 1 - Finish. Time Taken - ' + CONVERT(VARCHAR(20), @endTime - @startTime, 114);
	RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;

	/********************************** fc_DMC_PP_Intermediary_file_detail *************************************/
    SET @StepMessage = 'Block 2 - Start';
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;
	SET @startTime = GETDATE();

	Update 
		i
	SET
		Network_FSA_Reference = CASE
									WHEN i.Network_FSA_Reference IS NULL THEN NULL
									ELSE CONVERT(VARCHAR, d.fsa_firm_reference_id)
								END
	  , Network_Company_Name = CASE 
									WHEN i.Network_Company_Name IS NULL THEN NULL
									ELSE de.name 
								END
	  , Branch_Name = CASE 
						WHEN i.Branch_Name IS NULL THEN NULL
						ELSE dbe.name 
					END
	  , Intermediary_Title = CASE 
								WHEN i.Intermediary_Title IS NULL THEN NULL
								ELSE st.name
							END
	  , Intermediary_IRN = CASE 
								WHEN i.Intermediary_IRN IS NULL THEN NULL
								ELSE per.external_reference
							END
	  , Intermediary_Forenames = CASE 
									WHEN i.Intermediary_Forenames IS NULL THEN NULL
									ELSE aae.given_names
								END
	  , Intermediary_Surname = CASE 
									WHEN i.Intermediary_Surname IS NULL THEN NULL
									ELSE COALESCE(LEFT(aae.name, 30), ep.external_party_name)
								END

		FROM 
			fc_DMC_PP_Intermediary_file_detail i
			JOIN source_dealer d ON d.dealer_id = i.AUK_Network_Id
			JOIN source_entity de ON de.entity_id = d.entity_id
			LEFT JOIN source_dealer_branch db ON db.dealer_branch_id = i.AUK_Branch_Id
			LEFT JOIN source_entity dbe ON dbe.entity_id = db.entity_id
			LEFT JOIN source_adviser_account aa ON CAST(aa.adviser_account_id AS VARCHAR(10)) = i.AUK_Representative_Id 
					AND i.AUK_Representative_Id NOT LIKE 'P%'
			LEFT JOIN source_entity aae ON aae.entity_id = aa.entity_id
			LEFT JOIN source_title st ON st.title_id = aae.title_id 
			LEFT JOIN dmc_party_external_reference per ON per.party_id = aa.adviser_account_id
					AND per.party_type_id = 3 
					AND per.external_system = 'FSA RI NUM'
			LEFT JOIN source_external_party ep ON CAST(ep.external_party_id AS VARCHAR(10)) = RIGHT(AUK_Representative_Id,LEN(AUK_Representative_Id)-1) 
					AND ep.party_type_id = 43 
					AND i.AUK_Representative_Id LIKE 'P%'
 
	SELECT	@Rowcount = @@ROWCOUNT;
	PRINT '   Rows updated: ' + CAST(@Rowcount AS VARCHAR(10));

	SET @endTime = GETDATE();
	SET @StepMessage = 'Block 2 - Finish. Time Taken - ' + CONVERT(VARCHAR(20), @endTime - @startTime, 114);
	RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;

	/******************************** fc_DMC_PP_Intermediary_file_detail ***********************************/
	SET @StepMessage = 'Block 3 - Start';
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;
	SET @startTime = GETDATE();
 
	UPDATE
		i
	SET
		Branch_Address_Line_1 = CASE
									WHEN i.Branch_Address_Line_1 IS NULL THEN NULL
									ELSE a.property_name
								END
	  , Branch_Address_Line_2 = CASE
									WHEN i.Branch_Address_Line_2 IS NULL THEN NULL
									ELSE ISNULL(a.prefix_type + ' ' + a.prefix + ' ', '') +
										ISNULL(a.street_number + ' ', '') +
										ISNULL(a.street + ' ', '') +
										ISNULL('CNR ' + a.street2 + ' ', '') +
										ISNULL(a.suffix_type + ' ' + a.suffix + ' ', '')
								END
	  , Branch_Address_Line_3 = CASE
									WHEN i.Branch_Address_Line_3 IS NULL THEN NULL
									ELSE a.suburb
								END
	  , Branch_Address_Line_4 = CASE
									WHEN i.Branch_Address_Line_4 IS NULL THEN NULL
									ELSE LTRIM(ISNULL(a.district, '') + ' ' + ISNULL(a.statecode, ''))
								END
	  , Branch_Address_Line_5 = NULL
	  , Branch_Address_PostCode = CASE
									WHEN i.Branch_Address_PostCode IS NULL THEN NULL
									ELSE a.postcode
								END
	FROM
		fc_DMC_PP_Intermediary_file_detail i
		LEFT JOIN source_dealer_branch db ON db.dealer_branch_id = i.AUK_Branch_Id
		LEFT JOIN source_address a ON a.entity_id = db.entity_id
	WHERE
		a.address_id = (	SELECT TOP 1 
								a1.address_id
							FROM    
								source_address a1
							WHERE   
								a1.entity_id = db.entity_id
							ORDER BY
								(  CASE a1.address_type
										WHEN 'A' THEN 0
										WHEN 'P' THEN 1
										WHEN 'W' THEN 2
										WHEN 'D' THEN 3
										ELSE 4
									END )
						)

	SELECT	@Rowcount = @@ROWCOUNT;
    PRINT '   Rows updated: ' + CAST(@Rowcount AS VARCHAR(10));

	SET @endTime = GETDATE();
	SET @StepMessage = 'Block 3 - Finish. Time Taken - ' + CONVERT(VARCHAR(20), @endTime - @startTime, 114);
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;

END TRY

BEGIN CATCH
    SELECT @ErrorMessage = @StepMessage + ' - ' + ERROR_MESSAGE();
    RAISERROR(@ErrorMessage, 16, 1);

	IF @@TRANCOUNT > 0
		ROLLBACK
END CATCH
END