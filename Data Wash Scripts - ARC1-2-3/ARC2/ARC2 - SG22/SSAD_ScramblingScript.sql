BEGIN
    SET NOCOUNT ON;

	DECLARE	@StepMessage  NVARCHAR(4000)
		  , @ErrorMessage NVARCHAR(4000)
		  , @Rowcount INT
		  , @startTime DATETIME
		  , @endTime DATETIME;

BEGIN TRY

    /*************************************** Asset Manager Account ******************************************/
	SET @StepMessage = 'Block 1 - Start';
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;
	SET @startTime = GETDATE();

	UPDATE
		asset_manager_account
	SET
		bank_account_bic =	CASE
								WHEN bank_account_bic IS NULL THEN NULL
								ELSE 'MIDLGB00' /*Dummy bic_code*/
							END
	  , bank_account_name = asset_manager_trading_name
	  , bank_account_number =	CASE 
									WHEN bank_account_number IS NULL THEN NULL
									ELSE RIGHT('99999999' + CONVERT(VARCHAR, id % 234011), 8)
								END
	  , bank_account_sort_code = CASE
									WHEN bank_account_sort_code IS NULL THEN NULL
									ELSE RIGHT('99999999' + CONVERT(VARCHAR, id % 234011), 6)
								END

	SELECT	@Rowcount = @@ROWCOUNT;
    PRINT '   Rows updated: ' + CAST(@Rowcount AS VARCHAR(10));

	SET @endTime = GETDATE();
	SET @StepMessage = 'Block 1 - Finish. Time Taken - ' + CONVERT(VARCHAR(20), @endTime - @startTime, 114);
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;

	/*************************************** Temp Asset Manager Account ******************************************/
	SET @StepMessage = 'Block 2 - Start';
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;
	SET @startTime = GETDATE();

	UPDATE
		temp_asset_manager_account
	SET
		bank_account_bic =	CASE
								WHEN bank_account_bic IS NULL THEN NULL
								ELSE 'MIDLGB00' /*Dummy bic_code*/
							END
	  , bank_account_name =	asset_manager_trading_name
	  , bank_account_number = CASE 
								WHEN bank_account_number IS NULL THEN NULL
								ELSE RIGHT('99999999' + CONVERT(VARCHAR, id % 234011), 8)
							END
	  , bank_account_sort_code = CASE
									WHEN bank_account_sort_code IS NULL THEN NULL
									ELSE RIGHT('99999999' + CONVERT(VARCHAR, id % 234011), 6)
								END

	SELECT	@Rowcount = @@ROWCOUNT;
    PRINT '   Rows updated: ' + CAST(@Rowcount AS VARCHAR(10));

	SET @endTime = GETDATE();
	SET @StepMessage = 'Block 2 - Finish. Time Taken - ' + CONVERT(VARCHAR(20), @endTime - @startTime, 114);
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;

END TRY
BEGIN CATCH
    SELECT 
          @ErrorMessage = @StepMessage + ' - ' + ERROR_MESSAGE();

    RAISERROR(@ErrorMessage, 16, 1);
END CATCH;
END;
