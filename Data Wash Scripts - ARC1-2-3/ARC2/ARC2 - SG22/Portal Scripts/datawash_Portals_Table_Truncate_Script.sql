USE master;
/* 
    (c) Aegon DS 2018
    
	Data wash script for Adviser and Customer Index databases to truncate tables
	that are not requried post a refresh.

	Note that three part naming is required as this will be run from the master db

	Dec-2018 mdobbs    Use autocommit and chunk deletes to reduce the impact on the transaction log.
*/
SET NOCOUNT ON;
SET XACT_ABORT ON;
--SET IMPLICIT_TRANSACTIONS OFF;
GO


BEGIN TRY

    /* variables */
    DECLARE @StepMessage VARCHAR(300);
    DECLARE @SQL NVARCHAR(MAX) = '';
    DECLARE @DebugFlag BIT = 0; /* flag = 1 then verbose logging and the script will run with rollback */

    /* table to hold table list for those that neeed to be truncated */
    DECLARE @TablesToTruncate TABLE
        (
            TruncateOrder INT IDENTITY(1, 1)
          , TableName NVARCHAR(100)
          , ActionToTake VARCHAR(20)
        );

    SET @StepMessage = CONVERT(VARCHAR(23), GETDATE(), 121) + ': Starting Truncation script';
    PRINT @StepMessage;


    /* populate list of tables */
    INSERT INTO
        @TablesToTruncate
        (
            TableName
          , ActionToTake
        )
    VALUES
-- SQL Prompt formatting off
	    (N'ToWash_PRA_AdminSecurityData.dbo.SystemLog', 'TRUNCATE'),
		(N'ToWash_PRA_AdvPortal.dbo.S2S_Illustration_Reference', 'TRUNCATE'),
		(N'ToWash_PRA_AdvPortal.dbo.SystemLog', 'TRUNCATE'),
		(N'ToWash_PRA_AdvPortal.dbo.UserWizardStatusAudit', 'TRUNCATE'),
		(N'ToWash_PRA_AdvPortal.dbo.UserWizardScreen', 'TRUNCATE'),
		(N'ToWash_PRA_AdvPortal.dbo.UserWizardDataComponentAudit','TRUNCATE'),
		(N'ToWash_PRA_AdvPortal.dbo.UserWizardDataComponent', 'DELETE'),
		(N'ToWash_PRA_AdvPortal.dbo.UserWizardRelatedDocument', 'DELETE'),
		(N'ToWash_PRA_AdvPortal.dbo.UserWizard', 'DELETE'),
		(N'ToWash_PRA_AdvPortal.dbo.WORLDPAY_ORDER', 'DELETE'),
		(N'ToWash_PRA_AntiMoneyLaundering.dbo.SystemLog', 'TRUNCATE'),
		(N'ToWash_PRA_AntiMoneyLaundering.dbo.IdentificationCheckAudit', 'TRUNCATE'),
		(N'ToWash_PRA_AntiMoneyLaundering.dbo.ComposerUpdateAudit', 'TRUNCATE'),
		(N'ToWash_PRA_AntiMoneyLaundering.dbo.Client', 'DELETE'),
		(N'ToWash_PRA_AntiMoneyLaundering.dbo.Batch', 'DELETE')
		
		
-- SQL Prompt formatting on


    /* select the row count before */
    SELECT
        @SQL = @SQL + N'SELECT  ''' + tt.TableName + N''' AS [Table], format(COUNT(*), ''#,##0'') as [Row count before]  FROM ' + tt.TableName + N' UNION ALL '
    FROM
        @TablesToTruncate AS tt
    ORDER BY
        TruncateOrder;


    SET @SQL = LEFT(@SQL, LEN(@SQL) - 10);
    IF @DebugFlag = 1
        PRINT @SQL;
    EXEC sys.sp_executesql @SQL;

  --  IF @DebugFlag = 1
		--BEGIN TRANSACTION;

    /* prep statement to truncate the tables */
    SET @SQL = 'DECLARE @RowCount INT, @BatchSize INT = 100000;';
    SELECT
        @SQL = @SQL + CASE
                          WHEN [tt].[ActionToTake] = 'TRUNCATE' THEN N'TRUNCATE TABLE  ' + [tt].[TableName] + N';'
                          ELSE N'WHILE 1 = 1 BEGIN DELETE TOP (@BatchSize) FROM ' + [tt].[TableName] + N' WITH(TABLOCKX); SET @RowCount = @@ROWCOUNT; IF @RowCount = 0 BREAK;END;'
                      END
    FROM
        @TablesToTruncate AS [tt]
    ORDER BY
        TruncateOrder;



    IF @DebugFlag = 1
        PRINT @SQL;
    EXEC sys.sp_executesql @SQL;

    /* select row count afterwards*/
    SET @SQL = '';
    SELECT
        @SQL = @SQL + N'SELECT  ''' + tt.TableName + N''' AS [Table], format(COUNT(*), ''#,##0'') as [Row count after]  FROM ' + tt.TableName + N' UNION ALL '
    FROM
        @TablesToTruncate AS tt
    ORDER BY
        TruncateOrder;


    SET @SQL = LEFT(@SQL, LEN(@SQL) - 10);
    IF @DebugFlag = 1
        PRINT @SQL;
    EXEC sys.sp_executesql @SQL;

  --  IF @DebugFlag = 1
		--ROLLBACK TRANSACTION;

    SET @StepMessage = CONVERT(VARCHAR(23), GETDATE(), 121) + ': Truncation script completed';
    PRINT @StepMessage;

END TRY
BEGIN CATCH

    PRINT CONVERT(VARCHAR(23), GETDATE(), 121) + 'An error has occurred datawashing the data. Last successful step: ' + @StepMessage;
    PRINT ERROR_MESSAGE();
    --IF XACT_STATE() = 1
    --    ROLLBACK TRANSACTION;

	RAISERROR(50001, 10, 127);

END CATCH;



