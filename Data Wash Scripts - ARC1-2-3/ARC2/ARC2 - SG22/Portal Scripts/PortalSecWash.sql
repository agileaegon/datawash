BEGIN
    SET NOCOUNT ON;

	DECLARE	@StepMessage  NVARCHAR(4000)
		  , @ErrorMessage NVARCHAR(4000)
		  , @Rowcount INT
		  , @startTime DATETIME
		  , @endTime DATETIME;

BEGIN TRY

    /*************************************** Remove production URLs ******************************************/
	SET @StepMessage = 'Block 1 - Start';
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;
	SET @startTime = GETDATE();

	--Remove the production URLs
	UPDATE 
		[dbo].[SERVICE_PROVIDER]
	SET 
		destinationUrl = 'https://www.153b.suffolklife.co.uk/SLSecurePortal/SAML/AssertionConsumerService.aspx'
	  , audienceUri = 'https://www.153b.suffolklife.co.uk'
	WHERE 
		id = 1

	SELECT	@Rowcount = @@ROWCOUNT;
    PRINT '   Rows updated: ' + CAST(@Rowcount AS VARCHAR(10));

	SET @endTime = GETDATE();
	SET @StepMessage = 'Block 1 - Finish. Time Taken - ' + CONVERT(VARCHAR(20), @endTime - @startTime, 114);
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;

	/*************************************** SSO_REQUESTS ******************************************/
	SET @StepMessage = 'Block 2 - Start';
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;
	SET @startTime = GETDATE();

	UPDATE 
		s
	SET 
		username = CASE 
						WHEN s.username is NULL THEN NULL
						ELSE au.username
					END
	FROM 
		SSO_REQUESTS s
		LEFT JOIN ADVISER_BINDINGS ab on s.thirdPartyIdentifier = ab.thirdPartyIdentifier
		LEFT JOIN ADVISER_USERS au on au.Adviser_User_Id = ab.username
 
	SELECT	@Rowcount = @@ROWCOUNT;
    PRINT '   Rows updated: ' + CAST(@Rowcount AS VARCHAR(10));

	SET @endTime = GETDATE();
	SET @StepMessage = 'Block 2 - Finish. Time Taken - ' + CONVERT(VARCHAR(20), @endTime - @startTime, 114);
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;

END TRY
BEGIN CATCH
    SELECT 
          @ErrorMessage = @StepMessage + ' - ' + ERROR_MESSAGE();

    RAISERROR(@ErrorMessage, 16, 1);
END CATCH;
END;
