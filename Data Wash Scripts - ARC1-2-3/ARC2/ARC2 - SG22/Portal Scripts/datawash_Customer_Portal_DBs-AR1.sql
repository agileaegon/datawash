BEGIN

	/*	Full version of test Data wash script for Users across the Portal Databases: PortalSec, CustPortal & AdvPortal (prospects)
		The script is run as a single update across databases to ensure consistancyand within a transaction to ensure ACID properties are maintained
		Note that the environment variable should be set for the required target test environment as this is used to prefix emails
	*/

	SET NOCOUNT ON;
	SET XACT_ABORT ON;
	SET IMPLICIT_TRANSACTIONS OFF;
	SET DATEFORMAT YMD;

BEGIN TRY

    /* variables and constants */
    DECLARE
		@EnvironmentPrefix VARCHAR(10) = 'AR1'
      , @StepMessage VARCHAR(300)
      , @Rowcount INT
      , @DatawashUser VARCHAR(30) = 'DATAWASH_' + CONVERT(VARCHAR(10), GETDATE(), 112)
      , @SQL NVARCHAR(MAX) = ''
      , @DebugFlag BIT = 1 /* if data before vs after is required =1, else set to 0 to suppress */
	  , @startTime DATETIME
	  , @endTime DATETIME
    ;

    DECLARE @TableUpdates TABLE
        (
            TableName NVARCHAR(200)
        );
    
	IF OBJECT_ID(N'tempdb..#UserKeys') IS NOT NULL
        DROP TABLE #UserKeys;

    CREATE TABLE #UserKeys
        (
            email VARCHAR(100)
          , UserId INT
          , UserGUID NVARCHAR(255)/* they do not use the uniqueidentifier datatype */
          , washed_email VARCHAR(100)
        );

	--Tables to update
	INSERT INTO
        @TableUpdates
        (
            TableName
        )
    VALUES
        ('ToWash_PRA_PortalSec.dbo.USERS')
      , ('ToWash_PRA_CustPortal.dbo.EMAIL_MESSAGE_AUDIT')
      , ('ToWash_PRA_CustPortal.dbo.ONE_TIME_ACCESS_AUDIT')
      , ('ToWash_PRA_CustPortal.dbo.RESET_PASSWORD_REQUEST')
      , ('ToWash_PRA_CustPortal.dbo.SESSION_DATA')
	  , ('ToWash_PRA_AdvPortal.dbo.Prospect');

    SET @StepMessage = CONVERT(VARCHAR(23), GETDATE(), 121) + ': Starting Datawash';
    PRINT @StepMessage;

    IF @DebugFlag = 1
        BEGIN
            SELECT
                @SQL = @SQL + N'SELECT TOP (20) ''' + TableName + N''' AS [Table before], * FROM ' + TableName + '; '
            FROM
                @TableUpdates;

            EXECUTE sys.sp_executesql @stmt = @SQL;
        END;

	/*********************************** Identify the records to update *************************************/
	SET @StepMessage = 'Block 1 - Start. Identify records to update';
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;
	SET @startTime = GETDATE();

    INSERT INTO
        #UserKeys
        (
            email
          , UserId
          , UserGUID
          , washed_email
        )
    /* not all aspnet_users are Advisers e.g. AUK Admin / System Administrator / AUK Admin, Back Office User */
    SELECT
        u.username
      , u.id
      , u.guid
      , washed_email = @EnvironmentPrefix + '_User_' + CAST(u.id AS VARCHAR(10)) + '@Aegon.co.uk'
    FROM
        ToWash_PRA_PortalSec.dbo.USERS u;
	
	SET @endTime = GETDATE();
	SET @StepMessage = 'Block 1 - Finish. Time Taken - ' + CONVERT(VARCHAR(20), @endTime - @startTime, 114);
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;

	/*********************************** PortalSec.dbo.USERS *************************************/
	SET @StepMessage = 'Block 2 - Start. Update PortalSec.dbo.USERS';
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;
	SET @startTime = GETDATE();

    BEGIN TRANSACTION;

    UPDATE
        u
    SET
        username = k.washed_email
      , name = 'User ' + CAST(id AS VARCHAR(10)) + ' Fullname'
    FROM
        ToWash_PRA_PortalSec.dbo.USERS AS u
        INNER JOIN #UserKeys AS k
            ON k.email = u.username;

    SELECT
        @Rowcount = @@ROWCOUNT;
    PRINT '   Rows updated: ' + CAST(@Rowcount AS VARCHAR(10));

	SET @endTime = GETDATE();
	SET @StepMessage = 'Block 2 - Finish. Time Taken - ' + CONVERT(VARCHAR(20), @endTime - @startTime, 114);
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;

	/*********************************** CustPortal.dbo.EMAIL_MESSAGE_AUDIT *************************************/
	SET @StepMessage = 'Block 3 - Start. Update CustPortal.dbo.EMAIL_MESSAGE_AUDIT';
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;
	SET @startTime = GETDATE();

    UPDATE
        u
    SET
        toEmailAddress = k.washed_email
    FROM
        ToWash_PRA_CustPortal.dbo.EMAIL_MESSAGE_AUDIT AS u
        INNER JOIN #UserKeys AS k
            ON k.email = u.toEmailAddress;

    SELECT
        @Rowcount = @@ROWCOUNT;
    PRINT '   Rows updated: ' + CAST(@Rowcount AS VARCHAR(10));

	SET @endTime = GETDATE();
	SET @StepMessage = 'Block 3 - Finish. Time Taken - ' + CONVERT(VARCHAR(20), @endTime - @startTime, 114);
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;

	/*********************************** CustPortal.dbo.ONE_TIME_ACCESS_AUDIT *************************************/
	SET @StepMessage = 'Block 4 - Start. Update CustPortal.dbo.ONE_TIME_ACCESS_AUDIT';
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;
	SET @startTime = GETDATE();

    UPDATE
        u
    SET
        emailAddress = CASE
                           WHEN u.emailAddress IS NOT NULL THEN k.washed_email
                           ELSE NULL
                       END
      , phoneNumber = CASE
                          WHEN u.phoneNumber IS NOT NULL THEN RIGHT('00000000000' + CAST(k.UserId AS VARCHAR(10)), 11)
                          ELSE NULL
                      END
    FROM
        ToWash_PRA_CustPortal.dbo.ONE_TIME_ACCESS_AUDIT AS u
        INNER JOIN #UserKeys AS k
            ON k.UserGUID = u.wuid;

    SELECT
        @Rowcount = @@ROWCOUNT;
    PRINT '   Rows updated: ' + CAST(@Rowcount AS VARCHAR(10));

	SET @endTime = GETDATE();
	SET @StepMessage = 'Block 4 - Finish. Time Taken - ' + CONVERT(VARCHAR(20), @endTime - @startTime, 114);
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;

	/*********************************** CustPortal.dbo.RESET_PASSWORD_REQUEST *************************************/
	SET @StepMessage = 'Block 5 - Start. Update CustPortal.dbo.RESET_PASSWORD_REQUEST';
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;
	SET @startTime = GETDATE();

    UPDATE
        u
    SET
        emailAddress = k.washed_email
    FROM
        ToWash_PRA_CustPortal.dbo.RESET_PASSWORD_REQUEST AS u
        INNER JOIN #UserKeys AS k
            ON k.email = u.emailAddress;

    SELECT
        @Rowcount = @@ROWCOUNT;
    PRINT '   Rows updated: ' + CAST(@Rowcount AS VARCHAR(10));

	SET @endTime = GETDATE();
	SET @StepMessage = 'Block 5 - Finish. Time Taken - ' + CONVERT(VARCHAR(20), @endTime - @startTime, 114);
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;

	/*********************************** CustPortal.dbo.SESSION_DATA *************************************/
	SET @StepMessage = 'Block 6 - Start. Update CustPortal.dbo.SESSION_DATA';
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;
	SET @startTime = GETDATE();

    UPDATE
        u
    SET
        value = k.washed_email
    FROM
        ToWash_PRA_CustPortal.dbo.SESSION_DATA AS u
        INNER JOIN #UserKeys AS k
            ON k.email = u.value
    WHERE
        u.name = 'Username';

    SELECT
        @Rowcount = @@ROWCOUNT;
    PRINT '   Rows updated: ' + CAST(@Rowcount AS VARCHAR(10));

	SET @endTime = GETDATE();
	SET @StepMessage = 'Block 6 - Finish. Time Taken - ' + CONVERT(VARCHAR(20), @endTime - @startTime, 114);
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;

	/*********************************** AdvPortal.dbo.Prospect *************************************/
	SET @StepMessage = 'Block 7 - Start. Update AdvPortal.dbo.Prospect';
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;
	SET @startTime = GETDATE();

    UPDATE
        p
    SET
        Name1 = 'Prospect_' + CAST(ProspectId AS VARCHAR(10)) + '_Forename'
      , Name2 = 'Prospect_' + CAST(ProspectId AS VARCHAR(10)) + '_Surname'
      , DateOfBirth = DATEADD(DAY, CONVERT(INT, RAND() * 100), DateOfBirth)
      , FormData = NULL
      , CreatedBy = @DatawashUser
      , ModifiedBy = @DatawashUser
      , ModifiedOn = GETDATE()
    FROM
        ToWash_PRA_AdvPortal.dbo.Prospect AS p;

    SELECT
        @Rowcount = @@ROWCOUNT;
    PRINT '   Rows updated: ' + CAST(@Rowcount AS VARCHAR(10));

	SET @endTime = GETDATE();
	SET @StepMessage = 'Block 7 - Finish. Time Taken - ' + CONVERT(VARCHAR(20), @endTime - @startTime, 114);
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;
	/*****************************************************************************************/

    IF @DebugFlag = 1
        BEGIN
            SELECT
                @SQL = @SQL + N'SELECT TOP (20) ''' + TableName + N''' AS [Table after], * FROM ' + TableName + '; '
            FROM
                @TableUpdates;

            EXECUTE sys.sp_executesql @stmt = @SQL;
        END;
    
	--ROLLBACK TRANSACTION;
    COMMIT TRANSACTION;

	/* clean up temp table(s) */
	IF OBJECT_ID(N'tempdb..#UserKeys') IS NOT NULL
    DROP TABLE #UserKeys;

END TRY
BEGIN CATCH

    PRINT CONVERT(VARCHAR(23), GETDATE(), 121) + ': An error has occurred datawashing the data. Last successful step: ' + @StepMessage;
    PRINT ERROR_MESSAGE();

    IF XACT_STATE() = 1
        ROLLBACK TRANSACTION;

	RAISERROR(50001, 10, 127);

END CATCH
END;