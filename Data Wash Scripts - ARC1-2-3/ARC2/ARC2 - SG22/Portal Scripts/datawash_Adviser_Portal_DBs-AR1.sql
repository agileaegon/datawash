BEGIN
	
	/*	Full version of test Data wash script for Advisers across the Portal Databases: AdvIndex, AdvPortal & PortalSec
		The script is run as a single update across databases to ensure consistency and within a transaction to ensure 
		ACID properties are maintained	*/

	SET NOCOUNT ON;
	SET XACT_ABORT ON;
	SET IMPLICIT_TRANSACTIONS OFF;
	SET DATEFORMAT YMD;

BEGIN TRY

    /* variables and constants */
    DECLARE
        @EnvironmentPrefix VARCHAR(3) = 'AR1'
      , @StepMessage VARCHAR(300)
      , @Rowcount INT
      , @DatawashUser VARCHAR(30) = 'DATAWASH_' + CONVERT(VARCHAR(10), GETDATE(), 112)
      , @SQL NVARCHAR(MAX) = ''
	  , @DebugFlag BIT = 0 /* if data before vs after is required =1, else set to 0 to suppress */
	  , @startTime DATETIME
	  , @endTime DATETIME
	  , @iStart INT
	  , @iEnd INT
	  , @iIncr INT
	  , @iCntr INT
    ;

	IF OBJECT_ID(N'tempdb..#AdviserKeys') IS NOT NULL
        DROP TABLE #AdviserKeys;

    CREATE TABLE #AdviserKeys
        (
            Adviser_Master_Id INT
          , Email VARCHAR(100)
          , UniqueValue INT
          , UserId UNIQUEIDENTIFIER
          , UserName VARCHAR(100)
          , LoweredUserName VARCHAR(100)
          , CBOIdentifier VARCHAR(20)
          , WashedEmail VARCHAR(100)
          , WashedDateOfBirth DATE
          , WashedForename VARCHAR(100)
          , WashedSurname VARCHAR(100)
        );

	DECLARE @TableUpdates TABLE
        (
            TableName NVARCHAR(200)
        );

	INSERT INTO
        @TableUpdates
        (
            TableName
        )
    VALUES
        ('ToWash_PRA_AdvIndex.dbo.ADVISER_MASTER')
      , ('ToWash_PRA_AdvIndex.dbo.ADVISER_EXT_REF')
      , ('ToWash_PRA_AdvPortal.dbo.aspnet_Users')
      , ('ToWash_PRA_AdvPortal.dbo.aspnet_Membership')
      , ('ToWash_PRA_PortalSec.dbo.ADVISER_USERS')
	  , ('ToWash_PRA_AdvPortal.dbo.S2S_AUDIT_ENTRY')
	  , ('ToWash_PRA_AdvPortal.dbo.UserProfile') ;

	IF @DebugFlag = 1
        BEGIN
            SELECT
                @SQL = @SQL + N'SELECT TOP (20) ''' + TableName + N''' AS [Table before], * FROM ' + TableName + '; '
            FROM
                @TableUpdates;

            PRINT @SQL;
            EXECUTE sys.sp_executesql @stmt = @SQL;
        END;

	/*********************************** Identify the records to update *************************************/
	SET @StepMessage = 'Block 1 - Start. Identify records to update';
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;
	SET @startTime = GETDATE();
    
	/* Get the list of data to update into temp table first then use for multiple updates. Currently using dbo.aspnet_Users.NumericUserId 
	as the unique key as it will exist for all users, some do not have corresponding ADVISER_MASTER rows */

    INSERT INTO
        #AdviserKeys
        (
            Adviser_Master_Id
          , Email
          , UniqueValue
          , UserId
          , UserName
          , LoweredUserName
          , WashedEmail
          , WashedDateOfBirth
          , WashedForename
          , WashedSurname
        )
    /* not all aspnet_users are Advisers e.g. AUK Admin / System Administrator / AUK Admin, Back Office User */
	SELECT DISTINCT
        COALESCE(am.Adviser_Master_Id, am1.Adviser_Master_Id)
      , COALESCE(am.email, am1.email)
      , u.NumericUserId /* used to set the Adviser unique number */
      , u.UserId
      , u.UserName
      , u.LoweredUserName
      , @EnvironmentPrefix + '_Adviser_' + CAST(u.NumericUserId AS VARCHAR(10)) + '@Aegon.co.uk'
      , DATEADD(DAY, CONVERT(INT, RAND() * 100), COALESCE(am.dateOfBirth, am1.dateOfBirth))
      , 'AdviserForename' + char(u.NumericUserId % 26 + 65)
	  , 'AdviserSurname' + char(u.NumericUserId % 26 + 65)
    FROM
        [ToWash_PRA_AdvPortal].dbo.aspnet_Users u
        LEFT JOIN [ToWash_PRA_AdvIndex].dbo.ADVISER_EXT_REF er
            ON (CAST(u.UserId AS VARCHAR(50)) = er.extRefName)
			AND er.externalRefType = 'ADAPT_PORTAL_ID'
		LEFT JOIN [ToWash_PRA_AdvIndex].dbo.ADVISER_MASTER AS am
            ON am.Adviser_Master_Id = er.Adviser_Master_Id
		LEFT JOIN [ToWash_PRA_AdvIndex].dbo.ADVISER_EXT_REF er1
			ON (CAST(u.NumericUserId AS VARCHAR(50)) = er1.extRefName)
			AND er1.externalRefType = 'ADAPT_PORTAL_ID'
        LEFT JOIN [ToWash_PRA_AdvIndex].dbo.ADVISER_MASTER AS am1
            ON am1.Adviser_Master_Id = er1.Adviser_Master_Id

	UNION

	-- Include users which only exist in AdviserIndex
	SELECT DISTINCT
		am.Adviser_Master_Id
	  , am.email
      , COALESCE(u.NumericUserId, u1.NumericUserId) /* used to set the Adviser unique number */
      , COALESCE(u.UserId, u1.UserId)
      , COALESCE(u.UserName, u1.UserName)
      , COALESCE(u.LoweredUserName, u1.LoweredUserName)
      , @EnvironmentPrefix + '_Adviser_' + CAST(am.Adviser_Master_Id AS VARCHAR(10)) + '_' +  CAST(er.id AS VARCHAR(10)) + '@Aegon.co.uk'
      , DATEADD(DAY, CONVERT(INT, RAND() * 100), am.dateOfBirth)
      , 'AdviserForename' + char(am.Adviser_Master_Id % 26 + 65)
	  , 'AdviserSurname' + char(am.Adviser_Master_Id % 26 + 65)
	FROM 
		[ToWash_PRA_AdvIndex].dbo.ADVISER_MASTER AS am
		LEFT JOIN [ToWash_PRA_AdvIndex].dbo.ADVISER_EXT_REF er
			ON am.Adviser_Master_Id = er.adviser_master_id
            AND er.externalRefType = 'ADAPT_PORTAL_ID'
		LEFT JOIN [ToWash_PRA_AdvPortal].dbo.aspnet_Users u
			ON CAST(u.UserId AS VARCHAR(50)) = er.extRefName
		LEFT JOIN [ToWash_PRA_AdvPortal].dbo.aspnet_Users u1
			ON CAST(u1.NumericUserId AS VARCHAR(50)) = er.extRefName
	WHERE
		COALESCE(u.UserId, u1.UserId) IS NULL;

	SET @endTime = GETDATE();
	SET @StepMessage = 'Block 1 - Finish. Time Taken - ' + CONVERT(VARCHAR(20), @endTime - @startTime, 114);
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;

	/*********************************** Fix the duplicates *************************************/
	SET @StepMessage = 'Block 2 - Start. Fix duplicates';
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;
	SET @startTime = GETDATE();

	/*
	* Fix duplicates
	*
	* Problem:
	* The data contains single aspnet_Users records with more than 1 ADVISER_MASTER record via ADVISER_EXT_REF. In these cases, 
	* we will be assigning the same washed email to 2 different ADVISER_MASTER recordsand will break uniqueness.
	*
	* Solution:
	* Enumerate the records with ROW_NUMBER and use that ROW_NUMBER to make the washed email unique.
	*/

	WITH UserIdList AS
	(
		SELECT DISTINCT
			ak.Adviser_Master_Id
		  , ak.Email
		  , ak.LoweredUserName
		FROM
			#AdviserKeys ak
		WHERE
			EXISTS (
					SELECT
						1
					FROM
						#AdviserKeys akDuplicate
					WHERE
						akDuplicate.UniqueValue = ak.UniqueValue
						AND akDuplicate.Adviser_Master_Id <> ak.Adviser_Master_Id
					)
	),
	DuplicateList AS
	(
		SELECT
			Adviser_Master_Id
			, ROW_NUMBER() OVER(PARTITION BY Email ORDER BY Adviser_Master_Id ASC) RowNumber
		FROM
			UserIdList
		WHERE
			Email <> LoweredUserName
	)
	UPDATE
		ak
	SET
		WashedEmail = @EnvironmentPrefix + '_Adviser_' + CAST(ak.UniqueValue AS VARCHAR(10)) + '_' + CAST(dl.RowNumber AS VARCHAR(10)) + '@Aegon.co.uk'
	FROM
		#AdviserKeys ak
		INNER JOIN DuplicateList dl
			ON ak.Adviser_Master_Id = dl.Adviser_Master_Id;

    SET @endTime = GETDATE();
	SET @StepMessage = 'Block 2 - Finish. Time Taken - ' + CONVERT(VARCHAR(20), @endTime - @startTime, 114);
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;

	/*********************************** Adviser Master *************************************/
	SET @StepMessage = 'Block 3 - Start. Update AdvIndex.dbo.ADVISER_MASTER';
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;
	SET @startTime = GETDATE();

	BEGIN TRANSACTION;

    UPDATE
        am
    SET
        dateOfBirth = ak.WashedDateOfBirth
      , foreName = ak.WashedForename
      , surName = ak.WashedSurname
      , email = CASE
                    WHEN am.email IS NOT NULL THEN ak.WashedEmail
                    ELSE NULL
                END
	  , title = ''
	  , lastUpdatedBy = CASE
							WHEN am.lastUpdatedBy IS NULL THEN NULL
							ELSE 'DATAWASH_' + CONVERT(VARCHAR(10), GETDATE(), 112)
						END
    FROM
        [ToWash_PRA_AdvIndex].dbo.ADVISER_MASTER AS am
        INNER JOIN #AdviserKeys ak
            ON am.Adviser_Master_Id = ak.Adviser_Master_Id
               AND am.email = ak.Email;

    SELECT
        @Rowcount = @@ROWCOUNT;
    PRINT '   Rows updated: ' + CAST(@Rowcount AS VARCHAR(10));
	
	SET @endTime = GETDATE();
	SET @StepMessage = 'Block 3 - Finish. Time Taken - ' + CONVERT(VARCHAR(20), @endTime - @startTime, 114);
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;

	/*********************************** Adviser Ext Ref *************************************/
	SET @StepMessage = 'Block 4 - Start. Update AdvIndex.dbo.ADVISER_EXT_REF';
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;
	SET @startTime = GETDATE();

    UPDATE
        aef
    SET
        extRefName = ak.WashedEmail
      , lastUpdatedTime = GETDATE()
      , lastUpdatedBy = @DatawashUser
    FROM
        [ToWash_PRA_AdvIndex].dbo.ADVISER_EXT_REF AS aef
        INNER JOIN #AdviserKeys AS ak
            ON aef.Adviser_Master_Id = ak.Adviser_Master_Id
    WHERE
        aef.externalRefType = 'SECURITY_DB_ID';

    SELECT
        @Rowcount = @@ROWCOUNT;
    PRINT '   Rows updated: ' + CAST(@Rowcount AS VARCHAR(10));

	SET @endTime = GETDATE();
	SET @StepMessage = 'Block 4 - Finish. Time Taken - ' + CONVERT(VARCHAR(20), @endTime - @startTime, 114);
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;

	/*********************************** Adviser Ver Code *************************************/
	SET @StepMessage = 'Block 5 - Start. Update AdvIndex.dbo.ADVISER_VER_CODE';
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;
	SET @startTime = GETDATE();

    UPDATE
        avc
    SET
        lastUpdatedBy = @DatawashUser
    FROM
        [ToWash_PRA_AdvIndex].dbo.ADVISER_VER_CODE AS avc;

    SELECT
        @Rowcount = @@ROWCOUNT;
    PRINT '   Rows updated: ' + CAST(@Rowcount AS VARCHAR(10));

	SET @endTime = GETDATE();
	SET @StepMessage = 'Block 5 - Finish. Time Taken - ' + CONVERT(VARCHAR(20), @endTime - @startTime, 114);
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;

	/*********************************** Aspnet Users *************************************/
	SET @StepMessage = 'Block 6 - Start. Update AdvPortal.dbo.aspnet_Users';
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;
	SET @startTime = GETDATE();

    UPDATE
        u
    SET
        UserName = ak.WashedEmail
      , LoweredUserName = LOWER(ak.WashedEmail)
    FROM
        [ToWash_PRA_AdvPortal].dbo.aspnet_Users AS u
        INNER JOIN #AdviserKeys AS ak
            ON u.UserId = ak.UserId;

    SELECT
        @Rowcount = @@ROWCOUNT;
    PRINT '   Rows updated: ' + CAST(@Rowcount AS VARCHAR(10));

	SET @endTime = GETDATE();
	SET @StepMessage = 'Block 6 - Finish. Time Taken - ' + CONVERT(VARCHAR(20), @endTime - @startTime, 114);
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;

	/*********************************** Aspnet Membership *************************************/
	SET @StepMessage = 'Block 7 - Start. Update AdvPortal.dbo.aspnet_Membership';
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;
	SET @startTime = GETDATE();

    UPDATE
        m
    SET
        Email = ak.WashedEmail
      , LoweredEmail = LOWER(ak.WashedEmail)
      , [Password] = 'Password'
      , PasswordSalt = 'Password'
    FROM
        [ToWash_PRA_AdvPortal].dbo.aspnet_Membership AS m
        INNER JOIN #AdviserKeys AS ak
            ON m.UserId = ak.UserId;

    SELECT
        @Rowcount = @@ROWCOUNT;
    PRINT '   Rows updated: ' + CAST(@Rowcount AS VARCHAR(10));

	SET @endTime = GETDATE();
	SET @StepMessage = 'Block 7 - Finish. Time Taken - ' + CONVERT(VARCHAR(20), @endTime - @startTime, 114);
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;

	/*********************************** PortalSec.dbo.ADVISER_USERS *************************************/
	SET @StepMessage = 'Block 8 - Start. Update PortalSec.dbo.ADVISER_USERS';
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;
	SET @startTime = GETDATE();

    UPDATE
        u
    SET
        username = ak.WashedEmail
    FROM
        [ToWash_PRA_PortalSec].dbo.ADVISER_USERS AS u
        INNER JOIN #AdviserKeys AS ak
            ON u.username = ak.Email;

    SELECT
        @Rowcount = @@ROWCOUNT;
    PRINT '   Rows updated: ' + CAST(@Rowcount AS VARCHAR(10));

	SET @endTime = GETDATE();
	SET @StepMessage = 'Block 8 - Finish. Time Taken - ' + CONVERT(VARCHAR(20), @endTime - @startTime, 114);
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;

	/*********************************** PRA_PortalSec.dbo.ADVISER_USERS *************************************/
	SET @StepMessage = 'Block 9 - Start. Updating remaining data in PortalSec.dbo.ADVISER_USERS';
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;
	SET @startTime = GETDATE();

    UPDATE
        u
    SET
        username = @EnvironmentPrefix + '_AdviserUser_' + CAST(Adviser_User_id AS VARCHAR(10)) + '@Aegon.co.uk'
    FROM
        [ToWash_PRA_PortalSec].dbo.ADVISER_USERS AS u
    WHERE
        u.username NOT LIKE '%@Aegon.co.uk';

    SELECT
        @Rowcount = @Rowcount + @@ROWCOUNT;
    PRINT '   Rows updated: ' + CAST(@Rowcount AS VARCHAR(10));

	SET @endTime = GETDATE();
	SET @StepMessage = 'Block 9 - Finish. Time Taken - ' + CONVERT(VARCHAR(20), @endTime - @startTime, 114);
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;

	/*********************************** User Profile *************************************/
	SET @StepMessage = 'Block 10 - Start. Updating adviser data in AdvPortal.dbo.UserProfile';
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;
	SET @startTime = GETDATE();

    UPDATE
        up
    SET
        VALUE = CASE
                    WHEN upf.ShortCode = 'FULLNAME'
                         AND up.Value IS NOT NULL THEN ak.WashedForename + ' ' + ak.WashedSurname
                    WHEN upf.ShortCode = 'USER_FIRSTNAME'
                         AND up.Value IS NOT NULL THEN ak.WashedForename
                    WHEN upf.ShortCode = 'USER_SURNAME'
                         AND up.Value IS NOT NULL THEN ak.WashedSurname
                    WHEN upf.ShortCode = 'USER_DATE_OF_BIRTH'
                         AND up.Value IS NOT NULL THEN CONVERT(VARCHAR(10), ak.WashedDateOfBirth, 121)
                    WHEN upf.ShortCode = 'USER_TELEPHONE'
                         AND up.Value IS NOT NULL THEN RIGHT('00000000000' + CAST(ak.UniqueValue AS VARCHAR(10)), 11)
                    WHEN upf.ShortCode = 'USER_MOBILE'
                         AND up.Value IS NOT NULL THEN RIGHT('01000000000' + CAST(ak.UniqueValue AS VARCHAR(10)), 11)
                    ELSE up.Value
                END
    FROM
        [ToWash_PRA_AdvPortal].dbo.UserProfile AS up
        INNER JOIN [ToWash_PRA_AdvPortal].dbo.UserProfileField AS upf
            ON up.UserProfileFieldId = upf.UserProfileFieldId
        INNER JOIN #AdviserKeys AS ak
            ON ak.UserId = up.UserId;

    SELECT
        @Rowcount = @Rowcount + @@ROWCOUNT;
    PRINT '   Rows updated: ' + CAST(@Rowcount AS VARCHAR(10));

	SET @endTime = GETDATE();
	SET @StepMessage = 'Block 10 - Finish. Time Taken - ' + CONVERT(VARCHAR(20), @endTime - @startTime, 114);
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;
	
	/*********************************** User Profile *************************************/
	SET @StepMessage = 'Block 11 - Start. Updating change user details in AdvPortal.dbo.UserProfile';
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;
	SET @startTime = GETDATE();

	SELECT 
       @iStart = min(UserProfileId),
       @iCntr = max(UserProfileId)
	FROM 
       [ToWash_PRA_AdvPortal].dbo.UserProfile;

	SELECT 
		@iIncr = 20000,
		@iEnd = @iStart + @iIncr;
	
	WHILE @iStart <= @iCntr
	BEGIN
		UPDATE 
			up
		SET 
			CreatedBy = 'DATAWASH_' + CONVERT(VARCHAR(10), GETDATE(), 112)
		  , ModifiedBy = 'DATAWASH_' + CONVERT(VARCHAR(10), GETDATE(), 112)
		FROM
			[ToWash_PRA_AdvPortal].dbo.UserProfile up
		WHERE 	
			up.UserProfileId between @iStart and @iEnd
 
		SELECT 
			@iStart = @iEnd +1,
			@iEnd = @iEnd + @iIncr
	END

	SET @endTime = GETDATE();
	SET @StepMessage = 'Block 11 - Finish. Time Taken - ' + CONVERT(VARCHAR(20), @endTime - @startTime, 114);
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;

	/*********************************** User Widget *************************************/
	SET @StepMessage = 'Block 12 - Start. Updating change user details in AdvPortal.dbo.UserWidget';
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;
	SET @startTime = GETDATE();

	SELECT 
       @iStart = min(UserWidgetId),
       @iCntr = max(UserWidgetId)
	FROM 
       [ToWash_PRA_AdvPortal].dbo.UserWidget;

	SELECT 
		@iIncr = 20000,
		@iEnd = @iStart + @iIncr;
	
	WHILE @iStart <= @iCntr
	BEGIN
		UPDATE 
			uw	
		SET 
			CreatedBy = 'DATAWASH_' + CONVERT(VARCHAR(10), GETDATE(), 112)
		  , ModifiedBy = 'DATAWASH_' + CONVERT(VARCHAR(10), GETDATE(), 112)
		FROM
			[ToWash_PRA_AdvPortal].dbo.UserWidget uw
		WHERE 	
			uw.UserWidgetId between @iStart and @iEnd
 
		SELECT 
			@iStart = @iEnd +1,
			@iEnd = @iEnd + @iIncr
	END

	SET @endTime = GETDATE();
	SET @StepMessage = 'Block 12 - Finish. Time Taken - ' + CONVERT(VARCHAR(20), @endTime - @startTime, 114);
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;

	/*********************************** User Widget Configuration *************************************/
	SET @StepMessage = 'Block 13 - Start. Updating change user details in AdvPortal.dbo.UserWidgetConfiguration';
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;
	SET @startTime = GETDATE();

	SELECT 
       @iStart = min(UserWidgetConfigurationId),
       @iCntr = max(UserWidgetConfigurationId)
	FROM 
       [ToWash_PRA_AdvPortal].dbo.UserWidgetConfiguration;

	SELECT 
		@iIncr = 100000,
		@iEnd = @iStart + @iIncr;
	
	WHILE @iStart <= @iCntr
	BEGIN
		UPDATE 
			uwc
		SET 
			CreatedBy = 'DATAWASH_' + CONVERT(VARCHAR(10), GETDATE(), 112)
		  , ModifiedBy = 'DATAWASH_' + CONVERT(VARCHAR(10), GETDATE(), 112)
		FROM
			[ToWash_PRA_AdvPortal].dbo.UserWidgetConfiguration uwc
		WHERE 	
			uwc.UserWidgetConfigurationId between @iStart and @iEnd
 
		SELECT 
			@iStart = @iEnd +1,
			@iEnd = @iEnd + @iIncr
	END

	SET @endTime = GETDATE();
	SET @StepMessage = 'Block 13 - Finish. Time Taken - ' + CONVERT(VARCHAR(20), @endTime - @startTime, 114);
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;

	/*********************************** User Widget Dashboard *************************************/
	SET @StepMessage = 'Block 14 - Start. Updating change user details in AdvPortal.dbo.UserWidgetDashboard';
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;
	SET @startTime = GETDATE();

	SELECT 
       @iStart = min(UserWidgetDashboardId),
       @iCntr = max(UserWidgetDashboardId)
	FROM 
       [ToWash_PRA_AdvPortal].dbo.UserWidgetDashboard;

	SELECT 
		@iIncr = 20000,
		@iEnd = @iStart + @iIncr;
	
	WHILE @iStart <= @iCntr
	BEGIN
		UPDATE 
			uwd
		SET 
			CreatedBy = 'DATAWASH_' + CONVERT(VARCHAR(10), GETDATE(), 112)
		  , ModifiedBy = 'DATAWASH_' + CONVERT(VARCHAR(10), GETDATE(), 112)
		FROM
			[ToWash_PRA_AdvPortal].dbo.UserWidgetDashboard uwd
		WHERE 	
			uwd.UserWidgetDashboardId between @iStart and @iEnd
 
		SELECT 
			@iStart = @iEnd +1,
			@iEnd = @iEnd + @iIncr
	END

	SET @endTime = GETDATE();
	SET @StepMessage = 'Block 14 - Finish. Time Taken - ' + CONVERT(VARCHAR(20), @endTime - @startTime, 114);
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;

	/*********************************** User Widget Dashboard Layout *************************************/
	SET @StepMessage = 'Block 15 - Start. Updating change user details in AdvPortal.dbo.UserWidgetDashboardLayout';
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;
	SET @startTime = GETDATE();

	SELECT 
       @iStart = min(UserWidgetDashboardLayoutId),
       @iCntr = max(UserWidgetDashboardLayoutId)
	FROM 
       [ToWash_PRA_AdvPortal].dbo.UserWidgetDashboardLayout;

	SELECT 
		@iIncr = 20000,
		@iEnd = @iStart + @iIncr;
	
	WHILE @iStart <= @iCntr
	BEGIN
		UPDATE 
			uwdl
		SET 
			CreatedBy = 'DATAWASH_' + CONVERT(VARCHAR(10), GETDATE(), 112)
		  , ModifiedBy = 'DATAWASH_' + CONVERT(VARCHAR(10), GETDATE(), 112)
		FROM
			[ToWash_PRA_AdvPortal].dbo.UserWidgetDashboardLayout uwdl
		WHERE 	
			uwdl.UserWidgetDashboardLayoutId between @iStart and @iEnd
 
		SELECT 
			@iStart = @iEnd +1,
			@iEnd = @iEnd + @iIncr
	END

	SET @endTime = GETDATE();
	SET @StepMessage = 'Block 15 - Finish. Time Taken - ' + CONVERT(VARCHAR(20), @endTime - @startTime, 114);
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;

	/*********************************** User Widget Layout *************************************/
	SET @StepMessage = 'Block 16 - Start. Updating change user details in AdvPortal.dbo.UserWidgetLayout';
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;
	SET @startTime = GETDATE();

	SELECT 
       @iStart = min(UserWidgetLayoutId),
       @iCntr = max(UserWidgetLayoutId)
	FROM 
       [ToWash_PRA_AdvPortal].dbo.UserWidgetLayout;

	SELECT 
		@iIncr = 20000,
		@iEnd = @iStart + @iIncr;
	
	WHILE @iStart <= @iCntr
	BEGIN
		UPDATE 
			uwl
		SET 
			CreatedBy = 'DATAWASH_' + CONVERT(VARCHAR(10), GETDATE(), 112)
		  , ModifiedBy = 'DATAWASH_' + CONVERT(VARCHAR(10), GETDATE(), 112)
		FROM
			[ToWash_PRA_AdvPortal].dbo.UserWidgetLayout uwl
		WHERE 	
			uwl.UserWidgetLayoutId between @iStart and @iEnd
 
		SELECT 
			@iStart = @iEnd +1,
			@iEnd = @iEnd + @iIncr
	END

	SET @endTime = GETDATE();
	SET @StepMessage = 'Block 16 - Finish. Time Taken - ' + CONVERT(VARCHAR(20), @endTime - @startTime, 114);
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;

	/*********************************** User Widget Notification *************************************/
	SET @StepMessage = 'Block 17 - Start. Updating change user details in AdvPortal.dbo.UserWidgetNotification';
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;
	SET @startTime = GETDATE();

	SELECT 
       @iStart = min(UserWidgetNotificationId),
       @iCntr = max(UserWidgetNotificationId)
	FROM 
       [ToWash_PRA_AdvPortal].dbo.UserWidgetNotification;

	SELECT 
		@iIncr = 20000,
		@iEnd = @iStart + @iIncr;
	
	WHILE @iStart <= @iCntr
	BEGIN
		UPDATE 
			uwn
		SET 
			CreatedBy = 'DATAWASH_' + CONVERT(VARCHAR(10), GETDATE(), 112)
		  , ModifiedBy = 'DATAWASH_' + CONVERT(VARCHAR(10), GETDATE(), 112)
		FROM
			[ToWash_PRA_AdvPortal].dbo.UserWidgetNotification uwn
		WHERE 	
			uwn.UserWidgetNotificationId between @iStart and @iEnd
 
		SELECT 
			@iStart = @iEnd +1,
			@iEnd = @iEnd + @iIncr
	END

	SET @endTime = GETDATE();
	SET @StepMessage = 'Block 17 - Finish. Time Taken - ' + CONVERT(VARCHAR(20), @endTime - @startTime, 114);
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;

	/*********************************** S2S_Audit_Entry *************************************/
	SET @StepMessage = 'Block 18 - Start. Update AdvPortal.dbo.S2S_AUDIT_ENTRY';
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;
	SET @startTime = GETDATE();

	/* Need to set requestXML & responseXML columns to NULL. Updating the table is really slow, so dropping and re-adding the columns */
	ALTER TABLE [ToWash_PRA_AdvPortal].dbo.S2S_AUDIT_ENTRY
	DROP COLUMN requestXML, responseXml

	ALTER TABLE [ToWash_PRA_AdvPortal].dbo.S2S_AUDIT_ENTRY
	ADD requestXML NVARCHAR(MAX) NULL, responseXml NVARCHAR(MAX) NULL

	SET @endTime = GETDATE();
	SET @StepMessage = 'Block 18 - Finish. Time Taken - ' + CONVERT(VARCHAR(20), @endTime - @startTime, 114);
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;
	
	IF @DebugFlag = 1
        BEGIN
            SET @SQL = '';
            SELECT
                @SQL = @SQL + N'SELECT TOP (20) ''' + TableName + N''' AS [Table after], * FROM ' + TableName + '; '
            FROM
                @TableUpdates;

            /* print @SQL */
            EXECUTE sys.sp_executesql @stmt = @SQL;
		END;

    --ROLLBACK TRANSACTION;
    COMMIT TRANSACTION;

	/*********************************** Clean up *************************************/
	/* clean up temp table(s) */
	IF OBJECT_ID(N'tempdb..#AdviserKeys') IS NOT NULL
		DROP TABLE #AdviserKeys;
END TRY
BEGIN CATCH

    PRINT CONVERT(VARCHAR(23), GETDATE(), 121) + ': An error has occurred datawashing the data. Last successful step: ' + @StepMessage;
    PRINT ERROR_MESSAGE();

    IF XACT_STATE() = 1
        ROLLBACK TRANSACTION;

	RAISERROR(50001, 10, 127);

END CATCH
END;