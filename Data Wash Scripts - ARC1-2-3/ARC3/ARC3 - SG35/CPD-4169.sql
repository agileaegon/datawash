/************************************************************************************************************************************/
/*                                                                                                                                  */
/* Requirement: New dummy adviser ID and hierarchy for Customer Portal(NBS)																*/
/* Date:       OCT  11 2018  4:32PM                                                                                                 */
/* Author(s):  Ani Maitra                                                                                                        */
/*                                                                                                                                  */
/* Description: New dummy adviser ID and hierarchy for Customer Portal(NBS)																*/
/*                                                                                                                                  */
/*                                                                                                                                  */
/************************************************************************************************************************************/

/* Define variables */
Declare
		 @lNextKeySequenceAdvEntityId 	INT
		, @lNextKeySequenceEntityId2	INT
		, @lNextKeySequenceEntityId3	INT
		, @lNextKeySequencebankId       INT
		, @lNextKeyAddId                INT
		, @KeySeqDealId                 INT
		, @KeySeqDBId                   INT
		, @numKeys						INT
		, @rc1							INT
		, @dealerId						INT
		, @dealerBrchId					INT		
		, @v_refcode					VARCHAR(255)
		, @v_refcode_id					INT
		, @v_refcode_found				INT
		, @v_refcode_values_found		INT
		, @v_previous_run				INT
		, @v_error_code					INT
		, @v_error_msg					VARCHAR(255)

	BEGIN Tran

	/* Check if the script has been run previously                                                                                       */
	   SELECT @v_previous_run = COUNT (*)
	   FROM auk_script_control
	   WHERE script_name ='4169_New dummy adviser ID(NBS) and hierarchy for Customer Portal.sql' AND
			 run_number = 1

	IF ( @@error <> 0 )
	   BEGIN
		  SELECT @v_error_code = -1, @v_error_msg ='RC10: Error in SELECT of 4169_New dummy adviser ID(NBS) and hierarchy for Customer Portal.sql'
		  GOTO ExitRoutine
	   END

	/* Show if previous run has been identified - Should be zero                                                                         */
	   SELECT @v_previous_run AS 'Run Number'
	   IF ( @@error <> 0 )
	   BEGIN
		SELECT @v_error_code = -1, @v_error_msg =' RC20: Error in SELECT of 4169_New dummy adviser ID(NBS) and hierarchy for Customer Portal.sql'
		  GOTO ExitRoutine
	   END

	/* If @v_previous_run is NOT ZERO - Abandon the script execution                                                                     */
	   IF (@v_previous_run > 0)
	   BEGIN
		  SELECT @v_error_code = -1, @v_error_msg = ' RC30: ABANDON RUN - Script has been run previously - 4169_New dummy adviser ID(NBS) and hierarchy for Customer Portal.sql'
		  GOTO ExitRoutine
	   END

	/* Show the current key_sequence value for entity before insert                                                                  */
		Select * 
		FROM key_sequence
		WHERE table_name = 'entity_id'

		IF ( @@error <> 0)
		BEGIN
			SELECT @v_error_code = -1, @v_error_msg = 'RC0060: Error calling sp_get_unique_key_block of 4169_New dummy adviser ID(NBS) and hierarchy for Customer Portal.sql'
			GOTO ExitRoutine
		END

		EXEC @rc1 =  sp_get_unique_key_block @as_code_key = 'entity_id',  @ai_numkeys = 1, @oi_startkey = @lNextKeySequenceAdvEntityId  OUT
		EXEC @rc1 =  sp_get_unique_key_block @as_code_key = 'entity_id',  @ai_numkeys = 4, @oi_startkey = @lNextKeySequenceEntityId2  OUT
		EXEC @rc1 =  sp_get_unique_key_block @as_code_key = 'entity_id',  @ai_numkeys = 1, @oi_startkey = @lNextKeySequenceEntityId3  OUT
   
   
   
   /* Show the current key_sequence value for bank_account before insert                                                                  */
		Select * 
		FROM key_sequence
		WHERE table_name = 'bank_account'

		IF ( @@error <> 0)
		BEGIN
			SELECT @v_error_code = -1, @v_error_msg = 'RC0060: Error calling sp_get_unique_key_block of 4169_New dummy adviser ID(NBS) and hierarchy for Customer Portal.sql'
			GOTO ExitRoutine
		END

		EXEC @rc1 =  sp_get_unique_key_block @as_code_key = 'bank_account_id',  @ai_numkeys = 1, @oi_startkey = @lNextKeySequencebankId  OUT
		
		
/* Show the current key_sequence value for bank_account before insert                                                                  */
		Select * 
		FROM key_sequence
		WHERE table_name = 'address'

		IF ( @@error <> 0)
		BEGIN
			SELECT @v_error_code = -1, @v_error_msg = 'RC0060: Error calling sp_get_unique_key_block of 4169_New dummy adviser ID(NBS) and hierarchy for Customer Portal.sql'
			GOTO ExitRoutine
		END

		EXEC @rc1 =  sp_get_unique_key_block @as_code_key = 'address_id',  @ai_numkeys = 6, @oi_startkey = @lNextKeyAddId  OUT

      --Key sequence check
	  
	  
	    Select @KeySeqDealId = key_sequence from key_sequence where table_name = 'dealer'
		
		IF ( @KeySeqDealId > 200051)
		BEGIN
			SELECT @v_error_code = -1, @v_error_msg = 'RC0120: Error Dealer Key sequence value less than 200051'
			GOTO ExitRoutine
		END
   
   
       Select @KeySeqDBId = key_sequence from key_sequence where table_name = 'dealer_branch'
		
		IF ( @KeySeqDBId > 400051)
		BEGIN
			SELECT @v_error_code = -1, @v_error_msg = 'RC0140: Error Dealer Branch Key sequence value less than 400051'
			GOTO ExitRoutine
		END
   
   

		--Adviser Entity
		Insert into entity(entity_id, entity_type, old_system_reference, occupation, hazardous_occupation_id, anzsic_code, title_id, name, given_names, preferred_name, salutation, gender, acn_number, home_phone_number, work_phone_number, mobile_number, fax_number, email_address, birth_date, deceased_date, tax_file_number, tfn_flag, password, retirement_date, pref_correspondence_method, next_correspondence_method, annual_salary, salary_date, previous_benefit_amount, reporting_frequency_flag, preferred_payment_type, payee_name, bank_account_id, combined_gc_flag, birthplace, user_id, can_pass_on_tfn_flag, service_priority_flag, last_used_bank_account_id, transitional_pension_rbl, transitional_lumpsum_rbl, preferred_name_type, returned_mail_flag, contact_name, topup_flag, client_type, title_text, short_name, resident_status, residence_country_code, advertising_material_flag, returned_mail_datetime, dormant_flag, dormant_since_datetime, super_fund_flag, entity_stopped_flag, entity_stopped_datetime, fax_indemnity_flag, email_indemnity_flag, tfn_exemption_id, mail_returned_date1, mail_returned_date2, locator_code, abn, abn_flag, rcti_agreement_flag, mailing_title, company_rego_no, tax_code, direct_pay_type_code, investment_qualification_level, aml_status, birth_country_code, fatca_status, giin_status, giin, giin_verified_flag, grouping_code, money_purchase_applies_flag, money_purchase_trigger_date, deceased_date_status, deceased_notification_date, fcds_status, reporting_currency, pref_advertising_method, isa_aps_allowance, isa_aps_expiry_date,anonymised_flag)       
        																																												
        select @lNextKeySequenceAdvEntityId,'I',NULL,NULL,NULL,NULL,NULL,'AUK NBS Live Proving Adviser','AUK',NULL,NULL,'N',NULL,NULL,'0345 2720089',NULL,NULL,'aegonipsadministration@aegon.co.uk',29221,NULL,NULL,'N',NULL,NULL,'M','M',NULL,NULL,0,'A','Q',NULL,NULL,'N',NULL,NULL,'Y','N',NULL,0,0,NULL,NULL,NULL,'Y',NULL,NULL,NULL,NULL,NULL,'N',NULL,'N',NULL,'N','N', NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,'N','N',NULL,NULL,NULL,NULL,4,'X',NULL,'N','N','NULL','N',NULL,'N',NULL,NULL,NULL,NULL,'GBP',NULL,NULL,NULL,NULL
																																													


        
		IF ( @@error <> 0)
		BEGIN
			SELECT @v_error_code = -1, @v_error_msg = 'RC0160: Error calling sp_get_unique_key_block of 4169_New dummy adviser ID(NBS) and hierarchy for Customer Portal.sql'
			GOTO ExitRoutine
		END
		
			
		--Branch Entity
		Insert into entity(entity_id, entity_type, old_system_reference, occupation, hazardous_occupation_id, anzsic_code, title_id, name, given_names, preferred_name, salutation, gender, acn_number, home_phone_number, work_phone_number, mobile_number, fax_number, email_address, birth_date, deceased_date, tax_file_number, tfn_flag, password, retirement_date, pref_correspondence_method, next_correspondence_method, annual_salary, salary_date, previous_benefit_amount, reporting_frequency_flag, preferred_payment_type, payee_name, bank_account_id, combined_gc_flag, birthplace, user_id, can_pass_on_tfn_flag, service_priority_flag, last_used_bank_account_id, transitional_pension_rbl, transitional_lumpsum_rbl, preferred_name_type, returned_mail_flag, contact_name, topup_flag, client_type, title_text, short_name, resident_status, residence_country_code, advertising_material_flag, returned_mail_datetime, dormant_flag, dormant_since_datetime, super_fund_flag, entity_stopped_flag, entity_stopped_datetime, fax_indemnity_flag, email_indemnity_flag, tfn_exemption_id, mail_returned_date1, mail_returned_date2, locator_code, abn, abn_flag, rcti_agreement_flag, mailing_title, company_rego_no, tax_code, direct_pay_type_code, investment_qualification_level, aml_status, birth_country_code, fatca_status, giin_status, giin, giin_verified_flag, grouping_code, money_purchase_applies_flag, money_purchase_trigger_date, deceased_date_status, deceased_notification_date, fcds_status, reporting_currency, pref_advertising_method, isa_aps_allowance, isa_aps_expiry_date,anonymised_flag) 
		select @lNextKeySequenceEntityId2,'C',NULL,NULL,NULL,NULL,NULL,'Nationwide SFC',' ',NULL,'Dear Sir/Madam','N',NULL,NULL,'01737 374708',NULL,NULL,NULL,NULL,NULL,NULL,'N',NULL,NULL,'M','M',NULL,NULL,0,'A','Q',NULL,NULL,'N',NULL,NULL,NULL,'N',NULL,0,0,'N',NULL,NULL,'Y',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'N',NULL,'N','N',NULL,NULL,NULL,NULL,NULL,NULL,1,NULL,'N','N',NULL,NULL,NULL,NULL,NULL,'X',NULL,'N','N','NULL','N',NULL,'N',NULL,NULL,NULL,NULL,'GBP',NULL,NULL,NULL,NULL		
		Insert into entity(entity_id, entity_type, old_system_reference, occupation, hazardous_occupation_id, anzsic_code, title_id, name, given_names, preferred_name, salutation, gender, acn_number, home_phone_number, work_phone_number, mobile_number, fax_number, email_address, birth_date, deceased_date, tax_file_number, tfn_flag, password, retirement_date, pref_correspondence_method, next_correspondence_method, annual_salary, salary_date, previous_benefit_amount, reporting_frequency_flag, preferred_payment_type, payee_name, bank_account_id, combined_gc_flag, birthplace, user_id, can_pass_on_tfn_flag, service_priority_flag, last_used_bank_account_id, transitional_pension_rbl, transitional_lumpsum_rbl, preferred_name_type, returned_mail_flag, contact_name, topup_flag, client_type, title_text, short_name, resident_status, residence_country_code, advertising_material_flag, returned_mail_datetime, dormant_flag, dormant_since_datetime, super_fund_flag, entity_stopped_flag, entity_stopped_datetime, fax_indemnity_flag, email_indemnity_flag, tfn_exemption_id, mail_returned_date1, mail_returned_date2, locator_code, abn, abn_flag, rcti_agreement_flag, mailing_title, company_rego_no, tax_code, direct_pay_type_code, investment_qualification_level, aml_status, birth_country_code, fatca_status, giin_status, giin, giin_verified_flag, grouping_code, money_purchase_applies_flag, money_purchase_trigger_date, deceased_date_status, deceased_notification_date, fcds_status, reporting_currency, pref_advertising_method, isa_aps_allowance, isa_aps_expiry_date,anonymised_flag)		
		select @lNextKeySequenceEntityId2+1,'C',NULL,NULL,NULL,NULL,NULL,'Nationwide Direct Mail',' ',NULL,'Dear Sir/Madam','N',NULL,NULL,'01737 374708',NULL,NULL,NULL,NULL,NULL,NULL,'N',NULL,NULL,'M','M',NULL,NULL,0,'A','Q',NULL,NULL,'N',NULL,NULL,NULL,'N',NULL,0,0,'N',NULL,NULL,'Y',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'N',NULL,'N','N',NULL,NULL,NULL,NULL,NULL,NULL,1,NULL,'N','N',NULL,NULL,NULL,NULL,NULL,'X',NULL,'N','N','NULL','N',NULL,'N',NULL,NULL,NULL,NULL,'GBP',NULL,NULL,NULL,NULL
		Insert into entity(entity_id, entity_type, old_system_reference, occupation, hazardous_occupation_id, anzsic_code, title_id, name, given_names, preferred_name, salutation, gender, acn_number, home_phone_number, work_phone_number, mobile_number, fax_number, email_address, birth_date, deceased_date, tax_file_number, tfn_flag, password, retirement_date, pref_correspondence_method, next_correspondence_method, annual_salary, salary_date, previous_benefit_amount, reporting_frequency_flag, preferred_payment_type, payee_name, bank_account_id, combined_gc_flag, birthplace, user_id, can_pass_on_tfn_flag, service_priority_flag, last_used_bank_account_id, transitional_pension_rbl, transitional_lumpsum_rbl, preferred_name_type, returned_mail_flag, contact_name, topup_flag, client_type, title_text, short_name, resident_status, residence_country_code, advertising_material_flag, returned_mail_datetime, dormant_flag, dormant_since_datetime, super_fund_flag, entity_stopped_flag, entity_stopped_datetime, fax_indemnity_flag, email_indemnity_flag, tfn_exemption_id, mail_returned_date1, mail_returned_date2, locator_code, abn, abn_flag, rcti_agreement_flag, mailing_title, company_rego_no, tax_code, direct_pay_type_code, investment_qualification_level, aml_status, birth_country_code, fatca_status, giin_status, giin, giin_verified_flag, grouping_code, money_purchase_applies_flag, money_purchase_trigger_date, deceased_date_status, deceased_notification_date, fcds_status, reporting_currency, pref_advertising_method, isa_aps_allowance, isa_aps_expiry_date,anonymised_flag)
        select @lNextKeySequenceEntityId2+2,'C',NULL,NULL,NULL,NULL,NULL,'Nationwide Online',' ',NULL,'Dear Sir/Madam','N',NULL,NULL,'01737 374708',NULL,NULL,NULL,NULL,NULL,NULL,'N',NULL,NULL,'M','M',NULL,NULL,0,'A','Q',NULL,NULL,'N',NULL,NULL,NULL,'N',NULL,0,0,'N','Y',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'N',NULL,'N','N',NULL,NULL,NULL,NULL,NULL,NULL,1,NULL,'N','N',NULL,NULL,NULL,NULL,NULL,'X',NULL,'N','N',NULL,'N',NULL,'N',NULL,NULL,NULL,NULL,'GBP',NULL,NULL,NULL,NULL		
        --Select @lNextKeySequenceEntityId2+2,'C', NULL, NULL, NULL, NULL, NULL, 'Nationwide Online', ' ', NULL, 'Dear Sir/Madam', 'N', NULL, NULL, 01737374708, NULL, NULL, 'scrambled@noemail.nodomain', NULL, NULL, NULL, 'N', NULL, NULL, 'M', 'M', 0.00, NULL, 0.00, 'M', 'Q', NULL, NULL, 'Y', NULL, NULL, 'Y', 'N', NULL, 0.00, 0.00, 'A', 'Y', NULL, 'Y', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'N', NULL, 'Y', 'Y', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'X', NULL, 'N', 'N', NULL, 'N', NULL, 'N', NULL, NULL, NULL, NULL, 'GBP', NULL, NULL, NULL
		Insert into entity(entity_id, entity_type, old_system_reference, occupation, hazardous_occupation_id, anzsic_code, title_id, name, given_names, preferred_name, salutation, gender, acn_number, home_phone_number, work_phone_number, mobile_number, fax_number, email_address, birth_date, deceased_date, tax_file_number, tfn_flag, password, retirement_date, pref_correspondence_method, next_correspondence_method, annual_salary, salary_date, previous_benefit_amount, reporting_frequency_flag, preferred_payment_type, payee_name, bank_account_id, combined_gc_flag, birthplace, user_id, can_pass_on_tfn_flag, service_priority_flag, last_used_bank_account_id, transitional_pension_rbl, transitional_lumpsum_rbl, preferred_name_type, returned_mail_flag, contact_name, topup_flag, client_type, title_text, short_name, resident_status, residence_country_code, advertising_material_flag, returned_mail_datetime, dormant_flag, dormant_since_datetime, super_fund_flag, entity_stopped_flag, entity_stopped_datetime, fax_indemnity_flag, email_indemnity_flag, tfn_exemption_id, mail_returned_date1, mail_returned_date2, locator_code, abn, abn_flag, rcti_agreement_flag, mailing_title, company_rego_no, tax_code, direct_pay_type_code, investment_qualification_level, aml_status, birth_country_code, fatca_status, giin_status, giin, giin_verified_flag, grouping_code, money_purchase_applies_flag, money_purchase_trigger_date, deceased_date_status, deceased_notification_date, fcds_status, reporting_currency, pref_advertising_method, isa_aps_allowance, isa_aps_expiry_date,anonymised_flag) 
        --Select @lNextKeySequenceEntityId2+3, 'C', NULL, NULL, NULL, NULL, NULL, 'IPS Basic Direct', ' ', NULL, 'Dear Sir/Madam', 'N', NULL, NULL, 01737374708, NULL, NULL, 'scrambled@noemail.nodomain', NULL, NULL, NULL, 'N', NULL, NULL, 'M', 'M', 0.00, NULL, 0.00, 'M', 'Q', NULL, NULL, 'Y', NULL, NULL, 'Y', 'N', NULL, 0.00, 0.00, 'A', 'Y', NULL, 'Y', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'N', NULL, 'Y', 'Y', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'X', NULL, 'N', 'N', NULL, 'N', NULL, 'N', NULL, NULL, NULL, NULL, 'GBP', NULL, NULL, NULL
        select @lNextKeySequenceEntityId2+3,'C',NULL,NULL,NULL,NULL,NULL,'IPS Basic Direct',' ',NULL,'Dear Sir/Madam','N',NULL,NULL,'01737 374708',NULL,NULL,NULL,NULL,NULL,NULL,'N',NULL,NULL,'M','M',NULL,NULL,0,'A','Q',NULL,NULL,'N',NULL,NULL,NULL,'N',NULL,0,0,'N',NULL,NULL,'Y',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'N',NULL,'N','N',NULL,NULL,NULL,NULL,NULL,NULL,1,NULL,'N','N',NULL,NULL,NULL,NULL,NULL,'X',NULL,'N','N','NULL','N',NULL,'N',NULL,NULL,NULL,NULL,'GBP',NULL,NULL,NULL,NULL

		IF ( @@error <> 0)
		BEGIN
			SELECT @v_error_code = -1, @v_error_msg = 'RC0170: Error calling sp_get_unique_key_block of 4169_New dummy adviser ID(NBS) and hierarchy for Customer Portal.sql'
			GOTO ExitRoutine
		END
		

		--Dealer Entity
		Insert into entity(entity_id, entity_type, old_system_reference, occupation, hazardous_occupation_id, anzsic_code, title_id, name, given_names, preferred_name, salutation, gender, acn_number, home_phone_number, work_phone_number, mobile_number, fax_number, email_address, birth_date, deceased_date, tax_file_number, tfn_flag, password, retirement_date, pref_correspondence_method, next_correspondence_method, annual_salary, salary_date, previous_benefit_amount, reporting_frequency_flag, preferred_payment_type, payee_name, bank_account_id, combined_gc_flag, birthplace, user_id, can_pass_on_tfn_flag, service_priority_flag, last_used_bank_account_id, transitional_pension_rbl, transitional_lumpsum_rbl, preferred_name_type, returned_mail_flag, contact_name, topup_flag, client_type, title_text, short_name, resident_status, residence_country_code, advertising_material_flag, returned_mail_datetime, dormant_flag, dormant_since_datetime, super_fund_flag, entity_stopped_flag, entity_stopped_datetime, fax_indemnity_flag, email_indemnity_flag, tfn_exemption_id, mail_returned_date1, mail_returned_date2, locator_code, abn, abn_flag, rcti_agreement_flag, mailing_title, company_rego_no, tax_code, direct_pay_type_code, investment_qualification_level, aml_status, birth_country_code, fatca_status, giin_status, giin, giin_verified_flag, grouping_code, money_purchase_applies_flag, money_purchase_trigger_date, deceased_date_status, deceased_notification_date, fcds_status, reporting_currency, pref_advertising_method, isa_aps_allowance, isa_aps_expiry_date,anonymised_flag) 
		--Select @lNextKeySequenceEntityId3, 'C', NULL, NULL, NULL, NULL, NULL, 'Nationwide SFC', ' ', NULL, 'Dear Sir/Madam', 'N', NULL, NULL, 01737374708, NULL, NULL, 'scrambled@noemail.nodomain', NULL, NULL, NULL, 'N', NULL, NULL, 'M', 'M', 0.00, NULL, 0.00, 'M', 'Q', NULL, NULL, 'Y', NULL, NULL, 'Y', 'N', NULL, 0.00, 0.00, 'A', 'Y', NULL, 'Y', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'N', NULL, 'Y', 'Y', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'X', NULL, 'N', 'N', NULL, 'N', NULL, 'N', NULL, NULL, NULL, NULL, 'GBP', NULL, NULL, NULL
        select @lNextKeySequenceEntityId3,'C',NULL,NULL,NULL,NULL,NULL,'Nationwide SFC',' ',NULL,'Dear Sir/Madam','N',NULL,NULL,'01737 374708',NULL,NULL,NULL,NULL,NULL,NULL,'N',NULL,NULL,'M','M',NULL,NULL,0,'A','Q',NULL,NULL,'N',NULL,NULL,NULL,'N',NULL,0,0,'N',NULL,NULL,'Y',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'N',NULL,'N','N',NULL,NULL,NULL,NULL,NULL,NULL,1,NULL,'N','N',NULL,NULL,NULL,NULL,NULL,'X',NULL,'N','N','NULL','N',NULL,'N',NULL,NULL,NULL,NULL,'GBP',NULL,NULL,NULL,NULL

		IF ( @@error <> 0)
		BEGIN
			SELECT @v_error_code = -1, @v_error_msg = 'RC0180: Error calling sp_get_unique_key_block of 4169_New dummy adviser ID(NBS) and hierarchy for Customer Portal.sql'
			GOTO ExitRoutine
		END
		
		
		----------------
	    --Adviser Address
		insert into address(address_id,entity_id,address_type,care_of_name,property_name,prefix_type,prefix,suffix_type,suffix,street_number,street,street2,suburb,postcode,district,statecode,countrycode,dpid,barcode,bsp)
		Select @lNextKeyAddId,@lNextKeySequenceAdvEntityId, 'A', NULL, 'Aegon IPS Administration', NULL,NULL,NULL,NULL,NULL,'PO BOX 17942',NULL,NULL,'EH12 1PE','Edinburgh',NULL,'GBR',NULL,NULL,NULL
		
		
		IF ( @@error <> 0)
		BEGIN
			SELECT @v_error_code = -1, @v_error_msg = 'RC0190: Error calling sp_get_unique_key_block of 4169_New dummy adviser ID(NBS) and hierarchy for Customer Portal.sql'
			GOTO ExitRoutine
		END
		
	   --Branch Address
	   insert into address(address_id,entity_id,address_type,care_of_name,property_name,prefix_type,prefix,suffix_type,suffix,street_number,street,street2,suburb,postcode,district,statecode,countrycode,dpid,barcode,bsp)
	   Select @lNextKeyAddId+1,@lNextKeySequenceEntityId2, 'A', NULL, 'Temple Court', NULL,NULL,NULL,NULL,NULL,'11 Queen Victoria Street',NULL,NULL,'EC4N 4TP','London',NULL,'GBR',NULL,NULL,NULL
	   insert into address(address_id,entity_id,address_type,care_of_name,property_name,prefix_type,prefix,suffix_type,suffix,street_number,street,street2,suburb,postcode,district,statecode,countrycode,dpid,barcode,bsp)
	   Select @lNextKeyAddId+2,@lNextKeySequenceEntityId2+1, 'A', NULL, 'Nationwide Building Society', NULL,NULL,NULL,NULL,NULL,'Pipers Way',NULL,NULL,'SN3 1TX','SWINDON',NULL,'GBR',NULL,NULL,NULL
	   insert into address(address_id,entity_id,address_type,care_of_name,property_name,prefix_type,prefix,suffix_type,suffix,street_number,street,street2,suburb,postcode,district,statecode,countrycode,dpid,barcode,bsp)
	   Select @lNextKeyAddId+3,@lNextKeySequenceEntityId2+2, 'A', NULL, 'Nationwide Building Society', NULL,NULL,NULL,NULL,NULL,'Pipers Way',NULL,NULL,'SN3 1TX','SWINDON',NULL,'GBR',NULL,NULL,NULL
	   insert into address(address_id,entity_id,address_type,care_of_name,property_name,prefix_type,prefix,suffix_type,suffix,street_number,street,street2,suburb,postcode,district,statecode,countrycode,dpid,barcode,bsp)
	   Select @lNextKeyAddId+4,@lNextKeySequenceEntityId2+3, 'A', NULL, 'Nationwide Building Society', NULL,NULL,NULL,NULL,NULL,'Bay 308 Legal & General House',NULL,'Surrey','KT20 6EU','Kingswood',NULL,'GBR',NULL,NULL,NULL
		
	   IF ( @@error <> 0)
		BEGIN
			SELECT @v_error_code = -1, @v_error_msg = 'RC0200: Error calling sp_get_unique_key_block of 4169_New dummy adviser ID(NBS) and hierarchy for Customer Portal.sql'
			GOTO ExitRoutine
		END
	   
	   --Dealer Address
		
		insert into address(address_id,entity_id,address_type,care_of_name,property_name,prefix_type,prefix,suffix_type,suffix,street_number,street,street2,suburb,postcode,district,statecode,countrycode,dpid,barcode,bsp)
		Select @lNextKeyAddId+5,@lNextKeySequenceEntityId3, 'A', NULL, 'Temple Court', NULL,NULL,NULL,NULL,NULL,'11 Queen Victoria Street',NULL,NULL,'EC4N 4TP','London',NULL,'GBR',NULL,NULL,NULL
		
		IF ( @@error <> 0)
		BEGIN
			SELECT @v_error_code = -1, @v_error_msg = 'RC0210: Error calling sp_get_unique_key_block of 4169_New dummy adviser ID(NBS) and hierarchy for Customer Portal.sql'
			GOTO ExitRoutine
		END
		
		--------------------
		--Dealer
		Insert into dealer(dealer_id, entity_id, license_number, grouping_code, status, old_system_reference, start_date, end_date, preferred_payment_type, payee_name, bank_account_id, notation, pay_to_dealer_group_flag, payment_required_flag, ilink_last_sent_datetime, brokerage_on_disk_flag, fsa_firm_reference_id, fsa_firm_alternate_name_id, dealer_type, direct_pay_type_code) 
		Select 200051, @lNextKeySequenceEntityId3, NULL, NULL, 'A', NULL, '01/16/2009 14:43:29.073', NULL, 'D', NULL, NULL, NULL, 'Y', 'Y', NULL, 'N', 106078, NULL, NULL, 'BACS'

        IF ( @@error <> 0)
		BEGIN
			SELECT @v_error_code = -1, @v_error_msg = 'RC0220: Error calling sp_get_unique_key_block of 4169_New dummy adviser ID(NBS) and hierarchy for Customer Portal.sql'
			GOTO ExitRoutine
		END

		--Dealer_branch
		Insert into dealer_branch(dealer_branch_id, dealer_id, entity_id, grouping_code, status, old_system_reference, start_date, end_date, preferred_payment_type, payee_name, bank_account_id, pay_to_dealer_group_flag, pay_to_dealer_branch_id, notation, direct_pay_type_code, fsa_firm_appoint_rep_flag, fsa_firm_reference_id, fsa_firm_appointment_ref_id, fsa_firm_alternate_name_id, authorisation_override_flag)
		Select 400051, 200051, @lNextKeySequenceEntityId2, NULL, 'A', NULL, '01/16/2009 14:45:59.807', NULL, 'Q', NULL, NULL, 'Y', NULL, NULL, 'BACS', 'N', NULL, NULL, NULL, 'N'
		Insert into dealer_branch(dealer_branch_id, dealer_id, entity_id, grouping_code, status, old_system_reference, start_date, end_date, preferred_payment_type, payee_name, bank_account_id, pay_to_dealer_group_flag, pay_to_dealer_branch_id, notation, direct_pay_type_code, fsa_firm_appoint_rep_flag, fsa_firm_reference_id, fsa_firm_appointment_ref_id, fsa_firm_alternate_name_id, authorisation_override_flag)
        Select 400052, 200051, @lNextKeySequenceEntityId2+1, NULL, 'A', NULL, '08/18/2009 16:08:57.953', NULL, 'Q', NULL, NULL, 'Y', NULL, NULL, 'BACS', 'N', NULL, NULL, NULL, 'N'
		Insert into dealer_branch(dealer_branch_id, dealer_id, entity_id, grouping_code, status, old_system_reference, start_date, end_date, preferred_payment_type, payee_name, bank_account_id, pay_to_dealer_group_flag, pay_to_dealer_branch_id, notation, direct_pay_type_code, fsa_firm_appoint_rep_flag, fsa_firm_reference_id, fsa_firm_appointment_ref_id, fsa_firm_alternate_name_id, authorisation_override_flag)
		Select 400053, 200051, @lNextKeySequenceEntityId2+2, NULL, 'A', NULL, '08/19/2009 14:18:46.663', NULL, 'Q', NULL, NULL, 'Y', NULL, NULL, 'BACS', 'N', NULL, NULL, NULL, 'N'
		Insert into dealer_branch(dealer_branch_id, dealer_id, entity_id, grouping_code, status, old_system_reference, start_date, end_date, preferred_payment_type, payee_name, bank_account_id, pay_to_dealer_group_flag, pay_to_dealer_branch_id, notation, direct_pay_type_code, fsa_firm_appoint_rep_flag, fsa_firm_reference_id, fsa_firm_appointment_ref_id, fsa_firm_alternate_name_id, authorisation_override_flag)
		Select 400054, 200051, @lNextKeySequenceEntityId2+3, NULL, 'A', NULL, '05/12/2014 10:37:09.867', NULL, 'Q', NULL, NULL, 'Y', NULL, NULL, 'BACS', 'N', NULL, NULL, NULL, 'N'
		----------------------
		
		IF ( @@error <> 0)
		BEGIN
			SELECT @v_error_code = -1, @v_error_msg = 'RC0230: Error calling sp_get_unique_key_block of 4169_New dummy adviser ID(NBS) and hierarchy for Customer Portal.sql'
			GOTO ExitRoutine
		END
		
		-- adviser_account for NBS
		Insert into adviser_account(adviser_account_id, dealer_id, agent_id, adviser_group_id, entity_id, proper_authority_number, old_system_reference, grouping_code, status, start_date, end_date, member_correspondence_flag, address_flag, preferred_payment_type, payee_name, bank_account_id, notation, dealer_branch_id, payment_required_flag, fsa_firm_appoint_rep_flag, fsa_firm_reference_id, fsa_firm_alternate_name_id, fsa_firm_appointment_ref_id, direct_pay_type_code)
		Select 700000,NULL, NULL, NULL, @lNextKeySequenceAdvEntityId, NULL, NULL, NULL, 'A', Convert(datetime,'01/01/1900 00:00:00'), NULL, 'N', 'A', 'Q', NULL, NULL, NULL, 400051 , 'Y', 'N', NULL, NULL, NULL, 'BACS'
		
		IF ( @@error <> 0)
		BEGIN
			SELECT @v_error_code = -1, @v_error_msg = 'RC0240: Error calling sp_get_unique_key_block of 4169_New dummy adviser ID(NBS) and hierarchy for Customer Portal.sql'
			GOTO ExitRoutine
		END
		
		
		-- bank account
		
		Insert into bank_account(bank_account_id,institution_branch_id,account_number,name,cheque_facility_flag,direct_entry_id,perform_bankrec_flag,validate_bankrec_effdate,customer_reference,include_in_clientmoneyrep_flag,creation_date,closure_date,country_code,intnl_account_number,currency_code,bic_code,debit_card_user_id)
		Select @lNextKeySequencebankId,8386,20218063,'Legal & General PMS Ltd',NULL,NULL,'Y','N',NULL,NULL,Convert(datetime,'01/16/2009 00:00:00'),NULL,NULL,NULL,'GBP', NULL, NULL

        IF ( @@error <> 0)
		BEGIN
			SELECT @v_error_code = -1, @v_error_msg = 'RC0240: Error calling sp_get_unique_key_block of 4169_New dummy adviser ID(NBS) and hierarchy for Customer Portal.sql'
			GOTO ExitRoutine
		END
		
		-- Entity Bank account 
		
		Insert into entity_bank_account(entity_id,bank_account_id,default_flag,bank_account_type,third_party_validated_flag,assoc_internal_client_money_acc_id)
		Select @lNextKeySequenceEntityId3,@lNextKeySequencebankId,'Y','','N', NULL
		
		IF ( @@error <> 0)
		BEGIN
			SELECT @v_error_code = -1, @v_error_msg = 'RC0250: Error calling sp_get_unique_key_block of 4169_New dummy adviser ID(NBS) and hierarchy for Customer Portal.sql'
			GOTO ExitRoutine
		END
		
		--key sequence update
		
		
		update key_sequence
		set key_sequence = 400055
		WHERE table_name = 'dealer_branch'
       
	   	IF ( @@error <> 0)
		BEGIN
			SELECT @v_error_code = -1, @v_error_msg = 'RC0260: Error calling sp_get_unique_key_block of 4169_New dummy adviser ID(NBS) and hierarchy for Customer Portal.sql'
			GOTO ExitRoutine
		END

	    update key_sequence
		set key_sequence = 200052
		WHERE table_name = 'dealer'
		
		IF ( @@error <> 0)
		BEGIN
			SELECT @v_error_code = -1, @v_error_msg = 'RC0270: Error calling sp_get_unique_key_block of 4169_New dummy adviser ID(NBS) and hierarchy for Customer Portal.sql'
			GOTO ExitRoutine
		END
		
		--Update Entity
		
		update entity 
		set returned_mail_flag = NULL,
		topup_flag = 'Y'
		where entity_id IN (@lNextKeySequenceAdvEntityId,@lNextKeySequenceEntityId3,@lNextKeySequenceEntityId2,@lNextKeySequenceEntityId2+1,@lNextKeySequenceEntityId2+2,@lNextKeySequenceEntityId2+3)
		
		
	   --Update Entity
	   
	    update entity
		set given_names = NULL,
		gender = NULL
		where entity_id IN (@lNextKeySequenceEntityId3,@lNextKeySequenceEntityId2,@lNextKeySequenceEntityId2+1,@lNextKeySequenceEntityId2+2,@lNextKeySequenceEntityId2+3)
	   
	    update entity
		set gender = 'M',
		birth_date = '01/01/1980'
		where entity_id = @lNextKeySequenceAdvEntityId
		
	   
	   --party_attribute
	   
	   insert into party_attribute (attribute_id, party_type_id, party_id, attribute_value)
       values (281,3,700000,'Network')
		
		--Output
		
		--Adviser
		
		select * from adviser_account where adviser_account_id = 700000
		
		--Dealer
		
		Select * from dealer where dealer_id = 200051
		
		--Dealer_branch
		
		select * from dealer_branch where dealer_branch_id IN (400051,400052,400053,400054)
		
		--Bank Account
		
		select * from bank_account where bank_account_id IN (@lNextKeySequencebankId)
		
		--Entity Bank Account
		
		select * from entity_bank_account where entity_id IN (@lNextKeySequenceEntityId3)
		
		--Entity
		
		select * from entity where entity_id IN (@lNextKeySequenceAdvEntityId,@lNextKeySequenceEntityId3,@lNextKeySequenceEntityId2,@lNextKeySequenceEntityId2+1,@lNextKeySequenceEntityId2+2,@lNextKeySequenceEntityId2+3,@lNextKeySequenceEntityId2+4)
		
		--ADDRESS
		
		Select * from address where address_id IN (@lNextKeyAddId,@lNextKeyAddId+1,@lNextKeyAddId+2,@lNextKeyAddId+3,@lNextKeyAddId+4,@lNextKeyAddId+5)
		
		--Party attribute
		
		select * from party_attribute where party_id = 700000
		
		
  		IF ( @@error <> 0 )
		BEGIN
			SELECT @v_error_code = -1, @v_error_msg = 'RC5 : Error in Insert of 4169_New dummy adviser ID(NBS) and hierarchy for IPS.sql'
			GOTO ExitRoutine
		END
    
	/* Update the auk_script_control table with details of this update                                                                   */
	   INSERT INTO auk_script_control (script_name, run_number, description, reference, category, target_release)
	   VALUES ('New dummy adviser ID(NBS) and hierarchy for Customer Portal.sql', 1, 'New dummy adviser ID(NBS) and hierarchy for Customer Portal' , '4169','config','2018-12-10')

	   IF ( @@error <> 0 )
	   BEGIN
		  SELECT @v_error_code = -1, @v_error_msg =' RC0070: Error in INSERT of 4169_New dummy adviser ID(NBS) and hierarchy for Customer Portal.sql'
		  GOTO ExitRoutine
	   END      

	/* Show the data held on auk_script_control after UPDATEing new record above...                                                      */
	   SELECT *
	   FROM auk_script_control
	   WHERE script_name ='New dummy adviser ID(NBS) and hierarchy for Customer Portal.sql'

	   IF ( @@error <> 0 )
	   BEGIN
		  SELECT @v_error_code = -1, @v_error_msg =' RC0080: Error in SELECT of 4169_New dummy adviser ID(NBS) and hierarchy for Customer Portal.sql'
		  GOTO ExitRoutine
	   END   

	  COMMIT

	/* Exit Routine. */
	
	   ExitRoutine:
	   IF ( @v_error_code < 0 )
	   BEGIN
		  ROLLBACK
		  PRINT @v_error_msg
	   END 
	


