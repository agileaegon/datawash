/************************************************************************************************************************************/
/*                                                                                                                                  */
/* Requirement: New dummy adviser ID and hierarchy for Customer Portal(NBS)	desensitisation script															*/
/* Date:       OCT  11 2018  4:32PM                                                                                                 */
/* Author(s):  Ani Maitra                                                                                                        */
/*                                                                                                                                  */
/* Description: New dummy adviser ID and hierarchy for Customer Portal(NBS)	desensitisation script															*/
/*                                                                                                                                  */
/*                                                                                                                                  */
/************************************************************************************************************************************/

/* Define variables */
Declare
		  @numKeys						INT
		, @rc1							INT
		, @dealerId						INT
		, @dealerBrchId					INT		
		, @v_refcode					VARCHAR(255)
		, @v_refcode_id					INT
		, @v_refcode_found				INT
		, @v_refcode_values_found		INT
		, @v_previous_run				INT
		, @v_error_code					INT
		, @v_error_msg					VARCHAR(255)

	BEGIN Tran

	/* Check if the script has been run previously                                                                                       */
	   SELECT @v_previous_run = COUNT (*)
	   FROM auk_script_control
	   WHERE script_name ='4169_New dummy adviser ID(NBS) and hierarchy for Customer Portal desensitisation script.sql' AND
			 run_number = 1

	IF ( @@error <> 0 )
	   BEGIN
		  SELECT @v_error_code = -1, @v_error_msg ='RC10: Error in SELECT of 4169_New dummy adviser ID(NBS) and hierarchy for Customer Portal desensitisation script.sql'
		  GOTO ExitRoutine
	   END

	/* Show if previous run has been identified - Should be zero                                                                         */
	   SELECT @v_previous_run AS 'Run Number'
	   IF ( @@error <> 0 )
	   BEGIN
		SELECT @v_error_code = -1, @v_error_msg =' RC20: Error in SELECT of 4169_New dummy adviser ID(NBS) and hierarchy for Customer Portal desensitisation script.sql'
		  GOTO ExitRoutine
	   END

	/* If @v_previous_run is NOT ZERO - Abandon the script execution                                                                     */
	   IF (@v_previous_run > 0)
	   BEGIN
		  SELECT @v_error_code = -1, @v_error_msg = ' RC30: ABANDON RUN - Script has been run previously - 4169_New dummy adviser ID(NBS) and hierarchy for Customer Portal desensitisation script.sql'
		  GOTO ExitRoutine
	   END

			declare @EntityIds table
				(	
					entity_id int
				)
			
			Insert into @EntityIds
			select e.entity_id 
			from entity e
			where e.name  in
			(
			'AUK NBS Live Proving Adviser',
			'Nationwide SFC',
			'Nationwide Direct Mail',
			'Nationwide Online',
			'IPS Basic Direct',
			'Nationwide '
			) and (e.gender is null or e.gender = 'M')

	   IF (@v_previous_run > 0)
	   BEGIN
		  SELECT @v_error_code = -1, @v_error_msg = ' RC30: ABANDON RUN - Insert into Temp table - 4169_New dummy adviser ID(NBS) and hierarchy for Customer Portal desensitisation script.sql'
		  GOTO ExitRoutine
	   END

	-- select the data before update
	   Select a.*
	  from address a
	  join @EntityIds e on a.entity_id = e.entity_id

	  select e.* 
	  from entity e 
	  join @EntityIds te on e.entity_id = te.entity_id 
	  where e.email_address is not null

	  Select b.*
	  from bank_account b
	  Join entity_bank_account eba on b.bank_account_id = eba.bank_account_id
	  join @EntityIds e on e.entity_id = eba.entity_id

		Update a
		set a.property_name = 'Aegon building',a.street = 'Test Address, Lochside Crescent',a.suburb=null,a.postcode = 'EH12 9SE',a.district = 'Edinburgh'
		from address a
		join @EntityIds e on a.entity_id = e.entity_id

		
		IF ( @@error <> 0)
		BEGIN
			SELECT @v_error_code = -1, @v_error_msg = 'RC0270: Error calling Update address of 4169_New dummy adviser ID(NBS) and hierarchy for Customer Portal desensitisation script.sql'
			GOTO ExitRoutine
		END

		update e
		set email_address = 'noemail@aegon.co.uk'
		from entity e 
	    join @EntityIds te on e.entity_id = te.entity_id 
	    where e.email_address is not null

		IF ( @@error <> 0)
		BEGIN
			SELECT @v_error_code = -1, @v_error_msg = 'RC0270: Error calling Update entity of 4169_New dummy adviser ID(NBS) and hierarchy for Customer Portal desensitisation script.sql'
			GOTO ExitRoutine
		END


		Update b
		set account_number = '20999999'
		from bank_account b
		Join entity_bank_account eba on b.bank_account_id = eba.bank_account_id
		join @EntityIds e on e.entity_id = eba.entity_id

		IF ( @@error <> 0)
		BEGIN
			SELECT @v_error_code = -1, @v_error_msg = 'RC0270: Error calling Update bank_account of 4169_New dummy adviser ID(NBS) and hierarchy for Customer Portal desensitisation script.sql'
			GOTO ExitRoutine
		END

	-- select the data after update
	   Select a.*
	  from address a
	  join @EntityIds e on a.entity_id = e.entity_id

	  select e.* 
	  from entity e 
	  join @EntityIds te on e.entity_id = te.entity_id 
	  where e.email_address is not null

	  Select b.*
	  from bank_account b
	  Join entity_bank_account eba on b.bank_account_id = eba.bank_account_id
	  join @EntityIds e on e.entity_id = eba.entity_id
		

	   /* Update the auk_script_control table with details of this update                                                                   */
	   INSERT INTO auk_script_control (script_name, run_number, description, reference, category, target_release)
	   VALUES ('New dummy adviser ID(NBS) and hierarchy for Customer Portal desensitisation script.sql', 1, 'New dummy adviser ID(NBS) and hierarchy for Customer Portal desensitisation script' , '4169','config','2018-12-10')

	   IF ( @@error <> 0 )
	   BEGIN
		  SELECT @v_error_code = -1, @v_error_msg =' RC0070: Error in INSERT of 4169_New dummy adviser ID(NBS) and hierarchy for Customer Portal desensitisation script.sql'
		  GOTO ExitRoutine
	   END      

	/* Show the data held on auk_script_control after UPDATEing new record above...                                                      */
	   SELECT *
	   FROM auk_script_control
	   WHERE script_name ='New dummy adviser ID(NBS) and hierarchy for Customer Portal desensitisation script.sql'

	   IF ( @@error <> 0 )
	   BEGIN
		  SELECT @v_error_code = -1, @v_error_msg =' RC0080: Error in SELECT of 4169_New dummy adviser ID(NBS) and hierarchy for Customer Portal desensitisation script.sql'
		  GOTO ExitRoutine
	   END   

	  COMMIT

	/* Exit Routine. */
	
	   ExitRoutine:
	   IF ( @v_error_code < 0 )
	   BEGIN
		  ROLLBACK
		  PRINT @v_error_msg
	   END 