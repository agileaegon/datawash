/*
  DO NOT RUN IN PRODUCTION !!!!!!!
  
  This mini data wash/scramble script changes key data on Composer investment managers.
  It does NOT disable the audit triggers, so the previous data values will be audited, so
  this technique is only suitable to Investment Managers (and not any personal/customer data).
  This has been copied from the main Composer scripts script.
  
  
  John Cowan   Oct 2018
*/  




-- For NBS TEST enviroments - expect about 30 - 40 rows to be updated.  Do not run in PROD.

/*************************** Investment Manager ************************/

Update entity
   set fax_number = '+441234 567 890',
     home_phone_number = '01000 000 000',
     work_phone_number = '01000 111 111',
  salutation = case e.salutation when 'Sir/Madam' then e.salutation  when 'Administration Services' then e.salutation  else 'ZZZ' end ,
  email_address = case when e.email_address IS NULL then NULL  else 'scrambledInvestManager@noemail.nodomain' end
from investment_manager db
join  entity e on e.entity_id = db.entity_id




-- For NBS TEST enviroments - expect about 30 - 40 rows to be updated.  Do not run in PROD.

/*************************** Investment Manager Address ************************/

update address
set 
care_of_name = null,
property_name = case address_id % 8 when 0 then 'The Villa' when 1 then 'The House' when 2 then 'Mansion Manor' when 3 then 'The Vista' when 4 then 'The Greenroof' when 5 then 'Tinhouse' when 6 then 'The Flat' when 7 then 'The Stable' end,
street_number = convert(varchar,(address_id % 101 ) + 1),
street = (case address_id % 3 when 0 then 'Big' when 1 then 'Small' when 2 then 'Middle' end) + ' ' + (case address_id % 3 when 0 then 'Street' when 1 then 'Drive' when 2 then 'Avenue' end),
street2 = 'Over' + case address_id % 7 when 0 then 'two' when 1 then 'three' when 2 then 'four' when 3 then 'five' when 4 then 'six' when 5 then 'seven' when 6 then 'nine' end,
suburb = case address_id % 5 when 0 then 'Longton' when 1 then 'Ellen-on-Sea' when 2 then 'Brackenside' when 3 then 'Viewton' when 4 then 'Breen' end,
district = case address_id % 6 when 0 then 'Suburbian' when 1 then 'Localshire' when 2 then 'Otherside' when 3 then 'Outston' when 4 then 'Greyville' when 5 then 'Hillend' end,
postcode =  case address_id % 5 when 0 then 'NX' when 1 then 'GW' when 2 then 'SZ' when 3 then 'BY' when 4 then 'AX' end + convert(varchar,(address_id % 40) + 1) + 
           ' ' + convert(varchar,(address_id % 8)+1) + 
           case address_id % 6 when 0 then 'AZ' when 1 then 'TG' when 2 then 'ER' when 3 then 'NJ' when 4 then 'WX' when 5 then 'GA' end
from address ad
join investment_manager ma on ma.entity_id = ad.entity_id

