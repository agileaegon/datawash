BEGIN
    SET NOCOUNT ON;

	DECLARE	@StepMessage  NVARCHAR(4000)
	      , @ErrorMessage NVARCHAR(4000)
		  , @allowedNINOPrefixes NVARCHAR(4000) = 'AA, AB, AE, AH, AK, AL, AM, AP, AR, AS, AT, AW, AX, AY, AZ, BA, BB, BE, BH, BK, BL, BM, BT, CA, CB, CE, CH, CK, CL, CR, EA, EB, EE, EH, EK, EL, EM, EP, ER, ES, ET, EW, EX, EY, EZ, GY, HA, HB, HE, HH, HK, HL, HM, HP, HR, HS, HT, HW, HX, HY, HZ, JA, JB, JC, JE, JG, JH, JJ, JK, JL, JM, JN, JP, JR, JS, JT, JW, JX, JY, JZ, KA, KB, KE, KH, KK, KL, KM, KP, KR, KS, KT, KW, KX, KY, KZ, LA, LB, LE, LH, LK, LL, LM, LP, LR, LS, LT, LW, LX, LY, LZ, MA, MW, MX, NA, NB, NE, NH, NL, NM, NP, NR, NS, NW, NX, NY, NZ, OA, OB, OE, OH, OK, OL, OM, OP, OR, OS, OX, PA, PB, PC, PE, PG, PH, PJ, PK, PL, PM, PN, PP, PR, PS, PT, PW, PX, PY, RA, RB, RE, RH, RK, RM, RP, RR, RS, RT, RW, RX, RY, RZ, SA, SB, SC, SE, SG, SH, SJ, SK, SL, SM, SN, SP, SR, SS, ST, SW, SX, SY ,SZ, TA, TB, TE, TH, TK, TL, TM, TP, TR, TS, TT, TW, TX, TY, TZ, WA, WB, WE, WK, WL, WM, WP, YA, YB, YE, YH, YK, YL, YM, YP, YR, YS, YT, YW, YX, YY, YZ, ZA, ZB, ZE, ZH, ZK, ZL, ZM, ZP, ZR, ZS, ZT, ZW, ZX, ZY'
		  , @allowedNINOSuffixes NVARCHAR(100) = 'A,B,C,D'
		  , @delimiter CHAR(1) = ','
		  , @prefixCount INT
		  , @suffixCount INT
		  , @postcodeCount INT
		  , @batchSize INT = 20000
		  , @startTime DATETIME
		  , @endTime DATETIME;

BEGIN TRY

	SET @StepMessage = convert(varchar,getdate(),109) + ' Start Scramble'
	RAISERROR(@StepMessage,0,1) WITH NOWAIT

    /*************************************** Create Functions ******************************************/
	SET @StepMessage = 'Block 1 - Start';
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;
	SET @startTime = GETDATE();
    
	IF EXISTS
        ( SELECT 
                *
          FROM [sys].[objects]
          WHERE [name] = 'fn_RowConcatenation'
                AND [type] = 'FN'
        ) 
    BEGIN
        DROP FUNCTION 
              [dbo].[fn_RowConcatenation];
    END;
    EXECUTE [dbo].[sp_executesql] @Statement = N'
    CREATE FUNCTION [dbo].[fn_RowConcatenation] (@entity_id INT)
    RETURNS VARCHAR(60)
    AS
    BEGIN
        DECLARE @Desc VARCHAR(60)
        SELECT
            @Desc = ISNULL(@Desc + '' & '', '''') + ISNULL(se.salutation, '''')
        FROM
            dbo.entity AS je
            INNER JOIN dbo.joint_owner_details AS jod
                ON je.entity_id = jod.investor_entity_id
            INNER JOIN dbo.entity AS se
                ON jod.joint_entity_id = se.entity_id
        WHERE
            je.entity_id = @entity_id
            AND se.salutation IS NOT NULL
        RETURN LEFT(@Desc,60)
    END
    '; 

	/* Create function [dbo].[fn_CsvToTable] */
	IF EXISTS
        ( SELECT 
                *
          FROM [sys].[objects]
          WHERE [name] = 'fn_CsvToTable'
                AND [type] IN ( N'FN', N'IF', N'TF', N'FS', N'FT' )
        ) 
    BEGIN
        DROP FUNCTION 
              [dbo].[fn_CsvToTable];
    END;

	EXECUTE [dbo].[sp_executesql] @Statement = N'
    CREATE FUNCTION [dbo].[fn_CsvToTable] (@StringInput varchar(4000), @delimiter char(1))
	RETURNS @OutputTable TABLE (item varchar(10))
	AS
	BEGIN
		Declare @value NVARCHAR(4000);
		DECLARE @delimIndex INT;

		SET @StringInput = LTRIM(RTRIM(@StringInput))+ '',''
		SET @delimIndex = CHARINDEX(@delimiter, @StringInput, 0)

		WHILE (@delimIndex != 0)
		BEGIN
		   set @value = LTRIM(RTRIM(LEFT(@StringInput, @delimIndex - 1)));
		   Insert into @OutputTable(item) values (@value)

		   Set @StringInput = RIGHT(@StringInput, LEN(@StringInput) - @delimIndex)
		   Set @delimIndex = CHARINDEX(@delimiter, @StringInput, 0)
		END

	   RETURN
	END
	'; 

	SET @endTime = GETDATE();
	SET @StepMessage = 'Block 1 - Finish. Time Taken - ' + CONVERT(VARCHAR(20), @endTime - @startTime, 114);
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;

	/*************************************** Create Tables ******************************************/

    SET @StepMessage = 'Block 2 - Start';
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;
	SET @startTime = GETDATE();

    IF EXISTS
        ( SELECT 
                *
          FROM [sys].[objects]
          WHERE [name] = 'entity_scramble'
                AND [type] = 'U'
        ) 
    BEGIN
        DROP TABLE [dbo].[entity_scramble];
    END;

    CREATE TABLE [dbo].[entity_scramble]
    ( 
        [id]   [INT] IDENTITY(1, 1) NOT NULL 
      , [name] [VARCHAR](255) NULL
    ) 
    ON [PRIMARY];

	IF EXISTS
		( SELECT 
				*
			FROM [sys].[objects]
			WHERE [name] = 'postcode_scramble'
				AND [type] = 'U'
		) 
    BEGIN
        DROP TABLE [dbo].[postcode_scramble];
    END;

    CREATE TABLE [dbo].[postcode_scramble]
    ( 
        [id]   [INT] IDENTITY(1, 1) NOT NULL
      , [postcode] [VARCHAR](255) NULL
    ) 
    ON [PRIMARY];

	IF OBJECT_ID('tempdb..#investors_to_change') IS NOT NULL
		DROP TABLE #investors_to_change;
	
	CREATE TABLE #investors_to_change
	(
		entity_id INT
	);

	IF OBJECT_ID('tempdb..#NINOPrefix') IS NOT NULL
		DROP TABLE #NINOPrefix;
	
	CREATE TABLE #NINOPrefix
	(
		id INT IDENTITY(1,1) NOT NULL
	  , prefix CHAR(2)
	);

	IF OBJECT_ID('tempdb..#NINOSuffix') IS NOT NULL
		DROP TABLE #NINOSuffix;

	CREATE TABLE #NINOSuffix
	(
		id INT IDENTITY(1,1) NOT NULL
	  , suffix CHAR(1)
	);

	IF OBJECT_ID('tempdb..#master_model_scramble') IS NOT NULL
		DROP TABLE #master_model_scramble;

	CREATE TABLE #master_model_scramble
	(
		master_model_id INT,
		new_name VARCHAR(80)
	);

	SET @endTime = GETDATE();
	SET @StepMessage = 'Block 2 - Finish. Time Taken - ' + CONVERT(VARCHAR(20), @endTime - @startTime, 114);
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;

	/*************************************** Update Individuals ******************************************/

    SET @StepMessage = 'Block 3 - Start';
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;
	SET @startTime = GETDATE();

	IF EXISTS 
		(	SELECT * 
			FROM sys.objects 
			WHERE [name] = N'au_entity' AND [type] = 'TR'
		)
	BEGIN
		ALTER TABLE [entity] DISABLE TRIGGER [au_entity];      
	END;

    /* update individuals */
    /*get investor ids to be scrambled */
	INSERT INTO [#investors_to_change]
		(
			[entity_id]
		)
    SELECT 
          [e].[entity_id]
    FROM [dbo].[entity] AS [e]
        JOIN [dbo].[member_account] AS [ma]
            ON [e].[entity_id] = [ma].[entity_id]
        LEFT JOIN [product_type] AS [p]
            ON [ma].[member_account_id] = [p].[ext_over_under_acc_id]
    WHERE [p].[ext_over_under_acc_id] IS NULL
          AND [e].[entity_type] = 'I';

    SET @endTime = GETDATE();
	SET @StepMessage = 'Block 3 - Finish. Time Taken - ' + CONVERT(VARCHAR(20), @endTime - @startTime, 114);
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;

	/*************************************** name and salutation scrambling ******************************************/
    
	SET @StepMessage = 'Block 4 - Start';
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;
	SET @startTime = GETDATE();

    TRUNCATE TABLE [entity_scramble];
    INSERT INTO [entity_scramble]
        ( 
          [name]
        ) 
    SELECT 
          [e].[name]
    FROM [dbo].[entity] AS [e]
        JOIN [dbo].[member_account] AS [ma]
            ON [e].[entity_id] = [ma].[entity_id]
        LEFT JOIN [dbo].[product_type] AS [p]
            ON [ma].[member_account_id] = [p].[ext_over_under_acc_id]
    WHERE [p].[ext_over_under_acc_id] IS NULL
          AND [e].[entity_type] = 'I'
    GROUP BY 
          [e].[name]
    ORDER BY 
          [e].[name];

	SET @endTime = GETDATE();
	SET @StepMessage = 'Block 4 - Finish. Time Taken - ' + CONVERT(VARCHAR(20), @endTime - @startTime, 114);
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;

	/*************************************** salutation scrambling ******************************************/

    SET @StepMessage = 'Block 5 - Start';
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;
	SET @startTime = GETDATE();

    UPDATE [e]
       SET [e].[salutation] = CASE
                                WHEN [e].[gender] = 'M'
                                     AND [e].[deceased_date] IS NULL
                                  THEN 'Mr'
                                WHEN [e].[gender] = 'M'
                                     AND [e].[deceased_date] IS NOT NULL
                                  THEN 'The Late Mr'
                                WHEN [e].[gender] = 'F'
                                     AND [e].[deceased_date] IS NULL
                                  THEN 'Mrs'
                                WHEN [e].[gender] = 'F'
                                     AND [e].[deceased_date] IS NOT NULL
                                  THEN 'The Late Mrs'
                              END + ' Ind ' + CAST([e2].[id] AS VARCHAR(400)) + ' LastName'
    FROM [entity] [e]
        JOIN [entity_scramble] [e2]
            ON [e].[name] = [e2].[name]
        JOIN [#investors_to_change] [i]
            ON [i].[entity_id] = [e].[entity_id]
    WHERE 
          [e].[entity_type] = 'I';

	SET @endTime = GETDATE();
	SET @StepMessage = 'Block 5 - Finish. Time Taken - ' + CONVERT(VARCHAR(20), @endTime - @startTime, 114);
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;

	/*************************************** name scrambling ******************************************/
    
    SET @StepMessage = 'Block 6 - Start';
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;
	SET @startTime = GETDATE();

    UPDATE [e1]
       SET [e1].[name] = 'Ind' + CAST([e2].[id] AS VARCHAR(400)) + ' LastName'
    FROM [entity] [e1]
        JOIN [entity_scramble] [e2]
            ON [e1].[name] = [e2].[name]
        JOIN [#investors_to_change] [i]
            ON [e1].[entity_id] = [i].[entity_id];

	SET @endTime = GETDATE();
	SET @StepMessage = 'Block 6 - Finish. Time Taken - ' + CONVERT(VARCHAR(20), @endTime - @startTime, 114);
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;

	/*************************************** given_names scrambling for NULL or Empty records ******************************************/
    
	SET @StepMessage = 'Block 7 - Start';
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;
	SET @startTime = GETDATE();

    UPDATE [e]
       SET [e].[given_names] = CASE
                                 WHEN [given_names] IS NULL
                                   THEN 'Ind 1 FirstName'
                                 WHEN [given_names] = ''
                                   THEN 'Ind 2 FirstName'
                               END
    FROM [entity] [e]
        JOIN [#investors_to_change] [i]
            ON [i].[entity_id] = [e].[entity_id]
    WHERE 
          [entity_type] = 'I'
          AND
              ( [given_names] IS NULL
                OR [given_names] = ''
              );

	SET @endTime = GETDATE();
	SET @StepMessage = 'Block 7 - Finish. Time Taken - ' + CONVERT(VARCHAR(20), @endTime - @startTime, 114);
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;

	/*************************************** given_names scrambling for Not NULL records ******************************************/

    SET @StepMessage = 'Block 8 - Start';
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;
	SET @startTime = GETDATE();

    TRUNCATE TABLE [entity_scramble];
    INSERT INTO [entity_scramble]
        ( 
          [name]
        ) 
    SELECT 
          [e].[given_names]
    FROM [dbo].[entity] AS [e]
        JOIN [dbo].[member_account] AS [ma]
            ON [e].[entity_id] = [ma].[entity_id]
        LEFT JOIN [product_type] AS [p]
            ON [ma].[member_account_id] = [p].[ext_over_under_acc_id]
    WHERE [p].[ext_over_under_acc_id] IS NULL
          AND [e].[entity_type] = 'I'
    GROUP BY 
          [e].[given_names]
    ORDER BY 
          [e].[given_names];

    SET @endTime = GETDATE();
	SET @StepMessage = 'Block 8 - Finish. Time Taken - ' + CONVERT(VARCHAR(20), @endTime - @startTime, 114);
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;

	/*************************************** preferred_name scrambling ******************************************/
	SET @StepMessage = 'Block 9 - Start';
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;
	SET @startTime = GETDATE();

    UPDATE [e1]
       SET [e1].[given_names] = 'Ind' + CAST([e2].[id] AS VARCHAR(400)) + 'FirstName'
         , [e1].[preferred_name] = ( CASE
                                       WHEN [preferred_name] IS NOT NULL
                                         THEN 'Ind ' + CAST([e2].[id] AS VARCHAR(400)) + 'preferred_name'
                                     END )
    FROM [entity] [e1]
        JOIN [entity_scramble] [e2]
            ON [e1].[given_names] = [e2].[name]
        JOIN [#investors_to_change] [i]
            ON [i].[entity_id] = [e1].[entity_id]
    WHERE 
          [e1].[entity_type] = 'I';

	/* tax_file_number scrambling */

	--Populate NINO Prefix table
	INSERT INTO #NINOPrefix
	(
		prefix
	)
	SELECT 
		item
	FROM 
		[dbo].[fn_CsvToTable](@allowedNINOPrefixes, @delimiter)

	SELECT 
		@prefixCount = count(1) 
	FROM 
		#NINOPrefix

	--Populate NINO Suffix table
	INSERT INTO #NINOSuffix
	(
		suffix
	)
	SELECT 
		item
	FROM 
		[dbo].[fn_CsvToTable](@allowedNINOSuffixes, @delimiter)

	SELECT 
		@suffixCount = count(1) 
	FROM 
		#NINOSuffix
	
	--Control table for chunk updates
	IF OBJECT_ID('tempdb..#upd_entity_control') IS NOT NULL
		DROP TABLE #upd_entity_control;

	CREATE TABLE #upd_entity_control
	(
		control_id INT NOT NULL,
		complete INT DEFAULT 0 NOT NULL
	);

	INSERT INTO [#upd_entity_control]
	(
		control_id
	)
	SELECT
		entity_id
	FROM 
		[dbo].[entity] [e]
	WHERE 
		[e].[tax_file_number] IS NOT NULL
		AND [e].[tax_file_number] NOT IN ('', 'QQ888888A', 'XX999999X', 'QQ999999A', 'AB123456C', 'TA000000C', '000000000', 'AA000000D', 'YY888888Y') /*Dummy NINOs*/

	CREATE CLUSTERED INDEX idx_enity_control ON #upd_entity_control(control_id)

	WHILE EXISTS 
		(
			SELECT 
                1
			FROM
				[#upd_entity_control]
			WHERE 
				complete <> 2
		)
	BEGIN
		UPDATE TOP (@batchSize) 
			[#upd_entity_control]
		SET 
			complete = 1
		WHERE
			complete = 0

		UPDATE
			[e]
		SET
			e.tax_file_number = (select prefix from #NINOPrefix where id = CAST(RAND(entity_id) * @prefixCount + 1 AS int)) +
								RIGHT(CAST((RAND(entity_id) * 10000000 + 99999999) as int), 6) +					
								(select suffix from #NINOSuffix where id = CAST(RAND(entity_id) * @suffixCount + 1 AS int))
		FROM 
			[dbo].[entity] [e]
			JOIN [#upd_entity_control] u
				ON u.control_id = e.entity_id
				AND u.complete = 1

		UPDATE
			[#upd_entity_control]
		SET 
			complete = 2
		WHERE
			complete = 1
	END

	IF OBJECT_ID('tempdb..#upd_entity_control') IS NOT NULL
		DROP TABLE #upd_entity_control;

	SET @endTime = GETDATE();
	SET @StepMessage = 'Block 9 - Finish. Time Taken - ' + CONVERT(VARCHAR(20), @endTime - @startTime, 114);
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;
	
	/*************************************** other fields scrambling for individuals ******************************************/

    SET @StepMessage = 'Block 10 - Start';
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;
	SET @startTime = GETDATE();

    UPDATE [e]
       SET [e].[email_address] = CASE
                                   WHEN [e].[email_address] IS NOT NULL
                                     THEN 'ScrambledInd' + CAST([e].[entity_id] AS VARCHAR(10)) + '@noemail.nodomain'
                                   ELSE NULL
                                 END
         , [e].[home_phone_number] = CASE
                                       WHEN [e].[home_phone_number] IS NOT NULL
                                         THEN '01000 000 000'
                                       ELSE NULL
                                     END
         , [e].[work_phone_number] = CASE
                                       WHEN [e].[work_phone_number] IS NOT NULL
                                         THEN '01000 111 111'
                                       ELSE NULL
                                     END
         , [e].[mobile_number] = CASE
                                   WHEN [e].[mobile_number] IS NOT NULL
                                     THEN '07000000000'
                                   ELSE NULL
                                 END
         , [e].[birth_date] = CASE
                                WHEN [e].[birth_date] IS NOT NULL
                                     AND
                                         ( DATEDIFF(YEAR, [e].[birth_date], GETDATE()) > 18
                                           OR DATEDIFF(YEAR, [e].[birth_date], GETDATE()) < 1
                                         )
                                  THEN DATEADD([dd], CAST(RAND(DATEDIFF(DAY, 0, [e].[birth_date])) * 100 + 10 AS INT), [e].[birth_date])

                              /* add random days from 10 to 110 */
                                WHEN [e].[birth_date] IS NOT NULL
                                     AND
                                         ( DATEDIFF(YEAR, [e].[birth_date], GETDATE()) <= 18
                                           OR DATEDIFF(YEAR, [e].[birth_date], GETDATE()) >= 1
                                         )
                                  THEN DATEADD([dd], CAST(RAND(DATEDIFF(DAY, 0, [e].[birth_date])) * -100 - 10 AS INT), [e].[birth_date])

                              /* subract random days from 10 to 110 */
                                ELSE NULL
                              END

           /* need to ensure that JISA clients are still under 18 years old post washing */
         , [e].[title_id] = ( CASE
                                WHEN [e].[gender] = 'M'
                                  THEN 1
                                WHEN [e].[gender] = 'F'
                                  THEN 2
                                ELSE NULL
                              END )
         , [e].[fax_number] = NULL
         , [e].[contact_name] = NULL
         , [e].[mailing_title] = NULL
         , [e].[company_rego_no] = NULL
         , [e].[short_name] = ( CASE
                                  WHEN [e].[short_name] IS NULL
                                    THEN NULL
                                  ELSE 'ABC'
                                END )
    FROM [dbo].[entity] [e]
        JOIN [dbo].[member_account] [ma]
            ON [e].[entity_id] = [ma].[entity_id]
        LEFT JOIN [product_type] [p]
            ON [ma].[member_account_id] = [p].[ext_over_under_acc_id]
    WHERE 
          [p].[ext_over_under_acc_id] IS NULL
          AND [e].[entity_type] = 'I';

	SET @endTime = GETDATE();
	SET @StepMessage = 'Block 10 - Finish. Time Taken - ' + CONVERT(VARCHAR(20), @endTime - @startTime, 114);
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;

	/****** 'I' type entities that do not have any wrappers, aren't advisers, joint holders or related to another entity ******/

    SET @StepMessage = 'Block 11 - Start';
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;
	SET @startTime = GETDATE();

    TRUNCATE TABLE [#investors_to_change];
    INSERT INTO [#investors_to_change]
    SELECT 
          [e].[entity_id]
    FROM [dbo].[entity] AS [e]
        LEFT JOIN [dbo].[member_account] AS [ma]
            ON [e].[entity_id] = [ma].[entity_id]
        LEFT JOIN [product_type] AS [p]
            ON [ma].[member_account_id] = [p].[ext_over_under_acc_id]
        LEFT JOIN [adviser_account] AS [aa]
            ON [aa].[entity_id] = [e].[entity_id]
        LEFT JOIN [related_to] AS [rto]
            ON [e].[entity_id] = [rto].[related_entity_id]
        LEFT JOIN [joint_owner_details] AS [jod]
            ON [jod].[joint_entity_id] = [e].[entity_id]
    WHERE [p].[ext_over_under_acc_id] IS NULL
          AND [ma].[entity_id] IS NULL
          AND [e].[entity_type] = 'I'
          AND [aa].[entity_id] IS NULL
          AND [rto].[related_entity_id] IS NULL
          AND [jod].[joint_entity_id] IS NULL;

	SET @endTime = GETDATE();
	SET @StepMessage = 'Block 11 - Finish. Time Taken - ' + CONVERT(VARCHAR(20), @endTime - @startTime, 114);
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;

	/*************************************** name and salutation scrambling ******************************************/

    SET @StepMessage = 'Block 12 - Start';
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;
	SET @startTime = GETDATE();

    INSERT INTO [entity_scramble]
        ( 
          [name]
        ) 
    SELECT 
          [e].[name]
    FROM [dbo].[entity] AS [e]
        INNER JOIN [#investors_to_change] AS [i]
            ON [i].[entity_id] = [e].[entity_id]
    GROUP BY 
          [e].[name]
    ORDER BY 
          [e].[name];

	SET @endTime = GETDATE();
	SET @StepMessage = 'Block 12 - Finish. Time Taken - ' + CONVERT(VARCHAR(20), @endTime - @startTime, 114);
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;

	/*************************************** salutation scrambling ******************************************/

    SET @StepMessage = 'Block 13 - Start';
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;
	SET @startTime = GETDATE();

    UPDATE [e]
       SET [e].[salutation] = CASE
                                WHEN [e].[gender] = 'M'
                                     AND [e].[deceased_date] IS NULL
                                  THEN 'Mr'
                                WHEN [e].[gender] = 'M'
                                     AND [e].[deceased_date] IS NOT NULL
                                  THEN 'The Late Mr'
                                WHEN [e].[gender] = 'F'
                                     AND [e].[deceased_date] IS NULL
                                  THEN 'Mrs'
                                WHEN [e].[gender] = 'F'
                                     AND [e].[deceased_date] IS NOT NULL
                                  THEN 'The Late Mrs'
                              END + ' Ind ' + CAST([e2].[id] AS VARCHAR(400)) + ' LastName'
    FROM [entity] [e]
        JOIN [entity_scramble] [e2]
            ON [e].[name] = [e2].[name]
        JOIN [#investors_to_change] [i]
            ON [i].[entity_id] = [e].[entity_id]
    WHERE 
          [e].[entity_type] = 'I';

    SET @endTime = GETDATE();
	SET @StepMessage = 'Block 13 - Finish. Time Taken - ' + CONVERT(VARCHAR(20), @endTime - @startTime, 114);
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;

	/*************************************** name scrambling ******************************************/

    SET @StepMessage = 'Block 14 - Start';
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;
	SET @startTime = GETDATE();

    UPDATE [e1]
       SET [e1].[name] = 'Ind' + CAST([e2].[id] AS VARCHAR(400)) + ' LastName'
    FROM [entity] [e1]
        JOIN [entity_scramble] [e2]
            ON [e1].[name] = [e2].[name]
        JOIN [#investors_to_change] [i]
            ON [e1].[entity_id] = [i].[entity_id];

    SET @endTime = GETDATE();
	SET @StepMessage = 'Block 14 - Finish. Time Taken - ' + CONVERT(VARCHAR(20), @endTime - @startTime, 114);
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;

	/*************************************** given names scrambling ******************************************/

	SET @StepMessage = 'Block 15 - Start';
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;
	SET @startTime = GETDATE();

    TRUNCATE TABLE [entity_scramble];
    INSERT INTO [entity_scramble]
        ( 
          [name]
        ) 
    SELECT 
          [e].[given_names]
    FROM [dbo].[entity] AS [e]
        INNER JOIN [#investors_to_change] AS [i]
            ON [i].[entity_id] = [e].[entity_id]
    GROUP BY 
          [e].[given_names]
    ORDER BY 
          [e].[given_names];

    SET @endTime = GETDATE();
	SET @StepMessage = 'Block 15 - Finish. Time Taken - ' + CONVERT(VARCHAR(20), @endTime - @startTime, 114);
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;

	/*************************************** given_names and preferred_name scrambling ******************************************/

	SET @StepMessage = 'Block 16 - Start';
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;
	SET @startTime = GETDATE();

    UPDATE [e1]
       SET [e1].[given_names] = 'Ind' + CAST([e2].[id] AS VARCHAR(400)) + 'FirstName'
         , [e1].[preferred_name] = ( CASE
                                       WHEN [preferred_name] IS NOT NULL
                                         THEN 'Ind ' + CAST([e2].[id] AS VARCHAR(400)) + 'preferred_name'
                                     END )
    FROM [entity] [e1]
        JOIN [entity_scramble] [e2]
            ON [e1].[given_names] = [e2].[name]
        JOIN [#investors_to_change] [i]
            ON [i].[entity_id] = [e1].[entity_id]
    WHERE 
          [e1].[entity_type] = 'I';

    SET @endTime = GETDATE();
	SET @StepMessage = 'Block 16 - Finish. Time Taken - ' + CONVERT(VARCHAR(20), @endTime - @startTime, 114);
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;

	/*************************************** other fields scrambling for individuals ******************************************/
	
    SET @StepMessage = 'Block 17 - Start';
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;
	SET @startTime = GETDATE();

    UPDATE [e]
       SET [e].[email_address] = CASE
                                   WHEN [e].[email_address] IS NOT NULL
                                     THEN 'ScrambledInd' + CAST([e].[entity_id] AS VARCHAR(10)) + '@noemail.nodomain'
                                   ELSE NULL
                                 END
         , [e].[home_phone_number] = CASE
                                       WHEN [e].[home_phone_number] IS NOT NULL
                                         THEN '01000 000 000'
                                       ELSE NULL
                                     END
         , [e].[work_phone_number] = CASE
                                       WHEN [e].[work_phone_number] IS NOT NULL
                                         THEN '01000 111 111'
                                       ELSE NULL
                                     END
         , [e].[mobile_number] = CASE
                                   WHEN [e].[mobile_number] IS NOT NULL
                                     THEN '07000000000'
                                   ELSE NULL
                                 END
         , [e].[birth_date] = CASE
                                WHEN [e].[birth_date] IS NOT NULL
                                     AND
                                         ( DATEDIFF(YEAR, [e].[birth_date], GETDATE()) > 18
                                           OR DATEDIFF(YEAR, [e].[birth_date], GETDATE()) < 1
                                         )
                                  THEN DATEADD([dd], CAST(RAND(DATEDIFF(DAY, 0, [e].[birth_date])) * 100 + 10 AS INT), [e].[birth_date])

                              /* add random days from 10 to 110 */
                                WHEN [e].[birth_date] IS NOT NULL
                                     AND
                                         ( DATEDIFF(YEAR, [e].[birth_date], GETDATE()) <= 18
                                           OR DATEDIFF(YEAR, [e].[birth_date], GETDATE()) >= 1
                                         )
                                  THEN DATEADD([dd], CAST(RAND(DATEDIFF(DAY, 0, [e].[birth_date])) * -100 - 10 AS INT), [e].[birth_date])

                              /* subract random days from 10 to 100 */
                                ELSE NULL
                              END

           /* need to ensure that JISA clients are still under 18 years old post washing */
         , [e].[title_id] = ( CASE
                                WHEN [e].[gender] = 'M'
                                  THEN 1
                                WHEN [e].[gender] = 'F'
                                  THEN 2
                                ELSE NULL
                              END )
         , [e].[fax_number] = NULL
         , [e].[contact_name] = NULL
         , [e].[mailing_title] = NULL
         , [e].[company_rego_no] = NULL
         , [e].[short_name] = ( CASE
                                  WHEN [e].[short_name] IS NULL
                                    THEN NULL
                                  ELSE 'ABC'
                                END )
    FROM [entity] [e]
        JOIN [#investors_to_change] [i]
            ON [i].[entity_id] = [e].[entity_id];

    SET @endTime = GETDATE();
	SET @StepMessage = 'Block 17 - Finish. Time Taken - ' + CONVERT(VARCHAR(20), @endTime - @startTime, 114);
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;

	/*************************************** update corporates ******************************************/
    
	/*get corporate investor ids to be scrambled */
    SET @StepMessage = 'Block 18 - Start';
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;
	SET @startTime = GETDATE();

    TRUNCATE TABLE [#investors_to_change];
    INSERT INTO [#investors_to_change]
    SELECT 
          [e].[entity_id]
    FROM [dbo].[entity] AS [e]
        JOIN [dbo].[member_account] AS [ma]
            ON [e].[entity_id] = [ma].[entity_id]
        LEFT JOIN [product_type] AS [p]
            ON [ma].[member_account_id] = [p].[ext_over_under_acc_id]
    WHERE [p].[ext_over_under_acc_id] IS NULL
          AND [e].[entity_type] = 'C';

    SET @endTime = GETDATE();
	SET @StepMessage = 'Block 18 - Finish. Time Taken - ' + CONVERT(VARCHAR(20), @endTime - @startTime, 114);
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;

	/*************************************** populate entity_scramble ******************************************/

    SET @StepMessage = 'Block 19 - Start';
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;
	SET @startTime = GETDATE();

    TRUNCATE TABLE [entity_scramble];
    INSERT INTO [entity_scramble]
        ( 
          [name]
        ) 
    SELECT 
          [e].[name]
    FROM [dbo].[entity] AS [e]
        JOIN [dbo].[member_account] AS [ma]
            ON [e].[entity_id] = [ma].[entity_id]
        LEFT JOIN [product_type] AS [p]
            ON [ma].[member_account_id] = [p].[ext_over_under_acc_id]
    WHERE [p].[ext_over_under_acc_id] IS NULL
          AND [e].[entity_type] = 'C'
    GROUP BY 
          [e].[name]
    ORDER BY 
          [e].[name];

    SET @endTime = GETDATE();
	SET @StepMessage = 'Block 19 - Finish. Time Taken - ' + CONVERT(VARCHAR(20), @endTime - @startTime, 114);
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;

	/*************************************** name & preferred_name scrambling ******************************************/

	SET @StepMessage = 'Block 20 - Start';
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;
	SET @startTime = GETDATE();

    UPDATE [e1]
       SET [e1].[name] = 'Comp ' + CAST([e2].[id] AS VARCHAR(400)) + ' CompanyName'
         , [e1].[preferred_name] = 'Comp ' + CAST([e2].[id] AS VARCHAR(400)) + ' CompanyName'
    FROM [entity] [e1]
        JOIN [entity_scramble] [e2]
            ON [e1].[name] = [e2].[name]
        JOIN [#investors_to_change] [i]
            ON [e1].[entity_id] = [i].[entity_id];

    SET @endTime = GETDATE();
	SET @StepMessage = 'Block 20 - Finish. Time Taken - ' + CONVERT(VARCHAR(20), @endTime - @startTime, 114);
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;

	/*************************************** other fields scrambling ******************************************/

	SET @StepMessage = 'Block 21 - Start';
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;
	SET @startTime = GETDATE();

    UPDATE [e]
       SET [e].[home_phone_number] = CASE
                                       WHEN [e].[home_phone_number] IS NOT NULL
                                         THEN '01000 000 000'
                                       ELSE NULL
                                     END
         , [e].[work_phone_number] = CASE
                                       WHEN [e].[work_phone_number] IS NOT NULL
                                         THEN '01000 111 111'
                                       ELSE NULL
                                     END
         , [e].[fax_number] = CASE
                                WHEN [e].[mobile_number] IS NOT NULL
                                  THEN '07000000000'
                                WHEN [e].[fax_number] IS NOT NULL
                                  THEN '08000000000'
                                ELSE NULL
                              END
         , [e].[email_address] = CASE
                                   WHEN [e].[email_address] IS NOT NULL
                                     THEN 'ScrambledComp' + CAST([e].[entity_id] AS VARCHAR(10)) + '@noemail.nodomain'
                                   ELSE NULL
                                 END
    FROM [dbo].[entity] [e]
        JOIN [dbo].[member_account] [ma]
            ON [e].[entity_id] = [ma].[entity_id]
        LEFT JOIN [product_type] [p]
            ON [ma].[member_account_id] = [p].[ext_over_under_acc_id]
    WHERE 
          [p].[ext_over_under_acc_id] IS NULL
          AND [e].[entity_type] = 'C';

    SET @endTime = GETDATE();
	SET @StepMessage = 'Block 21 - Finish. Time Taken - ' + CONVERT(VARCHAR(20), @endTime - @startTime, 114);
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;

	/*************************************** update trusts  ******************************************/
    
	/*get trust investor ids to be scrambled */
    
	SET @StepMessage = 'Block 22 - Start';
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;
	SET @startTime = GETDATE();

    TRUNCATE TABLE [#investors_to_change];
    INSERT INTO [#investors_to_change]
    SELECT 
          [e].[entity_id]
    FROM [dbo].[entity] AS [e]
    WHERE [e].[entity_type] = 'T';

	SET @endTime = GETDATE();
	SET @StepMessage = 'Block 22 - Finish. Time Taken - ' + CONVERT(VARCHAR(20), @endTime - @startTime, 114);
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;

	/*************************************** name & preferred_name scrambling  ******************************************/

    SET @StepMessage = 'Block 23 - Start';
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;
	SET @startTime = GETDATE();

    TRUNCATE TABLE [entity_scramble];
    INSERT INTO [entity_scramble]
        ( 
          [name]
        ) 
    SELECT 
          [e].[name]
    FROM [dbo].[entity] AS [e]
    WHERE [e].[entity_type] = 'T'
    GROUP BY 
          [e].[name]
    ORDER BY 
          [e].[name];

    SET @endTime = GETDATE();
	SET @StepMessage = 'Block 23 - Finish. Time Taken - ' + CONVERT(VARCHAR(20), @endTime - @startTime, 114);
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;

	/*************************************** name & preferred_name scrambling  ******************************************/

	SET @StepMessage = 'Block 24 - Start';
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;
	SET @startTime = GETDATE();

    UPDATE [e1]
       SET [e1].[name] = 'Trust ' + CAST([e2].[id] AS VARCHAR(400)) + ' TrustName'
         , [e1].[preferred_name] = 'Trust ' + CAST([e2].[id] AS VARCHAR(400)) + ' TrustName'
    FROM [entity] [e1]
        JOIN [entity_scramble] [e2]
            ON [e1].[name] = [e2].[name]
        JOIN [#investors_to_change] [i]
            ON [e1].[entity_id] = [i].[entity_id];

    SET @endTime = GETDATE();
	SET @StepMessage = 'Block 24 - Finish. Time Taken - ' + CONVERT(VARCHAR(20), @endTime - @startTime, 114);
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;

	/*************************************** other fields scrambling  ******************************************/

	SET @StepMessage = 'Block 25 - Start';
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;
	SET @startTime = GETDATE();

    UPDATE [e]
       SET [e].[home_phone_number] = CASE
                                       WHEN [e].[home_phone_number] IS NOT NULL
                                         THEN '01000 000 000'
                                       ELSE NULL
                                     END
         , [e].[work_phone_number] = CASE
                                       WHEN [e].[work_phone_number] IS NOT NULL
                                         THEN '01000 111 111'
                                       ELSE NULL
                                     END
         , [e].[fax_number] = CASE
                                WHEN [e].[mobile_number] IS NOT NULL
                                  THEN '07000000000'
                                WHEN [e].[fax_number] IS NOT NULL
                                  THEN '08000000000'
                                ELSE NULL
                              END
         , [e].[email_address] = CASE
                                   WHEN [e].[email_address] IS NOT NULL
                                     THEN 'ScrambledTrust' + CAST([e].[entity_id] AS VARCHAR(10)) + '@noemail.nodomain'
                                   ELSE NULL
                                 END
    FROM [dbo].[entity] [e]
    WHERE 
          [e].[entity_type] = 'T';

    SET @endTime = GETDATE();
	SET @StepMessage = 'Block 25 - Finish. Time Taken - ' + CONVERT(VARCHAR(20), @endTime - @startTime, 114);
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;

	/*************************************** Joint Owners (who do NOT own a wrapper directly) ******************************************/
	
	/* joint entities using washed individual holdings */
    SET @StepMessage = 'Block 26 - Start';
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;
	SET @startTime = GETDATE();

	IF EXISTS 
		(	SELECT * 
			FROM sys.objects 
			WHERE [name] = N'au_entity' AND [type] = 'TR'
		)
	BEGIN
		ALTER TABLE [entity] DISABLE TRIGGER [au_entity];      
	END;

    UPDATE [dbo].[entity]
       SET [given_names] = 'Investor  ' + CONVERT(VARCHAR, [e].[entity_id])
         , [email_address] = 'scrambledJointOwner@noemail.nodomain'
         , [title_id] = ( CASE
                            WHEN [gender] = 'M'
                              THEN 1
                            WHEN [gender] = 'F'
                              THEN 2
                            ELSE NULL
                          END )
         , [mobile_number] = '07000000000'
         , [preferred_name] = NULL
         , [home_phone_number] = '01000 000 000'
         , [work_phone_number] = '01000 111 111'
         , [fax_number] = NULL
         , [salutation] = ( CASE
                              WHEN [gender] = 'M'
                                THEN 'Mr Investor'
                              WHEN [gender] = 'F'
                                THEN 'Mrs. Investor'
                              ELSE NULL
                            END )
         , [contact_name] = NULL
         , [mailing_title] = NULL
         , [company_rego_no] = NULL
         , [name] = CONCAT('Ind', RIGHT('00000' + CAST(ABS(CHECKSUM(NEWID()) % 1000) AS VARCHAR(5)), 5), ' LastName')
         , [birth_date] = CASE
                            WHEN [birth_date] IS NOT NULL
                                 AND
                                     ( DATEDIFF(YEAR, [birth_date], GETDATE()) > 18
                                       OR DATEDIFF(YEAR, [birth_date], GETDATE()) < 1
                                     )
                              THEN DATEADD([dd], CAST(RAND(DATEDIFF(DAY, 0, [birth_date])) * 100 + 10 AS INT), [birth_date])

                          /* add random days from 10 to 110 */
                            WHEN [e].[birth_date] IS NOT NULL
                                 AND
                                     ( DATEDIFF(YEAR, [birth_date], GETDATE()) <= 18
                                       OR DATEDIFF(YEAR, [birth_date], GETDATE()) >= 1
                                     )
                              THEN DATEADD([dd], CAST(RAND(DATEDIFF(DAY, 0, [birth_date])) * -100 - 10 AS INT), [birth_date])

                          /* subract random days from 10 to 110 */
                            ELSE NULL
                          END
         , [short_name] = ( CASE
                              WHEN [short_name] IS NULL
                                THEN NULL
                              ELSE 'ABC'
                            END )
    FROM [dbo].[joint_owner_details] [jod]
        INNER JOIN [dbo].[entity] [e]
            ON [jod].[joint_entity_id] = [e].[entity_id]
        LEFT OUTER JOIN [dbo].[member_account] [macc]
            ON [jod].[joint_entity_id] = [macc].[entity_id]
    WHERE 
          [macc].[member_account_id] IS NULL;

	IF EXISTS 
		(	SELECT * 
			FROM sys.objects 
			WHERE [name] = N'au_entity' AND [type] = 'TR'
		)
	BEGIN
		ALTER TABLE [entity] ENABLE TRIGGER [au_entity];
	END;
	
	SET @endTime = GETDATE();
	SET @StepMessage = 'Block 26 - Finish. Time Taken - ' + CONVERT(VARCHAR(20), @endTime - @startTime, 114);
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;

	/*************************************** Joint Owners - Other fields ******************************************/
    
	SET @StepMessage = 'Block 27 - Start';
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;
	SET @startTime = GETDATE();

	IF EXISTS 
		(	SELECT * 
			FROM sys.objects 
			WHERE [name] = N'au_entity' AND [type] = 'TR'
		)
	BEGIN
		ALTER TABLE [entity] DISABLE TRIGGER [au_entity];
	END;

    UPDATE [je2]
       SET [je2].[title_id] = ( CASE
                                  WHEN [je2].[gender] = 'M'
                                    THEN 1
                                  WHEN [je2].[gender] = 'F'
                                    THEN 2
                                  ELSE NULL
                                END )
         , [je2].[name] = ISNULL([dbo].[fn_RowConcatenation] ( [je2].[entity_id] ), '')
         , [je2].[given_names] = NULL
         , [je2].[birth_date] = NULL
         , [je2].[salutation] = NULL
         , [je2].[email_address] = CASE
                                     WHEN [email_address] IS NOT NULL
                                       THEN 'ScrambledJnt' + CAST([entity_id] AS VARCHAR(10)) + '@noemail.nodomain'
                                     ELSE NULL
                                   END
         , [je2].[home_phone_number] = CASE
                                         WHEN [home_phone_number] IS NOT NULL
                                           THEN '01000 000 000'
                                         ELSE NULL
                                       END
         , [je2].[work_phone_number] = CASE
                                         WHEN [work_phone_number] IS NOT NULL
                                           THEN '01000 111 111'
                                         ELSE NULL
                                       END
         , [je2].[mobile_number] = CASE
                                     WHEN [mobile_number] IS NOT NULL
                                       THEN '07000000000'
                                     ELSE NULL
                                   END
    FROM [dbo].[entity] AS [je2]
    WHERE 
          [je2].[entity_type] = 'J';
    
	IF EXISTS 
		(	SELECT * 
			FROM sys.objects 
			WHERE [name] = N'au_entity' AND [type] = 'TR'
		)
	BEGIN
		ALTER TABLE [entity] ENABLE TRIGGER [au_entity];
	END;

	SET @endTime = GETDATE();
	SET @StepMessage = 'Block 27 - Finish. Time Taken - ' + CONVERT(VARCHAR(20), @endTime - @startTime, 114);
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;

	/*************************************** Employer ******************************************/

	SET @StepMessage = 'Block 28 - Start';
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;
	SET @startTime = GETDATE();

	IF EXISTS 
		(	SELECT * 
			FROM sys.objects 
			WHERE [name] = N'au_entity' AND [type] = 'TR'
		)
	BEGIN
		ALTER TABLE [entity] DISABLE TRIGGER [au_entity];
	END;    

    UPDATE [entity]
       SET [given_names] = NULL
         , [email_address] = 'scrambled@noemail.nodomain'
         , [title_id] = ( CASE
                            WHEN [gender] = 'M'
                              THEN 1
                            WHEN [gender] = 'F'
                              THEN 2
                            ELSE NULL
                          END )
         , [mobile_number] = '07000000000'
         , [home_phone_number] = '01000 000 000'
         , [work_phone_number] = '01000 111 111'
         , [fax_number] = NULL
         , [salutation] = 'Dear Sir / Madam'
         , [contact_name] = NULL
         , [mailing_title] = NULL
         , [company_rego_no] = NULL
         , [name] = CASE [e].[entity_id] % 50
                      WHEN 0
                        THEN 'Brown Beautv'
                      WHEN 1
                        THEN 'Burbank'
                      WHEN 2
                        THEN 'Calrose'
                      WHEN 3
                        THEN 'Charles Downing'
                      WHEN 4
                        THEN 'Chippewa'
                      WHEN 5
                        THEN 'Dakota Red'
                      WHEN 6
                        THEN 'Earlaine'
                      WHEN 7
                        THEN 'Early Ohio'
                      WHEN 8
                        THEN 'Early Rose'
                      WHEN 9
                        THEN 'Erie'
                      WHEN 10
                        THEN 'Garnet Chili'
                      WHEN 11
                        THEN 'Green Mountain'
                      WHEN 12
                        THEN 'Harmon'
                      WHEN 13
                        THEN 'Houma I'
                      WHEN 14
                        THEN 'Irish Cobbler'
                      WHEN 15
                        THEN 'Kasota'
                      WHEN 16
                        THEN 'Katahdin'
                      WHEN 17
                        THEN 'Kennebec'
                      WHEN 18
                        THEN 'Menominee'
                      WHEN 19
                        THEN 'Mesaba'
                      WHEN 20
                        THEN 'Mohawk'
                      WHEN 21
                        THEN 'Norkota'
                      WHEN 22
                        THEN 'Ontario'
                      WHEN 23
                        THEN 'Pawnee'
                      WHEN 24
                        THEN 'Pontiac'
                      WHEN 25
                        THEN 'President'
                      WHEN 26
                        THEN 'Red McClure'
                      WHEN 27
                        THEN 'Red Warba'
                      WHEN 28
                        THEN 'Rural New Yorker'
                      WHEN 29
                        THEN 'Russet Burbank '
                      WHEN 30
                        THEN 'Russet Rural'
                      WHEN 31
                        THEN 'Sebago'
                      WHEN 32
                        THEN 'Sequoia'
                      WHEN 33
                        THEN 'Spaulding Rose'
                      WHEN 34
                        THEN 'Teton'
                      WHEN 35
                        THEN 'Triumphant Inc'
                      WHEN 36
                        THEN 'Warba'
                      WHEN 37
                        THEN 'Blue Christy'
                      WHEN 38
                        THEN 'Columbia'
                      WHEN 39
                        THEN 'Earlaine'
                      WHEN 40
                        THEN 'Early Blue '
                      WHEN 41
                        THEN 'Earlv Michigan'
                      WHEN 42
                        THEN 'Golden'
                      WHEN 43
                        THEN 'Cobbler'
                      WHEN 44
                        THEN 'Mahr'
                      WHEN 45
                        THEN 'McCormick'
                      WHEN 46
                        THEN 'Nittany'
                      WHEN 47
                        THEN 'Pennigan'
                      WHEN 48
                        THEN 'Strawberrv'
                      ELSE 'White Gold'
                    END + ' ' + UPPER(SUBSTRING(CONVERT(VARCHAR(100), NEWID()), 1, 1)) + CASE [e].[entity_id] % 3
                                                                                           WHEN 0
                                                                                             THEN ' Corp'
                                                                                           ELSE CASE [e].[entity_id] % 4
                                                                                                  WHEN 0
                                                                                                    THEN ' Ltd'
                                                                                                END
                                                                                         END
    FROM [entity] [e]
        JOIN [employer] [emp]
            ON [e].[entity_id] = [emp].[entity_id];

	SET @endTime = GETDATE();
	SET @StepMessage = 'Block 28 - Finish. Time Taken - ' + CONVERT(VARCHAR(20), @endTime - @startTime, 114);
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;

    /*************************************** Employer - preferred_name ******************************************/

	SET @StepMessage = 'Block 29 - Start';
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;
	SET @startTime = GETDATE();

    UPDATE [entity]
       SET [preferred_name] = [name]
    FROM [entity] [e]
        JOIN [employer] [emp]
            ON [e].[entity_id] = [emp].[entity_id];

    SET @endTime = GETDATE();
	SET @StepMessage = 'Block 29 - Finish. Time Taken - ' + CONVERT(VARCHAR(20), @endTime - @startTime, 114);
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;

	/*************************************** Giin for all entities ******************************************/

	SET @StepMessage = 'Block 30 - Start';
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;
	SET @startTime = GETDATE();

    UPDATE [entity]
       SET [giin] = CASE
                      WHEN [giin] IS NOT NULL
                        THEN 'Removed'
                    END;

	IF EXISTS 
		(	SELECT * 
			FROM sys.objects 
			WHERE [name] = N'au_entity' AND [type] = 'TR'
		)
	BEGIN
		ALTER TABLE [entity] ENABLE TRIGGER [au_entity];
	END;    

	SET @endTime = GETDATE();
	SET @StepMessage = 'Block 30 - Finish. Time Taken - ' + CONVERT(VARCHAR(20), @endTime - @startTime, 114);
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;

	/*************************************** Adviser ******************************************/

    SET @StepMessage = 'Block 31 - Start';
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;
	SET @startTime = GETDATE();

	IF EXISTS 
		(	SELECT * 
			FROM sys.objects 
			WHERE [name] = N'au_entity' AND [type] = 'TR'
		)
	BEGIN
		ALTER TABLE [entity] DISABLE TRIGGER [au_entity];
	END;        

    UPDATE [entity]
       SET [given_names] = ( CASE
                               WHEN [gender] = 'M'
                                 THEN CASE [e].[entity_id] % 11
                                        WHEN 0
                                          THEN 'Bobby'
                                        WHEN 1
                                          THEN 'Harry'
                                        WHEN 2
                                          THEN 'Jimmy'
                                        WHEN 3
                                          THEN 'Wayne'
                                        WHEN 4
                                          THEN 'Arthur'
                                        WHEN 5
                                          THEN 'Adam'
                                        WHEN 6
                                          THEN 'David'
                                        WHEN 7
                                          THEN 'Paul'
                                        WHEN 8
                                          THEN 'Thomas'
                                        WHEN 9
                                          THEN 'Donald'
                                        WHEN 10
                                          THEN 'Shawn'
                                      END
                               ELSE CASE [e].[entity_id] % 7
                                      WHEN 0
                                        THEN 'Jean'
                                      WHEN 1
                                        THEN 'Susan'
                                      WHEN 2
                                        THEN 'Lori'
                                      WHEN 3
                                        THEN 'Helen'
                                      WHEN 4
                                        THEN 'Sarah'
                                      WHEN 5
                                        THEN 'Norma'
                                      WHEN 6
                                        THEN 'Doris'
                                    END
                             END )
         , [email_address] = 'scrambledAdviser@noemail.nodomain'
         , [title_id] = ( CASE
                            WHEN [gender] = 'M'
                              THEN 1
                            WHEN [gender] = 'F'
                              THEN 2
                            ELSE NULL
                          END )
         , [mobile_number] = '07000000000'
         , [preferred_name] = CASE
                                WHEN [preferred_name] IS NOT NULL
                                  THEN ( CASE
                                           WHEN [gender] = 'M'
                                             THEN CASE [e].[entity_id] % 11
                                                    WHEN 0
                                                      THEN 'Bob'
                                                    WHEN 1
                                                      THEN 'Har'
                                                    WHEN 2
                                                      THEN 'Jim'
                                                    WHEN 3
                                                      THEN 'Way'
                                                    WHEN 4
                                                      THEN 'Art'
                                                    WHEN 5
                                                      THEN 'Ada'
                                                    WHEN 6
                                                      THEN 'Dav'
                                                    WHEN 7
                                                      THEN 'Pau'
                                                    WHEN 8
                                                      THEN 'Tho'
                                                    WHEN 9
                                                      THEN 'Don'
                                                    WHEN 10
                                                      THEN 'Sha'
                                                  END
                                           ELSE CASE [e].[entity_id] % 7
                                                  WHEN 0
                                                    THEN 'Jea'
                                                  WHEN 1
                                                    THEN 'Sus'
                                                  WHEN 2
                                                    THEN 'Lor'
                                                  WHEN 3
                                                    THEN 'Hel'
                                                  WHEN 4
                                                    THEN 'Sar'
                                                  WHEN 5
                                                    THEN 'Nor'
                                                  WHEN 6
                                                    THEN 'Dor'
                                                END
                                         END )
                              END
         , [home_phone_number] = '01000 000 000'
         , [work_phone_number] = '01000 111 111'
         , [fax_number] = NULL
         , [salutation] = ( CASE
                              WHEN [gender] = 'M'
                                THEN 'Mr ' + [name]
                              WHEN [gender] = 'F'
                                THEN 'Mrs ' + 'Adviser ' + CONVERT(VARCHAR, [adv_acc].[adviser_account_id])
                              ELSE NULL
                            END )
         , [contact_name] = NULL
         , [mailing_title] = NULL
         , [company_rego_no] = NULL
         , [name] = 'Adviser ' + CONVERT(VARCHAR, [adv_acc].[adviser_account_id])
    FROM [adviser_account] [adv_acc]
        JOIN [entity] [e]
            ON [e].[entity_id] = [adv_acc].[entity_id];

	IF EXISTS 
		(	SELECT * 
			FROM sys.objects 
			WHERE [name] = N'au_entity' AND [type] = 'TR'
		)
	BEGIN
		ALTER TABLE [entity] ENABLE TRIGGER [au_entity];
	END;        

	SET @endTime = GETDATE();
	SET @StepMessage = 'Block 31 - Finish. Time Taken - ' + CONVERT(VARCHAR(20), @endTime - @startTime, 114);
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;

	/*************************************** Dealer ******************************************/

    SET @StepMessage = 'Block 32 - Start';
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;
	SET @startTime = GETDATE();

    IF EXISTS 
		(	SELECT * 
			FROM sys.objects 
			WHERE [name] = N'au_entity' AND [type] = 'TR'
		)
	BEGIN
		ALTER TABLE [entity] DISABLE TRIGGER [au_entity];
	END;

    UPDATE [entity]
       SET [given_names] = NULL
         , [email_address] = 'scrambledNetwork@noemail.nodomain'
         , [title_id] = ( CASE
                            WHEN [gender] = 'M'
                              THEN 1
                            WHEN [gender] = 'F'
                              THEN 2
                            ELSE NULL
                          END )
         , [mobile_number] = '07000000000'
         , [preferred_name] = 'Mr Dealer  ' + CONVERT(VARCHAR, [dealer_id])
         , [home_phone_number] = '01000 000 000'
         , [work_phone_number] = '01000 111 111'
         , [fax_number] = NULL
         , [salutation] = 'Dear Sir / Madam'
         , [contact_name] = NULL
         , [mailing_title] = NULL
         , [company_rego_no] = NULL
         , [name] = ISNULL( ( CASE [e].[entity_id] % 8
                                WHEN 0
                                  THEN 'Light '
                                WHEN 1
                                  THEN 'Dark '
                                ELSE 'Mid '
                              END + CASE [e].[entity_id] % 50
                                      WHEN 0
                                        THEN 'Antique White'
                                      WHEN 1
                                        THEN 'Aqua'
                                      WHEN 2
                                        THEN 'Aquamarine'
                                      WHEN 3
                                        THEN 'Azure'
                                      WHEN 4
                                        THEN 'Beige'
                                      WHEN 5
                                        THEN 'Bisque'
                                      WHEN 6
                                        THEN 'Black'
                                      WHEN 7
                                        THEN 'Almond'
                                      WHEN 8
                                        THEN 'Blue'
                                      WHEN 9
                                        THEN 'Mustard'
                                      WHEN 10
                                        THEN 'Brown'
                                      WHEN 11
                                        THEN 'Burlywood'
                                      WHEN 12
                                        THEN 'Cadet Blue'
                                      WHEN 13
                                        THEN 'Chartreuse'
                                      WHEN 14
                                        THEN 'Chocolate'
                                      WHEN 15
                                        THEN 'Coral'
                                      WHEN 16
                                        THEN 'Cornflower'
                                      WHEN 17
                                        THEN 'Cornsilk'
                                      WHEN 18
                                        THEN 'Crimson'
                                      WHEN 19
                                        THEN 'Cyan'
                                      WHEN 20
                                        THEN 'Blue'
                                      WHEN 21
                                        THEN 'Cyan'
                                      WHEN 22
                                        THEN 'Goldenrod'
                                      WHEN 23
                                        THEN 'Gray'
                                      WHEN 24
                                        THEN 'Green'
                                      WHEN 25
                                        THEN 'Khaki'
                                      WHEN 26
                                        THEN 'Magenta'
                                      WHEN 27
                                        THEN 'Green'
                                      WHEN 28
                                        THEN 'Orange'
                                      WHEN 29
                                        THEN 'Orchid'
                                      WHEN 30
                                        THEN 'Red'
                                      WHEN 31
                                        THEN 'Salmon'
                                      WHEN 32
                                        THEN 'Sea Green'
                                      WHEN 33
                                        THEN 'Slate Blue'
                                      WHEN 34
                                        THEN 'Slate Gray'
                                      WHEN 35
                                        THEN 'Turquoise'
                                      WHEN 36
                                        THEN 'Violet'
                                      WHEN 37
                                        THEN 'Pink'
                                      WHEN 38
                                        THEN 'Sky Blue'
                                      WHEN 39
                                        THEN 'Dim Gray'
                                      WHEN 40
                                        THEN 'Dodger Blue'
                                      WHEN 41
                                        THEN 'Firebrick'
                                      WHEN 42
                                        THEN 'Off White'
                                      WHEN 43
                                        THEN 'Forest Green'
                                      WHEN 44
                                        THEN 'Fuchsia'
                                      WHEN 45
                                        THEN 'Gainsboro'
                                      WHEN 46
                                        THEN 'Eggshell'
                                      WHEN 47
                                        THEN 'Gold'
                                      WHEN 48
                                        THEN 'Goldenrod'
                                      ELSE 'Yellow'
                                    END + ' ' + CASE [e].[entity_id] % 3
                                                  WHEN 0
                                                    THEN 'Best'
                                                  WHEN 1
                                                    THEN 'Link'
                                                  ELSE 'Top'
                                                END + ' ' + CASE [e].[entity_id] % 4
                                                              WHEN 0
                                                                THEN 'Solutions'
                                                              WHEN 1
                                                                THEN 'Network'
                                                              WHEN 2
                                                                THEN 'Advisers'
                                                              ELSE 'Growth'
                                                            END + ' (' + CONVERT(VARCHAR, [d].[dealer_id]) + ')' + CASE
                                                                                                                     WHEN [e].[name] LIKE '% DM'
                                                                                                                       THEN ' DM'
                                                                                                                     ELSE ''
                                                                                                                   END ), '') -- add discretionary suffix
    FROM [entity] [e]
        JOIN [dealer] [d]
            ON [e].[entity_id] = [d].[entity_id]
    WHERE 
          [d].[dealer_id] <> 1  -- Blueplanet Direct Network
          AND [d].[dealer_id] <> 2  -- AEGON Retirement Choices Direct ARC 
          AND [d].[dealer_id] <> 3  -- AEGON Retirement Choices Sales ARC
          AND [d].[dealer_id] <> 299  -- AEGON One Retirement Direct AOR
          AND [d].[dealer_id] <> 1805;
    -- AEGON Retiready 

	IF EXISTS 
		(	SELECT * 
			FROM sys.objects 
			WHERE [name] = N'au_entity' AND [type] = 'TR'
		)
	BEGIN
		ALTER TABLE [entity] ENABLE TRIGGER [au_entity];
	END;

	SET @endTime = GETDATE();
	SET @StepMessage = 'Block 32 - Finish. Time Taken - ' + CONVERT(VARCHAR(20), @endTime - @startTime, 114);
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;

	/*************************************** Dealer ******************************************/
	
	/*	Branches are assigned to the subfund, so we will look at all branches to see if they are all of one "subfund type"
		and rename accordingly else .leave. the suffix alone. */

    SET @StepMessage = 'Block 33 - Start';
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;
	SET @startTime = GETDATE();

	IF EXISTS 
		(	SELECT * 
			FROM sys.objects 
			WHERE [name] = N'au_entity' AND [type] = 'TR'
		)
	BEGIN
		ALTER TABLE [entity] DISABLE TRIGGER [au_entity];
	END;

    UPDATE [entity]
       SET [name] = [name] + ' ' + ( CASE
                                       WHEN NOT EXISTS
        ( SELECT 
                [db].[dealer_id]
          FROM [dealer_branch] AS [db]
              JOIN [subfund_associated_party] AS [sap]
                  ON [sap].[party_id] = [db].[dealer_branch_id]
                     AND [sap].[party_type_id] = 26
              JOIN [subfund] AS [sf]
                  ON [sf].[subfund_id] = [sap].[subfund_id]
          WHERE [db].[dealer_id] = [d].[dealer_id]
                AND
                    ( [sf].[short_code] = 'AOR'
                      OR [sf].[short_code] = 'AEGON'
                      OR [sf].[short_code] = 'ARR'
                    )
        )
                                         THEN ''
                                       WHEN NOT EXISTS
        ( SELECT 
                [db].[dealer_id]
          FROM [dealer_branch] AS [db]
              JOIN [subfund_associated_party] AS [sap]
                  ON [sap].[party_id] = [db].[dealer_branch_id]
                     AND [sap].[party_type_id] = 26
              JOIN [subfund] AS [sf]
                  ON [sf].[subfund_id] = [sap].[subfund_id]
          WHERE [db].[dealer_id] = [d].[dealer_id]
                AND
                    ( [sf].[short_code] = 'AOR'
                      OR [sf].[short_code] = 'AEGON'
                    )
        )
                                         THEN 'RR'
                                       WHEN NOT EXISTS
        ( SELECT 
                [db].[dealer_id]
          FROM [dealer_branch] AS [db]
              JOIN [subfund_associated_party] AS [sap]
                  ON [sap].[party_id] = [db].[dealer_branch_id]
                     AND [sap].[party_type_id] = 26
              JOIN [subfund] AS [sf]
                  ON [sf].[subfund_id] = [sap].[subfund_id]
          WHERE [db].[dealer_id] = [d].[dealer_id]
                AND
                    ( [sf].[short_code] = 'ARR'
                      OR [sf].[short_code] = 'AEGON'
                    )
        )
                                         THEN 'AOR'
                                       WHEN NOT EXISTS
        ( SELECT 
                [db].[dealer_id]
          FROM [dealer_branch] AS [db]
              JOIN [subfund_associated_party] AS [sap]
                  ON [sap].[party_id] = [db].[dealer_branch_id]
                     AND [sap].[party_type_id] = 26
              JOIN [subfund] AS [sf]
                  ON [sf].[subfund_id] = [sap].[subfund_id]
          WHERE [db].[dealer_id] = [d].[dealer_id]
                AND
                    ( [sf].[short_code] = 'ARR'
                      OR [sf].[short_code] = 'AOR'
                    )
        )
                                         THEN 'ARC'
                                     END )
    FROM [entity] [e]
        JOIN [dealer] [d]
            ON [d].[entity_id] = [e].[entity_id]
    WHERE 
          [d].[dealer_id] <> 1  -- Blueplanet Direct Network
          AND [d].[dealer_id] <> 2  -- AEGON Retirement Choices Direct ARC 
          AND [d].[dealer_id] <> 3  -- AEGON Retirement Choices Sales ARC
          AND [d].[dealer_id] <> 299  -- AEGON One Retirement Direct AOR
          AND [d].[dealer_id] <> 1805;
    -- AEGON Retiready  

	SET @endTime = GETDATE();
	SET @StepMessage = 'Block 33 - Finish. Time Taken - ' + CONVERT(VARCHAR(20), @endTime - @startTime, 114);
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;
    
	/*************************************** Dealer - preferred_name ******************************************/

	SET @StepMessage = 'Block 34 - Start';
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;
	SET @startTime = GETDATE();

    UPDATE [entity]
       SET [preferred_name] = LEFT([name], 40)
    FROM [entity] [e]
        JOIN [dealer] [d]
            ON [e].[entity_id] = [d].[entity_id];

	IF EXISTS 
		(	SELECT * 
			FROM sys.objects 
			WHERE [name] = N'au_entity' AND [type] = 'TR'
		)
	BEGIN
		ALTER TABLE [entity] ENABLE TRIGGER [au_entity];
	END;

	SET @endTime = GETDATE();
	SET @StepMessage = 'Block 34 - Finish. Time Taken - ' + CONVERT(VARCHAR(20), @endTime - @startTime, 114);
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;

	/*************************************** Dealer Branch ******************************************/

    SET @StepMessage = 'Block 35 - Start';
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;
	SET @startTime = GETDATE();

	IF EXISTS 
		(	SELECT * 
			FROM sys.objects 
			WHERE [name] = N'au_entity' AND [type] = 'TR'
		)
	BEGIN
		ALTER TABLE [entity] DISABLE TRIGGER [au_entity];
	END;

    UPDATE [e]
       SET [given_names] = NULL
         , [email_address] = 'scrambledFirm@noemail.nodomain'
         , [title_id] = ( CASE
                            WHEN [e].[gender] = 'M'
                              THEN 1
                            WHEN [e].[gender] = 'F'
                              THEN 2
                            ELSE NULL
                          END )
         , [mobile_number] = '07000000000'
         , [preferred_name] = 'Mr IFA Branch ' + CONVERT(VARCHAR, [dealer_branch_id])
         , [home_phone_number] = '01000 000 000'
         , [work_phone_number] = '01000 111 111'
         , [fax_number] = NULL
         , [salutation] = 'Dear Sir / Madam'
         , [contact_name] = NULL
         , [mailing_title] = NULL
         , [company_rego_no] = NULL
         , [name] = SUBSTRING([de].[name], 1, CHARINDEX(' ', [de].[name], 1) - 1) + ' ' + ' (' + CONVERT(VARCHAR, [db].[dealer_id]) + '/' + CONVERT(VARCHAR, [db].[dealer_branch_id]) + ')' + ' ' + CASE
                                                                                                                                                                                                      WHEN [sf].[short_code] = 'AOR'
                                                                                                                                                                                                        THEN 'AOR'
                                                                                                                                                                                                      WHEN [sf].[short_code] = 'AEGON'
                                                                                                                                                                                                        THEN 'ARC'
                                                                                                                                                                                                      WHEN [sf].[short_code] = 'ARR'
                                                                                                                                                                                                        THEN 'RR'
                                                                                                                                                                                                      WHEN [sf].[short_code] = 'COFUNDS'
                                                                                                                                                                                                        THEN 'COF'
                                                                                                                                                                                                      ELSE ''
                                                                                                                                                                                                    END + ( CASE
                                                                                                                                                                                                              WHEN [e].[name] LIKE '% DM'
                                                                                                                                                                                                                THEN ' DM'
                                                                                                                                                                                                              ELSE ''
                                                                                                                                                                                                            END )  -- add discretionary suffix				            
    FROM [dealer_branch] [db]
        JOIN [entity] [e]
            ON [e].[entity_id] = [db].[entity_id]
        JOIN [dealer] [d]
            ON [db].[dealer_id] = [d].[dealer_id]
        JOIN [entity] [de]
            ON [de].[entity_id] = [d].[entity_id]
        LEFT JOIN [subfund_associated_party] [sap]
            ON [sap].[party_id] = [db].[dealer_branch_id]
        LEFT JOIN [subfund] [sf]
            ON [sf].[subfund_id] = [sap].[subfund_id]
    WHERE 
          [db].[dealer_branch_id] NOT IN
    ( 1, --          Blueplanet Direct Branch
          2, --          AEGON At Retirment
          3, --          Test branch
          4, --          AEGON Retirement Choices Sales
          5, --          AEGON Workplace
          526, --      AEGON One Retirement
          2895, --    AEGON Retiready 0%
          2896, --    AEGON Retiready 6%
          2897, --    AEGON Retiready 12%
          2898, --    AEGON Retiready 19%
          2899, --    AEGON Retiready 25%
          2900, --    AEGON Retiready 31%
          2901, --    AEGON Retiready 37%
          2902, --    AEGON Retiready 44%
          2903--  AEGON Retiready 50%
    )
          AND [d].[dealer_id] <> 1805;

    SET @endTime = GETDATE();
	SET @StepMessage = 'Block 35 - Finish. Time Taken - ' + CONVERT(VARCHAR(20), @endTime - @startTime, 114);
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;

	/*************************************** Dealer Branch - preferred_name ******************************************/

	SET @StepMessage = 'Block 36 - Start';
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;
	SET @startTime = GETDATE();

    UPDATE [entity]
       SET [preferred_name] = [name]
    FROM [entity] [e]
        JOIN [dealer_branch] [db]
            ON [e].[entity_id] = [db].[entity_id];

	IF EXISTS
		(	SELECT * 
			FROM sys.objects 
			WHERE [name] = N'au_entity' AND [type] = 'TR'
		)
	BEGIN
		ALTER TABLE [entity] ENABLE TRIGGER [au_entity];
	END;

    SET @endTime = GETDATE();
	SET @StepMessage = 'Block 36 - Finish. Time Taken - ' + CONVERT(VARCHAR(20), @endTime - @startTime, 114);
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;

	/*************************************** Investment Manager ******************************************/
	
    SET @StepMessage = 'Block 37 - Start';
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;
	SET @startTime = GETDATE();

	IF EXISTS
		(	SELECT * 
			FROM sys.objects 
			WHERE [name] = N'au_entity' AND [type] = 'TR'
		)
	BEGIN
		ALTER TABLE [entity] DISABLE TRIGGER [au_entity];
	END;

    UPDATE [entity]
       SET [fax_number] = '+441234 567 890'
         , [home_phone_number] = '01000 000 000'
         , [work_phone_number] = '01000 111 111'
         , [salutation] = CASE [e].[salutation]
                            WHEN 'Sir/Madam'
                              THEN [e].[salutation]
                            WHEN 'Administration Services'
                              THEN [e].[salutation]
                            ELSE 'ZZZ'
                          END
         , [email_address] = CASE
                               WHEN [e].[email_address] IS NULL
                                 THEN NULL
                               ELSE 'scrambledInvestManager@noemail.nodomain'
                             END
    FROM [investment_manager] [db]
        JOIN [entity] [e]
            ON [e].[entity_id] = [db].[entity_id];

	IF EXISTS
		(	SELECT * 
			FROM sys.objects 
			WHERE [name] = N'au_entity' AND [type] = 'TR'
		)
	BEGIN
		ALTER TABLE [entity] ENABLE TRIGGER [au_entity];
	END;

	SET @endTime = GETDATE();
	SET @StepMessage = 'Block 37 - Finish. Time Taken - ' + CONVERT(VARCHAR(20), @endTime - @startTime, 114);
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;

	/*************************************** Postcode Scrambling ******************************************/

	SET @StepMessage = 'Block 38 - Start';
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;
	SET @startTime = GETDATE();

	IF EXISTS
		(	SELECT * 
			FROM sys.objects 
			WHERE [name] = N'au_address' AND [type] = 'TR'
		)
	BEGIN
		ALTER TABLE [address] DISABLE TRIGGER [au_address];
	END;

	INSERT postcode_scramble 
	(
		postcode
	) 
	SELECT DISTINCT 
		a.postcode 
	FROM
		address a
	WHERE
		a.postcode IS NOT NULL
		AND a.postcode <> ''
	ORDER BY 
		a.postcode 

	SELECT 
		@postcodeCount = Count(1) 
	FROM
		postcode_scramble; 
	
	--Control table for chunk updates
	IF OBJECT_ID('tempdb..#upd_address_control') IS NOT NULL
		DROP TABLE #upd_address_control;

	CREATE TABLE #upd_address_control
	(
		control_id INT NOT NULL,
		complete INT DEFAULT 0 NOT NULL
	);

	INSERT INTO [#upd_address_control] 
	(
		control_id
	)
	SELECT
		address_id
	FROM 
		address a 
	WHERE
		a.postcode IS NOT NULL
		AND a.postcode <> ''
	
	CREATE CLUSTERED INDEX idx_address_control ON #upd_address_control(control_id)

	WHILE EXISTS 
		(
			SELECT 
                1
			FROM 
				[#upd_address_control]
			WHERE
				complete <> 2
		)
	BEGIN
		UPDATE TOP (@batchSize) 
			[#upd_address_control]
		SET 
			complete = 1
		WHERE
			complete = 0

		UPDATE top (@batchSize) a 
		SET
			postcode =	CASE
							WHEN a.postcode = p.postcode THEN
								CASE a.address_id % 5 
									WHEN 0 THEN 'NX' 
									WHEN 1 THEN 'GW' 
									WHEN 2 THEN 'SZ' 
									WHEN 3 THEN 'BY' 
									WHEN 4 THEN 'AX' 
								END 
								+ CONVERT(VARCHAR, ( [address_id] % 40 ) + 1) 
								+ ' ' + CONVERT(VARCHAR, ( [address_id] % 8 ) + 1) 
								+ CASE a.address_id % 6 
									WHEN 0 THEN 'AZ' 
									WHEN 1 THEN 'TG' 
									WHEN 2 THEN 'ER' 
									WHEN 3 THEN 'NJ' 
									WHEN 4 THEN 'WX' 
									WHEN 5 THEN 'GA' 
								END
						ELSE
							p.postcode 
						END 
		FROM
			address a
			JOIN [#upd_address_control] u
				ON u.control_id = a.address_id
				AND u.complete = 1
			JOIN postcode_scramble p 
				ON p.id = CAST(RAND(ABS(CHECKSUM(a.postcode))) * @postcodeCount + 1 AS INT)

		UPDATE
			[#upd_address_control]
		SET 
			complete = 2
		WHERE
			complete = 1
	END

	--clean up
	IF OBJECT_ID('tempdb..#upd_address_control') IS NOT NULL
		DROP TABLE #upd_address_control;

	IF EXISTS
		(	SELECT * 
			FROM sys.objects 
			WHERE [name] = N'au_address' AND [type] = 'TR'
		)
	BEGIN
		ALTER TABLE [address] ENABLE TRIGGER [au_address];
	END;

	SET @endTime = GETDATE();
	SET @StepMessage = 'Block 38 - Finish. Time Taken - ' + CONVERT(VARCHAR(20), @endTime - @startTime, 114);
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;

	/*************************************** Investment Manager Address ******************************************/
    
	SET @StepMessage = 'Block 39 - Start';
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;
	SET @startTime = GETDATE();

    IF EXISTS
		(	SELECT * 
			FROM sys.objects 
			WHERE [name] = N'au_address' AND [type] = 'TR'
		)
	BEGIN
		ALTER TABLE [address] DISABLE TRIGGER [au_address];
	END;

	UPDATE [address]
       SET [care_of_name] = NULL
         , [property_name] = CASE [address_id] % 8
                               WHEN 0
                                 THEN 'The Villa'
                               WHEN 1
                                 THEN 'The House'
                               WHEN 2
                                 THEN 'Mansion Manor'
                               WHEN 3
                                 THEN 'The Vista'
                               WHEN 4
                                 THEN 'The Greenroof'
                               WHEN 5
                                 THEN 'Tinhouse'
                               WHEN 6
                                 THEN 'The Flat'
                               WHEN 7
                                 THEN 'The Stable'
                             END
         , [street_number] = CONVERT(VARCHAR, ( [address_id] % 101 ) + 1)
         , [street] = ( CASE [address_id] % 3
                          WHEN 0
                            THEN 'Big'
                          WHEN 1
                            THEN 'Small'
                          WHEN 2
                            THEN 'Middle'
                        END ) + ' ' + ( CASE [address_id] % 3
                                          WHEN 0
                                            THEN 'Street'
                                          WHEN 1
                                            THEN 'Drive'
                                          WHEN 2
                                            THEN 'Avenue'
                                        END )
         , [street2] = 'Over' + CASE [address_id] % 7
                                  WHEN 0
                                    THEN 'two'
                                  WHEN 1
                                    THEN 'three'
                                  WHEN 2
                                    THEN 'four'
                                  WHEN 3
                                    THEN 'five'
                                  WHEN 4
                                    THEN 'six'
                                  WHEN 5
                                    THEN 'seven'
                                  WHEN 6
                                    THEN 'nine'
                                END
         , [suburb] = CASE [address_id] % 5
                        WHEN 0
                          THEN 'Longton'
                        WHEN 1
                          THEN 'Ellen-on-Sea'
                        WHEN 2
                          THEN 'Brackenside'
                        WHEN 3
                          THEN 'Viewton'
                        WHEN 4
                          THEN 'Breen'
                      END
         , [district] = CASE [address_id] % 6
                          WHEN 0
                            THEN 'Suburbian'
                          WHEN 1
                            THEN 'Localshire'
                          WHEN 2
                            THEN 'Otherside'
                          WHEN 3
                            THEN 'Outston'
                          WHEN 4
                            THEN 'Greyville'
                          WHEN 5
                            THEN 'Hillend'
                        END
		, [statecode] = CASE [address_id] % 7
                          WHEN 0
                            THEN 'Hillwood'
                          WHEN 1
                            THEN 'Sunnydale'
                          WHEN 2
                            THEN 'Venusville'
                          WHEN 3
                            THEN 'Bedrock'
						  WHEN 4
                            THEN 'Neverland'
						  WHEN 5
                            THEN 'Midgar'
						  WHEN 6
                            THEN 'Springfield'
                        END
    FROM [address] [ad]
        JOIN [investment_manager] [ma]
            ON [ma].[entity_id] = [ad].[entity_id];

	IF EXISTS
		(	SELECT * 
			FROM sys.objects 
			WHERE [name] = N'au_address' AND [type] = 'TR'
		)
	BEGIN
		ALTER TABLE [address] ENABLE TRIGGER [au_address];
	END;

	SET @endTime = GETDATE();
	SET @StepMessage = 'Block 39 - Finish. Time Taken - ' + CONVERT(VARCHAR(20), @endTime - @startTime, 114);
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;

	/*************************************** Related Entities ******************************************/

    SET @StepMessage = 'Block 40 - Start';
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;
	SET @startTime = GETDATE();

	IF EXISTS 
		(	SELECT * 
			FROM sys.objects 
			WHERE [name] = N'au_entity' AND [type] = 'TR'
		)
	BEGIN
		ALTER TABLE [entity] DISABLE TRIGGER [au_entity];      
	END;

    UPDATE [entity]
       SET [given_names] = CASE [e].[entity_id] % 10
                             WHEN 0
                               THEN 'Chris'
                             WHEN 1
                               THEN 'Pete'
                             WHEN 2
                               THEN 'Bo'
                             WHEN 3
                               THEN 'Terry'
                             WHEN 4
                               THEN 'Lesley'
                             WHEN 5
                               THEN 'Jess'
                             WHEN 6
                               THEN 'Hillary'
                             WHEN 7
                               THEN 'Alex'
                             WHEN 8
                               THEN 'Petra'
                             ELSE 'Bri'
                           END
         , [email_address] = 'scrambledRelated@noemail.nodomain'
         , [title_id] = ( CASE
                            WHEN [gender] = 'M'
                              THEN 1
                            WHEN [gender] = 'F'
                              THEN 2
                            ELSE NULL
                          END )
         , [mobile_number] = '07000000000'
         , [home_phone_number] = '01000 000 000'
         , [work_phone_number] = '01000 111 111'
         , [fax_number] = NULL
         , [salutation] = 'Dear Sir / Madam'
         , [contact_name] = NULL
         , [mailing_title] = NULL
         , [company_rego_no] = NULL
         , [name] = CASE [e].[entity_id] % 50
                      WHEN 0
                        THEN 'Agate'
                      WHEN 1
                        THEN 'Alexandrite'
                      WHEN 2
                        THEN 'Andalusite'
                      WHEN 3
                        THEN 'Axinite'
                      WHEN 4
                        THEN 'Benitoite'
                      WHEN 5
                        THEN 'Bixbite'
                      WHEN 6
                        THEN 'Cassiterite'
                      WHEN 7
                        THEN 'Chrysocolla'
                      WHEN 8
                        THEN 'Chrysoprase'
                      WHEN 9
                        THEN 'Clinohumite'
                      WHEN 10
                        THEN 'Cordierite'
                      WHEN 11
                        THEN 'Danburite'
                      WHEN 12
                        THEN 'Diamond'
                      WHEN 13
                        THEN 'Diopside'
                      WHEN 14
                        THEN 'Dioptase'
                      WHEN 15
                        THEN 'Dumortierite'
                      WHEN 16
                        THEN 'Emerald'
                      WHEN 17
                        THEN 'Feldspar'
                      WHEN 18
                        THEN 'Amazonite'
                      WHEN 19
                        THEN 'Labradorite'
                      WHEN 20
                        THEN 'Moonstone'
                      WHEN 21
                        THEN 'Sunstone'
                      WHEN 22
                        THEN 'Garnet'
                      WHEN 23
                        THEN 'Hessonite'
                      WHEN 24
                        THEN 'Hambergite'
                      WHEN 25
                        THEN 'Hematite'
                      WHEN 26
                        THEN 'Jade'
                      WHEN 27
                        THEN 'Jasper'
                      WHEN 28
                        THEN 'Kornerupine'
                      WHEN 29
                        THEN 'Kunzite'
                      WHEN 30
                        THEN 'Lapis lazuli'
                      WHEN 31
                        THEN 'Malachite'
                      WHEN 32
                        THEN 'Opal'
                      WHEN 33
                        THEN 'Peridot'
                      WHEN 34
                        THEN 'Prehnite'
                      WHEN 35
                        THEN 'Pyrite'
                      WHEN 36
                        THEN 'Quartz'
                      WHEN 37
                        THEN 'Agate'
                      WHEN 38
                        THEN 'Amethyst'
                      WHEN 39
                        THEN 'Aventurine'
                      WHEN 40
                        THEN 'Citrine'
                      WHEN 41
                        THEN 'Chalcedony'
                      WHEN 42
                        THEN 'Onyx'
                      WHEN 43
                        THEN 'Rhodochrosite'
                      WHEN 44
                        THEN 'Ruby'
                      WHEN 45
                        THEN 'Sapphire'
                      WHEN 46
                        THEN 'Spinel'
                      WHEN 47
                        THEN 'Sugilite'
                      WHEN 48
                        THEN 'Tanzanite'
                      ELSE 'Topaz'
                    END
    FROM [entity] [e]
        JOIN [related_to] [rto]
            ON [e].[entity_id] = [rto].[related_entity_id];

    SET @endTime = GETDATE();
	SET @StepMessage = 'Block 40 - Finish. Time Taken - ' + CONVERT(VARCHAR(20), @endTime - @startTime, 114);
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;

	/*************************************** Related Entity Name ******************************************/

    SET @StepMessage = 'Block 41 - Start';
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;
	SET @startTime = GETDATE();

    -- now change the related entity name
	IF EXISTS 
		(	SELECT * 
			FROM sys.objects 
			WHERE [name] = N'au_related_to' AND [type] = 'TR'
		)
	BEGIN
		ALTER TABLE [related_to] DISABLE TRIGGER [au_related_to];      
	END;

    UPDATE [related_to]
       SET [name] = [e].[given_names] + ' ' + [e].[name]
         , [notation] = ( CASE
                            WHEN [rto].[notation] IS NULL
                              THEN NULL
                            ELSE 'scramble'
                          END )
    FROM [related_to] [rto]
        JOIN [entity] [e]
            ON [e].[entity_id] = [rto].[related_entity_id];

	IF EXISTS 
		(	SELECT * 
			FROM sys.objects 
			WHERE [name] = N'au_related_to' AND [type] = 'TR'
		)
	BEGIN
		ALTER TABLE [related_to] ENABLE TRIGGER [au_related_to];      
	END;

	SET @endTime = GETDATE();
	SET @StepMessage = 'Block 41 - Finish. Time Taken - ' + CONVERT(VARCHAR(20), @endTime - @startTime, 114);
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;
		
	/*************************************** Related Entities - preferred_name ******************************************/
	SET @StepMessage = 'Block 42 - Start';
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;
	SET @startTime = GETDATE();

    UPDATE [entity]
       SET [preferred_name] = LEFT([rto].[name], 40)
    FROM [entity] [e]
        JOIN [related_to] [rto]
            ON [e].[entity_id] = [rto].[related_entity_id];

	IF EXISTS 
		(	SELECT * 
			FROM sys.objects 
			WHERE [name] = N'au_entity' AND [type] = 'TR'
		)
	BEGIN
		ALTER TABLE [entity] ENABLE TRIGGER [au_entity];      
	END;

	SET @endTime = GETDATE();
	SET @StepMessage = 'Block 42 - Finish. Time Taken - ' + CONVERT(VARCHAR(20), @endTime - @startTime, 114);
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;

	/*************************************** Entity - update the remaining fields ******************************************/

	SET @StepMessage = 'Block 43 - Start';
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;
	SET @startTime = GETDATE();

	IF EXISTS 
		(	SELECT * 
			FROM sys.objects 
			WHERE [name] = N'au_entity' AND [type] = 'TR'
		)
	BEGIN
		ALTER TABLE [entity] DISABLE TRIGGER [au_entity];      
	END;

	UPDATE
		entity
	SET
		short_name = CASE
						WHEN short_name IS NULL THEN NULL
                        ELSE 'ABC'
                    END
		, deceased_date = CASE
							WHEN deceased_date IS NULL 
								THEN NULL
							ELSE DATEADD([dd], CAST(RAND(DATEDIFF(DAY, 0, deceased_date)) * 100 + 10 AS INT), deceased_date)
						END
		, retirement_date = CASE
								WHEN retirement_date IS NULL 
									THEN NULL
								ELSE DATEADD([dd], CAST(RAND(DATEDIFF(DAY, 0, retirement_date)) * 100 + 10 AS INT), retirement_date)
							END
		, birthplace = CASE 
							WHEN birthplace IS NULL 
								THEN NULL
							ELSE
								CASE entity_id % 6
								  WHEN 0
									THEN 'Suburbian'
								  WHEN 1
									THEN 'Localshire'
								  WHEN 2
									THEN 'Otherside'
								  WHEN 3
									THEN 'Outston'
								  WHEN 4
									THEN 'Greyville'
								  WHEN 5
									THEN 'Hillend'
								END
						END
		, title_text =	CASE
							WHEN title_text IS NULL
								THEN NULL
							WHEN title_text = ''
								THEN ''
							ELSE
								CASE [entity_id] % 4
								  WHEN 0
									THEN 'Mr'
								  WHEN 1
									THEN 'Mrs'
								  WHEN 2
									THEN 'Mr & Mrs'
								  WHEN 3
									THEN 'Mr & Miss'
								END
						END

	IF EXISTS 
		(	SELECT * 
			FROM sys.objects 
			WHERE [name] = N'au_entity' AND [type] = 'TR'
		)
	BEGIN
		ALTER TABLE [entity] ENABLE TRIGGER [au_entity];      
	END;

	SET @endTime = GETDATE();
	SET @StepMessage = 'Block 43 - Finish. Time Taken - ' + CONVERT(VARCHAR(20), @endTime - @startTime, 114);
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;

	/*************************************** Address ******************************************/

    SET @StepMessage = 'Block 44 - Start';
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;
	SET @startTime = GETDATE();

	IF EXISTS 
		(	SELECT * 
			FROM sys.objects 
			WHERE [name] = N'au_address' AND [type] = 'TR'
		)
	BEGIN
		ALTER TABLE [address] DISABLE TRIGGER [au_address];      
	END;

    UPDATE [address]
       SET [care_of_name] = NULL
         , [property_name] = CASE [address_id] % 8
                               WHEN 0
                                 THEN 'The Villa'
                               WHEN 1
                                 THEN 'The House'
                               WHEN 2
                                 THEN 'Mansion Manor'
                               WHEN 3
                                 THEN 'The Vista'
                               WHEN 4
                                 THEN 'The Greenroof'
                               WHEN 5
                                 THEN 'Tinhouse'
                               WHEN 6
                                 THEN 'The Flat'
                               WHEN 7
                                 THEN 'The Stable'
                             END
         , [street_number] = CONVERT(VARCHAR, ( [address_id] % 101 ) + 1)
         , [street] = ( CASE [address_id] % 3
                          WHEN 0
                            THEN 'Big'
                          WHEN 1
                            THEN 'Small'
                          WHEN 2
                            THEN 'Middle'
                        END ) + ' ' + ( CASE [address_id] % 3
                                          WHEN 0
                                            THEN 'Street'
                                          WHEN 1
                                            THEN 'Drive'
                                          WHEN 2
                                            THEN 'Avenue'
                                        END )
         , [street2] = 'Over' + CASE [address_id] % 7
                                  WHEN 0
                                    THEN 'two'
                                  WHEN 1
                                    THEN 'three'
                                  WHEN 2
                                    THEN 'four'
                                  WHEN 3
                                    THEN 'five'
                                  WHEN 4
                                    THEN 'six'
                                  WHEN 5
                                    THEN 'seven'
                                  WHEN 6
                                    THEN 'nine'
                                END
         , [suburb] = CASE [address_id] % 5
                        WHEN 0
                          THEN 'Longton'
                        WHEN 1
                          THEN 'Ellen-on-Sea'
                        WHEN 2
                          THEN 'Brackenside'
                        WHEN 3
                          THEN 'Viewton'
                        WHEN 4
                          THEN 'Breen'
                      END
         , [district] = CASE [address_id] % 6
                          WHEN 0
                            THEN 'Suburbian'
                          WHEN 1
                            THEN 'Localshire'
                          WHEN 2
                            THEN 'Otherside'
                          WHEN 3
                            THEN 'Outston'
                          WHEN 4
                            THEN 'Greyville'
                          WHEN 5
                            THEN 'Hillend'
                        END
		, [statecode] = CASE [address_id] % 7
                          WHEN 0
                            THEN 'Hillwood'
                          WHEN 1
                            THEN 'Sunnydale'
                          WHEN 2
                            THEN 'Venusville'
                          WHEN 3
                            THEN 'Bedrock'
						  WHEN 4
                            THEN 'Neverland'
						  WHEN 5
                            THEN 'Midgar'
						  WHEN 6
                            THEN 'Springfield'
                        END
    FROM [address] [ad]
        JOIN [member_account] [ma]
            ON [ma].[entity_id] = [ad].[entity_id]
        LEFT JOIN [product_type] [p]
            ON [ma].[member_account_id] = [p].[ext_over_under_acc_id]
    WHERE 
          [p].[ext_over_under_acc_id] IS NULL;

	IF EXISTS 
		(	SELECT * 
			FROM sys.objects 
			WHERE [name] = N'au_address' AND [type] = 'TR'
		)
	BEGIN
		ALTER TABLE [address] ENABLE TRIGGER [au_address];      
	END;

	SET @endTime = GETDATE();
	SET @StepMessage = 'Block 44 - Finish. Time Taken - ' + CONVERT(VARCHAR(20), @endTime - @startTime, 114);
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;

    /*********************************** Joint Owners Address (who do NOT own a wrapper directly) **************************************/
    
    SET @StepMessage = 'Block 45 - Start';
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;
	SET @startTime = GETDATE();

	IF EXISTS 
		(	SELECT * 
			FROM sys.objects 
			WHERE [name] = N'au_address' AND [type] = 'TR'
		)
	BEGIN
		ALTER TABLE [address] DISABLE TRIGGER [au_address];      
	END;

    UPDATE [address]
       SET [property_name] = 'The House'
         , [street_number] = '26'
         , [street] = 'Acacia Avenue'
         , [street2] = 'Overbridge'
         , [suburb] = 'Longtown'
         , [district] = 'Acacialand'
		 , [statecode] = 'Hillwood'
    FROM [joint_owner_details] [jod]
        LEFT OUTER JOIN [member_account] [macc]
            ON [jod].[joint_entity_id] = [macc].[entity_id]
        INNER JOIN [entity] [e]
            ON [jod].[joint_entity_id] = [e].[entity_id]
        INNER JOIN [address] [a]
            ON [e].[entity_id] = [a].[entity_id]
    WHERE 
          [macc].[member_account_id] IS NULL;

	IF EXISTS 
		(	SELECT * 
			FROM sys.objects 
			WHERE [name] = N'au_address' AND [type] = 'TR'
		)
	BEGIN
		ALTER TABLE [address] ENABLE TRIGGER [au_address];      
	END;

	SET @endTime = GETDATE();
	SET @StepMessage = 'Block 45 - Finish. Time Taken - ' + CONVERT(VARCHAR(20), @endTime - @startTime, 114);
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;

	/*************************************** Adviser Address ******************************************/

    SET @StepMessage = 'Block 46 - Start';
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;
	SET @startTime = GETDATE();

	IF EXISTS 
		(	SELECT * 
			FROM sys.objects 
			WHERE [name] = N'au_address' AND [type] = 'TR'
		)
	BEGIN
		ALTER TABLE [address] DISABLE TRIGGER [au_address];      
	END;

    UPDATE [address]
       SET [care_of_name] = NULL
         , [property_name] = CASE [address_id] % 8
                               WHEN 0
                                 THEN 'The Villa'
                               WHEN 1
                                 THEN 'The House'
                               WHEN 2
                                 THEN 'Mansion Manor'
                               WHEN 3
                                 THEN 'The Vista'
                               WHEN 4
                                 THEN 'The Greenroof'
                               WHEN 5
                                 THEN 'Tinhouse'
                               WHEN 6
                                 THEN 'The Flat'
                               WHEN 7
                                 THEN 'The Stable'
                             END
         , [street_number] = CONVERT(VARCHAR, ( [address_id] % 101 ) + 1)
         , [street] = ( CASE [address_id] % 3
                          WHEN 0
                            THEN 'Big'
                          WHEN 1
                            THEN 'Small'
                          WHEN 2
                            THEN 'Middle'
                        END ) + ' ' + ( CASE [address_id] % 3
                                          WHEN 0
                                            THEN 'Street'
                                          WHEN 1
                                            THEN 'Drive'
                                          WHEN 2
                                            THEN 'Avenue'
                                        END )
         , [street2] = 'Over' + CASE [address_id] % 7
                                  WHEN 0
                                    THEN 'two'
                                  WHEN 1
                                    THEN 'three'
                                  WHEN 2
                                    THEN 'four'
                                  WHEN 3
                                    THEN 'five'
                                  WHEN 4
                                    THEN 'six'
                                  WHEN 5
                                    THEN 'seven'
                                  WHEN 6
                                    THEN 'nine'
                                END
         , [suburb] = CASE [address_id] % 5
                        WHEN 0
                          THEN 'Longton'
                        WHEN 1
                          THEN 'Ellen-on-Sea'
                        WHEN 2
                          THEN 'Brackenside'
                        WHEN 3
                          THEN 'Viewton'
                        WHEN 4
                          THEN 'Breen'
                      END
         , [district] = CASE [address_id] % 6
                          WHEN 0
                            THEN 'Suburbian'
                          WHEN 1
                            THEN 'Localshire'
                          WHEN 2
                            THEN 'Otherside'
                          WHEN 3
                            THEN 'Outston'
                          WHEN 4
                            THEN 'Greyville'
                          WHEN 5
                            THEN 'Hillend'
                        END
		, [statecode] = CASE [address_id] % 7
                          WHEN 0
                            THEN 'Hillwood'
                          WHEN 1
                            THEN 'Sunnydale'
                          WHEN 2
                            THEN 'Venusville'
                          WHEN 3
                            THEN 'Bedrock'
						  WHEN 4
                            THEN 'Neverland'
						  WHEN 5
                            THEN 'Midgar'
						  WHEN 6
                            THEN 'Springfield'
                        END
    FROM [address] [ad]
        JOIN [adviser_account] [aa]
            ON [aa].[entity_id] = [ad].[entity_id];

    SET @endTime = GETDATE();
	SET @StepMessage = 'Block 46 - Finish. Time Taken - ' + CONVERT(VARCHAR(20), @endTime - @startTime, 114);
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;

	/*************************************** Dealer Address ******************************************/

    SET @StepMessage = 'Block 47 - Start';
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;
	SET @startTime = GETDATE();

	IF EXISTS 
		(	SELECT * 
			FROM sys.objects 
			WHERE [name] = N'au_address' AND [type] = 'TR'
		)
	BEGIN
		ALTER TABLE [address] DISABLE TRIGGER [au_address];      
	END;

    UPDATE [address]
       SET [care_of_name] = NULL
         , [property_name] = CASE [address_id] % 8
                               WHEN 0
                                 THEN 'The Villa'
                               WHEN 1
                                 THEN 'The House'
                               WHEN 2
                                 THEN 'Mansion Manor'
                               WHEN 3
                                 THEN 'The Vista'
                               WHEN 4
                                 THEN 'The Greenroof'
                               WHEN 5
                                 THEN 'Tinhouse'
                               WHEN 6
                                 THEN 'The Flat'
                               WHEN 7
                                 THEN 'The Stable'
                             END
         , [street_number] = CONVERT(VARCHAR, ( [address_id] % 101 ) + 1)
         , [street] = ( CASE [address_id] % 3
                          WHEN 0
                            THEN 'Big'
                          WHEN 1
                            THEN 'Small'
                          WHEN 2
                            THEN 'Middle'
                        END ) + ' ' + ( CASE [address_id] % 3
                                          WHEN 0
                                            THEN 'Street'
                                          WHEN 1
                                            THEN 'Drive'
                                          WHEN 2
                                            THEN 'Avenue'
                                        END )
         , [street2] = 'Over' + CASE [address_id] % 7
                                  WHEN 0
                                    THEN 'two'
                                  WHEN 1
                                    THEN 'three'
                                  WHEN 2
                                    THEN 'four'
                                  WHEN 3
                                    THEN 'five'
                                  WHEN 4
                                    THEN 'six'
                                  WHEN 5
                                    THEN 'seven'
                                  WHEN 6
                                    THEN 'nine'
                                END
         , [suburb] = CASE [address_id] % 5
                        WHEN 0
                          THEN 'Longton'
                        WHEN 1
                          THEN 'Ellen-on-Sea'
                        WHEN 2
                          THEN 'Brackenside'
                        WHEN 3
                          THEN 'Viewton'
                        WHEN 4
                          THEN 'Breen'
                      END
         , [district] = CASE [address_id] % 6
                          WHEN 0
                            THEN 'Suburbian'
                          WHEN 1
                            THEN 'Localshire'
                          WHEN 2
                            THEN 'Otherside'
                          WHEN 3
                            THEN 'Outston'
                          WHEN 4
                            THEN 'Greyville'
                          WHEN 5
                            THEN 'Hillend'
                        END
		, [statecode] = CASE [address_id] % 7
                          WHEN 0
                            THEN 'Hillwood'
                          WHEN 1
                            THEN 'Sunnydale'
                          WHEN 2
                            THEN 'Venusville'
                          WHEN 3
                            THEN 'Bedrock'
						  WHEN 4
                            THEN 'Neverland'
						  WHEN 5
                            THEN 'Midgar'
						  WHEN 6
                            THEN 'Springfield'
                        END
    FROM [address] [ad]
        JOIN [dealer] [d]
            ON [d].[entity_id] = [ad].[entity_id];

	IF EXISTS 
		(	SELECT * 
			FROM sys.objects 
			WHERE [name] = N'au_address' AND [type] = 'TR'
		)
	BEGIN
		ALTER TABLE [address] ENABLE TRIGGER [au_address];      
	END;

	SET @endTime = GETDATE();
	SET @StepMessage = 'Block 47 - Finish. Time Taken - ' + CONVERT(VARCHAR(20), @endTime - @startTime, 114);
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;

	/*************************************** Employer Address ******************************************/

    SET @StepMessage = 'Block 48 - Start';
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;
	SET @startTime = GETDATE();

	IF EXISTS 
		(	SELECT * 
			FROM sys.objects 
			WHERE [name] = N'au_address' AND [type] = 'TR'
		)
	BEGIN
		ALTER TABLE [address] DISABLE TRIGGER [au_address];      
	END;

    UPDATE [address]
       SET [care_of_name] = NULL
         , [property_name] = CASE [address_id] % 8
                               WHEN 0
                                 THEN 'The Villa'
                               WHEN 1
                                 THEN 'The House'
                               WHEN 2
                                 THEN 'Mansion Manor'
                               WHEN 3
                                 THEN 'The Vista'
                               WHEN 4
                                 THEN 'The Greenroof'
                               WHEN 5
                                 THEN 'Tinhouse'
                               WHEN 6
                                 THEN 'The Flat'
                               WHEN 7
                                 THEN 'The Stable'
                             END
         , [street_number] = CONVERT(VARCHAR, ( [address_id] % 101 ) + 1)
         , [street] = ( CASE [address_id] % 3
                          WHEN 0
                            THEN 'Big'
                          WHEN 1
                            THEN 'Small'
                          WHEN 2
                            THEN 'Middle'
                        END ) + ' ' + ( CASE [address_id] % 3
                                          WHEN 0
                                            THEN 'Street'
                                          WHEN 1
                                            THEN 'Drive'
                                          WHEN 2
                                            THEN 'Avenue'
                                        END )
         , [street2] = 'Over' + CASE [address_id] % 7
                                  WHEN 0
                                    THEN 'two'
                                  WHEN 1
                                    THEN 'three'
                                  WHEN 2
                                    THEN 'four'
                                  WHEN 3
                                    THEN 'five'
                                  WHEN 4
                                    THEN 'six'
                                  WHEN 5
                                    THEN 'seven'
                                  WHEN 6
                                    THEN 'nine'
                                END
         , [suburb] = CASE [address_id] % 5
                        WHEN 0
                          THEN 'Longton'
                        WHEN 1
                          THEN 'Ellen-on-Sea'
                        WHEN 2
                          THEN 'Brackenside'
                        WHEN 3
                          THEN 'Viewton'
                        WHEN 4
                          THEN 'Breen'
                      END
         , [district] = CASE [address_id] % 6
                          WHEN 0
                            THEN 'Suburbian'
                          WHEN 1
                            THEN 'Localshire'
                          WHEN 2
                            THEN 'Otherside'
                          WHEN 3
                            THEN 'Outston'
                          WHEN 4
                            THEN 'Greyville'
                          WHEN 5
                            THEN 'Hillend'
                        END
		, [statecode] = CASE [address_id] % 7
                          WHEN 0
                            THEN 'Hillwood'
                          WHEN 1
                            THEN 'Sunnydale'
                          WHEN 2
                            THEN 'Venusville'
                          WHEN 3
                            THEN 'Bedrock'
						  WHEN 4
                            THEN 'Neverland'
						  WHEN 5
                            THEN 'Midgar'
						  WHEN 6
                            THEN 'Springfield'
                        END
    FROM [address] [ad]
        JOIN [employer] [e]
            ON [e].[entity_id] = [ad].[entity_id];

	IF EXISTS 
		(	SELECT * 
			FROM sys.objects 
			WHERE [name] = N'au_address' AND [type] = 'TR'
		)
	BEGIN
		ALTER TABLE [address] ENABLE TRIGGER [au_address];      
	END;

	SET @endTime = GETDATE();
	SET @StepMessage = 'Block 48 - Finish. Time Taken - ' + CONVERT(VARCHAR(20), @endTime - @startTime, 114);
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;

	/*************************************** Related Entity Address ******************************************/

    SET @StepMessage = 'Block 49 - Start';
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;
	SET @startTime = GETDATE();

    -- Address for related entities.
    IF EXISTS 
		(	SELECT * 
			FROM sys.objects 
			WHERE [name] = N'au_address' AND [type] = 'TR'
		)
	BEGIN
		ALTER TABLE [address] DISABLE TRIGGER [au_address];      
	END;

	UPDATE [address]
       SET [property_name] = 'The House'
         , [street_number] = '27'
         , [street] = 'Acacia Avenue'
         , [street2] = 'Overbridge'
         , [suburb] = 'Longtown'
         , [district] = 'Acacialand'
		 , [statecode] = 'Hillwood'
    FROM [address] [ad]
        JOIN [related_to] [rt]
            ON [rt].[related_entity_id] = [ad].[entity_id];

	IF EXISTS 
		(	SELECT * 
			FROM sys.objects 
			WHERE [name] = N'au_address' AND [type] = 'TR'
		)
	BEGIN
		ALTER TABLE [address] ENABLE TRIGGER [au_address];      
	END;

    SET @endTime = GETDATE();
	SET @StepMessage = 'Block 49 - Finish. Time Taken - ' + CONVERT(VARCHAR(20), @endTime - @startTime, 114);
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;

	/*************************************** Correspondence ******************************************/

    SET @StepMessage = 'Block 50 - Start';
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;
	SET @startTime = GETDATE();

	IF EXISTS 
		(	SELECT * 
			FROM sys.objects 
			WHERE [name] = N'au_correspondence' AND [type] = 'TR'
		)
	BEGIN
		ALTER TABLE [correspondence] DISABLE TRIGGER [au_correspondence];      
	END;

    UPDATE [correspondence]
       SET [given_names] = LEFT([e].[given_names], 40)
         , [name] = LEFT([e].[name], 40)
    FROM [correspondence] [corr]
        JOIN [entity] [e]
            ON [e].[entity_id] = [corr].[entity_id]
        JOIN [member_account] [ma]
            ON [ma].[entity_id] = [e].[entity_id];

	IF EXISTS 
		(	SELECT * 
			FROM sys.objects 
			WHERE [name] = N'au_correspondence' AND [type] = 'TR'
		)
	BEGIN
		ALTER TABLE [correspondence] ENABLE TRIGGER [au_correspondence];      
	END;

	SET @endTime = GETDATE();
	SET @StepMessage = 'Block 50 - Finish. Time Taken - ' + CONVERT(VARCHAR(20), @endTime - @startTime, 114);
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;

    /*************************************** Beneficiary ******************************************/

    SET @StepMessage = 'Block 51 - Start';
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;
	SET @startTime = GETDATE();

	IF EXISTS 
		(	SELECT * 
			FROM sys.objects 
			WHERE [name] = N'au_beneficiary' AND [type] = 'TR'
		)
	BEGIN
		ALTER TABLE [beneficiary] DISABLE TRIGGER [au_beneficiary];      
	END;

	UPDATE 
		beneficiary
	SET
		name = 'NameRemoved'
	  , date_of_birth =	CASE 
							WHEN date_of_birth IS NULL THEN NULL
							ELSE
								CASE 
									WHEN (DATEDIFF(YEAR, date_of_birth, GETDATE()) > 18 OR DATEDIFF(YEAR, date_of_birth, GETDATE()) < 1)
										THEN DATEADD(dd, CAST(RAND(DATEDIFF(DAY, 0, date_of_birth)) * 100 + 10 AS INT), date_of_birth) /* add random days from 10 to 110 */
									WHEN (DATEDIFF(YEAR, date_of_birth, GETDATE()) <= 18 OR DATEDIFF(YEAR, date_of_birth, GETDATE()) >= 1) 
										THEN DATEADD(dd, CAST(RAND(DATEDIFF(DAY, 0, date_of_birth)) * -100 - 10 AS INT), date_of_birth) /* subract random days from 10 to 110 */
								END
						END;

	IF EXISTS 
		(	SELECT * 
			FROM sys.objects 
			WHERE [name] = N'au_beneficiary' AND [type] = 'TR'
		)
	BEGIN
		ALTER TABLE [beneficiary] ENABLE TRIGGER [au_beneficiary];      
	END;

    SET @endTime = GETDATE();
	SET @StepMessage = 'Block 51 - Finish. Time Taken - ' + CONVERT(VARCHAR(20), @endTime - @startTime, 114);
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;

	/*************************************** Divorce Details ******************************************/

    SET @StepMessage = 'Block 52 - Start';
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;
	SET @startTime = GETDATE();

	IF EXISTS 
		(	SELECT * 
			FROM sys.objects 
			WHERE [name] = N'au_divorce_details' AND [type] = 'TR'
		)
	BEGIN
		ALTER TABLE [divorce_details] DISABLE TRIGGER [au_divorce_details];      
	END;

    UPDATE 
		[divorce_details]
    SET 
		[court_order_number] = CASE
									WHEN court_order_number IS NULL THEN NULL
									ELSE 'AA11A00000'
								END
      , [notation] = CASE
                          WHEN [notation] IS NULL THEN NULL
                          ELSE 'Text removed'
                        END;

	IF EXISTS 
		(	SELECT * 
			FROM sys.objects 
			WHERE [name] = N'au_divorce_details' AND [type] = 'TR'
		)
	BEGIN
		ALTER TABLE [divorce_details] ENABLE TRIGGER [au_divorce_details];      
	END;

    SET @endTime = GETDATE();
	SET @StepMessage = 'Block 52 - Finish. Time Taken - ' + CONVERT(VARCHAR(20), @endTime - @startTime, 114);
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;

	/*************************************** Received Money ******************************************/

    SET @StepMessage = 'Block 53 - Start';
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;
	SET @startTime = GETDATE();

    UPDATE [received_money]
       SET [reference_number] = '99999999'
         , [drawer_name] = LEFT([e].[name], 40)
    FROM [received_money] [rm]
        JOIN [correspondence] [corr]
            ON [rm].[correspondence_id] = [corr].[correspondence_id]
        JOIN [entity] [e]
            ON [e].[entity_id] = [corr].[entity_id]
        JOIN [member_account] [ma]
            ON [ma].[entity_id] = [e].[entity_id]
    WHERE 
          [rm].[bsb_number] IS NOT NULL;

    SET @endTime = GETDATE();
	SET @StepMessage = 'Block 53 - Finish. Time Taken - ' + CONVERT(VARCHAR(20), @endTime - @startTime, 114);
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;

	/*************************************** Expectation ******************************************/

    SET @StepMessage = 'Block 54 - Start';
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;
	SET @startTime = GETDATE();

	IF EXISTS 
		(	SELECT * 
			FROM sys.objects 
			WHERE [name] = N'au_expectation' AND [type] = 'TR'
		)
	BEGIN
		ALTER TABLE [expectation] DISABLE TRIGGER [au_expectation];      
	END;

    UPDATE [expectation]
       SET [notation] = NULL
    WHERE 
          [party_type_id] = 1
          AND [notation] IS NOT NULL
          AND CONVERT(VARCHAR, [notation]) != '';

	IF EXISTS 
		(	SELECT * 
			FROM sys.objects 
			WHERE [name] = N'au_expectation' AND [type] = 'TR'
		)
	BEGIN
		ALTER TABLE [expectation] ENABLE TRIGGER [au_expectation];      
	END;

	SET @endTime = GETDATE();
	SET @StepMessage = 'Block 54 - Finish. Time Taken - ' + CONVERT(VARCHAR(20), @endTime - @startTime, 114);
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;

	/*************************************** Bank Account / Sort Code ******************************************/

    SET @StepMessage = 'Block 55 - Start';
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;
	SET @startTime = GETDATE();

	IF EXISTS 
		(	SELECT * 
			FROM sys.objects 
			WHERE [name] = N'au_bank_account' AND [type] = 'TR'
		)
	BEGIN
		ALTER TABLE [bank_account] DISABLE TRIGGER [au_bank_account];      
	END;

	--Control table for chunk updates
	IF OBJECT_ID('tempdb..#upd_bank_account_control') IS NOT NULL
		DROP TABLE #upd_bank_account_control;

	CREATE TABLE #upd_bank_account_control
	(
		control_id INT NOT NULL,
		complete INT DEFAULT 0 NOT NULL
	);

	INSERT INTO #upd_bank_account_control
	(
		control_id
	)
	SELECT
		bank_account_id 
	FROM 
		[bank_account] 

	CREATE CLUSTERED INDEX idx_bank_account_control ON #upd_bank_account_control(control_id)

	WHILE EXISTS 
	(
		SELECT 
            1
		FROM
			#upd_bank_account_control
		WHERE 
			complete <> 2
	)
	BEGIN
		UPDATE TOP (@batchSize) 
			#upd_bank_account_control
		SET 
			complete = 1
		WHERE
			complete = 0

		UPDATE 
			[bank_account]
		SET 
			[account_number] = RIGHT('99999999' + CONVERT(VARCHAR, [bacc].[bank_account_id] % 234011), 8)
			, [name] = ( CASE 
							WHEN e.entity_id IS NULL THEN 'Data Obfuscated'
							ELSE LEFT(ISNULL([e].[given_names] + ' ', '') + [e].[name], 40)
						END )
			, [customer_reference] = ( CASE
										WHEN [bacc].[customer_reference] IS NULL THEN NULL
										ELSE 'scrambled ref'
									END )
			, [bic_code] = ( CASE
								WHEN bic_code IS NULL THEN NULL
								ELSE 'MIDLGB00' /*Dummy bic_code*/
							END )
			, [intnl_account_number] = ( CASE
											WHEN intnl_account_number IS NULL THEN NULL
											ELSE 'GB00MIDL00000000000000' /*Dummy intnl a/c no*/
										END )
		FROM 
			[bank_account] [bacc]
			JOIN [#upd_bank_account_control] u on [bacc].bank_account_id = u.control_id AND u.complete = 1
			LEFT JOIN [entity_bank_account] [entbacc] ON [entbacc].[bank_account_id] = [bacc].[bank_account_id]
			LEFT JOIN [entity] [e] ON [entbacc].[entity_id] = [e].[entity_id]
			
		UPDATE
			#upd_bank_account_control
		SET 
			complete = 2
		WHERE
			complete = 1
	END

	--clean up
	IF OBJECT_ID('tempdb..#upd_bank_account_control') IS NOT NULL
		DROP TABLE #upd_bank_account_control;

	IF EXISTS 
		(	SELECT * 
			FROM sys.objects 
			WHERE [name] = N'au_bank_account' AND [type] = 'TR'
		)
	BEGIN
		ALTER TABLE [bank_account] ENABLE TRIGGER [au_bank_account];      
	END;

	SET @endTime = GETDATE();
	SET @StepMessage = 'Block 55 - Finish. Time Taken - ' + CONVERT(VARCHAR(20), @endTime - @startTime, 114);
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;

	/*************************************** Direct Debits ******************************************/

    SET @StepMessage = 'Block 56 - Start';
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;
	SET @startTime = GETDATE();

    UPDATE [pdc_auddis]
       SET [pdc_auddis_reference] = ( CAST([pdc_auddis_id] AS VARCHAR) + ' scramble' );

	SET @endTime = GETDATE();
	SET @StepMessage = 'Block 56 - Finish. Time Taken - ' + CONVERT(VARCHAR(20), @endTime - @startTime, 114);
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;

	/*************************************** Direct Debits ******************************************/

    SET @StepMessage = 'Block 57 - Start';
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;
	SET @startTime = GETDATE();

    UPDATE
		auddis_submit_detail
	SET
		destination_account_number = RIGHT('99999999' + CONVERT(VARCHAR, auddis_submit_detail_id % 234011), 8)
	  , destination_sort_code = RIGHT('99999999' + CONVERT(VARCHAR, auddis_submit_detail_id % 234011), 6)
	  , users_reference = 'Data Obfuscated'
	  , destination_account_name = 'Data Obfuscated'
	  , originating_account_number = RIGHT('99999999' + CONVERT(VARCHAR, originating_account_number % 234012), 8)
	  , originating_sort_code = RIGHT('99999999' + CONVERT(VARCHAR, originating_sort_code % 234012), 6)

    SET @endTime = GETDATE();
	SET @StepMessage = 'Block 57 - Finish. Time Taken - ' + CONVERT(VARCHAR(20), @endTime - @startTime, 114);
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;

	/*************************************** Dealer Network Address ******************************************/

    SET @StepMessage = 'Block 58 - Start';
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;
	SET @startTime = GETDATE();

	IF EXISTS 
		(	SELECT * 
			FROM sys.objects 
			WHERE [name] = N'au_address' AND [type] = 'TR'
		)
	BEGIN
		ALTER TABLE [address] DISABLE TRIGGER [au_address];      
	END;

    UPDATE [address]
       SET [care_of_name] = NULL
         , [property_name] = CASE [address_id] % 8
                               WHEN 0
                                 THEN 'The Villa'
                               WHEN 1
                                 THEN 'The House'
                               WHEN 2
                                 THEN 'Mansion Manor'
                               WHEN 3
                                 THEN 'The Vista'
                               WHEN 4
                                 THEN 'The Greenroof'
                               WHEN 5
                                 THEN 'Tinhouse'
                               WHEN 6
                                 THEN 'The Flat'
                               WHEN 7
                                 THEN 'The Stable'
                             END
         , [street_number] = CONVERT(VARCHAR, ( [address_id] % 101 ) + 1)
         , [street] = ( CASE [address_id] % 3
                          WHEN 0
                            THEN 'Big'
                          WHEN 1
                            THEN 'Small'
                          WHEN 2
                            THEN 'Middle'
                        END ) + ' ' + ( CASE [address_id] % 3
                                          WHEN 0
                                            THEN 'Street'
                                          WHEN 1
                                            THEN 'Drive'
                                          WHEN 2
                                            THEN 'Avenue'
                                        END )
         , [street2] = 'Over' + CASE [address_id] % 7
                                  WHEN 0
                                    THEN 'two'
                                  WHEN 1
                                    THEN 'three'
                                  WHEN 2
                                    THEN 'four'
                                  WHEN 3
                                    THEN 'five'
                                  WHEN 4
                                    THEN 'six'
                                  WHEN 5
                                    THEN 'seven'
                                  WHEN 6
                                    THEN 'nine'
                                END
         , [suburb] = CASE [address_id] % 5
                        WHEN 0
                          THEN 'Longton'
                        WHEN 1
                          THEN 'Ellen-on-Sea'
                        WHEN 2
                          THEN 'Brackenside'
                        WHEN 3
                          THEN 'Viewton'
                        WHEN 4
                          THEN 'Breen'
                      END
         , [district] = CASE [address_id] % 6
                          WHEN 0
                            THEN 'Suburbian'
                          WHEN 1
                            THEN 'Localshire'
                          WHEN 2
                            THEN 'Otherside'
                          WHEN 3
                            THEN 'Outston'
                          WHEN 4
                            THEN 'Greyville'
                          WHEN 5
                            THEN 'Hillend'
                        END
		, [statecode] = CASE [address_id] % 7
                          WHEN 0
                            THEN 'Hillwood'
                          WHEN 1
                            THEN 'Sunnydale'
                          WHEN 2
                            THEN 'Venusville'
                          WHEN 3
                            THEN 'Bedrock'
						  WHEN 4
                            THEN 'Neverland'
						  WHEN 5
                            THEN 'Midgar'
						  WHEN 6
                            THEN 'Springfield'
                        END
    FROM [address] [ad]
        JOIN [dealer_branch] [db]
            ON [db].[entity_id] = [ad].[entity_id];

	IF EXISTS 
		(	SELECT * 
			FROM sys.objects 
			WHERE [name] = N'au_address' AND [type] = 'TR'
		)
	BEGIN
		ALTER TABLE [address] ENABLE TRIGGER [au_address];      
	END;

	SET @endTime = GETDATE();
	SET @StepMessage = 'Block 58 - Finish. Time Taken - ' + CONVERT(VARCHAR(20), @endTime - @startTime, 114);
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;

	/*************************************** External Party ******************************************/

    SET @StepMessage = 'Block 59 - Start';
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;
	SET @startTime = GETDATE();

    UPDATE [external_party]
       SET [external_party_name] = CASE [external_party_id] % 10
                                     WHEN 0
                                       THEN 'Zero Sun'
                                     WHEN 1
                                       THEN 'One Mercury'
                                     WHEN 2
                                       THEN 'Two Venus'
                                     WHEN 3
                                       THEN 'Three Earth'
                                     WHEN 4
                                       THEN 'Four Mars'
                                     WHEN 5
                                       THEN 'Five Jupiter'
                                     WHEN 6
                                       THEN 'Six Saturn'
                                     WHEN 7
                                       THEN 'Seven Neptune'
                                     WHEN 8
                                       THEN 'Eight Pluto'
                                     WHEN 9
                                       THEN 'Nine Solar'
                                   END
    WHERE 
          [party_type_id] IN ( 43, 44, 45, 46 );

    SET @endTime = GETDATE();
	SET @StepMessage = 'Block 59 - Finish. Time Taken - ' + CONVERT(VARCHAR(20), @endTime - @startTime, 114);
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;

	/*************************************** Adaptor Gateway (inbound CBIS XML requests) ******************************************/
    
	SET @StepMessage = 'Block 60 - Start';
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;
	SET @startTime = GETDATE();

	--Control table for chunk updates
	IF OBJECT_ID('tempdb..#upd_adaptor_gateway_control') IS NOT NULL
		DROP TABLE #upd_adaptor_gateway_control;

	CREATE TABLE #upd_adaptor_gateway_control
	(
		control_id INT NOT NULL,
		complete INT DEFAULT 0 NOT NULL
	);
	
	INSERT INTO #upd_adaptor_gateway_control
		(
			control_id
		)
		SELECT
			agm.adaptor_gateway_message_id
		FROM 
			dbo.adaptor_gateway_message agm

	CREATE CLUSTERED INDEX idx_adaptor_gateway_control ON #upd_adaptor_gateway_control(control_id)

	WHILE EXISTS 
		(
			SELECT 
				1
			FROM
				[#upd_adaptor_gateway_control]
			WHERE 
				complete <> 2
		)
	BEGIN

		UPDATE TOP (@batchSize) 
				[#upd_adaptor_gateway_control]
			SET 
				complete = 1
			WHERE
				complete = 0

		UPDATE
			agm
		SET
			xml_message = ''
			, web_user_name = CASE 
								WHEN web_user_name IS NULL THEN NULL
								ELSE
									CASE
										WHEN e.entity_id is null THEN 'Scrambled@noemail.nodomain'
									ELSE e.email_address
									END						END
		FROM 
			adaptor_gateway_message agm
			JOIN #upd_adaptor_gateway_control u 
				ON agm.adaptor_gateway_message_id = u.control_id 
				AND u.complete = 1
			LEFT JOIN entity e 
				ON e.entity_id = agm.web_user_id

		UPDATE
				[#upd_adaptor_gateway_control]
			SET 
				complete = 2
			WHERE
				complete = 1
	END

	IF OBJECT_ID('tempdb..#upd_adaptor_gateway_control') IS NOT NULL
		DROP TABLE #upd_adaptor_gateway_control;

	SET @endTime = GETDATE();
	SET @StepMessage = 'Block 60 - Finish. Time Taken - ' + CONVERT(VARCHAR(20), @endTime - @startTime, 114);
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;

	/*************************************** Account Designation ******************************************/

    SET @StepMessage = 'Block 61 - Start';
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;
	SET @startTime = GETDATE();

	IF EXISTS 
		(	SELECT * 
			FROM sys.objects 
			WHERE [name] = N'au_member_account' AND [type] = 'TR'
		)
	BEGIN
		ALTER TABLE [member_account] DISABLE TRIGGER [au_member_account];      
	END;

    UPDATE 
		[member_account]
    SET 
		[account_designation] = SUBSTRING(CONVERT(VARCHAR(100), NEWID()), 0, CHARINDEX('-', CONVERT(VARCHAR(100), NEWID())))
    WHERE 
        [account_designation] IS NOT NULL;

	IF EXISTS 
		(	SELECT * 
			FROM sys.objects 
			WHERE [name] = N'au_member_account' AND [type] = 'TR'
		)
	BEGIN
		ALTER TABLE [member_account] ENABLE TRIGGER [au_member_account];      
	END;

    SET @endTime = GETDATE();
	SET @StepMessage = 'Block 61 - Finish. Time Taken - ' + CONVERT(VARCHAR(20), @endTime - @startTime, 114);
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;

	/*************************************** MIFID tables ******************************************/

    SET @StepMessage = 'Block 62 - Start';
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;
	SET @startTime = GETDATE();

	IF EXISTS 
		(	SELECT * 
			FROM sys.objects 
			WHERE [name] = N'au_mifid_identification' AND [type] = 'TR'
		)
	BEGIN
		ALTER TABLE [mifid_identification] DISABLE TRIGGER [au_mifid_identification];      
	END;

    UPDATE [mifid_identification]
       SET [identification_reference] = SUBSTRING(CONVERT(VARCHAR(100), NEWID()), 0, CHARINDEX('-', CONVERT(VARCHAR(100), NEWID()))) + CHAR(CAST( ( 90 - 65 ) * RAND() + 65 AS INTEGER));

	IF EXISTS 
		(	SELECT * 
			FROM sys.objects 
			WHERE [name] = N'au_mifid_identification' AND [type] = 'TR'
		)
	BEGIN
		ALTER TABLE [mifid_identification] ENABLE TRIGGER [au_mifid_identification];      
	END;

    SET @endTime = GETDATE();
	SET @StepMessage = 'Block 62 - Finish. Time Taken - ' + CONVERT(VARCHAR(20), @endTime - @startTime, 114);
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;

	/*************************************** MIFID tables ******************************************/

	SET @StepMessage = 'Block 63 - Start';
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;
	SET @startTime = GETDATE();

    UPDATE [mifid_reporting_transaction]
       SET [buyer_first_name] = ( CASE
                                    WHEN [buyer_first_name] IS NULL
                                      THEN NULL
                                    ELSE 'Aaa'
                                  END )
         , [buyer_surname] = ( CASE
                                 WHEN [buyer_surname] IS NULL
                                   THEN NULL
                                 WHEN [buyer_surname] LIKE 'WINTERFLOOD%'
                                   THEN [buyer_surname]
                                 ELSE 'Bbb'
                               END )
         , [buy_decision_maker_first_name] = ( CASE
                                                 WHEN [buy_decision_maker_first_name] IS NULL
                                                   THEN NULL
                                                 ELSE 'Ccc'
                                               END )
         , [buy_decision_maker_surname] = ( CASE
                                              WHEN [buy_decision_maker_surname] IS NULL
                                                THEN NULL
                                              WHEN [buy_decision_maker_surname] LIKE 'WINTERFLOOD%'
                                                THEN [buy_decision_maker_surname]
                                              ELSE 'Ddd'
                                            END )
         , [seller_first_name] = ( CASE
                                     WHEN [seller_first_name] IS NULL
                                       THEN NULL
                                     ELSE 'Eee'
                                   END )
         , [seller_surname] = ( CASE
                                  WHEN [seller_surname] IS NULL
                                    THEN NULL
                                  WHEN [seller_surname] LIKE 'WINTERFLOOD%'
                                    THEN [seller_surname]
                                  WHEN [seller_surname] LIKE 'AEGON%'
                                    THEN [seller_surname]
                                  ELSE 'Fff'
                                END )
         , [sell_dec_maker_first_name] = ( CASE
                                             WHEN [sell_dec_maker_first_name] IS NULL
                                               THEN NULL
                                             ELSE 'Ggg'
                                           END )
         , [sell_dec_maker_surname] = ( CASE
                                          WHEN [sell_dec_maker_surname] IS NULL
                                            THEN NULL
                                          WHEN [sell_dec_maker_surname] LIKE 'WINTERFLOOD%'
                                            THEN [sell_dec_maker_surname]
                                          WHEN [sell_dec_maker_surname] LIKE 'AEGON%'
                                            THEN [sell_dec_maker_surname]
                                          ELSE 'Hhh'
                                        END )
		, [buy_decision_maker_dob] = ( CASE
										WHEN buy_decision_maker_dob IS NULL THEN NULL
										ELSE DATEADD(dd, CAST(RAND(DATEDIFF(DAY, 0, buy_decision_maker_dob)) * 100 + 10 AS INT), buy_decision_maker_dob)
									END )
		, [buyer_dob] = ( CASE
							WHEN buyer_dob IS NULL THEN NULL
							ELSE DATEADD(dd, CAST(RAND(DATEDIFF(DAY, 0, buyer_dob)) * 100 + 10 AS INT), buyer_dob)
						END)
		, [sell_decision_maker_dob] = ( CASE
										WHEN sell_decision_maker_dob IS NULL THEN NULL
										ELSE DATEADD(dd, CAST(RAND(DATEDIFF(DAY, 0, sell_decision_maker_dob)) * 100 + 10 AS INT), sell_decision_maker_dob)
									END )
		, [seller_dob] = ( CASE
							WHEN seller_dob IS NULL THEN NULL
							ELSE DATEADD(dd, CAST(RAND(DATEDIFF(DAY, 0, seller_dob)) * 100 + 10 AS INT), seller_dob)
						END )

    SET @endTime = GETDATE();
	SET @StepMessage = 'Block 63 - Finish. Time Taken - ' + CONVERT(VARCHAR(20), @endTime - @startTime, 114);
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;

	/*************************************** Party Attributes ******************************************/

    SET @StepMessage = 'Block 64 - Start';
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;
	SET @startTime = GETDATE();

	IF EXISTS 
		(	SELECT * 
			FROM sys.objects 
			WHERE [name] = N'au_party_attribute' AND [type] = 'TR'
		)
	BEGIN
		ALTER TABLE [party_attribute] DISABLE TRIGGER [au_party_attribute];      
	END;

	UPDATE 
		pa
	SET 
		attribute_value = CASE 
							WHEN a.description = 'PSO Number' 
								AND pa.attribute_value IS NOT NULL
									THEN SUBSTRING(CONVERT(VARCHAR(100), NEWID()), 0, CHARINDEX('-', CONVERT(VARCHAR(100), NEWID())))
							WHEN a.description = 'HMRC Certificate Number' 
								AND pa.attribute_value IS NOT NULL
									THEN SUBSTRING(CONVERT(VARCHAR(100), NEWID()), 0, CHARINDEX('-', CONVERT(VARCHAR(100), NEWID())))
							WHEN a.description = 'Post Sales Illustration File Name' 
								AND pa.attribute_value IS NOT NULL
									THEN '9999999_1-AAaZZZZZ\RemovedFilePath-' + CAST(pa.party_attribute_id AS VARCHAR) + '.pdf'
							WHEN a.description = 'SMS Preferences for receiving marketing material' 
								AND pa.attribute_value IS NOT NULL
									THEN 'deleted'
							WHEN a.description = 'SOCIAL_MEDIA Preferences for receiving marketing material' 
								AND pa.attribute_value IS NOT NULL
									THEN 'deleted'
							WHEN a.description = 'Trustee Title and Name' 
								AND pa.attribute_value IS NOT NULL
									THEN 'deleted'
							ELSE
								pa.attribute_value
						END
	FROM 
		party_attribute pa
		JOIN attribute a ON a.attribute_id = pa.attribute_id
	WHERE 
		a.description in (
							'HMRC Certificate Number'
						  , 'PSO Number'
						  , 'Post Sales Illustration File Name'
						  , 'SMS Preferences for receiving marketing material'
						  , 'SOCIAL_MEDIA Preferences for receiving marketing material'
						  , 'Trustee Title and Name'
						)
	
	--Update bank statement name
	UPDATE
		pa
	SET
		attribute_value =	CASE	
								WHEN ba.bank_account_id IS NULL THEN '333333, 22222222'
								ELSE ib.bsb_number + ', ' + RTRIM(ba.account_number)
							END
	FROM
		party_attribute pa
		JOIN attribute a ON a.attribute_id = pa.attribute_id
		JOIN member_account ma ON ma.member_account_id = pa.party_id AND pa.party_type_id = 1
		JOIN entity e ON e.entity_id = ma.entity_id
		LEFT JOIN entity_bank_account eba ON eba.entity_id = pa.party_id
		LEFT JOIN bank_account ba ON ba.bank_account_id = eba.bank_account_id
		LEFT JOIN institution_branch ib on ib.institution_branch_id = ba.institution_branch_id
	WHERE
		a.description = 'Bank Statement Name'
	
	--Update verified bank account details
	UPDATE
		pa
	SET
		attribute_value = x.new_value
	FROM 
		party_attribute pa
		JOIN (	
				SELECT 
					pa1.party_attribute_id
					, pa1.attribute_value
					, CASE
						WHEN ba.bank_account_id IS NULL THEN '333333, 22222222,'
						ELSE ib.bsb_number + ', ' + RTRIM(ba.account_number) + ', ' + ISNULL(ba.customer_reference, '')
					END + CAST(ROW_NUMBER() OVER (PARTITION BY pa1.party_id ORDER BY ba.bank_account_id) AS VARCHAR) AS new_value
				FROM 
					dbo.party_attribute pa1
					JOIN attribute a ON a.attribute_id = pa1.attribute_id
					LEFT JOIN entity_bank_account eba ON eba.entity_id = pa1.party_id
					LEFT JOIN bank_account ba ON ba.bank_account_id = eba.bank_account_id
					LEFT JOIN institution_branch ib on ib.institution_branch_id = ba.institution_branch_id
				WHERE 
					pa1.party_type_id = 20 
					and a.description = 'Verified bank details'
			) x ON x.party_attribute_id = pa.party_attribute_id
				AND x.attribute_value = pa.attribute_value

	IF EXISTS 
		(	SELECT * 
			FROM sys.objects 
			WHERE [name] = N'au_party_attribute' AND [type] = 'TR'
		)
	BEGIN
		ALTER TABLE [party_attribute] ENABLE TRIGGER [au_party_attribute];      
	END;

	SET @endTime = GETDATE();
	SET @StepMessage = 'Block 64 - Finish. Time Taken - ' + CONVERT(VARCHAR(20), @endTime - @startTime, 114);
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;

    /*************************************** FATCA Identification ******************************************/

    SET @StepMessage = 'Block 65 - Start';
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;
	SET @startTime = GETDATE();

	IF EXISTS 
		(	SELECT * 
			FROM sys.objects 
			WHERE [name] = N'au_identification' AND [type] = 'TR'
		)
	BEGIN
		ALTER TABLE [identification] DISABLE TRIGGER [au_identification];      
	END;

    UPDATE [identification]
       SET [tax_identification_number] = SUBSTRING(CONVERT(VARCHAR(100), NEWID()), 0, CHARINDEX('-', CONVERT(VARCHAR(100), NEWID()))) + CHAR(CAST( ( 90 - 65 ) * RAND() + 65 AS INTEGER))
    WHERE 
          [tax_identification_number] IS NOT NULL;

	IF EXISTS 
		(	SELECT * 
			FROM sys.objects 
			WHERE [name] = N'au_identification' AND [type] = 'TR'
		)
	BEGIN
		ALTER TABLE [identification] ENABLE TRIGGER [au_identification];      
	END;

    SET @endTime = GETDATE();
	SET @StepMessage = 'Block 65 - Finish. Time Taken - ' + CONVERT(VARCHAR(20), @endTime - @startTime, 114);
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;

	/*************************************** Payment Request ******************************************/

    SET @StepMessage = 'Block 66 - Start';
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;
	SET @startTime = GETDATE();

	IF EXISTS 
		(	SELECT * 
			FROM sys.objects 
			WHERE [name] = N'au_payment_request' AND [type] = 'TR'
		)
	BEGIN
		ALTER TABLE [payment_request] DISABLE TRIGGER [au_payment_request];      
	END;

    UPDATE [payment_request]
       SET [payee_name] = LEFT(	CASE
									WHEN [e].[title_id] = 1
										THEN 'Mr'
									WHEN [e].[title_id] = 2
										THEN 'Mrs'
									ELSE ''
								END + [e].[name], 60)
    FROM [payment_request] [pr]
        JOIN [entity] [e]
            ON [e].[entity_id] = [pr].[entity_id]
    WHERE 
          [party_type_id] = 1
          AND [pr].[payee_name] IS NOT NULL;

	IF EXISTS 
		(	SELECT * 
			FROM sys.objects 
			WHERE [name] = N'au_payment_request' AND [type] = 'TR'
		)
	BEGIN
		ALTER TABLE [payment_request] ENABLE TRIGGER [au_payment_request];      
	END;

	SET @endTime = GETDATE();
	SET @StepMessage = 'Block 66 - Finish. Time Taken - ' + CONVERT(VARCHAR(20), @endTime - @startTime, 114);
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;

    /*************************************** Pension Lifetime Allowance Protection ******************************************/

    SET @StepMessage = 'Block 67 - Start';
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;
	SET @startTime = GETDATE();

	IF EXISTS 
		(	SELECT * 
			FROM sys.objects 
			WHERE [name] = N'au_protection' AND [type] = 'TR'
		)
	BEGIN
		ALTER TABLE [protection] DISABLE TRIGGER [au_protection];      
	END;

    UPDATE [protection]
       SET [enhanced_hmrc_cert_no] = ( CASE
                                         WHEN [enhanced_hmrc_cert_no] IS NULL
                                           THEN NULL
                                         ELSE SUBSTRING(CONVERT(VARCHAR(100), NEWID()), 0, CHARINDEX('-', CONVERT(VARCHAR(100), NEWID()))) + CHAR(CAST( ( 90 - 65 ) * RAND() + 65 AS INTEGER))
                                       END )
         , [primary_hmrc_cert_no] = ( CASE
                                        WHEN [primary_hmrc_cert_no] IS NULL
                                          THEN NULL
                                        ELSE SUBSTRING(CONVERT(VARCHAR(100), NEWID()), 0, CHARINDEX('-', CONVERT(VARCHAR(100), NEWID()))) + CHAR(CAST( ( 90 - 65 ) * RAND() + 65 AS INTEGER))
                                      END )
         , [pension_hmrc_cert_no] = ( CASE
                                        WHEN [pension_hmrc_cert_no] IS NULL
                                          THEN NULL
                                        ELSE SUBSTRING(CONVERT(VARCHAR(100), NEWID()), 0, CHARINDEX('-', CONVERT(VARCHAR(100), NEWID()))) + CHAR(CAST( ( 90 - 65 ) * RAND() + 65 AS INTEGER))
                                      END )
         , [overseas_hmrc_cert_no] = ( CASE
                                         WHEN [overseas_hmrc_cert_no] IS NULL
                                           THEN NULL
                                         ELSE SUBSTRING(CONVERT(VARCHAR(100), NEWID()), 0, CHARINDEX('-', CONVERT(VARCHAR(100), NEWID()))) + CHAR(CAST( ( 90 - 65 ) * RAND() + 65 AS INTEGER))
                                       END )
         , [non_resident_hmrc_cert_no] = ( CASE
                                             WHEN [non_resident_hmrc_cert_no] IS NULL
                                               THEN NULL
                                             ELSE SUBSTRING(CONVERT(VARCHAR(100), NEWID()), 0, CHARINDEX('-', CONVERT(VARCHAR(100), NEWID()))) + CHAR(CAST( ( 90 - 65 ) * RAND() + 65 AS INTEGER))
                                           END )
         , [fixed_hmrc_cert_no] = ( CASE
                                      WHEN [fixed_hmrc_cert_no] IS NULL
                                        THEN NULL
                                      ELSE SUBSTRING(CONVERT(VARCHAR(100), NEWID()), 0, CHARINDEX('-', CONVERT(VARCHAR(100), NEWID()))) + CHAR(CAST( ( 90 - 65 ) * RAND() + 65 AS INTEGER))
                                    END )
         , [individual_hmrc_cert_no] = ( CASE
                                           WHEN [individual_hmrc_cert_no] IS NULL
                                             THEN NULL
                                           ELSE SUBSTRING(CONVERT(VARCHAR(100), NEWID()), 0, CHARINDEX('-', CONVERT(VARCHAR(100), NEWID()))) + CHAR(CAST( ( 90 - 65 ) * RAND() + 65 AS INTEGER))
                                         END );

	IF EXISTS 
		(	SELECT * 
			FROM sys.objects 
			WHERE [name] = N'au_protection' AND [type] = 'TR'
		)
	BEGIN
		ALTER TABLE [protection] ENABLE TRIGGER [au_protection];      
	END;

	SET @endTime = GETDATE();
	SET @StepMessage = 'Block 67 - Finish. Time Taken - ' + CONVERT(VARCHAR(20), @endTime - @startTime, 114);
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;

    /*************************************** Spouse ******************************************/

    SET @StepMessage = 'Block 68 - Start';
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;
	SET @startTime = GETDATE();

	IF EXISTS 
		(	SELECT * 
			FROM sys.objects 
			WHERE [name] = N'au_spouse' AND [type] = 'TR'
		)
	BEGIN
		ALTER TABLE [spouse] DISABLE TRIGGER [au_spouse];      
	END;

	UPDATE
		s
	SET
		name =	CASE
					WHEN s.name IS NULL THEN NULL
					ELSE CASE
							WHEN se.name IS NULL THEN 'deleted'
							ELSE se.name
					END
				END
		, birth_date =	CASE
							WHEN s.birth_date IS NULL THEN NULL
							ELSE CASE
									WHEN se.birth_date IS NULL THEN DATEADD(dd, CAST(RAND(DATEDIFF(DAY, 0, s.birth_date)) * 100 + 10 AS INT), s.birth_date)
									ELSE se.birth_date
							END
						END
		, gender =	CASE
						WHEN se.gender IS NULL THEN 'M'
						ELSE se.gender
					END
	FROM 
		spouse s
		JOIN entity e on e.entity_id = s.entity_id
		LEFT JOIN entity se on se.entity_id = s.spouse_entity_id

	IF EXISTS 
		(	SELECT * 
			FROM sys.objects 
			WHERE [name] = N'au_spouse' AND [type] = 'TR'
		)
	BEGIN
		ALTER TABLE [spouse] ENABLE TRIGGER [au_spouse];      
	END;

    SET @endTime = GETDATE();
	SET @StepMessage = 'Block 68 - Finish. Time Taken - ' + CONVERT(VARCHAR(20), @endTime - @startTime, 114);
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;

	/*************************************** Audit Transaction ******************************************/

    -- KEEP Update of audit_tranaction as the LAST part of this script - safer in-case any of above updates created audit rows 
	/*
	Audit Types:
	 6	=  Name, Title, Given Names and Preferred Name change
	 7  =  Salutation change
	 9  =  ACN, company_rego_no update
	 10	=  Contact details change
	 11	=  Birth Date change
	 12	=  Deceased Date change 
	 13	=  NINO change 
	 18	=  Salary details change 
	 21	=  Payment details change 
	 30 =  Short name
	 38	=  ABN change
	 39	=  Contact Name change
	 54 =  Mailing Title change
	 100 / 101 / 102 = address change 
	 202 = spouse details change 
	 200 / 201  = spouse address change 
	 3902 = Salary history change
	 1504 = beneficiary name change
	 1507 = beneficiary dob change
	 1500 / 1501 = beneficiary details change
	 3900 = Salary history details,
	 1400 / 1401 / 1402 / 1403 = Bank Account Details
	 1447 = Mifid National Identity (includes NINO) 
	 3308 / 3307 / 3309 = Related to details
	 6100 = PAYE reference
	 6102 / 6103 = Old gateway user id
	 6320 = Account Designation 
	*/

    SET @StepMessage = 'Block 69 - Start';
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;
	SET @startTime = GETDATE();

    UPDATE [audit_transaction]
       SET [old_value] = ( CASE
                             WHEN LEN(LTRIM([old_value])) > 1
                               THEN 'Old value for party id ' + CONVERT(VARCHAR, [party_id])
                             ELSE [old_value]
                           END )
         , [new_value] = ( CASE
                             WHEN LEN(LTRIM([new_value])) > 1
                               THEN 'New value for Party id ' + CONVERT(VARCHAR, [party_id])
                             ELSE [new_value]
                           END )
    WHERE 
          [audit_type_id] IN ( 6, 7, 9, 10, 11, 13, 18, 21, 30, 38, 39, 54, 200, 100, 101, 102, 201, 202, 3902, 1447, 1504, 1507, 1501, 1500, 3900, 1400, 1401, 1402, 1403, 3307, 3308, 3309, 6100, 6102, 6103, 6320 );

	SET @endTime = GETDATE();
	SET @StepMessage = 'Block 69 - Finish. Time Taken - ' + CONVERT(VARCHAR(20), @endTime - @startTime, 114);
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;

	/*************************************** Truncate fino_rec_6b_report ******************************************/

    SET @StepMessage = 'Block 70 - Start';
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;
	SET @startTime = GETDATE();

	IF EXISTS
		( SELECT 
				*
			FROM [sys].[objects]
			WHERE [name] = 'fino_rec_6b_report'
				AND [type] = 'U'
		) 
    BEGIN
        TRUNCATE TABLE fino_rec_6b_report
    END;

    SET @endTime = GETDATE();
	SET @StepMessage = 'Block 70 - Finish. Time Taken - ' + CONVERT(VARCHAR(20), @endTime - @startTime, 114);
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;

	/*************************************** addacs_file_entry ******************************************/
	
	SET @StepMessage = 'Block 71 - Start';
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;
	SET @startTime = GETDATE();

	UPDATE
		afe
	SET
		afe.existing_bank_account_name = CASE 
											WHEN e.entity_id IS NULL THEN 'Data Obfuscated'
											ELSE LEFT(ISNULL(e.given_names + ' ', '') + e.name, 40)
										END
		, afe.existing_bank_account_number = CASE 
												WHEN ba.account_number IS NULL THEN RIGHT('99999999' + CONVERT(VARCHAR, afe.addacs_file_entry_id % 234011), 8)
												ELSE ba.account_number 
											END
		, afe.existing_sort_code = RIGHT('99999999' + CONVERT(VARCHAR, afe.addacs_file_entry_id % 234011), 6)
		, afe.new_bank_account_name = CASE
										WHEN new_bank_account_name IS NULL THEN NULL
										ELSE 
											CASE 
												WHEN e.entity_id IS NULL THEN 'Data Obfuscated'
												ELSE LEFT('New' + ' ' + ISNULL(e.given_names, '') + ' ' + e.name, 40)
											END
										END
		, afe.new_bank_account_number = CASE
											WHEN new_bank_account_number IS NULL THEN NULL
											ELSE 
												CASE 
													WHEN ba.account_number IS NULL THEN RIGHT('88888888' + CONVERT(VARCHAR, afe.addacs_file_entry_id % 234011), 8)
													ELSE ba.account_number 
												END
											END
		, afe.new_sort_code = CASE 
								WHEN new_sort_code IS NULL THEN NULL
								ELSE RIGHT('88888888' + CONVERT(VARCHAR, afe.addacs_file_entry_id % 234011), 6)
							END
		, afe.reference = CASE 
							WHEN e.name IS NULL THEN 'Data Obfuscated'
							ELSE STUFF(reference, CHARINDEX(' ', reference) + 1, LEN(e.name), LEFT(UPPER(e.name), 20 - CHARINDEX(' ', reference)))
						END
	FROM 
		addacs_file_entry afe
		LEFT JOIN pdc p ON p.pdc_id = afe.pdc_id
		LEFT JOIN pdc_member_account pma ON pma.pdc_id = p.pdc_id
		LEFT JOIN member_account ma on ma.member_account_id = pma.member_account_id
		LEFT JOIN entity e ON e.entity_id = ma.entity_id
		LEFT JOIN entity_bank_account eba on eba.entity_id = e.entity_id
		LEFT JOIN bank_account ba on ba.bank_account_id = eba.bank_account_id

	SET @endTime = GETDATE();
	SET @StepMessage = 'Block 71 - Finish. Time Taken - ' + CONVERT(VARCHAR(20), @endTime - @startTime, 114);
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;

	/*************************************** arudd_file_entry ******************************************/

	SET @StepMessage = 'Block 72 - Start';
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;
	SET @startTime = GETDATE();

	UPDATE
		afe
	SET
		afe.direct_debit_account_number = CASE 
											WHEN ba.account_number IS NULL THEN RIGHT('99999999' + CONVERT(VARCHAR, afe.arudd_file_entry_id % 234011), 8)
											ELSE ba.account_number 
										 END
	   , afe.direct_debit_sort_code = RIGHT('99999999' + CONVERT(VARCHAR, afe.arudd_file_entry_id % 234011), 6)
	   , afe.direct_debit_account_name = CASE 
											WHEN e.entity_id IS NULL THEN 'Data Obfuscated'
											ELSE LEFT(ISNULL(e.given_names + ' ', '') + e.name, 40)
										END
	   , afe.direct_debit_reference =  CASE
										WHEN e.name IS NULL THEN 'Data Obfuscated'
										ELSE STUFF(direct_debit_reference, CHARINDEX(' ', direct_debit_reference) + 1, LEN(e.name), LEFT(UPPER(e.name), 20 - CHARINDEX(' ', direct_debit_reference)))
									END
	  , afe.originator_account_number = RIGHT('99999999' + CONVERT(VARCHAR, afe.originator_account_number % 234012), 8)
	  , afe.originator_account_sortcode = RIGHT('99999999' + CONVERT(VARCHAR, afe.originator_account_sortcode % 234012), 6)
	FROM 
		arudd_file_entry afe
		LEFT JOIN member_account ma on ma.member_account_id = afe.member_account_id
		LEFT JOIN entity e on e.entity_id = ma.entity_id
		LEFT JOIN entity_bank_account eba on eba.entity_id = e.entity_id
		LEFT JOIN bank_account ba on ba.bank_account_id = eba.bank_account_id
	
	SET @endTime = GETDATE();
	SET @StepMessage = 'Block 72 - Finish. Time Taken - ' + CONVERT(VARCHAR(20), @endTime - @startTime, 114);
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;

	/*************************************** Bank_reconciliation ******************************************/

	SET @StepMessage = 'Block 73 - Start';
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;
	SET @startTime = GETDATE();

	IF EXISTS 
		(	SELECT * 
			FROM sys.objects 
			WHERE [name] = N'au_bank_reconciliation' AND [type] = 'TR'
		)
	BEGIN
		ALTER TABLE [bank_reconciliation] DISABLE TRIGGER [au_bank_reconciliation];      
	END;

	UPDATE 
		bank_reconciliation
	SET 
		notation = 'Data Obfuscated'
	WHERE 
		notation IS NOT NULL
 
	IF EXISTS 
		(	SELECT * 
			FROM sys.objects 
			WHERE [name] = N'au_bank_reconciliation' AND [type] = 'TR'
		)
	BEGIN
		ALTER TABLE [bank_reconciliation] ENABLE TRIGGER [au_bank_reconciliation];      
	END;

	SET @endTime = GETDATE();
	SET @StepMessage = 'Block 73 - Finish. Time Taken - ' + CONVERT(VARCHAR(20), @endTime - @startTime, 114);
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;

	/*************************************** Bank_statement ******************************************/

	SET @StepMessage = 'Block 74 - Start';
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;
	SET @startTime = GETDATE();

	--Control table for chunk updates
	IF OBJECT_ID('tempdb..#upd_bank_statement_control') IS NOT NULL
		DROP TABLE #upd_bank_statement_control;

	CREATE TABLE #upd_bank_statement_control
	(
		control_id INT NOT NULL,
		complete INT DEFAULT 0 NOT NULL
	);
	
	INSERT INTO #upd_bank_statement_control
			(
				control_id
			)
	SELECT
		bank_statement_id
	FROM
		bank_statement

	CREATE CLUSTERED INDEX idx_bank_statement_control ON #upd_bank_statement_control(control_id)

	WHILE EXISTS 
	(
		SELECT 
			1
		FROM
			[#upd_bank_statement_control]
		WHERE 
			complete <> 2
	)
	BEGIN

		UPDATE TOP (@batchSize) 
				[#upd_bank_statement_control]
			SET 
				complete = 1
			WHERE
				complete = 0

		UPDATE 
			bs
		SET 
			reference_number = CASE
								WHEN reference_number IS NULL THEN NULL
								ELSE 'Data Obfuscated'
							END
		  , additional_reference = CASE
										WHEN additional_reference IS NULL THEN NULL
										ELSE 'Data Obfuscated'
									END
		FROM
			bank_statement bs
			JOIN #upd_bank_statement_control u 
				ON bs.bank_statement_id = u.control_id 
				AND u.complete = 1

		UPDATE
				[#upd_bank_statement_control]
			SET 
				complete = 2
			WHERE
				complete = 1
	END
	
	IF OBJECT_ID('tempdb..#upd_bank_statement_control') IS NOT NULL
		DROP TABLE #upd_bank_statement_control;

	SET @endTime = GETDATE();
	SET @StepMessage = 'Block 74 - Finish. Time Taken - ' + CONVERT(VARCHAR(20), @endTime - @startTime, 114);
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;

	/*************************************** Bank_transfer_request ******************************************/

	SET @StepMessage = 'Block 75 - Start';
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;
	SET @startTime = GETDATE();

	UPDATE 
		bank_transfer_request
	SET 
		notation = 'Data Obfuscated'
	WHERE 
		notation IS NOT NULL

	SET @endTime = GETDATE();
	SET @StepMessage = 'Block 75 - Finish. Time Taken - ' + CONVERT(VARCHAR(20), @endTime - @startTime, 114);
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;

	/*************************************** Benefit_calculation ******************************************/

	SET @StepMessage = 'Block 76 - Start';
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;
	SET @startTime = GETDATE();

	UPDATE
		benefit_calculation
	SET
		enhanced_hmrc_cert_no = (CASE WHEN enhanced_hmrc_cert_no IS NULL THEN NULL
									  ELSE SUBSTRING(CONVERT(VARCHAR(100), NEWID()), 0, CHARINDEX('-', CONVERT(VARCHAR(100), NEWID()))) + CHAR(CAST((90 - 65) * RAND() + 65 AS INTEGER))
								 END)
	  , primary_hmrc_cert_no = (CASE WHEN primary_hmrc_cert_no IS NULL THEN NULL
									 ELSE SUBSTRING(CONVERT(VARCHAR(100), NEWID()), 0, CHARINDEX('-', CONVERT(VARCHAR(100), NEWID()))) + CHAR(CAST((90 - 65) * RAND() + 65 AS INTEGER))
								END)
	  , pension_hmrc_cert_no = (CASE WHEN pension_hmrc_cert_no IS NULL THEN NULL
									 ELSE SUBSTRING(CONVERT(VARCHAR(100), NEWID()), 0, CHARINDEX('-', CONVERT(VARCHAR(100), NEWID()))) + CHAR(CAST((90 - 65) * RAND() + 65 AS INTEGER))
								END)
	  , overseas_hmrc_cert_no = (CASE WHEN overseas_hmrc_cert_no IS NULL THEN NULL
									  ELSE SUBSTRING(CONVERT(VARCHAR(100), NEWID()), 0, CHARINDEX('-', CONVERT(VARCHAR(100), NEWID()))) + CHAR(CAST((90 - 65) * RAND() + 65 AS INTEGER))
								 END)
	  , non_resident_hmrc_cert_no = (CASE WHEN non_resident_hmrc_cert_no IS NULL THEN NULL
										  ELSE SUBSTRING(CONVERT(VARCHAR(100), NEWID()), 0, CHARINDEX('-', CONVERT(VARCHAR(100), NEWID()))) + CHAR(CAST((90 - 65) * RAND() + 65 AS INTEGER))
									 END)
	  , fixed_hmrc_cert_no = (CASE WHEN fixed_hmrc_cert_no IS NULL THEN NULL
								   ELSE SUBSTRING(CONVERT(VARCHAR(100), NEWID()), 0, CHARINDEX('-', CONVERT(VARCHAR(100), NEWID()))) + CHAR(CAST((90 - 65) * RAND() + 65 AS INTEGER))
							  END)
	  , individual_hmrc_cert_no = (CASE WHEN individual_hmrc_cert_no IS NULL THEN NULL
										ELSE SUBSTRING(CONVERT(VARCHAR(100), NEWID()), 0, CHARINDEX('-', CONVERT(VARCHAR(100), NEWID()))) + CHAR(CAST((90 - 65) * RAND() + 65 AS INTEGER))
								   END)
	  , court_order_reference = (CASE WHEN court_order_reference IS NULL THEN NULL
										ELSE SUBSTRING(CONVERT(VARCHAR(100), NEWID()), 0, CHARINDEX('-', CONVERT(VARCHAR(100), NEWID()))) + CHAR(CAST((90 - 65) * RAND() + 65 AS INTEGER))
								   END)

	SET @endTime = GETDATE();
	SET @StepMessage = 'Block 76 - Finish. Time Taken - ' + CONVERT(VARCHAR(20), @endTime - @startTime, 114);
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;

	/*************************************** Benefit_crystallisation_event ******************************************/

	SET @StepMessage = 'Block 77 - Start';
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;
	SET @startTime = GETDATE();

	IF EXISTS 
		(	SELECT * 
			FROM sys.objects 
			WHERE [name] = N'au_benefit_crystalisationevent' AND [type] = 'TR'
		)
	BEGIN
		ALTER TABLE [benefit_crystallisation_event] DISABLE TRIGGER [au_benefit_crystalisationevent];      
	END;

	UPDATE
		benefit_crystallisation_event
	SET
		enhanced_hmrc_cert_no = (CASE WHEN enhanced_hmrc_cert_no IS NULL THEN NULL
									  ELSE SUBSTRING(CONVERT(VARCHAR(100), NEWID()), 0, CHARINDEX('-', CONVERT(VARCHAR(100), NEWID()))) + CHAR(CAST((90 - 65) * RAND() + 65 AS INTEGER))
								 END)
	  , primary_hmrc_cert_no = (CASE WHEN primary_hmrc_cert_no IS NULL THEN NULL
									 ELSE SUBSTRING(CONVERT(VARCHAR(100), NEWID()), 0, CHARINDEX('-', CONVERT(VARCHAR(100), NEWID()))) + CHAR(CAST((90 - 65) * RAND() + 65 AS INTEGER))
								END)
	  , pension_hmrc_cert_no = (CASE WHEN pension_hmrc_cert_no IS NULL THEN NULL
									 ELSE SUBSTRING(CONVERT(VARCHAR(100), NEWID()), 0, CHARINDEX('-', CONVERT(VARCHAR(100), NEWID()))) + CHAR(CAST((90 - 65) * RAND() + 65 AS INTEGER))
								END)
	  , overseas_hmrc_cert_no = (CASE WHEN overseas_hmrc_cert_no IS NULL THEN NULL
									  ELSE SUBSTRING(CONVERT(VARCHAR(100), NEWID()), 0, CHARINDEX('-', CONVERT(VARCHAR(100), NEWID()))) + CHAR(CAST((90 - 65) * RAND() + 65 AS INTEGER))
								 END)
	  , non_resident_hmrc_cert_no = (CASE WHEN non_resident_hmrc_cert_no IS NULL THEN NULL
										  ELSE SUBSTRING(CONVERT(VARCHAR(100), NEWID()), 0, CHARINDEX('-', CONVERT(VARCHAR(100), NEWID()))) + CHAR(CAST((90 - 65) * RAND() + 65 AS INTEGER))
									 END)
	  , fixed_hmrc_cert_no = (CASE WHEN fixed_hmrc_cert_no IS NULL THEN NULL
								   ELSE SUBSTRING(CONVERT(VARCHAR(100), NEWID()), 0, CHARINDEX('-', CONVERT(VARCHAR(100), NEWID()))) + CHAR(CAST((90 - 65) * RAND() + 65 AS INTEGER))
							  END)
	  , individual_hmrc_cert_no = (CASE WHEN individual_hmrc_cert_no IS NULL THEN NULL
										ELSE SUBSTRING(CONVERT(VARCHAR(100), NEWID()), 0, CHARINDEX('-', CONVERT(VARCHAR(100), NEWID()))) + CHAR(CAST((90 - 65) * RAND() + 65 AS INTEGER))
								   END)
 
	IF EXISTS 
		(	SELECT * 
			FROM sys.objects 
			WHERE [name] = N'au_benefit_crystalisationevent' AND [type] = 'TR'
		)
	BEGIN
		ALTER TABLE [benefit_crystallisation_event] ENABLE TRIGGER [au_benefit_crystalisationevent];      
	END;

	SET @endTime = GETDATE();
	SET @StepMessage = 'Block 77 - Finish. Time Taken - ' + CONVERT(VARCHAR(20), @endTime - @startTime, 114);
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;

	/*************************************** Investment_manager ******************************************/

	SET @StepMessage = 'Block 78 - Start';
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;
	SET @startTime = GETDATE();

	IF EXISTS 
		(	SELECT * 
			FROM sys.objects 
			WHERE [name] = N'au_investment_manager' AND [type] = 'TR'
		)
	BEGIN
		ALTER TABLE [investment_manager] DISABLE TRIGGER [au_investment_manager];      
	END;
 
	UPDATE 
		investment_manager
	SET 
		notation = 'Data Obfuscated'
	WHERE 
		notation IS NOT NULL

	IF EXISTS 
		(	SELECT * 
			FROM sys.objects 
			WHERE [name] = N'au_investment_manager' AND [type] = 'TR'
		)
	BEGIN
		ALTER TABLE [investment_manager] ENABLE TRIGGER [au_investment_manager];      
	END;

	SET @endTime = GETDATE();
	SET @StepMessage = 'Block 78 - Finish. Time Taken - ' + CONVERT(VARCHAR(20), @endTime - @startTime, 114);
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;

	/*************************************** pension_payment_method ******************************************/

	SET @StepMessage = 'Block 79 - Start';
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;
	SET @startTime = GETDATE();

	IF EXISTS 
		(	SELECT * 
			FROM sys.objects 
			WHERE [name] = N'au_pension_payment_method' AND [type] = 'TR'
		)
	BEGIN
		ALTER TABLE [pension_payment_method] DISABLE TRIGGER [au_pension_payment_method];      
	END;

	UPDATE
		ppm
	SET
		payee_name = CASE
						WHEN ppm.payee_name IS NULL THEN NULL
						ELSE 
							LEFT(CASE 
									WHEN e.title_id = 1 THEN 'Mr '
									WHEN e.title_id = 2 THEN 'Mrs '
									ELSE ''
								END + ISNULL(e.given_names + ' ', '') + e.name, 40)
						END
	FROM 
		dbo.pension_payment_method ppm
		JOIN dbo.member_account ma ON ma.member_account_id = ppm.member_account_id
		JOIN dbo.entity e ON e.entity_id = ma.entity_id
	WHERE 
		ppm.payee_name IS NOT NULL

	IF EXISTS 
		(	SELECT * 
			FROM sys.objects 
			WHERE [name] = N'au_pension_payment_method' AND [type] = 'TR'
		)
	BEGIN
		ALTER TABLE [pension_payment_method] ENABLE TRIGGER [au_pension_payment_method];      
	END;
 
	SET @endTime = GETDATE();
	SET @StepMessage = 'Block 79 - Finish. Time Taken - ' + CONVERT(VARCHAR(20), @endTime - @startTime, 114);
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;

	/*************************************** Benefit_calculation_payment ******************************************/

	SET @StepMessage = 'Block 80 - Start';
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;
	SET @startTime = GETDATE();

	UPDATE
		bcp
	SET
		payee_name = CASE 
						 WHEN bcp.payee_name IS NULL THEN NULL
						 ELSE
							LEFT(CASE
									WHEN [e].[gender] = 'M' THEN 'Mr '
									WHEN [e].[gender] = 'F' THEN 'Mrs '
								END + ISNULL(e.given_names + ' ', '') + e.name, 60)
					END
	FROM benefit_calculation_payment bcp
	JOIN benefit_calculation bc ON bc.benefit_calculation_id = bcp.benefit_calculation_id
	JOIN member_account ma ON ma.member_account_id = bc.related_account_id
	JOIN entity e on e.entity_id = ma.entity_id

	SET @endTime = GETDATE();
	SET @StepMessage = 'Block 80 - Finish. Time Taken - ' + CONVERT(VARCHAR(20), @endTime - @startTime, 114);
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;

	/*************************************** Complying_fund ******************************************/

	SET @StepMessage = 'Block 81 - Start';
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;
	SET @startTime = GETDATE();

	UPDATE
		complying_fund
	SET
		email_address = CASE
							WHEN email_address IS NULL THEN NULL
							ELSE 'Scrambled@noemail.nodomain'
						END
		, fax_number = CASE
							WHEN fax_number IS NULL THEN NULL
							ELSE '08000000000'
						END
		, hmrc_employer_accts_office_ref = CASE 
											   WHEN hmrc_employer_accts_office_ref IS NULL THEN NULL
											   ELSE SUBSTRING(CONVERT(VARCHAR(100), NEWID()), 0, CHARINDEX('-', CONVERT(VARCHAR(100), NEWID())))
											END
		 , hmrc_gateway_user_id = CASE
									WHEN hmrc_gateway_user_id IS NULL THEN NULL
									ELSE '444444444444'
								END
		 , hmrc_gateway_user_password = CASE
											WHEN hmrc_gateway_user_password IS NULL THEN NULL
											ELSE 'deleted'
										END
		 , paye_reference = CASE
								WHEN paye_reference IS NULL THEN NULL
								ELSE SUBSTRING(CONVERT(VARCHAR(100), NEWID()), 0, CHARINDEX('-', CONVERT(VARCHAR(100), NEWID())))
							END
		 , pension_scheme_tax_ref_number = CASE
												WHEN pension_scheme_tax_ref_number IS NULL THEN NULL
												ELSE SUBSTRING(CONVERT(VARCHAR(100), NEWID()), 0, CHARINDEX('-', CONVERT(VARCHAR(100), NEWID())))
											END
		 , phone_number = CASE
							WHEN phone_number IS NULL THEN NULL
							ELSE '01000 111 111'
						END
		 , settlement_system_reference = CASE
											WHEN settlement_system_reference IS NULL THEN NULL
											ELSE SUBSTRING(CONVERT(VARCHAR(100), NEWID()), 0, CHARINDEX('-', CONVERT(VARCHAR(100), NEWID())))
										END

	SET @endTime = GETDATE();
	SET @StepMessage = 'Block 81 - Finish. Time Taken - ' + CONVERT(VARCHAR(20), @endTime - @startTime, 114);
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;

	/*************************************** Employee_details ******************************************/

	SET @StepMessage = 'Block 82 - Start';
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;
	SET @startTime = GETDATE();

	/* We're updating the salary as someone with an extreme salary may easily be uniquely identified e.g. a chief executive.
	The below code will generate a random number between 10k and 1m. */

	UPDATE
		employee_details
	SET
		salary = CAST(RAND(employee_details_id) * (1000000 - 10000) + 10000 AS decimal(10,2))
	WHERE
		salary > 10000

	SET @endTime = GETDATE();
	SET @StepMessage = 'Block 82 - Finish. Time Taken - ' + CONVERT(VARCHAR(20), @endTime - @startTime, 114);
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;

	/*************************************** Master_model ******************************************/

	SET @StepMessage = 'Block 83 - Start';
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;
	SET @startTime = GETDATE();

	IF EXISTS 
		(	SELECT * 
			FROM sys.objects 
			WHERE [name] = N'au_master_model' AND [type] = 'TR'
		)
	BEGIN
		ALTER TABLE [master_model] DISABLE TRIGGER [au_master_model];      
	END;

	--generate the new scrabled names
	INSERT INTO #master_model_scramble 
	(
		master_model_id, 
		new_name
	)
	SELECT
		master_model_id,
		CASE master_model_id % 14
			WHEN 0 THEN 'Dynamic'
			WHEN 1 THEN 'Balanced'
			WHEN 2 THEN 'Cautious'
			WHEN 3 THEN 'Adventurous'
			WHEN 4 THEN 'Strategic'
			WHEN 5 THEN 'Speculative'
			WHEN 6 THEN 'Defensive'
			WHEN 7 THEN 'Moderate'
			WHEN 8 THEN 'Global'
			WHEN 9 THEN 'Capital'
			WHEN 10 THEN 'Managed'
			WHEN 11 THEN 'Alpha'
			WHEN 12 THEN 'Conservative'
			WHEN 13 THEN 'Aggressive'
		END + ' ' +
		CASE master_model_id % 13
			WHEN 0 THEN ''
			WHEN 1 THEN 'Explicit'
			WHEN 2 THEN 'High'
			WHEN 3 THEN 'Medium'
			WHEN 4 THEN 'Low'
			WHEN 5 THEN 'Passive'
			WHEN 6 THEN 'Active'
			WHEN 7 THEN 'Equity'
			WHEN 8 THEN 'Above'
			WHEN 9 THEN 'Ethical'
			WHEN 10 THEN 'Recovery'
			WHEN 11 THEN 'UT'
			WHEN 12 THEN 'ISA'
		END + ' ' +
		CASE master_model_id % 10
			WHEN 0 THEN ''
			WHEN 1 THEN 'Income'
			WHEN 2 THEN 'Portfolio'
			WHEN 3 THEN 'Risk'
			WHEN 4 THEN 'Level'
			WHEN 5 THEN 'Profile'
			WHEN 6 THEN 'Yield'
			WHEN 7 THEN 'Equity'
			WHEN 8 THEN 'Index'
			WHEN 9 THEN 'Growth'
		END + ' ' +
		CASE master_model_id % 28
			WHEN 0 THEN ''
			WHEN 1 THEN '1'
			WHEN 2 THEN '2'
			WHEN 3 THEN '3'
			WHEN 4 THEN '4'
			WHEN 5 THEN '5'
			WHEN 4 THEN '10%'
			WHEN 6 THEN '20%'
			WHEN 7 THEN '30%'
			WHEN 8 THEN '40%'
			WHEN 9 THEN '50%'
			WHEN 10 THEN 'Q1'
			WHEN 11 THEN 'Q2'
			WHEN 12 THEN 'Q3'
			WHEN 13 THEN 'Q4'
			WHEN 14 THEN 'Monthly'
			WHEN 15 THEN 'Quarterly'
			WHEN 16 THEN 'Jan'
			WHEN 17 THEN 'Feb'
			WHEN 18 THEN 'Mar'
			WHEN 19 THEN 'Apr'
			WHEN 20 THEN 'May'
			WHEN 21 THEN 'Jun'
			WHEN 22 THEN 'Jul'
			WHEN 23 THEN 'Aug'
			WHEN 24 THEN 'Sep'
			WHEN 25 THEN 'Oct'
			WHEN 26 THEN 'Nov'
			WHEN 27 THEN 'Dec'
		END + ' ' +
		CASE master_model_id % 9
			WHEN 0 THEN ''
			WHEN 1 THEN '2013'
			WHEN 2 THEN '2014'
			WHEN 3 THEN '2015'
			WHEN 4 THEN '2016'
			WHEN 5 THEN '2017'
			WHEN 6 THEN '2018'
			WHEN 7 THEN '2019'
			WHEN 8 THEN '2020'
		END + ' ' +
		CASE master_model_id % 8
			WHEN 0 THEN ''
			WHEN 1 THEN 'Cash'
			WHEN 2 THEN '- V1'
			WHEN 3 THEN '- V2'
			WHEN 4 THEN '- V3'
			WHEN 5 THEN '- V4'
			WHEN 6 THEN '- V5'
			WHEN 7 THEN '- V6'
		END 
	FROM 
		master_model

	--update master model name to the new scrambled name
	UPDATE
		mm
	SET
		name = mms.new_name
	FROM 
		#master_model_scramble mms
		JOIN master_model mm on mms.master_model_id = mm.master_model_id

	--master model name needs to be unique at each level
	IF EXISTS (	SELECT 
					name
				FROM
					master_model
				GROUP BY
					owner_party_id,
					name
				HAVING
					COUNT(*) > 1 )
	BEGIN
		WITH cte_models_to_update AS (
			SELECT 
				mm.master_model_id
				, mm.owner_party_id
				, mm.name + CAST(ROW_NUMBER() OVER (partition by mm.owner_party_id, mm.name ORDER BY mm.owner_party_id) AS VARCHAR) AS updated_name
			FROM
				master_model mm
				JOIN (	SELECT 
							owner_party_id
						  , name
						FROM
							master_model
						GROUP BY
							owner_party_id
						  , name
						HAVING
							COUNT(*) > 1 
					) x ON mm.owner_party_id = x.owner_party_id
						AND mm.name	= x.name
		)
		UPDATE
			mm
		SET
			mm.name = mu.updated_name
		FROM 
			master_model mm
			JOIN cte_models_to_update mu ON mu.master_model_id = mm.master_model_id;
	END

	IF EXISTS 
		(	SELECT * 
			FROM sys.objects 
			WHERE [name] = N'au_master_model' AND [type] = 'TR'
		)
	BEGIN
		ALTER TABLE [master_model] ENABLE TRIGGER [au_master_model];      
	END;

	SET @endTime = GETDATE();
	SET @StepMessage = 'Block 83 - Finish. Time Taken - ' + CONVERT(VARCHAR(20), @endTime - @startTime, 114);
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;
	
	/*************************************** Investment_model ******************************************/

	SET @StepMessage = 'Block 84 - Start';
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;
	SET @startTime = GETDATE();

	IF EXISTS 
		(	SELECT * 
			FROM sys.objects 
			WHERE [name] = N'au_investment_model' AND [type] = 'TR'
		)
	BEGIN
		ALTER TABLE [investment_model] DISABLE TRIGGER [au_investment_model];      
	END;

	UPDATE
		im
	SET
		short_code = LEFT(mm.name, 40)
	  , description = mm.name
	FROM
		investment_model im
		JOIN master_model mm ON mm.master_model_id = im.master_model_id

	IF EXISTS 
		(	SELECT * 
			FROM sys.objects 
			WHERE [name] = N'au_investment_model' AND [type] = 'TR'
		)
	BEGIN
		ALTER TABLE [investment_model] ENABLE TRIGGER [au_investment_model];      
	END;

	SET @endTime = GETDATE();
	SET @StepMessage = 'Block 84 - Finish. Time Taken - ' + CONVERT(VARCHAR(20), @endTime - @startTime, 114);
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;

	/*************************************** DELETE TABLES ******************************************/

	--There is a foreign key constraint, so can't truncate
	SET @StepMessage = 'Block 85 - Start';
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;
	SET @startTime = GETDATE();

	DELETE FROM note;
	DELETE FROM lisa_property;
	DELETE FROM tax_office_file_sub_header;
	DELETE FROM external_bank_account;

	SET @endTime = GETDATE();
	SET @StepMessage = 'Block 85 - Finish. Time Taken - ' + CONVERT(VARCHAR(20), @endTime - @startTime, 114);
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;

	/*************************************** TRUNCATE TABLES ******************************************/

	SET @StepMessage = 'Block 86 - Start';
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;
	SET @startTime = GETDATE();

	TRUNCATE TABLE ann_idd_review_results;
	TRUNCATE TABLE trans_int_payment;
	TRUNCATE TABLE tmp_product_provider_customer;
	TRUNCATE TABLE tmp_payment_request;
	TRUNCATE TABLE tmp_il_takeup;
	TRUNCATE TABLE tmp_audit_member_account;
	TRUNCATE TABLE tmp_audit_entity;
	TRUNCATE TABLE tmp_audit_bank_account;
	TRUNCATE TABLE tmp_audit_address;
	TRUNCATE TABLE trans_int_recv_provider;
	TRUNCATE TABLE transfer_client_details;
	TRUNCATE TABLE transfer_p45_detail;
	TRUNCATE TABLE surcharge_extract;
	TRUNCATE TABLE tax_office_file_details;
	TRUNCATE TABLE entity_scramble;
    
	SET @endTime = GETDATE();
	SET @StepMessage = 'Block 86 - Finish. Time Taken - ' + CONVERT(VARCHAR(20), @endTime - @startTime, 114);
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;

	/*************************************** Clean up ******************************************/

	IF EXISTS
		( SELECT 
				*
			FROM [sys].[objects]
			WHERE [name] = 'postcode_scramble'
				AND [type] = 'U'
		) 
    BEGIN
        DROP TABLE [dbo].[postcode_scramble];
    END;

	IF OBJECT_ID('tempdb..#investors_to_change') IS NOT NULL
		DROP TABLE [#investors_to_change];

	IF OBJECT_ID('tempdb..#NINOPrefix') IS NOT NULL
		DROP TABLE [#NINOPrefix];
			
	IF OBJECT_ID('tempdb..#NINOSuffix') IS NOT NULL
		DROP TABLE [#NINOSuffix];

	IF OBJECT_ID('tempdb..#master_model_scramble') IS NOT NULL
		DROP TABLE #master_model_scramble;

	SET @StepMessage = convert(varchar,getdate(),109) + ' Finish Scramble'
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;

END TRY
BEGIN CATCH
    SELECT 
          @ErrorMessage = @StepMessage + ' - ' + ERROR_MESSAGE();

    RAISERROR(@ErrorMessage, 16, 1);
END CATCH;
END;