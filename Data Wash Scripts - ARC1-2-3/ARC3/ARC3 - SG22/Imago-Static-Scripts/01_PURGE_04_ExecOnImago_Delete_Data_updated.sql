/*
 * Who: Ramon Buckland
 * History: 
 * May 2014   - RamonB - Original Version
 * 8 Jul 2014 - RamonB - Added Set Password to a default
 * 04/05/2017 - Liz Hall - update for Imago v5
 *
 * Step 1 - Delete all the old Imago Data we don't want
 *
 *  -- For Future redevelopment - this list was created from 
 *     select 'DELETE FROM [' + name  + ']' from sys.tables 
 *  -- Imago v5 - have added all the tables that contain quote data, audit data, migrated data
 *     and batch data. Also passwords are held on three separate tables now
 */ 
 
 
 PRINT 'DATA RowCounts Before'
 SELECT sc.name +'.'+ ta.name TableName
 ,SUM(pa.rows) RowCnt
 FROM sys.tables ta
 INNER JOIN sys.partitions pa
 ON pa.OBJECT_ID = ta.OBJECT_ID
 INNER JOIN sys.schemas sc
 ON ta.schema_id = sc.schema_id
 WHERE ta.is_ms_shipped = 0 AND pa.index_id IN (1,0)
 GROUP BY sc.name,ta.name
 -- ORDER BY SUM(pa.rows) DESC
 ORDER BY ta.name
 


-- DELETE ALL DATA - dbo.VARIABLE_INCOME_DETAILS	0
-- DELETE ALL DATA - dbo.TRANCHE_DETAILS	0
-- DELETE ALL DATA - dbo.USER_SESSION	0
-- DELETE ALL DATA - dbo.HISTORY	0
-- DELETE ALL DATA - dbo.HISTORY_DETAILS	0
-- DELETE ALL DATA - dbo.LEGACY_CHARGE	0
-- DELETE ALL DATA - dbo.LEGACY_POLICY	0
-- DELETE ALL DATA - dbo.MEMBER_DETAILS	0
-- DELETE ALL DATA - dbo.MEMBER_EVENT	0
-- DELETE ALL DATA - dbo.AUDIT_LOG	0
-- DELETE ALL DATA - dbo.AUTHENTICATION_TICKET	0
-- DELETE ALL DATA - dbo.CONTRIBUTIONS	0
-- DELETE ALL DATA - dbo.CLIENT_PARAMS	0
-- DELETE ALL DATA - dbo.PERSONAL_DETAIL	0
-- DELETE ALL DATA - dbo.QUOTE_DETAILS	0


-- auth and session data
DELETE FROM [AuditLog]
DELETE FROM [UserAudit]
DELETE FROM [UserAuditHelperTable]
--DELETE FROM [USER_SESSION] : not in Imago v5
DELETE FROM [AuthenticationTicket]
--DELETE FROM [HISTORY_DETAILS] : not in Imago v5
--DELETE FROM [HISTORY]: not in Imago v5
DELETE FROM [ContactAddress]
DELETE FROM [ContactAddressHistory]
DELETE FROM [ContactInstance]
DELETE FROM [ContactInstanceHistory]
DELETE FROM [ContactMethod]
DELETE FROM [ContactMethodHistory]

-- quotes data
DELETE FROM [QuoteProductCharge]
DELETE FROM [QuoteFundCharge]
DELETE FROM [QuoteFund]
DELETE FROM [QuoteVariableIncome]
DELETE FROM [QuoteContribution]
DELETE FROM [QuoteTranche]
DELETE FROM [QuoteOrganisation]
DELETE FROM [QuoteOrganisationHistory]
DELETE FROM [Quote]
DELETE FROM [QuoteProductChargeHistory]
DELETE FROM [QuoteFundChargeHistory]
DELETE FROM [QuoteFundHistory]
DELETE FROM [QuoteVariableIncomeHistory]
DELETE FROM [QuoteContributionHistory]
DELETE FROM [QuoteTrancheHistory]
DELETE FROM [QuoteHistory]
DELETE FROM [QuoteFundProvider]
DELETE FROM [QuoteFundProviderHistory]
DELETE FROM [QuoteIllustrationVersion]
DELETE FROM [QuotePerson]
DELETE FROM [QuoteProduct]
DELETE FROM [QuoteProductHistory]
DELETE FROM [QuoteProductChargeTier]
DELETE FROM [QuoteProductChargeTierHistory]
DELETE FROM [QuoteResultBlock]
DELETE FROM [QuoteResultBlockHistory]
DELETE FROM [QuoteSystemDefault]
DELETE FROM [QuoteSystemDefaultHistory]
DELETE FROM [PdfMigration]

--- batch item data

DELETE FROM BatchItemData
DELETE FROM BatchItem
DELETE FROM Batch



-- member / person data
DELETE FROM [Client]
DELETE FROM [ClientHistory]
DELETE FROM [Person]
DELETE FROM [PersonHistory]

--these tables are either not in Imago v5 or hold no data
--DELETE FROM [LEGACY_CHARGE] 
--DELETE FROM [LEGACY_POLICY]
--DELETE FROM [MEMBER_EVENT]
--DELETE FROM [CLIENT_PARAMS]


--update RegisteredUser
--set [Password] = 'bMLR3/l53PoECvk8uDAtog==' -- Imago2 not working with imago v5
--where [Username] LIKE '%[^0-9]%'

--update RegisteredUserHistory
--set [Password] = 'bMLR3/l53PoECvk8uDAtog==' -- Imago2 not working with imago v5
--where [Username] LIKE '%[^0-9]%'

--update webpages_Membership
--set [Password] = 'bMLR3/l53PoECvk8uDAtog==' -- Imago2 not working with imago v5

	
-- DELETE FROM [GROUPS] -- This has AEGON, AOR and ARR .. leaving it in there 
-- We need to rewrite TABLE [USER_GROUPS_LINKS] -- so We don't Empty it out
-- We KEEP [GENERATORS]
-- We KEEP  [MEMBER_REFERENCE_GENERATOR]
-- We KEEP some Rows from REGISTERED_USER need to rewrite TABLE [REGISTERED_USER]

PRINT 'DATA RowCounts After'
 SELECT sc.name +'.'+ ta.name TableName
 ,SUM(pa.rows) RowCnt
 FROM sys.tables ta
 INNER JOIN sys.partitions pa
 ON pa.OBJECT_ID = ta.OBJECT_ID
 INNER JOIN sys.schemas sc
 ON ta.schema_id = sc.schema_id
 WHERE ta.is_ms_shipped = 0 AND pa.index_id IN (1,0)
 GROUP BY sc.name,ta.name
 -- ORDER BY SUM(pa.rows) DESC
 ORDER BY ta.name
 