/*
05/01/2019

New script from Adam Hayzer.   Database = xxx_PortalSec

New script to be run post a refresh. This is required to remove the production URL that is stored within xxx_PortalSec.dbo.SERVICE_PROVIDER that will exist when the database gets refreshed. Please can this be associated with the other refresh scripts. 

10/06/2019 - Adam advised to use the same URL for ARC3 NW Test environments as SuffolLife is not used by ARC3 so all ok.
*/

update [dbo].[SERVICE_PROVIDER]
set destinationUrl = 'https://www.153b.suffolklife.co.uk/SLSecurePortal/SAML/AssertionConsumerService.aspx', audienceUri = 'https://www.153b.suffolklife.co.uk'
where id = 1
