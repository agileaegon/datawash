/* 
    (c) Aegon 2018
    
	Full version of test Data wash script for Advisers across the Portal Databases:
		AdvIndex
		AdvPortal
		PortalSec
	
	The script is run as a single update across databases to ensure
	consistency in the data and within a transaction to ensure ACID properties
	are maintained.
		
	Date		Developer			Comments
	------------------------------------------------------------------------------------
	11/06/2019			    Removed S2S Objects as not present in ARC NBS and caused Data Wash to abend.
	17/05/2018	Tony Taylor	    Intial version
	31/10/2018  	Tony Taylor         Fix duplicate data

*/
SET NOCOUNT ON;
SET XACT_ABORT ON;
SET IMPLICIT_TRANSACTIONS OFF;
SET DATEFORMAT YMD;
GO


BEGIN TRY

    /* variables and constants */
    DECLARE
        @EnvironmentPrefix VARCHAR(3) = 'NW1'
      , @StepMessage VARCHAR(300)
      , @Rowcount INT
      , @DatawashUser VARCHAR(30) = 'DATAWASH_' + CONVERT(VARCHAR(10), GETDATE(), 112)
      , @SQL NVARCHAR(MAX) = ''
      , @DebugFlag BIT = 1 /* if data before vs after is required =1, else set to 0 to suppress */
    ;

    DECLARE @TableUpdates TABLE
        (
            TableName NVARCHAR(200)
        );

    INSERT INTO
        @TableUpdates
        (
            TableName
        )
-- SQL Prompt formatting off
    VALUES
        ('ToWash_PRW_AdvIndex.dbo.ADVISER_MASTER')
      , ('ToWash_PRW_AdvIndex.dbo.ADVISER_EXT_REF')
      , ('ToWash_PRW_AdvPortal.dbo.aspnet_Users')
      , ('ToWash_PRW_AdvPortal.dbo.aspnet_Membership')
      , ('ToWash_PRW_PortalSec.dbo.ADVISER_USERS')
--	  , ('ToWash_PRW_AdvPortal.dbo.S2S_AUDIT_ENTRY')                 S2S not present in NBS so removed from PRW Data Wash
	  , ('ToWash_PRW_AdvPortal.dbo.UserProfile') ;

-- SQL Prompt formatting on

    SET @StepMessage = CONVERT(VARCHAR(23), GETDATE(), 121) + ': Starting Datawash';
    PRINT @StepMessage;

    IF @DebugFlag = 1
        BEGIN
            SELECT
                @SQL = @SQL + N'SELECT TOP (20) ''' + TableName + N''' AS [Table before], * FROM ' + TableName + '; '
            FROM
                @TableUpdates;

            PRINT @SQL;

            EXECUTE sys.sp_executesql @stmt = @SQL;

        END;

    /* 
	Get the list of data to update into temp table first then use for multiple updates 
	
	Currently useing AdvPortal.dbo.aspnet_Users.NumericUserId as the unique key as it will 
	exist for all users, some do not have corresponding ADVISER_MASTER rows */
    IF OBJECT_ID(N'tempdb..#AdviserKeys') IS NOT NULL
        DROP TABLE #AdviserKeys;

    CREATE TABLE #AdviserKeys
        (
            Adviser_Master_Id INT
          , Email VARCHAR(100)
          , UniqueValue INT
          , UserId UNIQUEIDENTIFIER
          , UserName VARCHAR(100)
          , LoweredUserName VARCHAR(100)
          , CBOIdentifier VARCHAR(20)
          , WashedEmail VARCHAR(100)
          , WashedDateOfBirth DATE
          , WashedForename VARCHAR(100)
          , WashedSurname VARCHAR(100)
        );

    INSERT INTO
        #AdviserKeys
        (
            Adviser_Master_Id
          , Email
          , UniqueValue
          , UserId
          , UserName
          , LoweredUserName
          , WashedEmail
          , WashedDateOfBirth
          , WashedForename
          , WashedSurname
        )
    /* not all aspnet_users are Advisers e.g. AUK Admin / System Administrator / AUK Admin, Back Office User */
    SELECT
        am.Adviser_Master_Id
      , am.email
      , u.NumericUserId /* used to set the Adviser unique number */
      , u.UserId
      , u.UserName
      , u.LoweredUserName
      , @EnvironmentPrefix + '_Adviser_' + CAST(u.NumericUserId AS VARCHAR(10)) + '@Aegon.co.uk'
      , DATEADD(DAY, CONVERT(INT, RAND() * 100), am.dateOfBirth)
      , 'AdviserForename' + char(u.NumericUserId % 26 + 65)
	  , 'AdviserSurname' + char(u.NumericUserId % 26 + 65)
    FROM
        ToWash_PRW_AdvPortal.dbo.aspnet_Users u
        LEFT JOIN ToWash_PRW_AdvIndex.dbo.ADVISER_EXT_REF er
            ON (
                   (CAST(u.UserId AS VARCHAR(50)) = er.extRefName)
                   OR (CAST(u.NumericUserId AS VARCHAR(50)) = er.extRefName)
               )
               AND er.externalRefType = 'ADAPT_PORTAL_ID'
        LEFT JOIN ToWash_PRW_AdvIndex.dbo.ADVISER_MASTER AS am
            ON am.Adviser_Master_Id = er.Adviser_Master_Id;

	/*
	* Fix duplicates
	*
	* Problem:
	* The data contains single aspnet_Users records with more than 1 ADVISER_MASTER record via ADVISER_EXT_REF.
	* In these cases, we will be assigning the same washed email to 2 different ADVISER_MASTER records
	* and will break uniqueness.
	*
	* Solution:
	* Enumerate the records with ROW_NUMBER and use that ROW_NUMBER to make the washed email unique.
	*/
	WITH UserIdList AS
	(
		SELECT DISTINCT
			ak.Adviser_Master_Id
		  , ak.Email
		  , ak.LoweredUserName
		FROM
			#AdviserKeys ak
		WHERE
			EXISTS (
					SELECT
						1
					FROM
						#AdviserKeys akDuplicate
					WHERE
						akDuplicate.UniqueValue = ak.UniqueValue
						AND akDuplicate.Adviser_Master_Id <> ak.Adviser_Master_Id
					)
	),
	DuplicateList AS
	(
		SELECT
			Adviser_Master_Id
			, ROW_NUMBER() OVER(PARTITION BY Email ORDER BY Adviser_Master_Id ASC) RowNumber
		FROM
			UserIdList
		WHERE
			Email <> LoweredUserName
	)
	UPDATE
		ak
	SET
		WashedEmail = @EnvironmentPrefix + '_Adviser_' + CAST(ak.UniqueValue AS VARCHAR(10)) + '_' + CAST(dl.RowNumber AS VARCHAR(10)) + '@Aegon.co.uk'
	FROM
		#AdviserKeys ak
		INNER JOIN DuplicateList dl
			ON ak.Adviser_Master_Id = dl.Adviser_Master_Id;

    BEGIN TRANSACTION;

    /* AdvIndex.dbo.ADVISER_MASTER */
    SET @StepMessage = CONVERT(VARCHAR(23), GETDATE(), 121) + ': Updating data in AdvIndex.dbo.ADVISER_MASTER...';
    PRINT @StepMessage;

    UPDATE
        am
    SET
        dateOfBirth = ak.WashedDateOfBirth
      , foreName = ak.WashedForename
      , surName = ak.WashedSurname
      , email = CASE
                    WHEN am.email IS NOT NULL THEN ak.WashedEmail
                    ELSE NULL
                END
    FROM
        ToWash_PRW_AdvIndex.dbo.ADVISER_MASTER AS am
        INNER JOIN #AdviserKeys ak
            ON am.Adviser_Master_Id = ak.Adviser_Master_Id
               AND am.email = ak.Email;

    SELECT
        @Rowcount = @@ROWCOUNT;
    PRINT '   Rows updated: ' + CAST(@Rowcount AS VARCHAR(10));

    /* AdvIndex.dbo.ADVISER_EXT_REF */
    SET @StepMessage = CONVERT(VARCHAR(23), GETDATE(), 121) + ': Updating data in AdvIndex.dbo.ADVISER_EXT_REF...';
    PRINT @StepMessage;

    UPDATE
        aef
    SET
        extRefName = ak.WashedEmail
      , lastUpdatedTime = GETDATE()
      , lastUpdatedBy = @DatawashUser
    FROM
        ToWash_PRW_AdvIndex.dbo.ADVISER_EXT_REF AS aef
        INNER JOIN #AdviserKeys AS ak
            ON aef.Adviser_Master_Id = ak.Adviser_Master_Id
    WHERE
        aef.externalRefType = 'SECURITY_DB_ID';

    SELECT
        @Rowcount = @@ROWCOUNT;
    PRINT '   Rows updated: ' + CAST(@Rowcount AS VARCHAR(10));

    /* AdvIndex.dbo.ADVISER_VER_CODE */
    SET @StepMessage = CONVERT(VARCHAR(23), GETDATE(), 121) + ': Updating data in AdvIndex.dbo.ADVISER_VER_CODE...';
    PRINT @StepMessage;

    UPDATE
        avc
    SET
        lastUpdatedBy = @DatawashUser
    FROM
        ToWash_PRW_AdvIndex.dbo.ADVISER_VER_CODE AS avc;

    SELECT
        @Rowcount = @@ROWCOUNT;
    PRINT '   Rows updated: ' + CAST(@Rowcount AS VARCHAR(10));

    /* AdvPortal.dbo.aspnet_Users */
    SET @StepMessage = CONVERT(VARCHAR(23), GETDATE(), 121) + ': Updating data in AdvPortal.dbo.aspnet_Users...';
    PRINT @StepMessage;

    UPDATE
        u
    SET
        UserName = ak.WashedEmail
      , LoweredUserName = LOWER(ak.WashedEmail)
    FROM
        ToWash_PRW_AdvPortal.dbo.aspnet_Users AS u
        INNER JOIN #AdviserKeys AS ak
            ON u.UserId = ak.UserId;

    SELECT
        @Rowcount = @@ROWCOUNT;
    PRINT '   Rows updated: ' + CAST(@Rowcount AS VARCHAR(10));

    /* AdvPortal.dbo.aspnet_Membership */
    SET @StepMessage = CONVERT(VARCHAR(23), GETDATE(), 121) + ': Updating data in AdvPortal.dbo.aspnet_Membership...';
    PRINT @StepMessage;

    UPDATE
        m
    SET
        Email = ak.WashedEmail
      , LoweredEmail = LOWER(ak.WashedEmail)
      , [Password] = 'Password'
      , PasswordSalt = 'Password'
    FROM
        ToWash_PRW_AdvPortal.dbo.aspnet_Membership AS m
        INNER JOIN #AdviserKeys AS ak
            ON m.UserId = ak.UserId;

    SELECT
        @Rowcount = @@ROWCOUNT;
    PRINT '   Rows updated: ' + CAST(@Rowcount AS VARCHAR(10));

    /* PortalSec.dbo.ADVISER_USERS - userName = ADVISER_MASTER.email */
    SET @StepMessage = CONVERT(VARCHAR(23), GETDATE(), 121) + ': Updating data in PortalSec.dbo.ADVISER_USERS...';
    PRINT @StepMessage;

    UPDATE
        u
    SET
        username = ak.WashedEmail
    FROM
        ToWash_PRW_PortalSec.dbo.ADVISER_USERS AS u
        INNER JOIN #AdviserKeys AS ak
            ON u.username = ak.Email;

    SELECT
        @Rowcount = @@ROWCOUNT;
    PRINT '   Rows updated: ' + CAST(@Rowcount AS VARCHAR(10));


/* AdvPortal.dbo.S2S_AUDIT_ENTRY 
S2S is not present in NBS so removed from the PRW Data Wash.
    SET @StepMessage = CONVERT(VARCHAR(23), GETDATE(), 121) + ': Updating data in AdvPortal.dbo.S2S_AUDIT_ENTRY...';
    PRINT @StepMessage;

    UPDATE
        u
    SET
        requestXml = NULL
      , responseXml = NULL
    FROM
        ToWash_PRW_AdvPortal.dbo.S2S_AUDIT_ENTRY AS u;


    SELECT
        @Rowcount = @@ROWCOUNT;
    PRINT '   Rows updated: ' + CAST(@Rowcount AS VARCHAR(10));

*/

    /* second sweep through where the rows don't exist on the AdvIndex.dbo.ADVISER_MASTER table using internal table id */
    SET @StepMessage = CONVERT(VARCHAR(23), GETDATE(), 121) + ': Updating remaining data in PortalSec.dbo.ADVISER_USERS...';
    PRINT @StepMessage;
    UPDATE
        u
    SET
        username = @EnvironmentPrefix + '_AdviserUser_' + CAST(Adviser_User_id AS VARCHAR(10)) + '@Aegon.co.uk'
    FROM
        ToWash_PRW_PortalSec.dbo.ADVISER_USERS AS u
    WHERE
        u.username NOT LIKE '%@Aegon.co.uk';

    SELECT
        @Rowcount = @Rowcount + @@ROWCOUNT;

    PRINT '   Rows updated: ' + CAST(@Rowcount AS VARCHAR(10));



    /* Update UserProfile table in AdvPortal. This is a value pair type table so the codes sprecify the data type */
    SET @StepMessage = CONVERT(VARCHAR(23), GETDATE(), 121) + ': Updating data in AdvPortal.dbo.UserProfile...';
    PRINT @StepMessage;
    UPDATE
        up
    SET
        VALUE = CASE
                    WHEN upf.ShortCode = 'FULLNAME'
                         AND up.Value IS NOT NULL THEN ak.WashedForename + ' ' + ak.WashedSurname
                    WHEN upf.ShortCode = 'USER_FIRSTNAME'
                         AND up.Value IS NOT NULL THEN ak.WashedForename
                    WHEN upf.ShortCode = 'USER_SURNAME'
                         AND up.Value IS NOT NULL THEN ak.WashedSurname
                    WHEN upf.ShortCode = 'USER_DATE_OF_BIRTH'
                         AND up.Value IS NOT NULL THEN CONVERT(VARCHAR(10), ak.WashedDateOfBirth, 121)
                    WHEN upf.ShortCode = 'USER_TELEPHONE'
                         AND up.Value IS NOT NULL THEN RIGHT('00000000000' + CAST(ak.UniqueValue AS VARCHAR(10)), 11)
                    WHEN upf.ShortCode = 'USER_MOBILE'
                         AND up.Value IS NOT NULL THEN RIGHT('01000000000' + CAST(ak.UniqueValue AS VARCHAR(10)), 11)
                    ELSE up.Value
                END
    FROM
        ToWash_PRW_AdvPortal.dbo.UserProfile AS up
        INNER JOIN ToWash_PRW_AdvPortal.dbo.UserProfileField AS upf
            ON up.UserProfileFieldId = upf.UserProfileFieldId
        INNER JOIN #AdviserKeys AS ak
            ON ak.UserId = up.UserId;


    SELECT
        @Rowcount = @Rowcount + @@ROWCOUNT;

    PRINT '   Rows updated: ' + CAST(@Rowcount AS VARCHAR(10));



    IF @DebugFlag = 1
        BEGIN
            SET @SQL = '';
            SELECT
                @SQL = @SQL + N'SELECT TOP (20) ''' + TableName + N''' AS [Table after], * FROM ' + TableName + '; '
            FROM
                @TableUpdates;

            /* print @SQL */
            EXECUTE sys.sp_executesql @stmt = @SQL;

        END;

    --ROLLBACK TRANSACTION;
    COMMIT TRANSACTION;

    SET @StepMessage = CONVERT(VARCHAR(23), GETDATE(), 121) + ': Datawash Complete';
    PRINT @StepMessage;

END TRY
BEGIN CATCH

    PRINT CONVERT(VARCHAR(23), GETDATE(), 121) + ': An error has occurred datawashing the data. Last successful step: ' + @StepMessage;
    PRINT ERROR_MESSAGE();

    IF XACT_STATE() = 1
        ROLLBACK TRANSACTION;

	RAISERROR(50001, 10, 127);

END CATCH;

/* clean up temp table(s) */
IF OBJECT_ID(N'tempdb..#AdviserKeys') IS NOT NULL
    DROP TABLE #AdviserKeys;

