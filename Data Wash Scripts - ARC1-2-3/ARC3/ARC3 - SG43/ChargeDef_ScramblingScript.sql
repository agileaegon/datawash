BEGIN
    SET NOCOUNT ON;

	DECLARE	@StepMessage  NVARCHAR(4000)
		  , @ErrorMessage NVARCHAR(4000)
		  , @batchSize INT = 20000
		  , @Rowcount INT
		  , @startTime DATETIME
		  , @endTime DATETIME;

	--control table to update records in chunks of 20k
	IF OBJECT_ID('tempdb..#upd_control') IS NOT NULL
		DROP TABLE #upd_control;

	CREATE TABLE #upd_control
	(
		control_id INT NOT NULL PRIMARY KEY,
		complete INT DEFAULT 0 NOT NULL
	);

BEGIN TRY
	
    /*************************************** Intermediary ******************************************/
	SET @StepMessage = 'Block 1 - Start';
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;
	SET @startTime = GETDATE();

	UPDATE
		i
	SET
		name = COALESCE(de.name, dbe.name, aae.name)
	FROM
		intermediary i
		JOIN intermediary_external_system_identifier iesi
			ON i.id = iesi.intermediary_id
		JOIN external_system_identifier esi
			ON esi.id = iesi.external_system_identifier_id
		LEFT JOIN PRA_STGComposer..dealer d
			ON d.dealer_id = esi.external_identifier
		LEFT JOIN PRA_STGComposer..entity de
			ON de.entity_id = d.entity_id
		LEFT JOIN PRA_STGComposer..dealer_branch db
			ON db.dealer_branch_id = esi.external_identifier
		LEFT JOIN PRA_STGComposer..entity dbe
			ON dbe.entity_id = db.entity_id
		LEFT JOIN PRA_STGComposer..adviser_account aa
			ON aa.adviser_account_id = esi.external_identifier
		LEFT JOIN PRA_STGComposer..entity aae
			ON aae.entity_id = aa.entity_id

	SELECT	@Rowcount = @@ROWCOUNT;
    PRINT '   Rows updated: ' + CAST(@Rowcount AS VARCHAR(10));

	SET @endTime = GETDATE();
	SET @StepMessage = 'Block 1 - Finish. Time Taken - ' + CONVERT(VARCHAR(20), @endTime - @startTime, 114);
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;

	/*************************************** Investor ******************************************/
	SET @StepMessage = 'Block 2 - Start';
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;
	SET @startTime = GETDATE();

	IF EXISTS 
		(	
			SELECT 
				1
			FROM 
				#upd_control
		)
	BEGIN
		TRUNCATE TABLE #upd_control;
	END

	INSERT INTO #upd_control
	(
		control_id
	)
	SELECT
		id 
	FROM 
		investor
	ORDER BY
		id 

	WHILE EXISTS 
	(
		SELECT 
            1
		FROM
			#upd_control
		WHERE 
			complete <> 2
	)
	BEGIN
		UPDATE TOP (@batchSize) 
			#upd_control
		SET 
			complete = 1
		WHERE
			complete = 0
		
		UPDATE
			i
		SET
			name = ISNULL(e.given_names + ' ', '') + e.name
		FROM
			investor i
			JOIN investor_external_system_identifier iesi
				ON iesi.investor_id = i.id
			JOIN external_system_identifier esi
				ON esi.id = iesi.external_system_identifier_id
			JOIN PRA_STGComposer..entity e
				ON e.entity_id = esi.external_identifier
			JOIN #upd_control u 
				ON u.control_id = i.id
				AND u.complete = 1
  	
		UPDATE
			#upd_control
		SET 
			complete = 2
		WHERE
			complete = 1
	END

	SELECT	
		@Rowcount = count(*) 
	FROM 
		#upd_control 
	WHERE 
		complete = 2;

    PRINT '   Rows updated: ' + CAST(@Rowcount AS VARCHAR(10));

	SET @endTime = GETDATE();
	SET @StepMessage = 'Block 2 - Finish. Time Taken - ' + CONVERT(VARCHAR(20), @endTime - @startTime, 114);
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;

	/*************************************** Investor Account ******************************************/
	SET @StepMessage = 'Block 3 - Start';
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;
	SET @startTime = GETDATE();

	IF EXISTS 
		(	
			SELECT 
				1
			FROM 
				#upd_control
		)
	BEGIN
		TRUNCATE TABLE #upd_control;
	END

	INSERT INTO #upd_control
	(
		control_id
	)
	SELECT
		id 
	FROM 
		investor_account
	ORDER BY
		id 

	WHILE EXISTS 
	(
		SELECT 
            1
		FROM
			#upd_control
		WHERE 
			complete <> 2
	)
	BEGIN
		UPDATE TOP (@batchSize) 
			#upd_control
		SET 
			complete = 1
		WHERE
			complete = 0
		
		UPDATE
			ia
		SET
			name = ISNULL(e.given_names + ' ', '') + e.name + ISNULL(' ' + ma.account_designation, '')
		FROM 
			investor_account ia
			JOIN investor_account_external_system_identifier iaesi
				ON iaesi.investor_account_id = ia.id
			JOIN external_system_identifier esi
				ON esi.id = iaesi.external_system_identifier_id
			JOIN PRA_STGComposer..member_account ma
				ON ma.member_account_id = esi.external_identifier
			JOIN PRA_STGComposer..entity e
				ON e.entity_id = ma.entity_id
			JOIN #upd_control u 
				ON u.control_id = ia.id
				AND u.complete = 1

		UPDATE
			#upd_control
		SET 
			complete = 2
		WHERE
			complete = 1
	END

	SELECT	
		@Rowcount = count(*) 
	FROM 
		#upd_control 
	WHERE 
		complete = 2;

    PRINT '   Rows updated: ' + CAST(@Rowcount AS VARCHAR(10));

	SET @endTime = GETDATE();
	SET @StepMessage = 'Block 3 - Finish. Time Taken - ' + CONVERT(VARCHAR(20), @endTime - @startTime, 114);
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;

	/*************************************** Audit Log ******************************************/
	SET @StepMessage = 'Block 4 - Start';
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;
	SET @startTime = GETDATE();

	UPDATE
		audit_log
	SET
		old_value = CASE 
						WHEN old_value IS NULL THEN NULL
						ELSE 'Data obfuscated'
					END
	  , new_value = CASE
						WHEN new_value IS NULL THEN NULL
						ELSE 'Data obfuscated'
					END

	SELECT	@Rowcount = @@ROWCOUNT;
    PRINT '   Rows updated: ' + CAST(@Rowcount AS VARCHAR(10));

	SET @endTime = GETDATE();
	SET @StepMessage = 'Block 4 - Finish. Time Taken - ' + CONVERT(VARCHAR(20), @endTime - @startTime, 114);
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;

	/******************************** Investor_account_group_charge_rate_card_version_mapping ***********************************/
	SET @StepMessage = 'Block 5 - Start';
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;
	SET @startTime = GETDATE();

	IF EXISTS 
		(	
			SELECT 
				1
			FROM 
				#upd_control
		)
	BEGIN
		TRUNCATE TABLE #upd_control;
	END

	INSERT INTO #upd_control
	(
		control_id
	)
	SELECT
		id 
	FROM 
		investor_account_group_charge_rate_card_version_mapping
	ORDER BY
		id 

	WHILE EXISTS 
	(
		SELECT 
            1
		FROM
			#upd_control
		WHERE 
			complete <> 2
	)
	BEGIN
		UPDATE TOP (@batchSize) 
			#upd_control
		SET 
			complete = 1
		WHERE
			complete = 0
  	
		UPDATE
			iag
		SET
			link_user = CASE 
							WHEN link_user IS NULL THEN NULL
							ELSE 'Data obfuscated'
						END
		  , delink_user = CASE
							WHEN delink_user IS NULL THEN NULL
							ELSE 'Data obfuscated'
						END
		FROM
			investor_account_group_charge_rate_card_version_mapping iag
			JOIN #upd_control u 
				ON u.control_id = iag.id
				AND u.complete = 1
		
		UPDATE
			#upd_control
		SET 
			complete = 2
		WHERE
			complete = 1
	END

	SELECT	
		@Rowcount = count(*) 
	FROM 
		#upd_control 
	WHERE 
		complete = 2;

    PRINT '   Rows updated: ' + CAST(@Rowcount AS VARCHAR(10));

	SET @endTime = GETDATE();
	SET @StepMessage = 'Block 5 - Finish. Time Taken - ' + CONVERT(VARCHAR(20), @endTime - @startTime, 114);
    RAISERROR(@StepMessage, 0, 1) WITH NOWAIT;

	/********************* Clean up ********************/
	IF OBJECT_ID('tempdb..#upd_control') IS NOT NULL
		DROP TABLE #upd_control;

END TRY
BEGIN CATCH
    SELECT 
          @ErrorMessage = @StepMessage + ' - ' + ERROR_MESSAGE();

    RAISERROR(@ErrorMessage, 16, 1);
END CATCH;
END;

